var jsonToHtml = function jsonToHtml() {

    var args = null;

    var Table = function Table() {

        args = arguments;
        var tbl = new table();

        if (args[0].Data !== undefined) {
            tbl.data = args[0].Data;
        }
        if (args[0].AppendTo !== undefined) {
            tbl.appendTo = args[0].AppendTo;
        }
        if (args[0].TableID !== undefined) {
            tbl.tableID = args[0].TableID;
        } else {
            tbl.tableID = '#tbJsonToHtml';
        }
        if (args[0].CustomHeader !== undefined) {
            tbl.customHeader = args[0].CustomHeader;
        }
        if (args[0].DefaultHeader !== undefined) {
            tbl.defaultHeader = args[0].DefaultHeader;
        }
        if (args[0].AddToColumn !== undefined) {
            tbl.addToColumn = args[0].AddToColumn;
        }
        if (args[0].SelectRows !== undefined) {
            tbl.selectRows = args[0].SelectRows;
        }
        if (args[0].UpdateFunction !== undefined) {
            tbl.updateFun = args[0].UpdateFunction;
        }
        if (args[0].DeleteFunction !== undefined) {
            tbl.deleteFunction = args[0].DeleteFunction;
        }

        if (args[0].CreateFunction !== undefined) {
            tbl.createFunction = args[0].CreateFunction;
        }

        if (args[0].Create !== undefined) {
            tbl.createRow = args[0].Create;
        }

        if (args[0].TextLimit !== undefined) {
            tbl.textLimit = args[0].TextLimit;
        }

        if (args[0].ModalID !== undefined) {
            tbl.modalID = args[0].ModalID;
        }

        if (args[0].PopupContinueText !== undefined) {
            tbl.popupContinueText = args[0].PopupContinueText;
        }

        tbl.getTable();

        return this;
    };

    var HeadingStyle = function HeadingStyle() {

        args = arguments;
        var tbl = new table();
        if (args[0].TableID !== undefined) {
            tbl.tableID = args[0].TableID;
        } else {
            tbl.tableID = '#tbJsonToHtml';
        }
        if (args[0].BackgroundColor !== undefined) {
            tbl.tableHeadingBackgroundColor = args[0].BackgroundColor;
        }
        if (args[0].ForeColor !== undefined) {
            tbl.tableHeadingForeColor = args[0].ForeColor;
        }

        tbl.j2HTMLTableHeadingStyle();

        //j2HTMLHeadingStyle(args);
        return this;
    };

    var TableBodyStyle = function TableBodyStyle() {
        args = arguments;

        var tbl = new table();
        if (args[0].TableID !== undefined) {
            tbl.tableID = args[0].TableID;
        } else {
            tbl.tableID = '#tbJsonToHtml';
        }
        if (args[0].BackgroundColor !== undefined) {
            tbl.tableBackgroundColor = args[0].BackgroundColor;
        }
        if (args[0].ForeColor !== undefined) {
            tbl.tableForeColor = args[0].ForeColor;
        }

        tbl.j2HTMLTableBodyStyle();
        return this;
    };

    var Paging = function Paging() {

        args = arguments;
        var p = new paging();

        // TableID: '#tbTest',
        // PaginationAppendTo: '#divPagination',
        // RowsPerPage: 5,
        // ShowPages: 8,
        // StartPage: 7

        if (args[0].TableID !== undefined) {
            p.tableID = args[0].TableID;
        } else {
            p.tableID = '#tbJsonToHtml';
        }

        if (args[0].PaginationAppendTo !== undefined) {
            p.paginationAppendTo = args[0].PaginationAppendTo;
        }
        if (args[0].RowsPerPage !== undefined) {
            p.rowsPerPage = args[0].RowsPerPage;
        }
        if (args[0].ShowPages !== undefined) {
            p.showPages = args[0].ShowPages;
        }
        if (args[0].StartPage !== undefined) {
            p.startPage = args[0].StartPage;
        }

        p.getPaging();

        //paging(args);
        return this;
    };

    var Search = function Search() {

        args = arguments;

        var tbl = new table();
        if (args[0].TableID !== undefined) {
            tbl.tableID = args[0].TableID;
        } else {
            tbl.tableID = '#tbJsonToHtml';
        }

        if (args[0].SearchTextBoxID !== undefined) {
            tbl.searchTextBox = args[0].SearchTextBoxID;
        }

        if (args[0].SearchTextBoxID !== undefined && args[0].SearchInColumns !== undefined) {
            tbl.searchInColumns = args[0].SearchInColumns;
            tbl.j2HTMLSearchWithinSpeificColumns();
        } else {
            tbl.j2HTMLSearchWithinWholeTable();
        }

        return this;
    };

    var Print = function Print() {

        args = arguments;

        var tbl = new table();
        if (args[0].TableID !== undefined) {
            tbl.tableID = args[0].TableID;
        } else {
            tbl.tableID = '#tbJsonToHtml';
        }
        if (args[0].Print !== undefined) {
            tbl.printType = args[0].Print;
        }
        if (args[0].FileName !== undefined) {
            tbl.fileName = args[0].FileName;
        } else {
            if (args[0].TableID !== undefined) {
                var name = args[0].TableID.slice(1, args[0].TableID.length);
                tbl.fileName = name + '-CSV';
            } else {
                tbl.fileName = 'tbJsonToHtml-CSV';
            }
        }
        if (args[0].ButtonID !== undefined) {
            tbl.printButtonID = args[0].ButtonID;
        }

        tbl.j2HTMLPrint();

        return this;
    };

    var Dropdown = function Dropdown() {

        args = arguments;
        var ddl = new dropdown();
        if (args[0].Data !== undefined) {
            ddl.data = args[0].Data;
        }
        if (args[0].Text !== undefined) {
            ddl.text = args[0].Text;
        }
        if (args[0].GetSelectedValue !== undefined) {
            ddl.selectedValue = args[0].GetSelectedValue;
        }
        if (args[0].Value !== undefined) {
            ddl.ddlValue = args[0].Value;
        }
        if (args[0].AppendTo !== undefined) {
            ddl.appendTo = args[0].AppendTo;
        }
        if (args[0].isModal === true) {
            ddl.isModal = args[0].isModal;
        }
        if (args[0].Title !== undefined) {
            ddl.ddlTitle = args[0].Title;
        }
        ddl.getDropdown();
        return this;
    };

    var Radio = function Radio() {

        args = arguments;

        var rdb = new radio();

        if (args[0].Data !== undefined) {
            rdb.data = args[0].Data;
        }
        if (args[0].AppendTo !== undefined) {
            rdb.appendTo = args[0].AppendTo;
        }
        if (args[0].GroupName !== undefined) {
            rdb.groupName = args[0].GroupName;
        } else if (args[0].AppendTo !== undefined) {
            rdb.groupName = 'rdb' + args[0].AppendTo.slice(1);
        }
        if (args[0].Text !== undefined) {
            rdb.text = args[0].Text;
        }
        if (args[0].GetSelectedValue !== undefined) {
            rdb.selectedValue = args[0].GetSelectedValue;
        }
        if (args[0].Direction !== undefined) {
            rdb.direction = args[0].Direction;
        }
        if (args[0].Value !== undefined) {
            rdb.value = args[0].Value;
        }
        if (args[0].isModal === true) {
            rdb.isModal = args[0].isModal;
        }

        rdb.getRadioButton();
        return this;
    };

    var Checkbox = function Checkbox() {
        args = arguments;

        var chk = new checkbox();

        if (args[0].Data !== undefined) {
            chk.data = args[0].Data;
        }
        if (args[0].AppendTo !== undefined) {
            chk.appendTo = args[0].AppendTo;
        }
        if (args[0].GroupName !== undefined) {
            chk.groupName = args[0].GroupName;
        } else if (args[0].AppendTo !== undefined) {
            chk.groupName = 'chk' + args[0].AppendTo.slice(1);
        }
        if (args[0].Text !== undefined) {
            chk.text = args[0].Text;
        }
        if (args[0].GetSelectedValue !== undefined) {
            chk.selectedValue = args[0].GetSelectedValue;
        }
        if (args[0].Direction !== undefined) {
            chk.direction = args[0].Direction;
        }
        if (args[0].Value !== undefined) {
            chk.value = args[0].Value;
        }
        if (args[0].isModal === true) {
            chk.isModal = args[0].isModal;
        }

        chk.getCheckbox();
        return this;
    };

    var List = function List() {

        args = arguments;

        var lst = new list();
        if (args[0].Data !== undefined) {
            lst.data = args[0].Data;
        }
        if (args[0].Text !== undefined) {
            lst.text = args[0].Text;
        }
        if (args[0].Value !== undefined) {
            lst.dataListValue = args[0].Value;
        }
        if (args[0].Type !== undefined) {
            lst.type = "dataList";
        }
        if (args[0].DataList !== undefined) {
            lst.dataList = args[0].DataList;
        }
        if (args[0].AppendTo !== undefined) {
            lst.appendTo = args[0].AppendTo;
        }

        lst.getList();
        return this;
    };

    var Filter = function Filter() {

        args = arguments;
        var controlName = args[0];
        var liName = args[1];
        var Val = args[2];

        j2HTMLFilter(controlName, liName, Val);
        return this;
    };

    var Switch = function Switch() {

        args = arguments;
        switchButton(args);
        return this;
    };

    var Inst_Modal = function Inst_Modal() {
        args = arguments;
        var sl = new selector();
        if (args[0].ModalID !== undefined) {
            sl.modalID = args[0].ModalID;
        } else {
            sl.modalID = '#j2HTMLModal';
        }
        if (args[0].Options !== undefined) {
            sl.modalOptions = args[0].Options;
        }
        return sl.modal_Instance();
    };

    var Modal = function Modal() {

        args = arguments;
        var mod = new j2HTMLModal();

        if (args[0].Data !== undefined) {
            mod.modalData = args[0].Data;
        }
        if (args[0].ModalID !== undefined) {
            mod.modalID = args[0].ModalID;
        } else {
            mod.modalID = '#j2HTMLModal';
        }
        if (args[0].AppendTo !== undefined) {
            mod.appendTo = args[0].AppendTo;
        }
        if (args[0].Footer !== undefined) {
            mod.modalFooter = args[0].Footer;
        }
        if (args[0].Header !== undefined) {
            mod.modalHeader = args[0].Header;
        }
        if (args[0].Heading !== undefined) {
            mod.modalHeading = args[0].Heading;
        }

        if (args[0].Size !== undefined) {

            if (args[0].Size.toLowerCase() === 'x-large') {
                mod.modalSize = 'modal-lg modal-xlg';
            }

            if (args[0].Size.toLowerCase() === 'large') {
                mod.modalSize = 'modal-lg';
            }
            if (args[0].Size.toLowerCase() === 'small') {
                mod.modalSize = 'modal-sm';
            }
        }

        if (args[0].Display !== undefined) {
            mod.display = args[0].Display;
        }

        if (args[0].Exclude !== undefined) {
            mod.exclude = args[0].Exclude;
        }

        if (args[0].Include !== undefined) {
            mod.include = args[0].Include;
        }
        if (args[0].CustomColumns !== undefined) {
            mod.customColumns = args[0].CustomColumns;
        }
        //****** ONLY FOR TABLE ********
        if (args[0].TableID !== undefined) {
            mod.tableID = args[0].TableID;
        }
        if (args[0].RowID !== undefined) {
            mod.rowID = args[0].RowID;
        }
        if (args[0].UpdateFunction !== undefined) {
            mod.updateFunction = args[0].UpdateFunction;
        }
        if (args[0].CreateFunction !== undefined) {
            mod.createFunction = args[0].CreateFunction;
        }
        if (args[0].SelectRow !== undefined) {
            mod.selectRows = args[0].SelectRow;
        }

        mod.getModal();

        return this;
    };

    var ShowModal = function ShowModal() {

        args = arguments;
        var sl = new selector();

        if (args[0] !== undefined) {
            if (args[0].ModalID !== undefined) {
                sl.modalID = args[0].ModalID;
            } else {
                sl.modalID = '#j2HTMLModal';
            }
            if (args[0].Options !== undefined) {
                sl.modalOptions = args[0].Options;
            }
        } else {
            sl.modalID = '#j2HTMLModal';
        }

        sl.showModal();
        //return this;
    };
    var HideModal = function HideModal() {

        args = arguments;
        var sl = new selector();

        if (args[0] !== undefined) {
            if (args[0].ModalID !== undefined) {
                sl.modalID = args[0].ModalID;
            } else {
                sl.modalID = '#j2HTMLModal';
            }
            if (args[0].Options !== undefined) {
                sl.modalOptions = args[0].Options;
            }
        } else {
            sl.modalID = '#j2HTMLModal';
        }

        sl.hideModal();

        //return this;
    };

    var CloseModal = function CloseModal() {

        args = arguments;
        var mod = new j2HTMLModal();

        if (args[0] !== undefined) {
            if (args[0].ModalID !== undefined) {
                mod.modalID = args[0].ModalID;
            } else {
                mod.modalID = '#j2HTMLModal';
            }
        } else {
            throw new Error('ModalID is required');
        }

        mod.closeModal();

        //return this;
    };

    var ShowLoadingModal = function ShowLoadingModal() {
        args = arguments;
        var mod = new j2HTMLModal();
        if (args[0].Text !== undefined) {
            mod.loadingModalText = args[0].Text;
        }
        if (args[0].ParentID !== undefined) {
            mod.parentID = args[0].ParentID;
        }
        if (args[0].Image !== undefined) {
            mod.image = args[0].Image;
        }
        if (args[0].Class !== undefined) {
            mod.class = args[0].Class;
        }

        mod.showWaitingModal();
    };

    var HideLoadingModal = function HideLoadingModal() {
        var mod = new j2HTMLModal();
        mod.hideWaitingModal();
    };

    var PopupMessage = function PopupMessage() {

        args = arguments;

        var pop = new popupMessage();

        if (args[0].PopupID !== undefined) {
            pop.popupId = args[0].PopupID;
        } else {
            pop.popupId = '#j2HTMLPopupMessage';
        }
        if (args[0].Type !== undefined) {
            pop.type = args[0].Type;
        }
        if (args[0].Heading !== undefined) {
            pop.heading = args[0].Heading;
        }

        if (args[0].HeaderBGColor !== undefined) {
            pop.headerBGColor = args[0].HeaderBGColor;
        }
        if (args[0].HeaderForecolor !== undefined) {
            pop.headerForeColor = args[0].HeaderForecolor;
        }

        if (args[0].Message !== undefined) {
            pop.message = args[0].Message;
        }
        if (args[0].PopupHeader !== undefined) {
            pop.popupHeader = args[0].PopupHeader;
        }

        if (args[0].PopupFooter !== undefined) {
            pop.popupFooter = args[0].PopupFooter;
        }

        if (args[0].Confirmation !== undefined) {
            pop.confirmation = args[0].Confirmation;
        }

        pop.getPopup();

        return this;
    };

    var ShowPopup = function ShowPopup() {

        args = arguments;

        var pop = new popupMessage();

        if (args[0] !== undefined) {
            if (args[0].PopupId !== undefined) {
                pop.popupId = args[0].PopupID;
            }
        }
        pop.showPopup();

        return this;
    };

    var HidePopup = function HidePopup() {
        args = arguments;

        var pop = new popupMessage();

        if (args[0] !== undefined) {
            if (args[0].PopupId !== undefined) {
                pop.popupId = args[0].PopupID;
            }
        }
        pop.hidePopup();

        return this;
    };

    var TextBox = function TextBox() {
        args = arguments;
        textBox(args);
        return this;
    };

    var Create = function Create() {
        args = arguments;
        createTableForInsert(args);
        return this;
    };

    var Validate = function Validate() {

        args = arguments;
        var v = new j2HTMLValidation();

        if (args[0].ElementIDs !== undefined) {
            v.elementIDs = args[0].ElementIDs;
        }
        if (args[0].DisplayType !== undefined) {
            v.displayType = args[0].DisplayType;
        }
        if (args[0].Position !== undefined) {
            v.position = args[0].Position;
        }
        if (args[0].ErrorMessage !== undefined) {
            v.errorMessage = args[0].ErrorMessage;
        }
        if (args[0].ValidationType !== undefined) {
            v.validationType = args[0].ValidationType;
        }
        if (args[0].Validate !== undefined) {
            v.validate = args[0].Validate;
        }
        if (args[0].Regex !== undefined) {
            v.regex = args[0].Regex;
        }
        if (args[0].CSS !== undefined) {
            v.css = args[0].CSS;
        }
        if (args[0].Length !== undefined) {
            v.length = args[0].Length;
        }
        v.setValidation();
        return this;
    };

    var Placeholder = function Placeholder() {

        args = arguments;
        var ph = new placeholder();
        if (args[0].ElementIDs !== undefined) {
            ph.elementIDs = args[0].ElementIDs;
        }
        ph.getPlaceholder();

        return this;
    };

    var Form = function Form() {

        args = arguments;
        var frm = new j2HTMLForm();
        if (args[0].Data !== undefined) {
            frm.Data = args[0].Data;
        }
        if (args[0].ID !== undefined) {
            frm.ID = args[0].ID;
        } else {
            frm.ID = '#j2HTMLFrom';
        }
        if (args[0].AppendTo !== undefined) {
            frm.AppendTo = args[0].AppendTo;
        }
        if (args[0].AppendPaginationTo !== undefined) {
            frm.AppendPaginationTo = args[0].AppendPaginationTo;
        }
        if (args[0].Type !== undefined) {
            frm.Type = args[0].Type;
        } else {
            frm.Type = 'Empty';
        }
        if (args[0].Exclude !== undefined) {
            frm.Exclude = args[0].Exclude;
        }
        if (args[0].Include !== undefined) {
            frm.Include = args[0].Include;
        }
        if (args[0].GetData !== undefined) {
            frm.GetData = args[0].GetData;
        }
        if (args[0].CustomElement !== undefined) {
            frm.customElement = args[0].CustomElement;
        }
        if (args[0].TextLimit !== undefined) {
            frm.textLimit = args[0].TextLimit;
        }

        frm.getForm();
        return this;
    };

    var GetElement = function GetElement() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        return sl.getElement();

        //return getElement(args);
    };

    var GetElementByName = function GetElementByName() {
        args = arguments;

        args = arguments;
        var sl = new selector();
        if (args[0].Name !== undefined) {
            sl.name = args[0].Name;
        }
        return sl.getElementByName();
    };

    var GetElementbyTagName = function GetElementbyTagName() {
        args = arguments;
        return getElementbyTagName(args);
    };

    var GetChildren = function GetChildren() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        return sl.getChildren();
    };

    var GetClosestParentElement = function GetClosestParentElement() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        if (args[0].Selector !== undefined) {
            sl.sel = args[0].Selector;
        }

        return sl.getClosest();
    };

    var GetParent = function GetParent() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        return sl.getParent();
    };

    var GetSiblings = function GetSiblings() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        return sl.getSiblings();
    };

    var GetPreviousSibling = function GetPreviousSibling() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        return sl.getPrviousSibling();
    };

    var GetNextSibling = function GetNextSibling() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        return sl.getNextSibling();
    };

    var GetElementValue = function GetElementValue() {
        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        return sl.getElementValue();
    };

    var SetElementValue = function SetElementValue() {
        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        if (args[0].Value !== undefined) {
            sl.elementValue = args[0].Value;
        }
        return sl.setElementValue();
    };

    var GetElementText = function GetElementText() {
        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        return sl.getElementText();
    };

    var SetElementText = function SetElementText() {
        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        if (args[0].Value !== undefined) {
            sl.elementValue = args[0].Value;
        }
        return sl.setElementText();
    };

    var DatePicker = function DatePicker() {

        var dp = new j2HTMLDatePicker();

        args = arguments;

        if (args.length !== 0) {

            //SET TITLE
            if (args[0].Title !== undefined) {
                dp.title = args[0].Title;
            }

            //SET DATE
            if (args[0].StartDate !== undefined) {
                dp.startDate = args[0].StartDate;
            }

            //SET DATE
            if (args[0].SetDate !== undefined) {
                dp.setDate = args[0].SetDate;
            }

            //MaxDate
            if (args[0].MaxDate !== undefined) {
                dp.maxDate = args[0].MaxDate;
            }

            //MinDate
            if (args[0].MinDate !== undefined) {
                dp.minDate = args[0].MinDate;
            }

            //CLOSE ON BUTTON CLICK (BUTTON TEXT)
            if (args[0].ButtonText !== undefined) {
                dp.closeButtonText = args[0].ButtonText;
            }

            //ASSIGN SELECTED DATE TO
            if (args[0].AssignSelectedDateTo !== undefined) {
                dp.assignSelectedDateTo = args[0].AssignSelectedDateTo;
            }

            //START WEEK ON
            if (args[0].WeekStartOn !== undefined) {
                dp.weekStartOn = args[0].WeekStartOn;
            }

            //SHOW NUMBER OF MONTHS
            if (args[0].ShowMonths !== undefined) {
                dp.showMonths = args[0].ShowMonths;
            }

            //SET TOOLTIPS
            if (args[0].ToolTips !== undefined) {
                dp.toolTips = args[0].ToolTips;
            }

            //SET MAXIMUM DATES TO SELECT (int Example: 4)
            if (args[0].MaxDatesToSelect !== undefined) {
                dp.maxDatesToSelect = args[0].MaxDatesToSelect;
            }

            //SHOW CALENDAR IN DIV (CONTAINER INSTEAD OF MODAL)
            if (args[0].AppendTo !== undefined) {
                dp.appendTo = args[0].AppendTo;
            }

            //SHOW CALENDAR IN DIV (CONTAINER INSTEAD OF MODAL)
            if (args[0].AppendTo !== undefined) {
                dp.appendTo = args[0].AppendTo;
            }

            //DISABLED DAYS 
            //ARRAY OF int, Example: [0,1]
            if (args[0].DisabledDays !== undefined) {
                dp.disabledDays = args[0].DisabledDays;
            }
            //DISABLED DAYS 
            //ARRAY OF DATES, Example: ['06/23/2019', '07/23/2019']
            if (args[0].DisabledDates !== undefined) {
                dp.disabledDates = args[0].DisabledDates;
            }

            //HIDE DAY NAMES
            if (args[0].HideDayNames !== undefined) {
                dp.hideDayNames = args[0].HideDayNames;
            }

            //SHOW FULL SCREEN CALENDAR
            if (args[0].ShowFullScreen !== undefined) {
                dp.showFullScreen = args[0].ShowFullScreen;
            }

            //SHOW FULL SCREEN CALENDAR
            if (args[0].LockCalendar !== undefined) {
                dp.loclCalendar = args[0].LockCalendar;
            }

            //SHOW GET MULTIPLE SELECTED DATES
            if (args[0].GetMultipleSelectedDates !== undefined) {
                dp.getMultipleSelectedDates = args[0].GetMultipleSelectedDates;
            }

            //HIGH LIGHT DATES
            if (args[0].HighLightDates !== undefined) {
                dp.highlight = args[0].HighLightDates;
            }
        }

        dp.getDatePicker();
        return dp.getDatePicker();
    };

    var Carousel = function Carousel() {

        // this._Id = false;
        // this._appendTo = false;
        // this._data = false;
        // this._caption = false;
        // this._image = false;
        // this._start = 0;

        args = arguments;
        var carousel = new j2HTMLCarousel();

        if (args[0].ID !== undefined) {
            carousel.Id = args[0].ID;
        }
        if (args[0].AppendTo !== undefined) {
            carousel.appendTo = args[0].AppendTo;
        }
        if (args[0].Data !== undefined) {
            carousel.data = args[0].Data;
        }
        if (args[0].Caption !== undefined) {
            carousel.caption = args[0].Caption;
        }
        if (args[0].Image !== undefined) {
            carousel.image = args[0].Image;
        }
        if (args[0].Start !== undefined) {
            carousel.start = args[0].Start;
        }
        if (args[0].Option !== undefined) {
            carousel.option = args[0].Option;
        }
        carousel.getCarousel();
    };

    //Just inside the element, after its last child
    var AppendElementToElement = function AppendElementToElement() {

        args = arguments;
        var sl = new selector();
        if (args[0].AppendTo !== undefined) {
            sl.appendTo = args[0].AppendTo;
        }
        if (args[0].NewElement !== undefined) {
            sl.newElement = args[0].NewElement;
        }
        sl.appendElementToElement();
    };

    //Just inside the element, before its first child
    var PrependElementToElement = function PrependElementToElement() {

        args = arguments;
        var sl = new selector();
        if (args[0].AppendTo !== undefined) {
            sl.appendTo = args[0].AppendTo;
        }
        if (args[0].NewElement !== undefined) {
            sl.newElement = args[0].NewElement;
        }
        sl.prependElementToElement();
    };

    var AppendElementBeforeElement = function AppendElementBeforeElement() {

        args = arguments;
        var sl = new selector();
        if (args[0].AppendBefore !== undefined) {
            sl.appendBefore = args[0].AppendBefore;
        }
        if (args[0].NewElement !== undefined) {
            sl.newElement = args[0].NewElement;
        }
        sl.appendElementBeforeElement();
    };

    var AppendElementAfterElement = function AppendElementAfterElement() {

        args = arguments;
        var sl = new selector();
        if (args[0].AppendAfter !== undefined) {
            sl.appendAfter = args[0].AppendAfter;
        }
        if (args[0].NewElement !== undefined) {
            sl.newElement = args[0].NewElement;
        }
        sl.appendElementAfterElement();
    };

    var SetNavBar = function SetNavBar() {

        args = arguments;
        var nb = new j2HTMLNavbar();

        //DEFAULT COLORS
        var TitleBGColor = 'rgb(29,29,29)';
        var TitleForecolor = 'white';
        var NavbarBGcolor = 'rgb(0,123,255)';
        var NavbarForecolor = 'white';
        var ActiveBGcolor = 'rgb(29,29,29)';
        var ActiveForecolor = 'white';
        var HoverBGcolor = 'rgb(40, 167, 69)';
        var HoverForecolor = 'white';
        var LargeMenuBGcolor = 'rgb(29,29,29)';
        var LargeMenuHoverBGcolor = 'rgb(40,167,69)';
        var LargeMenuHoverForecolor = 'white';
        var LargeMenuForecolor = 'white';
        var HeadingColor = 'rgb(40, 167,69)';
        var SubheadingColor = 'rgb(0,123,255)';

        if (args[0] !== undefined) {

            if (args[0].Data !== undefined) {
                nb.data = args[0].Data;
            }
            if (args[0].ID !== undefined) {
                nb.menuId = args[0].ID;
            }

            if (args[0].Position !== undefined) {
                nb.position = args[0].Position;
            } else {
                nb.position = "top";
            }

            if (args[0].AppendTo !== undefined) {
                nb.appendTo = args[0].AppendTo;
            }

            if (args[0].Text !== undefined) {
                nb.text = args[0].Text;
            }

            if (args[0].Link !== undefined) {
                nb.link = args[0].Link;
            }

            if (args[0].Titlebgcolor !== undefined) {
                nb.titlebgcolor = args[0].Titlebgcolor;
            } else {
                nb.titlebgcolor = TitleBGColor;
            }

            if (args[0].Titleforecolor !== undefined) {
                nb.titleforecolor = args[0].Titleforecolor;
            } else {
                nb.titleforecolor = TitleForecolor;
            }

            if (args[0].Navbarbgcolor !== undefined) {
                nb.navbarbgcolor = args[0].Navbarbgcolor;
            } else {
                nb.navbarbgcolor = NavbarBGcolor;
            }
            if (args[0].Navbarforecolor !== undefined) {
                nb.navbarforecolor = args[0].Navbarforecolor;
            } else {
                nb.navbarforecolor = NavbarForecolor;
            }
            if (args[0].ActiveBGColor !== undefined) {
                nb.selectedmneubgcolor = args[0].ActiveBGColor;
            } else {
                nb.selectedmneubgcolor = ActiveBGcolor;
            }
            if (args[0].ActiveForecolor !== undefined) {
                nb.selectedmenuforecolor = args[0].ActiveForecolor;
            } else {
                nb.selectedmenuforecolor = ActiveForecolor;
            }

            if (args[0].Hoverbgcolor !== undefined) {
                nb.hoverbgcolor = args[0].Hoverbgcolor;
            } else {
                nb.hoverbgcolor = HoverBGcolor;
            }

            if (args[0].Hoverforecolor !== undefined) {
                nb.hoverforecolor = args[0].Hoverforecolor;
            } else {
                nb.hoverforecolor = HoverForecolor;
            }

            if (args[0].LargeMenuBGColor !== undefined) {
                nb.largemenubgcolor = args[0].LargeMenuBGColor;
            } else {
                nb.largemenubgcolor = LargeMenuBGcolor;
            }

            if (args[0].LargeMenuForecolor !== undefined) {
                nb.largemenuforecolor = args[0].LargeMenuForecolor;
            } else {
                nb.largemenuforecolor = LargeMenuForecolor;
            }

            if (args[0].LargeMenuHoverBGColor !== undefined) {
                nb.largemenuhoverbgcolor = args[0].LargeMenuHoverBGColor;
            } else {
                nb.largemenuhoverbgcolor = LargeMenuHoverBGcolor;
            }

            if (args[0].LargeMenuHoverForecolor !== undefined) {
                nb.largemenuhoverforecolor = args[0].LargeMenuHoverForecolor;
            } else {
                nb.largemenuhoverforecolor = LargeMenuHoverForecolor;
            }

            if (args[0].SubmenuBGColor !== undefined) {
                nb.submenubgcolor = args[0].SubmenuBGColor;
            } else {
                nb.largemenubgcolor = NavbarBGcolor;
            }

            if (args[0].SubmenuForecolor !== undefined) {
                nb.submenuforecolor = args[0].SubmenuForecolor;
            } else {
                nb.submenuforecolor = NavbarForecolor;
            }
            if (args[0].HeadingColor !== undefined) {
                nb.headingcolor = args[0].HeadingColor;
            } else {
                nb.headingcolor = HeadingColor;
            }
            if (args[0].SubheadingColor !== undefined) {
                nb.subheadingcolor = args[0].SubheadingColor;
            } else {
                nb.subheadingcolor = SubheadingColor;
            }
        } else {
            nb.navbarbgcolor = NavbarBGcolor;
            nb.navbarforecolor = NavbarForecolor;
            nb.selectedmneubgcolor = ActiveBGcolor;
            nb.selectedmenuforecolor = ActiveForecolor;
            nb.hoverbgcolor = HoverBGcolor;
            nb.hoverforecolor = HoverForecolor;
            nb.largemenubgcolor = LargeMenuBGcolor;
            nb.largemenuforecolor = LargeMenuForecolor;
            nb.submenubgcolor = NavbarBGcolor;
            nb.submenuforecolor = NavbarForecolor;
            nb.headingcolor = HeadingColor;
            nb.subheadingcolor = SubheadingColor;
            nb.largemenuhoverbgcolor = LargeMenuHoverBGcolor;
            nb.largemenuhoverforecolor = LargeMenuHoverForecolor;
        }

        nb.getnavbar();
    };

    var SetTabs = function SetTabs() {

        args = arguments;
        var tab = new j2HTMLTabs();
        tab.getTabs();
    };

    var HideElement = function HideElement() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        sl.hideElement();
    };

    var ShowElement = function ShowElement() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        sl.showElement();
    };

    var EmptyElement = function EmptyElement() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        sl.emptyElement();
    };

    var RemoveElement = function RemoveElement() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        sl.removeElement();
    };

    var Clone = function Clone() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        return sl.clone();

        // args = arguments;
        // return clone(args);
    };

    var ToggleElement = function ToggleElement() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        return sl.toggle();
    };

    var AddClass = function AddClass() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        if (args[0].ClassName !== undefined) {
            sl.className = args[0].ClassName;
        }
        return sl.addClass();

        // args = arguments;
        // addClass(args);
    };

    var RemoveClass = function RemoveClass() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        if (args[0].ClassName !== undefined) {
            sl.className = args[0].ClassName;
        }
        return sl.removeClass();

        // args = arguments;
        // removeClass(args);
    };

    var IsElementVisible = function IsElementVisible() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        return sl.isElementVisible();

        // args = arguments;
        // return isElementVisible(args);
    };

    var IsElementChecked = function IsElementChecked() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        return sl.isElementChecked();
        // args = arguments;
        // return isElementChecked(args);
    };

    var IsElementExsit = function IsElementExsit() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        return sl.isElementExsit();
    };

    var LoadHTML = function LoadHTML() {

        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        if (args[0].Path !== undefined) {
            sl.path = args[0].Path;
        }
        if (args[1] !== undefined) {
            sl.callback = args[1];
        }

        // let sl = new selector();
        // if (Elemt !== undefined) {
        //     sl.element = Elemt;
        // }
        // if (Path !== undefined) {
        //     sl.path = Path;
        // }
        // if (fn !== undefined) {
        //     sl.callback = fn;
        // }
        return sl.loadHTML();

        //loadHTML(Elemt, Path, callback);
    };

    var AppendHTMLString = function AppendHTMLString() {

        args = arguments;

        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        if (args[0].HTMLString !== undefined) {
            sl.HTMLString = args[0].HTMLString;
        }

        return sl.appendHTMLString();
    };

    var PrependHTMLString = function PrependHTMLString() {

        args = arguments;

        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        if (args[0].HTMLString !== undefined) {
            sl.HTMLString = args[0].HTMLString;
        }

        return sl.prependHTMLString();
    };

    var AppendHTMLStringAfter = function AppendHTMLStringAfter() {

        args = arguments;

        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        if (args[0].HTMLString !== undefined) {
            sl.HTMLString = args[0].HTMLString;
        }

        return sl.appendHTMLStringAfter();
    };

    var AppendHTMLStringBefore = function AppendHTMLStringBefore() {

        args = arguments;

        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        if (args[0].HTMLString !== undefined) {
            sl.HTMLString = args[0].HTMLString;
        }

        return sl.appendHTMLStringBefore();
    };

    var CreateElement = function CreateElement() {

        args = arguments;
        var sl = new selector();

        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        if (args[0].ID !== undefined) {
            sl.ID = args[0].ID;
        }

        if (args[0].ClassName !== undefined) {
            sl.className = args[0].ClassName;
        }

        if (args[0].Text !== undefined) {
            sl.text = args[0].Text;
        }

        if (args[0].AppendTo !== undefined) {
            sl.appendTo = args[0].AppendTo;
        }

        return sl.createElement();
    };

    var CreateEvent = function CreateEvent() {

        var elem = void 0,
            type = void 0,
            fn = void 0;
        args = arguments;
        if (args[0].Element !== undefined) {
            elem = args[0].Element;
        }
        if (args[0].Type !== undefined) {
            type = args[0].Type;
        }
        if (args[1] !== undefined) {
            fn = args[1];
        }

        return createEvent(elem, type, fn);
    };

    var DocumentReady = function DocumentReady() {
        var fun = void 0;
        args = arguments;

        if (args[0] === undefined) {
            console.log('Callback function is required');
            alert('Callback function is required');
        } else {
            fun = args[0];
            return documentReady(fun);
        }
    };

    var GetSelectedValueAndText = function GetSelectedValueAndText() {
        args = arguments;
        var sl = new selector();
        if (args[0].Element !== undefined) {
            sl.element = args[0].Element;
        }
        if (args[0].Type !== undefined) {
            sl.type = args[0].Type;
        }
        if (args[0].Selected !== undefined) {
            sl.selected = args[0].Selected;
        }

        sl.getSelectedValueAndText();
        return sl._returnSelectedValueAndText;
    };

    var Get = function Get() {

        var fun = void 0;

        args = arguments;
        var verbs = new httpVerbs();

        if (args[0].url !== undefined) {
            verbs.url = args[0].url;
        }
        if (args[0].Headers !== undefined) {
            verbs.headers = args[0].Headers;
        }
        if (args[1] !== undefined) {
            fun = args[1];
        }

        verbs.GetData(function (error, data) {

            return fun(error, data);
        });
    };

    var Post = function Post() {

        var fun = void 0;

        args = arguments;
        var verbs = new httpVerbs();

        if (args[0].url !== undefined) {
            verbs.url = args[0].url;
        }
        if (args[0].Headers !== undefined) {
            verbs.headers = args[0].Headers;
        }
        if (args[0].Body !== undefined) {
            verbs.body = args[0].Body;
        }
        if (args[1] !== undefined) {
            fun = args[1];
        }

        verbs.PostData(function (error, data) {

            return fun(error, data);
        });
    };

    var Put = function Put() {

        var fun = void 0;

        args = arguments;
        var verbs = new httpVerbs();

        if (args[0].url !== undefined) {
            verbs.url = args[0].url;
        }
        if (args[0].Headers !== undefined) {
            verbs.headers = args[0].Headers;
        }
        if (args[0].Body !== undefined) {
            verbs.body = args[0].Body;
        }
        if (args[1] !== undefined) {
            fun = args[1];
        }

        verbs.PutData(function (error, data) {

            return fun(error, data);
        });
    };

    var Delete = function Delete() {

        var fun = void 0;

        args = arguments;
        var verbs = new httpVerbs();

        if (args[0].url !== undefined) {
            verbs.url = args[0].url;
        }
        if (args[0].Headers !== undefined) {
            verbs.headers = args[0].Headers;
        }
        if (args[0].Body !== undefined) {
            verbs.body = args[0].Body;
        }
        if (args[1] !== undefined) {
            fun = args[1];
        }

        verbs.DeleteData(function (error, data) {

            return fun(error, data);
        });
    };

    var UnitTest = function UnitTest() {

        var rpt = void 0;
        var env = jasmine.getEnv();
        rpt = new whahidTech_jasmine_reporter();
        env.addReporter(rpt);

        return rpt;
    };

    return {
        Table: Table,
        HeadingStyle: HeadingStyle,
        TableBodyStyle: TableBodyStyle,
        Paging: Paging,
        Search: Search,
        Print: Print,
        Dropdown: Dropdown,
        Radio: Radio,
        Checkbox: Checkbox,
        List: List,
        Filter: Filter,
        Switch: Switch,
        Modal: Modal,
        Inst_Modal: Inst_Modal,
        ShowModal: ShowModal,
        //ShowModalJS: ShowModalJS,
        HideModal: HideModal,
        CloseModal: CloseModal,
        ShowLoadingModal: ShowLoadingModal,
        HideLoadingModal: HideLoadingModal,
        PopupMessage: PopupMessage,
        ShowPopup: ShowPopup,
        HidePopup: HidePopup,
        TextBox: TextBox,
        Create: Create,
        Validate: Validate,
        Placeholder: Placeholder,
        GetElement: GetElement,
        GetElementByName: GetElementByName,
        GetElementbyTagName: GetElementbyTagName,
        GetChildren: GetChildren,
        GetClosestParentElement: GetClosestParentElement,
        GetParent: GetParent,
        GetSiblings: GetSiblings,
        GetNextSibling: GetNextSibling,
        GetPreviousSibling: GetPreviousSibling,
        GetElementValue: GetElementValue,
        SetElementValue: SetElementValue,
        GetElementText: GetElementText,
        SetElementText: SetElementText,
        AppendElementToElement: AppendElementToElement,
        PrependElementToElement: PrependElementToElement,
        AppendElementBeforeElement: AppendElementBeforeElement,
        AppendElementAfterElement: AppendElementAfterElement,
        SetNavBar: SetNavBar,
        SetTabs: SetTabs,
        RemoveElement: RemoveElement,
        Clone: Clone,
        CreateElement: CreateElement,
        CreateEvent: CreateEvent,
        ToggleElement: ToggleElement,
        AddClass: AddClass,
        RemoveClass: RemoveClass,
        HideElement: HideElement,
        ShowElement: ShowElement,
        EmptyElement: EmptyElement,
        IsElementVisible: IsElementVisible,
        IsElementChecked: IsElementChecked,
        IsElementExsit: IsElementExsit,
        LoadHTML: LoadHTML,
        AppendHTMLString: AppendHTMLString,
        PrependHTMLString: PrependHTMLString,
        AppendHTMLStringAfter: AppendHTMLStringAfter,
        AppendHTMLStringBefore: AppendHTMLStringBefore,
        GetSelectedValueAndText: GetSelectedValueAndText,
        Form: Form,
        Get: Get,
        Post: Post,
        Put: Put,
        Delete: Delete,
        UnitTest: UnitTest,
        DatePicker: DatePicker,
        Carousel: Carousel,
        DocumentReady: DocumentReady

    };
};

//CREATE INSTANCE
var j2HTML = new jsonToHtml();

//DOCUMENT READY
// function DocumentReady(fn) {

//     if (document.readyState === 'complete' && document.readyState !== 'loading') { // Or also compare to 'interactive'
//         setTimeout(fn, 1); // Schedule to run immediately
//     } else {
//         let readyStateCheckInterval = setInterval(function () {
//             if (document.readyState === 'complete') { // Or also compare to 'interactive'
//                 clearInterval(readyStateCheckInterval);
//                 fn();
//             }
//         }, 1500);


//     }
// }

// function WindowReady(fn) {
//     window.onload = fn();
// }


(function (funcName, baseObj) {
    // The public function name defaults to window.docReady
    // but you can pass in your own object and own function name and those will be used
    // if you want to put them in a different namespace
    funcName = funcName || "docReady";
    baseObj = baseObj || window;
    var readyList = [];
    var readyFired = false;
    var readyEventHandlersInstalled = false;

    // call this when the document is ready
    // this function protects itself against being called more than once
    function ready() {
        if (!readyFired) {
            // this must be set to true before we start calling callbacks
            readyFired = true;
            for (var i = 0; i < readyList.length; i++) {
                // if a callback here happens to add new ready handlers,
                // the docReady() function will see that it already fired
                // and will schedule the callback to run right after
                // this event loop finishes so all handlers will still execute
                // in order and no new ones will be added to the readyList
                // while we are processing the list
                readyList[i].fn.call(window, readyList[i].ctx);
            }
            // allow any closures held by these functions to free
            readyList = [];
        }
    }

    function readyStateChange() {
        if (document.readyState === "complete") {
            ready();
        }
    }

    // This is the one public interface
    // docReady(fn, context);
    // the context argument is optional - if present, it will be passed
    // as an argument to the callback
    baseObj[funcName] = function (callback, context) {
        if (typeof callback !== "function") {
            throw new TypeError("callback for docReady(fn) must be a function");
        }
        // if ready has already fired, then just schedule the callback
        // to fire asynchronously, but right away
        if (readyFired) {
            setTimeout(function () {
                callback(context);
            }, 1);
            return;
        } else {
            // add the function and context to the list
            readyList.push({
                fn: callback,
                ctx: context
            });
        }
        // if document already ready to go, schedule the ready function to run
        if (document.readyState === "complete") {
            setTimeout(ready, 1);
        } else if (!readyEventHandlersInstalled) {
            // otherwise if we don't have event handlers installed, install them
            if (document.addEventListener) {
                // first choice is DOMContentLoaded event
                document.addEventListener("DOMContentLoaded", ready, false);
                // backup is window load event
                window.addEventListener("load", ready, false);
            } else {
                // must be IE
                document.attachEvent("onreadystatechange", readyStateChange);
                window.attachEvent("onload", ready);
            }
            readyEventHandlersInstalled = true;
        }
    };
})("DocumentReady", window);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var popupMessage = function () {

    //CREAT CLASS
    var popupMessage = function () {
        function popupMessage() {
            _classCallCheck(this, popupMessage);

            this._popupId = false;
            this._type = false;
            this._heading = false;
            this._message = false;
            this._popupFooter = true;
            this._popupHeader = true;
            this._headerBGcolor = false;
            this._headerforecolor = false;
            this._confirmation = false;
            this._parameters = false;
        }

        //#region ************** PROPERTIES ******************


        _createClass(popupMessage, [{
            key: 'getPopup',


            //endregion

            //#region ************** PUBLIC FUNCTIONS ************
            value: function getPopup() {

                var error = '';
                var headerColor = '';

                if (j2HTML.IsElementExsit({
                    Element: this._popupId
                })) {
                    j2HTML.RemoveElement({
                        Element: this._popupId
                    });
                }
                if (this._message === false) {
                    error += 'Message required\n';
                }
                if (this._heading === false) {
                    error += 'Heading required\n';
                }

                if (this._type.toLowerCase() === 'confirm' && this._confirmation === false) {
                    error += 'Confirm funtion required';
                }

                if (error === '') {

                    var header = '';
                    //WHEN POPUP HEADER IS TRUE
                    if (this._popupHeader === true) {

                        if (this._headerBGcolor !== false && this._headerforecolor !== false) {
                            if (this._type.toLowerCase() === 'error') {
                                headerColor = 'style="background-color: ' + this._headerBGcolor + '; color:' + this._headerforecolor + '"';
                            } else if (this._type.toLowerCase() === 'alert' || this._type.toLowerCase() === 'information') {
                                headerColor = 'style="background-color: ' + this._headerBGcolor + '; color:' + this._headerforecolor + '"';
                            } else if (this._type.toLowerCase() === 'confirmation') {
                                headerColor = 'style="background-color: ' + this._headerBGcolor + '; color:' + this._headerforecolor + '"';
                            }
                        } else if (this._headerBGcolor !== false && this._headerforecolor === false) {
                            if (this._type.toLowerCase() === 'error') {
                                headerColor = 'style="background-color: ' + this._headerBGcolor + '; color:white"';
                            } else if (this._type.toLowerCase() === 'alert' || this._type.toLowerCase() === 'information') {
                                headerColor = 'style="background-color: ' + this._headerBGcolor + '; color:white"';
                            } else if (this._type.toLowerCase() === 'confirmation') {
                                headerColor = 'style="background-color: ' + this._headerBGcolor + '; color:white"';
                            }
                        } else if (this._headerBGcolor === false && this._headerforecolor !== false) {
                            if (this._type.toLowerCase() === 'error') {
                                headerColor = 'style="background-color: red; color:' + this._headerforecolor + '"';
                            } else if (this._type.toLowerCase() === 'alert' || this._type.toLowerCase() === 'information') {
                                headerColor = 'style="background-color: blue; color:' + this._headerforecolor + '"';
                            } else if (this._type.toLowerCase() === 'confirmation') {
                                headerColor = 'style="background-color: green; color:' + this._headerforecolor + '"';
                            }
                        } else if (this._headerBGcolor === false && this._headerforecolor === false) {
                            if (this._type.toLowerCase() === 'error') {
                                headerColor = 'style="background-color: red; color:white"';
                            } else if (this._type.toLowerCase() === 'alert' || this._type.toLowerCase() === 'information') {
                                headerColor = 'style="background-color: blue; color:white"';
                            } else if (this._type.toLowerCase() === 'confirmation') {
                                headerColor = 'style="background-color: green; color:white"';
                            }
                        }

                        //let pm = new popupMessage();
                        header = '<div class="modal-header" ' + headerColor + '>\n                                <h4 class="modal-title">' + this._heading + '</h4>\n                                <button type="button" id="btnHidePopupTop" class="close">&times;</button>\n                              </div>';
                    }

                    var footer = '';
                    //WHEN POPUP FOOTER IS TRUE
                    if (this._popupFooter === true && (this._type.toLowerCase() === 'error' || this._type.toLowerCase() === 'information' || this._type.toLowerCase() === 'alert')) {

                        footer = '<div class="modal-footer">\n                                <button type="button" id="btnHidePopupClose" class="btn btn-sm btn-primary">Close</button>\n                              </div>';
                    }
                    if (this._popupFooter === true && this._type.toLowerCase() === 'confirmation') {

                        if (this._confirmation !== false) {
                            confirmFun = this._confirmation.Function !== undefined ? this._confirmation.Function : false;
                            funParameters = this._confirmation.Parameters !== undefined ? this._confirmation.Parameters : false;
                        }
                        footer = '<div class="modal-footer">\n                                <button id="btnj2HTMLConfirmYesClicked" type="button" class="btn btn-sm btn-primary">Yes</button>\n                                <button type = "button" id="btnHidePopupNo" class="btn btn-sm btn-primaryt">No</button >\n                              </div>';
                    }

                    var body = '';
                    popupModalId = this._popupId;
                    body = '<div id="' + this._popupId.replace('#', '') + 'ModalBody" class="modal-body"><p>' + this._message + '</p></div>';

                    var strModal = '<div id="' + this._popupId.replace('#', '') + '" class="modal fade" role="dialog">\n                                  <div class="modal-dialog">\n                                     <!-- Modal content-->\n                                     <div class="modal-content">\n                                        ' + header + ' \n                                        ' + body + ' \n                                        ' + footer + '\n                                     </div >\n                                 </div>\n                                </div>';

                    j2HTML.AppendHTMLString({
                        Element: 'body',
                        HTMLString: strModal
                    });

                    var pm = new popupMessage();

                    // ********** ADD EVENT LISTENER FOR TOP CLOSE BUTTON (X) *********
                    if (j2HTML.IsElementExsit({
                        Element: '#btnHidePopupTop'
                    })) {
                        var topbtn = j2HTML.GetElement({
                            Element: '#btnHidePopupTop'
                        });

                        //REMOVE PREVIOUSLY ADDED CLICK EVENT LISTENER
                        topbtn[0].removeEventListener("click", pm.hidePopup, false);

                        //ADD ON-CHANGE EVENT LISTENER
                        topbtn[0].addEventListener("click", pm.hidePopup, false);
                    }

                    // ********** ADD EVENT LISTENER FOR TOP NO BUTTON (FOOTER) *********
                    if (j2HTML.IsElementExsit({
                        Element: '#btnHidePopupNo'
                    })) {
                        var Nobtn = j2HTML.GetElement({
                            Element: '#btnHidePopupNo'
                        });

                        //REMOVE PREVIOUSLY ADDED CLICK EVENT LISTENER
                        Nobtn[0].removeEventListener("click", pm.hidePopup, false);

                        //ADD ON-CHANGE EVENT LISTENER
                        Nobtn[0].addEventListener("click", pm.hidePopup, false);
                    }

                    // ********** ADD EVENT LISTENER FOR TOP CLOSE BUTTON (FOOTER) *********
                    if (j2HTML.IsElementExsit({
                        Element: '#btnHidePopupClose'
                    })) {
                        var closebtn = j2HTML.GetElement({
                            Element: '#btnHidePopupClose'
                        });

                        //REMOVE PREVIOUSLY ADDED CLICK EVENT LISTENER
                        closebtn[0].removeEventListener("click", pm.hidePopup, false);

                        //ADD ON-CHANGE EVENT LISTENER
                        closebtn[0].addEventListener("click", pm.hidePopup, false);
                    }

                    // ********** ADD EVENT LISTENER FOR TOP CLOSE BUTTON (FOOTER) *********
                    if (j2HTML.IsElementExsit({
                        Element: '#btnj2HTMLConfirmYesClicked'
                    })) {
                        var confirmYesbtn = j2HTML.GetElement({
                            Element: '#btnj2HTMLConfirmYesClicked'
                        });

                        //REMOVE PREVIOUSLY ADDED CLICK EVENT LISTENER
                        confirmYesbtn[0].removeEventListener("click", pm.callConfirmFunction, false);

                        //ADD ON-CHANGE EVENT LISTENER
                        confirmYesbtn[0].addEventListener("click", pm.callConfirmFunction, false);
                    }
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }
        }, {
            key: 'showPopup',
            value: function showPopup() {

                if (this._popupId === false) {
                    this._popupId = '#j2HTMLPopupMessage';
                }

                j2HTML.ShowModal({
                    ModalID: this._popupId
                });
            }
        }, {
            key: 'hidePopup',
            value: function hidePopup() {

                if (popupModalId === false) {
                    popupModalId = '#j2HTMLPopupMessage';
                }

                j2HTML.HideModal({
                    ModalID: popupModalId
                });
                j2HTML.RemoveElement({
                    Element: popupModalId
                });
            }
        }, {
            key: 'callConfirmFunction',
            value: function callConfirmFunction() {

                //confirmFun();
                j2HTML.HideModal({
                    ModalID: popupModalId
                });
                j2HTML.RemoveElement({
                    Element: popupModalId
                });

                if (funParameters !== false) {
                    confirmFun(JSON.stringify(funParameters));
                } else {
                    confirmFun();
                }
            }
            //endregion


        }, {
            key: 'popupId',
            get: function get() {
                return this._popupId;
            },
            set: function set(value) {
                this._popupId = value;
            }
        }, {
            key: 'type',
            get: function get() {
                return this._type;
            },
            set: function set(value) {
                this._type = value;
            }
        }, {
            key: 'heading',
            get: function get() {
                return this._heading;
            },
            set: function set(value) {
                this._heading = value;
            }
        }, {
            key: 'message',
            get: function get() {
                return this._message;
            },
            set: function set(value) {
                this._message = value;
            }
        }, {
            key: 'popupFooter',
            get: function get() {
                return this._popupFooter;
            },
            set: function set(value) {
                this._popupFooter = value;
            }
        }, {
            key: 'popupHeader',
            get: function get() {
                return this._popupHeader;
            },
            set: function set(value) {
                this._popupHeader = value;
            }
        }, {
            key: 'headerBGColor',
            get: function get() {
                return this._headerBGcolor;
            },
            set: function set(value) {
                this._headerBGcolor = value;
            }
        }, {
            key: 'headerForeColor',
            get: function get() {
                return this._headerforecolor;
            },
            set: function set(value) {
                this._headerforecolor = value;
            }
        }, {
            key: 'confirmation',
            get: function get() {
                return this._confirmation;
            },
            set: function set(value) {
                this._confirmation = value;
            }
        }, {
            key: 'parameters',
            get: function get() {
                return this._parameters;
            },
            set: function set(value) {
                this._parameters = value;
            }
        }]);

        return popupMessage;
    }();

    //#region ************** PRIVATE VARIABLE AND METHOD *******


    var confirmFun = void 0;
    var popupModalId = void 0;
    var funParameters = void 0;

    //endregion


    return popupMessage;
}();
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var radio = function () {

    //*********** CLASS ***************/
    var radio = function () {
        function radio() {
            _classCallCheck(this, radio);

            this._data = false;
            this._groupName = 'rdbGroupName';
            this._value = false;
            this._text = false;
            this._selectedValue = false;
            this._direction = 'Horizontal';
            this._appendTo = false;

            //ONLY FOR MODAL
            this._isModal = false;
        }

        //#region PROPERTIES

        //PROPERTY -- DATA


        _createClass(radio, [{
            key: 'getRadioButton',


            //endregion 


            //#region PUBLIC FUNCTIONS
            value: function getRadioButton() {

                var error = '';

                if (this._data === false) {
                    error += 'Data is required \n';
                } else if (this._appendTo === false && this._isModal === false) {
                    error += 'Control ID is required to append checkbox\n';
                } else if (this._text === false) {
                    error += 'Text is required\n';
                }

                if (this._appendTo !== false) {

                    if (this._isModal === true) {

                        this._appendTo = '#' + this._appendTo.slice(1) + 'body';
                        isModalShowing = this._isModal;

                        this._groupName = 'rdb' + this._appendTo.slice(1).slice(0, -4);
                        groupName = 'rdb' + this._appendTo.slice(1).slice(0, -4);
                    }
                } else {
                    if (this._isModal === true) {

                        this._appendTo = '#j2HTMLModalbody';
                        isModalShowing = this._isModal;
                    }
                }

                if (this._groupName !== false) {
                    groupName = this._groupName;
                } else {
                    groupName = false;
                }

                if (this._isModal === true && this._selectedValue === false) {
                    j2HTML.HideElement({ Element: '#btnJ2HTMLModalSubmitData' });
                }

                if (error === '') {
                    var radioButtonArray = this._data.map(privateMethods.createRadioButton.bind(null, this._text, this._value, this._direction, this._groupName));

                    //EMPTY THE APPEND TO ELELEMNT 
                    j2HTML.EmptyElement({
                        Element: this._appendTo
                    });

                    apptTo = this._appendTo;
                    //APPEND VAUE TO ELEMENT
                    radioButtonArray.map(function (obj) {

                        j2HTML.AppendElementToElement({
                            AppendTo: apptTo,
                            NewElement: obj
                        });
                    });

                    if (this._selectedValue !== false) {
                        //getSelectedValueFun= this._selectedValue;
                        sendData = this._data;
                        functionName = this._selectedValue.Function;
                        OnEvent = this._selectedValue.On.toLowerCase();
                        var rdb = new radio();

                        if (this._selectedValue.On.toLowerCase() === "change" && this._isModal === false) {

                            var elems = j2HTML.GetElementByName({ Name: this._groupName });

                            [].slice.call(elems).map(function (rdbElem) {

                                //REMOVE PREVIOUSLY ADDED ON-CHANGE EVENT LISTENER
                                rdbElem.removeEventListener("change", rdb.setj2HTMLRadioSelectedValue, false);

                                //ADD ON-CHANGE EVENT LISTENER
                                rdbElem.addEventListener("change", rdb.setj2HTMLRadioSelectedValue, false);
                            });
                        }
                        if (this._selectedValue.On.toLowerCase() === "click" && this._isModal === false) {

                            //REMOVE ON-CHANGE EVENT LISTENER
                            var ddlElem = j2HTML.GetElement({ Element: this._appendTo });
                            ddlElem[0].removeEventListener("change", rdb.setj2HTMLRadioSelectedValue, false);

                            //REMOVE PREVIOUSLY ADDED ON-CLICK EVENT LISTENER
                            var elem = j2HTML.GetElement({ Element: this._selectedValue.ButtonID });

                            elem[0].removeEventListener('click', rdb.setj2HTMLRadioSelectedValue, false);

                            //ADD ON-CLICK EVENT LISTENER
                            elem[0].addEventListener('click', rdb.setj2HTMLRadioSelectedValue, false);
                        }
                        if (this._selectedValue.On.toLowerCase() === "click" && this._isModal === true) {

                            //REMOVE ON-CHANGE EVENT LISTENER
                            var _ddlElem = j2HTML.GetElement({ Element: this._appendTo });
                            _ddlElem[0].removeEventListener("change", rdb.setj2HTMLRadioSelectedValue, false);

                            //REMOVE PREVIOUSLY ADDED ON-CLICK EVENT LISTENER
                            var _elem = j2HTML.GetElement({ Element: '#btnJ2HTMLModalSubmitData' });

                            _elem[0].removeEventListener('click', rdb.setj2HTMLRadioSelectedValue, false);

                            //ADD ON-CLICK EVENT LISTENER
                            _elem[0].addEventListener('click', rdb.setj2HTMLRadioSelectedValue, false);
                        }
                    }
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }
        }, {
            key: 'setj2HTMLRadioSelectedValue',
            value: function setj2HTMLRadioSelectedValue() {

                var selectedValue = '';
                var selectedText = '';
                var rdbs = '';

                if (groupName !== false) {
                    rdbs = document.querySelectorAll('[name="' + groupName + '"]');
                } else {
                    rdbs = document.querySelectorAll('[name="rdb' + apptTo.slice(1) + '"]');
                }
                [].slice.call(rdbs).map(function (rdb) {

                    if (rdb.checked) {

                        if (rdb.value !== undefined) {
                            selectedValue = rdb.value;
                        }
                        selectedText = rdb.getAttribute('text');
                    }
                });

                functionName(selectedValue, selectedText, sendData);

                if (isModalShowing === true) {
                    if (apptTo === false) {
                        j2HTML.HideModal({ ModalID: '#j2HTMLModal' });
                        j2HTML.RemoveElement({ Element: '#j2HTMLModal' });
                    } else {
                        j2HTML.HideModal({ ModalID: apptTo.slice(0, -4) });
                        j2HTML.RemoveElement({ Element: apptTo.slice(0, -4) });
                    }
                }
            }

            //endregion

        }, {
            key: 'data',
            get: function get() {
                return this._data;
            },
            set: function set(value) {
                this._data = value;
            }

            //PROPERTY -- GROUP NAME 

        }, {
            key: 'groupName',
            get: function get() {
                return this._groupName;
            },
            set: function set(value) {
                this._groupName = value;
            }

            //PROPERTY -- VALUE

        }, {
            key: 'value',
            get: function get() {
                return this._value;
            },
            set: function set(value) {
                this._value = value;
            }

            //PROPERTY -- TEXT

        }, {
            key: 'text',
            get: function get() {
                return this._text;
            },
            set: function set(value) {
                this._text = value;
            }

            //SELECTED VALUE

        }, {
            key: 'selectedValue',
            get: function get() {
                return this._selectedValue;
            },
            set: function set(value) {
                this._selectedValue = value;
            }

            //PROPERTY -- DIRECTION

        }, {
            key: 'direction',
            get: function get() {
                return this._direction;
            },
            set: function set(value) {
                this._direction = value;
            }

            //PROPERTY -- APPEND TO

        }, {
            key: 'appendTo',
            get: function get() {
                return this._appendTo;
            },
            set: function set(value) {
                this._appendTo = value;
            }
        }, {
            key: 'isModal',
            get: function get() {
                return this._isModal;
            },
            set: function set(value) {
                this._isModal = value;
            }
        }]);

        return radio;
    }();

    //#region PRIVATE VARIABLES AND FUNCTIONS


    var apptTo = '';
    var sendData = '';
    var functionName = '';
    var groupName = '';
    var isModalShowing = '';

    //ADD ALL PRIVATE METHODS INSIDE privateMethods
    var privateMethods = {
        createRadioButton: function createRadioButton(text, value, direction, groupName, obj) {

            //CREATE LABEL
            var lbl = document.createElement('label');
            //MARGIN top right left bottom
            lbl.style.margin = '0px 0px 10px,opx';
            var lblText = document.createTextNode(obj[text]);

            //CREATE CHECKBOX
            var rdb = document.createElement('input');
            rdb.setAttribute('type', 'radio');

            //************* HORIZONTAL *****************
            //HORIZONTAL WITH VALUE
            if (value !== false && direction === 'Horizontal') {

                rdb.setAttribute('name', groupName);
                rdb.setAttribute('value', obj[value]);
                rdb.setAttribute('text', obj[text]);
            }
            //HORIZONTAL WITH NO VALUE
            else if (value === false && direction === 'Horizontal') {

                    rdb.setAttribute('name', groupName);
                    rdb.setAttribute('text', obj[text]);
                }

            //************* VERTICAL ******************* */
            //VERTICAL WITH VALUE
            if (value !== false && direction !== 'Horizontal') {

                lbl.style.display = 'block';
                rdb.setAttribute('name', groupName);
                rdb.setAttribute('value', obj[value]);
                rdb.setAttribute('text', obj[text]);
            }
            //VERTICAL WITH NO VALUE
            else if (value === false && direction !== 'Horizontal') {
                    lbl.style.display = 'block';
                    rdb.setAttribute('name', groupName);
                    rdb.setAttribute('text', obj[text]);
                }

            lbl.appendChild(rdb);
            lbl.appendChild(lblText);
            return lbl;
        }
    };
    //endregion

    return radio;
}();

// function radio() {
//     var args = arguments[0][0];


//     var data = false;
//     var groupName = false;
//     var value = false;
//     var text = false;
//     var direction = 'Horizontal';
//     var appendTo = false;
//     var error = '';

//     //ONLY FOR MODAL
//     var isModal = false;


//     if (args.Data !== undefined) {
//         data = args.Data;
//     }
//     if (args.GroupName !== undefined) {
//         groupName = args.GroupName;
//     }
//     if (args.Value !== undefined) {
//         value = args.Value;

//         if (data !== false) {
//             if (data[0].hasOwnProperty(value) === false) {
//                 error += 'Value "' + value + '" not found\n';
//             }
//         }
//     }
//     if (args.Text !== undefined) {
//         text = args.Text;

//         if (data !== false) {
//             if (data[0].hasOwnProperty(text) === false) {
//                 error += 'Text "' + text + '" not found\n';
//             }
//         }
//     } else {
//         error += 'Text is required';
//     }
//     if (args.Direction !== undefined) {
//         direction = args.Direction;
//     }

//     if (args.isModal !== undefined) {
//         isModal = args.isModal;
//     }

//     if (args.AppendTo !== undefined) {
//         appendTo = args.AppendTo;

//         if (isModal === true) {

//             var rdbModalId = appendTo.replace('#', '');
//             appendTo = '#rdb' + rdbModalId;
//         }
//     } else {
//         if (isModal === true) {

//             appendTo = '#rdbj2HTMLModal';
//         }
//     }


//     if (data === false) {
//         error += 'Data is required \n';
//     }
//     if (appendTo === false) {
//         error += 'Control ID is required to append radion button(AppendTo) \n';
//     }
//     var rdb = '';

//     if (error === '') {
//         //IF OBJECT
//         if (data.length > 0) {

//             $.each(data, function(i, v) {

//                 var count = 0;
//                 var rdbValue = '';
//                 var rdbText = '';
//                 $.each(data[i], function(ii, vv) {

//                     if (value !== undefined && ii === value) {
//                         rdbValue = vv;
//                     }

//                     if (ii === text) {
//                         rdbText = vv;
//                     }

//                     count = count + 1;

//                     if (Object.keys(data[0]).length == count) {

//                         //VERTICAL WITH VALUE
//                         if (value !== false && direction !== 'Horizontal') {

//                             rdb += '<label style="display:block"><input type="radio" name="' + groupName + '" value="' + rdbValue + '">' + rdbText + '</label>';
//                         }
//                         //HORIZONTAL WITH VALUE
//                         if (value !== false && direction === 'Horizontal') {
//                             rdb += '<label style="margin-left:10px;"><input type="radio" name="' + groupName + '" value="' + rdbValue + '">' + rdbText + '</label>';
//                         }
//                         //VERTICAL WITH NO VALUE
//                         if (value === false && direction !== 'Horizontal') {

//                             rdb += '<label style="display:block"><input type="radio" name="' + groupName + '">' + rdbText + '</label>';
//                         }
//                         //HORIZONTAL WITH NO VALUE
//                         if (value === false && direction === 'Horizontal') {
//                             rdb += '<label style="margin-left:10px;"><input type="radio" name="' + groupName + '">' + rdbText + '</label>';
//                         }


//                     }
//                 });
//             });

//             $(appendTo).empty();
//             $(appendTo).append(rdb);
//         }
//         // IF ARRAY
//         else {
//             var count = 0;
//             var rdbValue = '';
//             var rdbText = '';
//             $.each(data, function(ii, vv) {

//                 if (value !== undefined && ii === value) {
//                     rdbValue = vv;
//                 }

//                 if (ii === text) {
//                     rdbText = vv;
//                 }

//                 count = count + 1;

//                 if (Object.keys(data).length == count) {

//                     //VERTICAL WITH VALUE
//                     if (value !== false && direction !== 'Horizontal') {

//                         rdb += '<label style="display:block"><input type="radio" name="' + groupName + '" value="' + rdbValue + '">' + rdbText + '</label>';
//                     }
//                     //HORIZONTAL WITH VALUE
//                     if (value !== false && direction === 'Horizontal') {
//                         rdb += '<label style="margin-left:10px;"><input type="radio" name="' + groupName + '" value="' + rdbValue + '">' + rdbText + '</label>';
//                     }
//                     //VERTICAL WITH NO VALUE
//                     if (value === false && direction !== 'Horizontal') {

//                         rdb += '<label style="display:block"><input type="radio" name="' + groupName + '">' + rdbText + '</label>';
//                     }
//                     //HORIZONTAL WITH NO VALUE
//                     if (value === false && direction === 'Horizontal') {
//                         rdb += '<label style="margin-left:10px;"><input type="radio" name="' + groupName + '">' + rdbText + '</label>';
//                     }
//                 }
//             });

//             $(appendTo).empty();
//             $(appendTo).append(rdb);
//         }

//     } else {
//         alert(error);
//     }

// }
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * JAVASCRIPT SELECTORS
 * 
 * 
 */

var selector = function () {

    //CREAT CLASS
    var selector = function () {
        function selector() {
            _classCallCheck(this, selector);

            this._element = false;
            this._name = false;

            this._appendTo = false;
            this._newElement = false;

            this._appendBefore = false;
            this._appendAfter = false;

            this._className = false;
            this._id = false;
            this._text = false;

            this._path = false;
            this._callback = false;

            this._modalID = false;
            this._modalOptions = false;

            this._htmlString = false;

            this._elementValue = false;
            this._sel = false;

            this._type = false;

            this._selected = false;

            this._returnSelectedValueAndText = false;
        }

        //#region ************** PROPERTIES ******************

        // PROPERTIES


        _createClass(selector, [{
            key: 'getElement',


            //endregion

            //#region ************** PUBLIC FUNCTIONS ************
            /**
             * 
             * GET ELEMENT
             * 
             */
            value: function getElement() {

                var error = '';

                if (this._element === false) {
                    error += 'Element is required';
                }

                if (error === '') {
                    return document.querySelectorAll(this._element);
                } else {

                    //alert(error);
                    throw new Error(error);
                }
            }
            /**
             * 
             * GET ELEMENT BY NAME
             * 
             */

        }, {
            key: 'getElementByName',
            value: function getElementByName() {

                var error = '';

                if (this._name === false) {
                    error += 'Element name required';
                }

                if (error === '') {
                    return [].slice.call(document.getElementsByName(this._name));
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * GET CHILDREN
             * 
             */

        }, {
            key: 'getChildren',
            value: function getChildren() {

                var error = '';
                var elemList = '';
                var list = [];

                if (this._element === false) {
                    error += 'Element required to get children';
                } else if (this._element.charAt(0) === '#') {

                    if (document.querySelectorAll(this._element).length === 0) {
                        error += 'elemnt not found, check element id or class name and try again';
                    } else {
                        elemList = document.querySelectorAll(this._element)[0].children;
                    }
                } else if (this._element.charAt(0) === '.') {

                    var elemArray = this._element.split(' ');

                    if (document.querySelectorAll(elemArray[0]).length === 0) {
                        error += 'elemnt not found, check element id or class name and try again';
                    } else {
                        elemList = document.querySelectorAll(elemArray[0])[0].children;
                    }
                } else {
                    if (document.querySelectorAll(this._element)[0] === 0) {
                        error += 'elemnt not found, check element id or class name and try again';
                    } else {
                        elemList = document.querySelectorAll(this._element)[0].children;
                    }
                }
                if (error === '') {

                    for (i = 0; i < elemList.length; i++) {
                        if (elemList[i].localName !== 'br') {
                            list.push(elemList[i]);
                        }
                    }
                    return list;
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }
            /**
             * 
             * GET CLOSEST
             * 
             */

        }, {
            key: 'getClosest',
            value: function getClosest() {
                var error = '';
                if (this._element === false) {
                    error += 'Element is required to get closest';
                } else if (this._sel === false) {
                    error += 'Selector is required to get closest';
                }

                if (error === '') {
                    //let elem = j2HTML.GetElement({element:this._element});
                    var elem = this._element;
                    var _selector = this._sel;

                    var firstChar = _selector.charAt(0);

                    // Get closest match
                    for (; elem && elem !== document; elem = elem.parentNode) {

                        // If selector is a class
                        if (firstChar === '.') {
                            if (elem.classList.contains(_selector.substr(1))) {
                                return elem;
                            }
                        }

                        // If selector is an ID
                        if (firstChar === '#') {
                            if (elem.id === _selector.substr(1)) {
                                return elem;
                            }
                        }

                        // If selector is a data attribute
                        if (firstChar === '[') {
                            if (elem.hasAttribute(_selector.substr(1, _selector.length - 2))) {
                                return elem;
                            }
                        }

                        // If selector is a tag
                        if (elem.tagName.toLowerCase() === _selector) {
                            return elem;
                        }
                    }
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * GET PARENT
             * 
             */

        }, {
            key: 'getParent',
            value: function getParent() {

                var error = '';

                if (this._element === false) {
                    error += 'Element is required to get parent';
                }

                if (error === '') {
                    var elem = j2HTML.GetElement({
                        Element: this._element
                    });
                    return elem[0].parentElement;
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * GET SIBLINGS
             * 
             */

        }, {
            key: 'getSiblings',
            value: function getSiblings() {

                var error = '';
                if (this._element === false) {
                    error += 'Element required to get siblings';
                }
                if (error === '') {
                    var siblings = [];
                    var parentElem = '';
                    var children = '';

                    var parent = j2HTML.GetParent({
                        Element: this._element
                    });

                    if (parent.id !== null && parent.id !== '') {
                        parentElem = '#' + parent.id;
                        children = j2HTML.GetChildren({
                            Element: parentElem
                        });
                    } else if (parent.classList.value !== null && parent.classList.value !== '' && parent.classList.value !== undefined) {
                        parentElem = '.' + parent.classList.value;
                        children = j2HTML.GetChildren({
                            Element: parentElem
                        });
                    } else {
                        parentElem = parent;
                        children = j2HTML.GetChildren({
                            Element: parentElem.localName
                        });
                    }

                    if (children.length !== 0) {
                        for (i = 0; i < children.length; i++) {
                            siblings.push(children[i]);
                        }
                    }

                    return siblings;
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * GET PREVIOUS SIBLINGS
             * 
             */

        }, {
            key: 'getPrviousSibling',
            value: function getPrviousSibling() {
                var error = '';
                if (this._element === false) {
                    error += 'Element required to get previous siblings';
                }
                if (error === '') {

                    return j2HTML.GetElement({
                        Element: this._element
                    })[0].previousElementSibling;
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * GET NEXT SIBLINGS
             * 
             */

        }, {
            key: 'getNextSibling',
            value: function getNextSibling() {
                var error = '';
                if (this._element === false) {
                    error += 'Element required to get next siblings';
                }
                if (error === '') {

                    return j2HTML.GetElement({
                        Element: this._element
                    })[0].nextElementSibling;
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * GET ELEMENT VALUE
             * 
             */

        }, {
            key: 'getElementValue',
            value: function getElementValue() {
                var elem = j2HTML.GetElement({
                    Element: this._element
                });

                return elem[0].getAttribute('value');
            }

            /**
             * 
             * SET ELEMENT VALUE
             * 
             */

        }, {
            key: 'setElementValue',
            value: function setElementValue() {
                var elem = j2HTML.GetElement({
                    Element: this._element
                });

                elem[0].setAttribute('value', this._elementValue);
            }

            /**
             * 
             * GET ELEMENT TEXT
             * 
             */

        }, {
            key: 'getElementText',
            value: function getElementText() {
                var elem = j2HTML.GetElement({
                    Element: this._element
                });

                return elem[0].innerHTML;
            }

            /**
             * 
             * GET ELEMENT TEXT
             * 
             */

        }, {
            key: 'setElementText',
            value: function setElementText() {
                var elem = j2HTML.GetElement({
                    Element: this._element
                });

                elem[0].innerHTML = this._elementValue;
            }

            /**
             * 
             * APPEND TO ELEMENT (Just inside the element, after its last child)
             * 
             */

        }, {
            key: 'appendElementToElement',
            value: function appendElementToElement() {

                var error = '';

                if (this._appendTo === false) {
                    error += 'Element is required to append new element';
                } else if (this._newElement === false) {
                    error += 'New Element is required';
                }

                if (error === '') {
                    var newElem = this._newElement;
                    var elem = [].slice.call(j2HTML.GetElement({
                        Element: this._appendTo
                    }));

                    elem.map(function (element) {

                        //element.appendChild(newElem);
                        element.insertAdjacentElement('beforeend', newElem);
                    });
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }
            /**
             * 
             * APPEND TO ELEMENT (Just inside the element, before its first child)
             * 
             */

        }, {
            key: 'prependElementToElement',
            value: function prependElementToElement() {

                var error = '';

                if (this._appendTo === false) {
                    error += 'Element is required to append new element';
                } else if (this._newElement === false) {
                    error += 'New Element is required';
                }

                if (error === '') {
                    var newElem = this._newElement;
                    var elem = [].slice.call(j2HTML.GetElement({
                        Element: this._appendTo
                    }));

                    elem.map(function (element) {

                        //element.appendChild(newElem);
                        element.insertAdjacentElement('afterbegin', newElem);
                    });
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * APPEND BEFORE
             * 
             */

        }, {
            key: 'appendElementBeforeElement',
            value: function appendElementBeforeElement() {
                var error = '';

                if (this._appendBefore === false) {
                    error += 'Element is required to append before';
                } else if (this._newElement === false) {
                    error += 'New Element is required';
                }

                if (error === '') {
                    var elem = j2HTML.GetElement({
                        Element: this._appendBefore
                    });
                    elem[0].insertAdjacentElement('beforebegin', this._newElement);
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * APPEND AFTER
             * 
             */

        }, {
            key: 'appendElementAfterElement',
            value: function appendElementAfterElement() {
                var error = '';

                if (this._appendAfter === false) {
                    error += 'Element is required to append after';
                } else if (this._newElement === false) {
                    error += 'New Element is required';
                }

                if (error === '') {
                    var elem = j2HTML.GetElement({
                        Element: this._appendAfter
                    });
                    elem[0].insertAdjacentElement('afterend', this._newElement);
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * HIDE ELEMENT
             * 
             */

        }, {
            key: 'hideElement',
            value: function hideElement() {
                var error = '';
                if (this._element === false) {
                    error += 'Element required to hide';
                }
                if (error === '') {
                    var elem = j2HTML.GetElement({
                        Element: this._element
                    });
                    [].slice.call(elem).map(function (el) {

                        el.style.display = 'none';
                    });
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * SHOW ELEMENT
             * 
             */

        }, {
            key: 'showElement',
            value: function showElement() {
                var error = '';
                if (this._element === false) {
                    error += 'Element required to show';
                }
                if (error === '') {
                    var elem = j2HTML.GetElement({
                        Element: this._element
                    });

                    [].slice.call(elem).map(function (el) {

                        el.style.display = '';
                    });
                    //elem[0].style.display = '';
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * EMPTY ELEMENT
             * 
             */

        }, {
            key: 'emptyElement',
            value: function emptyElement() {
                var error = '';
                if (this._element === false) {
                    error += 'Element required to hide';
                }
                if (error === '') {
                    var elem = j2HTML.GetElement({
                        Element: this._element
                    });
                    elem[0].innerHTML = '';
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * REMOVE ELEMENT
             * 
             */

        }, {
            key: 'removeElement',
            value: function removeElement() {
                var error = '';
                if (this._element === false) {
                    error += 'Element required to remove';
                }
                if (error === '') {
                    var elem = j2HTML.GetElement({
                        Element: this._element
                    });
                    elem[0].parentNode.removeChild(elem[0]);
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * CLONE ELEMENT(s)
             * 
             */

        }, {
            key: 'clone',
            value: function clone() {
                var error = '';
                if (this._element === false) {
                    error += 'Element required to hide';
                }
                if (error === '') {
                    var elem = j2HTML.GetElement({
                        Element: this._element
                    });
                    return elem[0].cloneNode(true);
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * TOGGLE ELEMENT(s)
             * 
             */

        }, {
            key: 'toggle',
            value: function toggle() {
                var error = '';
                if (this._element === false) {
                    error += 'Element required to hide';
                }
                if (error === '') {
                    var elem = j2HTML.GetElement({
                        Element: this._element
                    });
                    var elemToToggle = [].slice.call(elem);

                    elemToToggle.map(function (ele) {

                        if (ele.style.display === 'none') {
                            ele.style.display = '';
                        } else {
                            ele.style.display = 'none';
                        }
                    });
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * ADD CLASS
             * 
             */

        }, {
            key: 'addClass',
            value: function addClass() {

                var error = '';
                if (this._element === false) {
                    error += 'Element is required to remove class from\n';
                } else if (this._className === false) {
                    error += 'Class name required to remove\n';
                }

                if (error === '') {
                    var elem = this._element;
                    var cl = this._className;

                    var addElemClass = [].slice.call(j2HTML.GetElement({
                        Element: elem
                    }));

                    addElemClass.map(function (ele) {

                        var arr = ele.className.split(" ");
                        if (arr.indexOf(cl) == -1) {
                            ele.className += " " + cl;
                        }
                    });
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * REMOVE CLASS
             * 
             */

        }, {
            key: 'removeClass',
            value: function removeClass() {

                var error = '';
                if (this._element === false) {
                    error += 'Element is required to remove class from\n';
                } else if (this._className === false) {
                    error += 'Class name required to remove\n';
                }

                if (error === '') {
                    var elem = this._element;
                    var cl = this._className;

                    var removeElemClass = [].slice.call(j2HTML.GetElement({
                        Element: elem
                    }));

                    removeElemClass.map(function (ele) {

                        var regex = new RegExp('\\b' + cl + '\\b', 'g');
                        ele.className = ele.className.replace(regex, "");
                    });
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * IS ELEMENT VISIBLE
             * 
             */

        }, {
            key: 'isElementVisible',
            value: function isElementVisible() {

                var error = '';
                if (this._element === false) {
                    error += 'Element is required for visibility\n';
                }
                if (error === '') {

                    var elem = j2HTML.GetElement({
                        Element: this._element
                    });

                    if (elem[0].offsetWidth === 0 && elem[0].offsetHeight === 0) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * IS ELEMENT CHECKED
             * 
             */

        }, {
            key: 'isElementChecked',
            value: function isElementChecked() {

                var error = '';
                if (this._element === false) {
                    error += 'Element is required for status\n';
                }
                if (error === '') {

                    var elem = j2HTML.GetElement({
                        Element: this._element
                    });

                    if (elem[0].checked) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * Is ELEMENT EXSIT
             * 
             */

        }, {
            key: 'isElementExsit',
            value: function isElementExsit() {
                var error = '';
                if (this._element === false) {
                    error += 'Element is required for status\n';
                }
                if (error === '') {

                    var elem = document.querySelector(this._element);
                    if (elem === null) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * LOAD EXTERNAL HTML
             * 
             */

        }, {
            key: 'loadHTML',
            value: function loadHTML() {

                var error = '';
                if (this._element === false) {
                    error += 'Element is required to load HTML file\n';
                } else if (this._path === false) {
                    error += 'HTML file location required\n';
                }

                if (error === '') {

                    var elmnt = j2HTML.GetElement({
                        Element: this._element
                    });
                    var fn_callback = this._callback;
                    //*make an HTTP request using the attribute value as the file name:*/
                    var xhttp = new XMLHttpRequest();
                    xhttp.open("GET", this._path, false);
                    xhttp.onload = function () {

                        if (this.status === 200) {
                            if (fn_callback !== false) {

                                elmnt[0].insertAdjacentHTML('afterbegin', xhttp.responseText);
                                fn_callback();
                                //callback();
                            } else {
                                elmnt[0].insertAdjacentHTML('afterbegin', xhttp.responseText);
                            }
                        }
                    };

                    xhttp.onerror = function () {

                        alert('Request Error...');
                        throw new Error('Request Error...');
                    };
                    xhttp.send();
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * APPEND HTML STRING (just inside the element, after its last child.)
             * 
             */

        }, {
            key: 'appendHTMLString',
            value: function appendHTMLString() {
                var _this = this;

                var error = '';
                if (this._element === false) {
                    error += 'Element is required to load HTML file\n';
                } else if (this._htmlString === false) {
                    error += 'HTML string required\n';
                }

                if (error === '') {

                    var elmnt = j2HTML.GetElement({
                        Element: this._element
                    });

                    if (this._element.charAt(0) === '#') {
                        elmnt[0].insertAdjacentHTML('afterbegin', this._htmlString);
                    } else if (this._element.charAt(0) === '.') {
                        [].slice.call(elmnt).map(function (elm) {

                            elm.insertAdjacentHTML('afterbegin', _this._htmlString);
                        });
                    } else {
                        elmnt[0].insertAdjacentHTML('beforeend', this._htmlString);
                    }

                    //elmnt[0].insertAdjacentHTML('beforeend', this._htmlString);
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * PREPEND HTML STRING (just inside the element, after its last child.)
             * 
             */

        }, {
            key: 'prependHTMLString',
            value: function prependHTMLString() {

                var error = '';
                if (this._element === false) {
                    error += 'Element is required to load HTML file\n';
                } else if (this._htmlString === false) {
                    error += 'HTML string required\n';
                }

                if (error === '') {

                    var elmnt = j2HTML.GetElement({
                        Element: this._element
                    });
                    elmnt[0].insertAdjacentHTML('afterbegin', this._htmlString);
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * PREPEND HTML STRING ( After the element itself)
             * 
             */

        }, {
            key: 'appendHTMLStringAfter',
            value: function appendHTMLStringAfter() {

                var error = '';
                if (this._element === false) {
                    error += 'Element is required to load HTML file\n';
                } else if (this._htmlString === false) {
                    error += 'HTML string required\n';
                }

                if (error === '') {

                    var elmnt = j2HTML.GetElement({
                        Element: this._element
                    });
                    elmnt[0].insertAdjacentHTML('afterend', this._htmlString);
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * PREPEND HTML STRING (Before the element itself.)
             * 
             */

        }, {
            key: 'appendHTMLStringBefore',
            value: function appendHTMLStringBefore() {

                var error = '';
                if (this._element === false) {
                    error += 'Element is required to load HTML file\n';
                } else if (this._htmlString === false) {
                    error += 'HTML string required\n';
                }

                if (error === '') {

                    var elmnt = j2HTML.GetElement({
                        Element: this._element
                    });
                    elmnt[0].insertAdjacentHTML('beforebegin', this._htmlString);
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * SHOW MODAL
             * 
             */

        }, {
            key: 'showModal',
            value: function showModal() {

                var error = '';
                if (this._modalID === false) {
                    error += 'Modal is required.\n';
                }
                if (error === '') {

                    var elem = j2HTML.GetElement({
                        Element: this._modalID
                    });
                    var showModalInstance = '';
                    if (this._modalOptions === false) {
                        showModalInstance = new Modal(elem[0]);
                    } else {
                        showModalInstance = new Modal(elem[0], this._modalOptions);
                    }
                    showModalInstance.show();
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            /**
             * 
             * HIDE MODAL
             * 
             */

        }, {
            key: 'hideModal',
            value: function hideModal() {

                var error = '';
                if (this._modalID === false) {
                    error += 'Element is required for visibility\n';
                }
                if (error === '') {

                    var modID = this._modalID;
                    var modOptions = this._modalOptions;
                    privateMethods.closeModal(modID, modOptions);

                    // setTimeout(function () {
                    //     privateMethods.closeModal(modID, modOptions);
                    // }, 100);
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }
        }, {
            key: 'getSelectedValueAndText',
            value: function getSelectedValueAndText() {

                var error = '';
                var sl = new selector();
                elementType = this._type.toLowerCase();
                elemt = this._element;

                if (this._element === false) {
                    error += 'Element ID is required';
                }
                if (this._selected !== false && this._selected.On.toLowerCase() === 'click' && (this._selected.ButtonID === undefined || this._selected.ButtonID === null)) {
                    error += 'Button ID is required';
                }

                if (error === '') {

                    if (this._selected.Function !== undefined) {
                        selectedValueFunctionName = this._selected.Function;
                    }

                    if (this._selected !== false) {
                        //CREATE EVENT FOR DROPDOWN
                        if (this._type.toLowerCase() === 'dropdown' && this._selected.On.toLowerCase() === 'change') {

                            var elemnt = j2HTML.GetElement({
                                Element: this._element
                            });
                            elemnt[0].removeEventListener("change", sl.getSelectedValueAndTextOfElement, false);
                            elemnt[0].addEventListener("change", sl.getSelectedValueAndTextOfElement, false);
                        }

                        //CREATE EVENT FOR CHECKBOX
                        if (this._type.toLowerCase() === 'checkbox' && this._selected.On.toLowerCase() === 'change') {

                            var name = elemt.slice(1);
                            var chks = document.querySelectorAll('[name="chk' + name + '"]');

                            [].slice.call(chks).map(function (checkbox) {

                                //REMOVE PREVIOUSLY ADDED ON-CHANGE EVENT LISTENER
                                checkbox.removeEventListener("change", sl.getSelectedValueAndTextOfElement, false);

                                //ADD ON-CHANGE EVENT LISTENER
                                checkbox.addEventListener("change", sl.getSelectedValueAndTextOfElement, false);
                            });
                        }

                        //CREATE EVENT FOR RADIO BUTTON
                        if (this._type.toLowerCase() === 'radio' && this._selected.On.toLowerCase() === 'change') {

                            var _name = elemt.slice(1);
                            var rdbs = document.querySelectorAll('[name="rdb' + _name + '"]');

                            [].slice.call(rdbs).map(function (radio) {

                                //REMOVE PREVIOUSLY ADDED ON-CHANGE EVENT LISTENER
                                radio.removeEventListener("change", sl.getSelectedValueAndTextOfElement, false);

                                //ADD ON-CHANGE EVENT LISTENER
                                radio.addEventListener("change", sl.getSelectedValueAndTextOfElement, false);
                            });
                        }

                        //CREATE EVENT FOR ON CLICK
                        if ((this._type.toLowerCase() === 'dropdown' || this._type.toLowerCase() === 'checkbox' || this._type.toLowerCase() === 'radio') && this._selected.On.toLowerCase() === 'click') {

                            //REMOVE CHANGE EVENT
                            var _elemnt = j2HTML.GetElement({
                                Element: this._element
                            });
                            _elemnt[0].removeEventListener("change", sl.getSelectedValueAndTextOfElement, false);

                            var btn = j2HTML.GetElement({
                                Element: this._selected.ButtonID
                            });
                            //REMOVE PREVIOUSLY ADD CLICK EVENT
                            btn[0].removeEventListener("click", sl.getSelectedValueAndTextOfElement, false);

                            //ADD CLICK EVENT
                            btn[0].addEventListener("click", sl.getSelectedValueAndTextOfElement, false);
                        }
                    } else {

                        //this._returnSelectedValueAndText;
                        this._returnSelectedValueAndText = sl.getSelectedValueAndTextOfElement();
                    }
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }
        }, {
            key: 'getSelectedValueAndTextOfElement',
            value: function getSelectedValueAndTextOfElement() {

                if (elementType === 'dropdown') {
                    var ddl = j2HTML.GetElement({
                        Element: elemt
                    });
                    var selectedValue = ddl[0].value;
                    var selectedText = ddl[0].options[ddl[0].selectedIndex].text;

                    if (selectedValueFunctionName !== '') {
                        selectedValueFunctionName(selectedText, selectedValue);
                    } else {

                        var obj = {
                            Value: selectedValue,
                            Text: selectedText
                        };

                        return obj;
                    }
                } else if (elementType === 'checkbox') {

                    var name = elemt.slice(1);

                    var _selectedValue = [];
                    var _selectedText = [];

                    var chks = document.querySelectorAll('[name="chk' + name + '"]');

                    [].slice.call(chks).map(function (chk) {

                        if (chk.checked) {
                            _selectedValue.push(chk.value);
                            _selectedText.push(chk.getAttribute('text'));
                        }
                    });

                    if (selectedValueFunctionName !== '') {
                        selectedValueFunctionName(_selectedText, _selectedValue);
                    } else {
                        var _obj = {
                            Value: _selectedValue,
                            Text: _selectedText
                        };
                        return _obj;
                    }
                } else if (elementType === 'radio') {

                    var _name2 = elemt.slice(1);

                    var _selectedValue2 = [];
                    var _selectedText2 = [];

                    var rdbs = document.querySelectorAll('[name="rdb' + _name2 + '"]');

                    [].slice.call(rdbs).map(function (rdb) {

                        if (rdb.checked) {
                            _selectedValue2.push(rdb.value);
                            _selectedText2.push(rdb.getAttribute('text'));
                        }
                    });
                    if (selectedValueFunctionName !== '') {
                        selectedValueFunctionName(_selectedText2, _selectedValue2);
                    } else {
                        var _obj2 = {
                            Value: _selectedValue2,
                            Text: _selectedText2
                        };
                        return _obj2;
                    }

                    //selectedValueFunctionName(selectedText, selectedValue);
                } else if (elementType === 'datalist') {

                    //let name = elemt.slice(1);


                    var val = document.querySelectorAll(elemt + ' input')[0].value;
                    var opts = document.querySelectorAll(elemt + ' datalist')[0].childNodes;
                    for (var i = 0; i < opts.length; i++) {
                        if (opts[i].value === val) {

                            if (selectedValueFunctionName !== '') {
                                selectedValueFunctionName(opts[i].innerText, opts[i].value, JSON.stringify(dataListData));
                            } else {
                                var _obj3 = {};
                                _obj3.Value = opts[i].value;
                                _obj3.Text = opts[i].innerText;
                                return _obj3;
                            }
                            break;
                        }
                    }
                }
            }
        }, {
            key: 'createElement',
            value: function createElement() {

                var error = '';

                if (this._element === false) {
                    error += 'Element is required';
                }

                if (error === '') {
                    var elem = document.createElement(this._element);

                    if (this._text !== false) {
                        var txt = document.createTextNode(this._text);
                        elem.appendChild(txt);
                    }

                    //ADD ELEMENT ID
                    if (this._id !== false) {
                        elem.id = this._id;
                    }
                    //ADD ELEMENT CLASS
                    if (this._className !== false) {
                        elem.className = this._className;
                    }

                    //APPEND ELEMENT TO
                    if (this._appendTo !== false) {
                        appendTo = this._appendTo;
                        var _elemAppendTo = j2HTML.GetElement({
                            Element: appendTo
                        });
                        _elemAppendTo[0].appendChild(elem);
                    } else {
                        elemAppendTo = j2HTML.GetElement({
                            Element: 'body'
                        });
                        elemAppendTo[0].appendChild(elem);
                    }

                    return elem;
                }
            }
        }, {
            key: 'documentReady',
            value: function documentReady() {}

            //endregion


        }, {
            key: 'element',
            get: function get() {
                return this._element;
            },
            set: function set(value) {
                this._element = value;
            }
        }, {
            key: 'name',
            get: function get() {
                return this._name;
            },
            set: function set(value) {
                this._name = value;
            }
        }, {
            key: 'appendTo',
            get: function get() {
                return this._appendTo;
            },
            set: function set(value) {
                this._appendTo = value;
            }
        }, {
            key: 'newElement',
            get: function get() {
                return this._newElement;
            },
            set: function set(value) {
                this._newElement = value;
            }
        }, {
            key: 'appendBefore',
            get: function get() {
                return this._appendBefore;
            },
            set: function set(value) {
                this._appendBefore = value;
            }
        }, {
            key: 'appendAfter',
            get: function get() {
                return this._appendAfter;
            },
            set: function set(value) {
                this._appendAfter = value;
            }
        }, {
            key: 'className',
            get: function get() {
                return this._className;
            },
            set: function set(value) {
                this._className = value;
            }
        }, {
            key: 'ID',
            get: function get() {
                return this._id;
            },
            set: function set(value) {
                this._id = value;
            }
        }, {
            key: 'text',
            get: function get() {
                return this._text;
            },
            set: function set(value) {
                this._text = value;
            }
        }, {
            key: 'path',
            get: function get() {
                return this._path;
            },
            set: function set(value) {
                this._path = value;
            }
        }, {
            key: 'callback',
            get: function get() {
                return this._callback;
            },
            set: function set(value) {
                this._callback = value;
            }
        }, {
            key: 'modalID',
            get: function get() {
                return this._modalID;
            },
            set: function set(value) {
                this._modalID = value;
            }
        }, {
            key: 'modalOptions',
            get: function get() {
                return this._modalOptions;
            },
            set: function set(value) {
                this._modalOptions = value;
            }
        }, {
            key: 'HTMLString',
            get: function get() {
                return this._htmlString;
            },
            set: function set(value) {
                this._htmlString = value;
            }
        }, {
            key: 'elementValue',
            get: function get() {
                return this._elementValue;
            },
            set: function set(value) {
                this._elementValue = value;
            }

            //SELECTOR

        }, {
            key: 'sel',
            get: function get() {
                return this._sel;
            },
            set: function set(value) {
                this._sel = value;
            }

            //SELECTOR

        }, {
            key: 'type',
            get: function get() {
                return this._type;
            },
            set: function set(value) {
                this._type = value;
            }
        }, {
            key: 'selected',
            get: function get() {
                return this._selected;
            },
            set: function set(value) {
                this._selected = value;
            }
        }, {
            key: 'returnSelectedValueAndText',
            get: function get() {
                return this._returnSelectedValueAndText;
            },
            set: function set(value) {
                this._returnSelectedValueAndText = value;
            }
        }]);

        return selector;
    }();

    var appendTo = '';
    var elementType = '';
    var elemt = '';
    var selectedValueFunctionName = '';
    var returnValueAndText = void 0;
    var privateMethods = {
        closeModal: function closeModal(modalID, modalOptions) {
            var elem = j2HTML.GetElement({
                Element: modalID
            });
            var hideModalInstance = '';
            if (modalOptions === false) {
                hideModalInstance = new Modal(elem[0]);
            } else {
                hideModalInstance = new Modal(elem[0], modalOptions);
            }

            hideModalInstance.hide();
        }
    };

    return selector;
}(); //END OF CLASS


//CREATE EVENT
var createEvent = function (e) {

    var filter = function filter(elem, type, fn) {
        var _loop = function _loop(_i) {

            var el = j2HTML.GetElement({
                Element: elem.split(',')[_i]
            });

            [].slice.call(el).map(function (elemt) {

                createEvent(elemt[_i], type, fn);
            });
        };

        for (var _i = 0; _i < elem.split(',').length; _i++) {
            _loop(_i);
        }
    };
    if (document.addEventListener) {

        return function (elem, type, fn) {

            for (var _i2 = 0; _i2 < elem.split(',').length; _i2++) {

                var el = j2HTML.GetElement({
                    Element: elem.split(',')[_i2]
                });
                [].slice.call(el).map(function (elemt) {
                    if (elemt && elemt.nodeName || elemt === window) {

                        elemt.removeEventListener(type, fn, true);
                        elemt.addEventListener(type, fn, true);
                    } else if (elemt && elemt.length) {
                        filter(elemt, type, fn);
                    }
                });
            }
        };
    }

    return function (elem, type, fn) {
        var _loop2 = function _loop2(_i3) {

            var el = j2HTML.GetElement({
                Element: elem.split(',')[_i3]
            })[0];
            if (el && el.nodeName || el === window) {
                el.attachEvent('on' + type, function () {
                    return fn.call(el, window.event);
                });
            } else if (el && el.length) {
                filter(el, type, fn);
            }
        };

        for (var _i3 = 0; _i3 < elem.split(',').length; _i3++) {
            _loop2(_i3);
        }
    };
}();

var documentReady = function documentReady(e) {

    return j2HTML.CreateEvent({ Element: 'document', Type: 'DOMContentLoaded' }, arguments[0]);

    // //var run = function () {}


    // //if (document.addEventListener) {
    // return function (fun) {
    //     if (document.readyState != 'loading') {
    //         fun();
    //     }
    //     // modern browsers
    //     else if (document.addEventListener) {
    //         document.addEventListener('DOMContentLoaded', run);
    //     }
    //     // IE <= 8
    //     else document.attachEvent('onreadystatechange', function () {
    //         if (document.readyState == 'complete') {
    //             fun();
    //         }
    //     });
    //}
    //}
};

//#endregion
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var table = function () {

    //CREAT CLASS
    var table = function () {
        function table() {
            _classCallCheck(this, table);

            this._appendTo = false;

            this._data = false;
            this._tableID = '#tbJsonToHtml';
            this._defaultHeader = true;
            this._customHeader = false;
            this._addToColumn = false;
            this._tableSort = true;

            this._createButtonAppendTo = false;
            this._funCreate = false;

            this._createRow = false;

            this._funUpdate = false;
            this._funDelete = false;

            this._selectRows = false;

            this._selectedrowBGColor = false;
            this._selectedRowforeColor = false;

            this._tableHeadingBackgroundColor = false;
            this._tableHeadingForeColor = false;

            this._tableBackgroundColor = false;
            this._tableForeColor = false;

            this._searchTextBox = false;
            this._searchInColumns = false;

            this._textLimit = false;

            //FOR PRINT
            this._printButtonID = false;
            this._fileName = false;
            this._printType = false; //THERE ARE TWO TYPES 1.CVS, 2.PDF

            //IF TABLE IS IN MODAL THAN NEED MODAL ID TO SHOW TEXT LIMIT DETAIL IN MODAL
            this._modalID = false;
            this._popupContinueText = false;
        }

        //#region ************** PROPERTIES ******************

        _createClass(table, [{
            key: 'getTable',


            //endregion

            //#region ************** PUBLIC FUNCTIONS ************

            /**
             * 
             * GET TABLE
             * 
             */
            value: function getTable() {

                if (this._data === false) {
                    error += 'Data is required in order to create table';
                }

                if (error === '') {

                    if (this._textLimit !== false) {
                        limitedText = this._textLimit;
                    }

                    if (this._modalID !== false) {
                        modID = this._modalID;
                    }
                    if (this._popupContinueText !== false) {
                        popupText = this._popupContinueText;
                    }

                    //***************************************************
                    //****************** CREATE TABLE  ******************
                    //***************************************************
                    var j2HTMLTableID = this._tableID.slice(1, this._tableID.length);

                    tb = '<table id="' + j2HTMLTableID + '" class="table table-striped table-hover">';
                    privateMethods.j2HTMLCreateTableHeader(this._data, this._tableID, this._tableSort, this._defaultHeader, this._customHeader, this._funUpdate, this._funDelete, this._selectRows);

                    if (this._defaultHeader === true && this._customHeader !== false) {
                        alert("hasDefaultHeader and custom header both cannot be present at same time");
                    } else {

                        privateMethods.j2HTMLCreateTableRow(j2HTMLTableID, this._data, this._addToColumn, this._customHeader, this._funUpdate, this._funDelete, this._selectRows);

                        // ************  GET ROW DATA **********
                        if (this._selectRows !== false) {
                            var btnText = '';

                            if (this.selectRows.GetSelectedRowDataButtonText === undefined) {
                                btnText = 'Submit';
                            } else {
                                btnText = this.selectRows.GetSelectedRowDataButtonText;
                            }

                            if (this._selectRows.SelectRow.toLowerCase() !== 'single' && this._selectRows["GetSelectedRowDataButtonAppendTo"] === undefined && this._selectRows["GetSelectedRowsFunction"] !== undefined) {

                                tb += '<button id=\'j2HTMLBtnMultipleSelectedRowData\' class=\'btn btn-sm btn-primary pull-right\' style=\'margin-bottom:5px;\'>' + btnText + '</button></br></br>';
                            } else if (this._selectRows["SelectRow"].toLowerCase() !== 'single' && this._selectRows["GetSelectedRowDataButtonAppendTo"] !== undefined && this._selectRows["GetSelectedRowsFunction"] !== undefined) {
                                var strHTML = '<button id=\'j2HTMLBtnMultipleSelectedRowData\' class=\'btn btn-sm btn-primary pull-right\' style=\'margin-bottom:5px;\'>' + btnText + '</button></br></br>';
                                j2HTML.AppendHTMLString({
                                    Element: this._selectRows["GetSelectedRowDataButtonAppendTo"],
                                    HTMLString: strHTML
                                });
                            }

                            if (this._selectRows["SelectedRowBackgroundColor"] !== undefined) {
                                selectedRowBackgroundColor = this._selectRows["SelectedRowBackgroundColor"];
                            } else {
                                selectedRowBackgroundColor = 'rgb(40, 167,69)';
                            }

                            if (this._selectRows["SelectedRowForeColor"] !== undefined) {
                                selectedRowForecolor = this._selectRows["SelectedRowForeColor"];
                            } else {
                                selectedRowForecolor = 'white';
                            }
                        }

                        if (this._appendTo !== false) {

                            j2HTML.EmptyElement({
                                Element: this._appendTo
                            });

                            j2HTML.AppendHTMLString({
                                Element: this._appendTo,
                                HTMLString: tb
                            });
                        } else {

                            if (j2HTML.IsElementExsit({
                                Element: '#divJson2HTMLTable'
                            })) {
                                j2HTML.RemoveElement({
                                    Element: '#divJson2HTMLTable'
                                });
                                j2HTML.AppendHTMLString({
                                    Element: 'body',
                                    HTMLString: '<div id="divJson2HTMLTable" style="overflow-x:auto;"></div>'
                                });
                                j2HTML.AppendHTMLString({
                                    Element: '#divJson2HTMLTable',
                                    HTMLString: tb
                                });
                            } else {
                                j2HTML.AppendHTMLString({
                                    Element: 'body',
                                    HTMLString: '<div id="divJson2HTMLTable" style="overflow-x:auto;"></div>'
                                });
                                j2HTML.AppendHTMLString({
                                    Element: '#divJson2HTMLTable',
                                    HTMLString: tb
                                });
                            }
                        }

                        //ADD EVENT FOR GET SELECTED ROW DATA
                        selectRowsForModal = this._selectRows;
                        var t = new table();
                        t.addeventForSelectedRows(this._selectRows);

                        if (j2HTML.IsElementExsit({
                            Element: '.textLimit'
                        })) {

                            j2HTML.CreateEvent({
                                Element: '.textLimit',
                                Type: 'click'
                            }, privateMethods.showJ2HTMLTableDetailText);
                        }

                        //ADD UPDATE FUNCTION EVENT
                        if (this._funUpdate !== false) {

                            j2HTML.CreateEvent({
                                Element: '.j2HTMLUpdateSelectedRow',
                                Type: 'click'
                            }, privateMethods.j2HTMLUpdateTableRow);
                            updateRowFunction = this._funUpdate;
                        }

                        //ADD DELETE FUNCTION EVENT
                        if (this._funDelete !== false) {
                            j2HTML.CreateEvent({
                                Element: '.j2HTMLDeleteSelectedRow',
                                Type: 'click'
                            }, privateMethods.j2HTMLDeleteTableRow);
                            deleteRowFunction = this._funDelete;
                        }

                        //ADD CREATE NEW ROW FUNCTION EVENT  this._createRow
                        if (this._createRow !== false) {

                            var tableID = this._tableID;
                            //funCreate = this._funCreate;
                            funCreate = this._createRow.Function;

                            var btnNewCreateRow = j2HTML.GetElement({
                                Element: this._createRow.ButtonID
                            });
                            tblID = tableID;
                            createNewRowFun = this._createRow.Function;
                            btnNewCreateRow[0].removeEventListener('click', privateMethods.j2HTMLCreateNewTableRow, false);
                            btnNewCreateRow[0].addEventListener('click', privateMethods.j2HTMLCreateNewTableRow, false);
                        }
                    }
                } else {
                    throw new Error(error);
                }
            }
            /**
             * 
             * SET TABLE HEADING COLOR
             * 
             */

        }, {
            key: 'j2HTMLTableHeadingStyle',
            value: function j2HTMLTableHeadingStyle() {

                var tb = j2HTML.GetElement({
                    Element: this._tableID + ' thead tr'
                });
                if (this._tableHeadingBackgroundColor !== false) {
                    tb[0].style.backgroundColor = this._tableHeadingBackgroundColor;
                }
                if (this._tableHeadingForeColor !== false) {
                    tb[0].style.color = this._tableHeadingForeColor;
                }
            }

            /**
             * 
             * SET TABLE BODY COLOR
             * 
             */

        }, {
            key: 'j2HTMLTableBodyStyle',
            value: function j2HTMLTableBodyStyle() {
                var tb = j2HTML.GetElement({
                    Element: this._tableID + ' tbody tr'
                });

                for (var i = 0; i < tb.length; i++) {
                    if (this._tableBackgroundColor !== false) {
                        tb[i].style.backgroundColor = this._tableBackgroundColor;
                    }
                    if (this._tableForeColor !== false) {
                        tb[i].style.color = this._tableForeColor;
                    }
                }
            }

            /**
             * 
             * SEARCH IN WHOLE TABLE
             * 
             */

        }, {
            key: 'j2HTMLSearchWithinWholeTable',
            value: function j2HTMLSearchWithinWholeTable() {
                var _this = this;

                if (this._tableID === false) {
                    error += 'Table Id reqquired\n';
                } else if (this._searchTextBox === false) {
                    error += 'Search text box Id reqquired\n';
                }
                if (error === '') {
                    j2HTML.CreateEvent({
                        Element: this._searchTextBox,
                        Type: 'keyup'
                    }, function () {

                        var input, filter, table, tr, td, i;
                        input = j2HTML.GetElement({
                            Element: _this._searchTextBox
                        })[0];
                        filter = input.value.toUpperCase();
                        table = j2HTML.GetElement({
                            Element: _this._tableID
                        })[0];
                        tr = j2HTML.GetElement({
                            Element: _this._tableID + ' tbody tr'
                        });

                        for (i = 0; i < tr.length; i++) {

                            td = tr[i].getElementsByTagName("td");

                            if (td.length !== 0) {

                                for (j = 0; j < td.length; j++) {

                                    if (td[j].innerHTML.toUpperCase().indexOf(filter) > -1) {
                                        tr[i].style.display = "";
                                        break;
                                    } else {
                                        tr[i].style.display = "none";
                                    }
                                }
                            }
                        }
                    });
                } else {
                    throw new Error(error);
                }
            }

            /** 
             * 
             * SEARCH WITHIN SPECIFIC COLUMNS
             * 
             * */

        }, {
            key: 'j2HTMLSearchWithinSpeificColumns',
            value: function j2HTMLSearchWithinSpeificColumns() {
                var _this2 = this;

                if (this._tableID === false) {
                    error += 'Table Id reqquired\n';
                } else if (this._searchTextBox === false) {
                    error += 'Search text box Id reqquired\n';
                }
                if (error === '') {
                    j2HTML.CreateEvent({
                        Element: this._searchTextBox,
                        Type: 'keyup'
                    }, function () {

                        var input, filter, table, tr, td, i;
                        input = j2HTML.GetElement({
                            Element: _this2._searchTextBox
                        })[0];
                        filter = input.value.toUpperCase();
                        table = j2HTML.GetElement({
                            Element: _this2._tableID
                        })[0];
                        tr = j2HTML.GetElement({
                            Element: _this2._tableID + ' tbody tr'
                        });

                        for (i = 0; i < tr.length; i++) {

                            td = tr[i].getElementsByTagName("td");

                            if (td.length !== 0) {

                                for (j = 0; j < td.length; j++) {

                                    if (_this2._searchInColumns.includes(td[j].getAttribute('data-label'))) {

                                        if (td[j].innerHTML.toUpperCase().indexOf(filter) > -1) {
                                            tr[i].style.display = "";
                                            break;
                                        } else {
                                            tr[i].style.display = "none";
                                        }
                                    }
                                }
                            }
                        }
                    });
                } else {
                    throw new Error(error);
                }
            }

            /**
             * 
             * PRINT HTML TABLE
             * 
             */

        }, {
            key: 'j2HTMLPrint',
            value: function j2HTMLPrint() {

                var printTableID = this._tableID.slice(1, this._tableID.length);

                if (this._printButtonID !== false) {

                    var elemt = j2HTML.GetElement({
                        Element: this._printButtonID
                    });

                    // PRINT CSV
                    if (this._printType.toLowerCase() === 'csv') {

                        tblID = this._tableID;
                        pntFileName = this._fileName;
                        elemt[0].removeEventListener('click', privateMethods.toCSV, false);
                        elemt[0].addEventListener('click', privateMethods.toCSV, false);
                    }

                    // PRINT PDF
                    // if (this._printType.toLowerCase() === 'pdf') {

                    // tblID = this._tableID;
                    // pntFileName = this._fileName;
                    // elemt[0].removeEventListener('click',privateMethods.toPDF, false);
                    // elemt[0].addEventListener('click',privateMethods.toPDF, false);

                    // }
                } else {
                    throw new Error("Print button Id is required");
                }
            }
        }, {
            key: 'addeventForSelectedRows',
            value: function addeventForSelectedRows(selectedRows) {

                //ADD EVENT FOR GET SELECTED ROW DATA
                if (selectedRows !== false) {

                    selectRow = selectedRows.SelectRow;

                    if (selectRow.toLowerCase() === 'single') {

                        if (selectedRows.GetSelectedRowsFunction !== undefined) {
                            selectedRowsFunction = selectedRows.GetSelectedRowsFunction;
                        }

                        j2HTML.CreateEvent({
                            Element: '.getSelectedRow',
                            Type: 'click'
                        }, privateMethods.j2HTMLHighlight_And_GetSelectedSingleRowData);

                        type = selectedRows.Type;
                    } else {
                        if (selectedRows.GetSelectedRowsFunction !== undefined) {

                            selectedRowsFunction = selectedRows.GetSelectedRowsFunction;

                            j2HTML.CreateEvent({
                                Element: '.getSelectedRow',
                                Type: 'click'
                            }, privateMethods.j2HTMLHighlightMultipleRows);

                            j2HTML.CreateEvent({
                                Element: '#j2HTMLBtnMultipleSelectedRowData',
                                Type: 'click'
                            }, privateMethods.j2HTMLGetSelectedMultipleRowsData);
                        } else {

                            j2HTML.CreateEvent({
                                Element: '.getSelectedRow',
                                Type: 'click'
                            }, privateMethods.j2HTMLHighlightMultipleRows);
                        }
                        type = selectedRows.Type;
                    }
                }
            }

            //endregion


        }, {
            key: 'appendTo',
            get: function get() {
                return this._appendTo;
            },
            set: function set(value) {
                this._appendTo = value;
            }
        }, {
            key: 'data',
            get: function get() {
                return this._data;
            },
            set: function set(value) {
                this._data = value;
            }
        }, {
            key: 'tableID',
            get: function get() {
                return this._tableID;
            },
            set: function set(value) {
                this._tableID = value;
            }
        }, {
            key: 'defaultHeader',
            get: function get() {
                return this._defaultHeader;
            },
            set: function set(value) {
                this._defaultHeader = value;
            }
        }, {
            key: 'customHeader',
            get: function get() {
                return this._customHeader;
            },
            set: function set(value) {
                this._customHeader = value;
            }
        }, {
            key: 'addToColumn',
            get: function get() {
                return this._addToColumn;
            },
            set: function set(value) {
                this._addToColumn = value;
            }
        }, {
            key: 'tableSort',
            get: function get() {
                return this._tableSort;
            },
            set: function set(value) {
                this._tableSort = value;
            }

            // get selectRows() { return this._selectRows; }
            // set selectRows(value) { this._selectRows = value; }

        }, {
            key: 'selectedrowBGColor',
            get: function get() {
                return this._selectedrowBGColor;
            },
            set: function set(value) {
                this._selectedrowBGColor = value;
            }
        }, {
            key: 'selectedRowforeColor',
            get: function get() {
                return this._selectedRowforeColor;
            },
            set: function set(value) {
                this._selectedRowforeColor = value;
            }
        }, {
            key: 'createRow',
            get: function get() {
                return this._createRow;
            },
            set: function set(value) {
                this._createRow = value;
            }
        }, {
            key: 'createButtonAppendTo',
            get: function get() {
                return this._createButtonAppendTo;
            },
            set: function set(value) {
                this._createButtonAppendTo = value;
            }
        }, {
            key: 'createFunction',
            get: function get() {
                return this._funCreate;
            },
            set: function set(value) {
                this._funCreate = value;
            }
        }, {
            key: 'updateFun',
            get: function get() {
                return this._funUpdate;
            },
            set: function set(value) {
                this._funUpdate = value;
            }
        }, {
            key: 'deleteFunction',
            get: function get() {
                return this._funDelete;
            },
            set: function set(value) {
                this._funDelete = value;
            }
        }, {
            key: 'selectRows',
            get: function get() {
                return this._selectRows;
            },
            set: function set(value) {
                this._selectRows = value;
            }
        }, {
            key: 'tableHeadingBackgroundColor',
            get: function get() {
                return this._tableHeadingBackgroundColor;
            },
            set: function set(value) {
                this._tableHeadingBackgroundColor = value;
            }
        }, {
            key: 'tableHeadingForeColor',
            get: function get() {
                return this._tableHeadingForeColor;
            },
            set: function set(value) {
                this._tableHeadingForeColor = value;
            }
        }, {
            key: 'tableBackgroundColor',
            get: function get() {
                return this._tableBackgroundColor;
            },
            set: function set(value) {
                this._tableBackgroundColor = value;
            }
        }, {
            key: 'tableForeColor',
            get: function get() {
                return this._tableForeColor;
            },
            set: function set(value) {
                this._tableForeColor = value;
            }
        }, {
            key: 'searchTextBox',
            get: function get() {
                return this._searchTextBox;
            },
            set: function set(value) {
                this._searchTextBox = value;
            }
        }, {
            key: 'searchInColumns',
            get: function get() {
                return this._searchInColumns;
            },
            set: function set(value) {
                this._searchInColumns = value;
            }
        }, {
            key: 'textLimit',
            get: function get() {
                return this._textLimit;
            },
            set: function set(value) {
                this._textLimit = value;
            }
        }, {
            key: 'fileName',
            get: function get() {
                return this._fileName;
            },
            set: function set(value) {
                this._fileName = value;
            }
        }, {
            key: 'printType',
            get: function get() {
                return this._printType;
            },
            set: function set(value) {
                this._printType = value;
            }
        }, {
            key: 'printButtonID',
            get: function get() {
                return this._printButtonID;
            },
            set: function set(value) {
                this._printButtonID = value;
            }
        }, {
            key: 'modalID',
            get: function get() {
                return this._modalID;
            },
            set: function set(value) {
                this._modalID = value;
            }
        }, {
            key: 'popupContinueText',
            get: function get() {
                return this._popupContinueText;
            },
            set: function set(value) {
                this._popupContinueText = value;
            }
        }]);

        return table;
    }();

    //#region ************** PRIVATE VARIABLE AND METHOD *******

    var error = '';
    var tb = '';
    var selectedRowsFunction = false;
    var type = '';
    var selectRow = '';
    var selectedRowBackgroundColor = void 0;
    var selectedRowForecolor = void 0;
    var updateRowFunction = false;
    var deleteRowFunction = false;
    var tblID = false;
    var pntFileName = false;
    var limitedText = void 0;

    var createNewRowFun = false;
    var selectRowsForModal = '';

    //WHEN TABLE IS IN MODAL
    var modID = false;
    var popupText = false;
    //let custColumns = false;

    //PRIVATE FUNCTIONS
    var privateMethods = {

        /**
         * CREATE TABLE HEADER
         * @param {JSON DATA} data 
         * @param {TABLE ID} tableID 
         * @param {SORT TABLE} tableSort 
         * @param {DEFAULT HEADER} hasDefaultHeader 
         * @param {CUSTOM HEADER} customHeader 
         * @param {UPDATE FUNCTION} funUpdate 
         * @param {DELETE FUNCTION} funDelete 
         */
        j2HTMLCreateTableHeader: function j2HTMLCreateTableHeader(data, tableID, tableSort, hasDefaultHeader, customHeader, funUpdate, funDelete, selectRows) {

            //***************** CREATE DEFAULT HEADER ***************
            if (hasDefaultHeader === true && customHeader === false) {

                tb += '<thead><tr>';

                if (data.length > 0) {

                    Object.keys(data[0]).map(function (key, index) {

                        if (tableSort === true) {

                            var childIndex = parseInt(index) + 1;
                            var parm = 'j2HTMLSort(\'' + tableID + '\',\'.item\',\'td:nth-child(' + childIndex + ')\')';
                            tb += '<th onclick="' + parm + '" style="cursor:pointer">' + toTitleCase(key) + '</th>';
                        } else {
                            tb += '<th>' + toTitleCase(key) + '</th>';
                        }

                        //return toTitleCase(key);
                    });
                    //UPDATE AND DELETE
                    if (funUpdate !== false && funDelete !== false) {
                        tb += "<th></th>";
                        tb += "<th></th>";
                    }

                    // DELETE ONLY
                    if (funUpdate === false && funDelete !== false) {
                        tb += "<th></th>";
                    }

                    //UPDATE ONLY
                    if (funUpdate !== false && funDelete === false) {
                        tb += "<th></th>";
                    }

                    //SELECT ROWS
                    if (selectRows !== false) {
                        tb += "<th></th>";
                    }

                    tb += '</tr></thead>';
                }
            }

            //****************** CREATE CUSTOMER HEADER *****************
            if (hasDefaultHeader === false && customHeader !== false) {

                if (data.length > 0 && customHeader.length > 0) {

                    tb += '<thead><tr>';

                    for (var i = 0; i < customHeader.length; i++) {

                        var orginalPropertyName = customHeader[i]['orginalColumnName'];
                        var newPropertyName = customHeader[i]['newColumnName'];

                        var customColumnName = customHeader[i]['customColumnName'];
                        var customColumnValue = customHeader[i]['customColumnValue'];

                        var childIndex = parseInt(i) + 1;

                        var parm = 'j2HTMLSort(\'' + tableID + '\',\'.item\',\'td:nth-child(' + childIndex + ')\')';

                        if (newPropertyName === '' || newPropertyName === undefined) {

                            if (customHeader[i]['Visible'] !== false) {
                                if (orginalPropertyName !== undefined) {
                                    tb += '<th onclick="' + parm + '" style="cursor:pointer">' + toTitleCase(orginalPropertyName) + '</th>';
                                }
                            } else {

                                if (orginalPropertyName !== undefined) {
                                    tb += '<th onclick="' + parm + '" style="cursor:pointer; display:none">' + toTitleCase(orginalPropertyName) + '</th>';
                                }
                            }
                        } else {

                            if (customHeader[i]['Visible'] !== false) {
                                if (newPropertyName !== undefined) {
                                    tb += '<th onclick="' + parm + '" style="cursor:pointer">' + toTitleCase(newPropertyName) + '</th>';
                                }
                            } else {
                                if (newPropertyName !== undefined) {
                                    tb += '<th onclick="' + parm + '" style="cursor:pointer; display:none">' + toTitleCase(newPropertyName) + '</th>';
                                }
                            }
                        }

                        //ADD CUSTOM COLUMN HEADER
                        if (customColumnName !== undefined) {
                            tb += '<th>' + toTitleCase(customColumnName) + '</th>';
                        }
                    }
                    //UPDATE AND DELETE
                    if (funUpdate !== false && funDelete !== false) {
                        tb += "<th></th>";
                        tb += "<th></th>";
                    }

                    // DELETE ONLY
                    if (funUpdate === false && funDelete !== false) {
                        tb += "<th></th>";
                    }

                    //UPDATE ONLY
                    if (funUpdate !== false && funDelete === false) {
                        tb += "<th></th>";
                    }
                    //SELECT ROWS
                    if (selectRows !== false) {
                        tb += "<th></th>";
                    }

                    tb += '</tr></thead>';
                }
            }

            //return headerRow;
        },

        /**
         * CRERATE TABLE ROW
         * @param {JSON DATA} data 
         * @param {APPEND OT PREPND VALUES} addToColumn 
         * @param {CUSTOM HEADER} customHeader 
         * @param {UPDATE FUNCTION} funUpdate 
         * @param {DELETE FUNCTION} funDelete 
         */
        j2HTMLCreateTableRow: function j2HTMLCreateTableRow(tableID, data, addToColumn, customHeader, funUpdate, funDelete, selectRows) {

            tb += '<tbody>';
            data.map(function (rowData, rowIndex) {

                var rowId = 'tr' + rowIndex;
                tb += '<tr id="' + rowId + '" class="item" align="left">';
                var calCount = void 0;

                //DEFAULT ROW
                if (customHeader === false) {

                    Object.keys(rowData).map(function (colName, colIndex) {

                        calCount = colIndex;
                        var colId = 'td_' + rowIndex + '_' + colIndex;
                        var currentValue = rowData[colName];

                        //ADD APPEND OR PREPEND
                        if (addToColumn !== false) {
                            currentValue = privateMethods.AppendOrPrependToColumn(addToColumn, colName, currentValue);
                        }
                        if (typeof currentValue === 'string') {

                            var reISO = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*))(?:Z|(\+|-)([\d|:]*))?$/;
                            var a = reISO.exec(currentValue);
                            if (a) {

                                currentValue = currentValue.split('T')[0];

                                //let current_datetime = new Date(currentValue);
                                //currentValue = current_datetime.getDate() + "/" + (current_datetime.getMonth() + 1) + "/" + current_datetime.getFullYear()

                                //currentValue = new Date(currentValue).toLocaleDateString();
                            }
                        }
                        if (currentValue === null) {
                            currentValue = '';
                        }

                        if (limitedText !== undefined) {

                            var _limitedTextCol = [];

                            for (var i = 0; i < limitedText.length; i++) {

                                _limitedTextCol.push(limitedText[i].Column);
                            }

                            for (var i = 0; i < limitedText.length; i++) {

                                if (limitedText[i].Column !== undefined && limitedText[i].Column === colName && currentValue.length > limitedText[i].Limit) {
                                    var _txtLimited = currentValue.slice(0, limitedText[i].Limit) + ('...<a id="lDetailText' + colId + '" href="#" class="textLimit" fulltext="' + currentValue + '" columanName="' + colName + '">more</a>');
                                    tb += '<td data-label="' + toTitleCase(colName) + '" id="' + colId + '" showFullText="' + currentValue + '">' + _txtLimited + '</td>';

                                    _limitedTextCol = limitedText[i].Column;
                                } else if (limitedText[i].Column === undefined && currentValue.length > limitedText[i].Limit) {
                                    var _txtLimited2 = currentValue.slice(0, limitedText[i].Limit) + ('...<a id="lDetailText' + colId + '" href="#" class="textLimit" fulltext="' + currentValue + '" columanName="' + colName + '">more</a>');
                                    tb += '<td data-label="' + toTitleCase(colName) + '" id="' + colId + '" showFullText="' + currentValue + '">' + _txtLimited2 + '</td>';
                                    _limitedTextCol = limitedText[i].Column;
                                } else {
                                    if (i === 0 && _limitedTextCol.indexOf(colName) <= 0) {
                                        tb += '<td data-label="' + toTitleCase(colName) + '" id="' + colId + '">' + currentValue + '</td>';
                                    }
                                }
                            }
                        } else {
                            tb += '<td data-label="' + toTitleCase(colName) + '" id="' + colId + '">' + currentValue + '</td>';
                        }
                    });
                }
                //CUSTOMER ROW
                else {
                        customHeader.map(function (colName, colIndex) {

                            calCount = colIndex;

                            var orginalPropertyName = colName['orginalColumnName'];
                            var newPropertyName = colName['newColumnName'];

                            var customColumnName = colName['customColumnName'];
                            var customColumnValue = colName['customColumnValue'];

                            var colId = 'td_' + rowIndex + '_' + colIndex;
                            var currentValue = rowData[orginalPropertyName];

                            //ADD APPEND OR PREPEND
                            if (addToColumn !== false && orginalPropertyName !== undefined) {

                                currentValue = privateMethods.AppendOrPrependToColumn(addToColumn, orginalPropertyName, currentValue);
                            }

                            if (orginalPropertyName !== undefined && (newPropertyName === undefined || newPropertyName === '')) {

                                if (typeof currentValue === 'string') {

                                    var reISO = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*))(?:Z|(\+|-)([\d|:]*))?$/;
                                    var a = reISO.exec(currentValue);
                                    if (a) {
                                        currentValue = currentValue.split('T')[0];
                                    }
                                }
                                if (currentValue === null) {
                                    currentValue = '';
                                }

                                if (limitedText !== undefined) {

                                    var _limitedTextCol2 = [];

                                    for (var i = 0; i < limitedText.length; i++) {

                                        _limitedTextCol2.push(limitedText[i].Column);
                                    }

                                    for (var i = 0; i < limitedText.length; i++) {

                                        if (limitedText[i].Column !== undefined && limitedText[i].Column === colName && currentValue.length > limitedText[i].Limit) {
                                            var _txtLimited3 = currentValue.slice(0, limitedText[i].Limit) + ('...<a id="lDetailText' + colId + '" href="#" class="textLimit" fulltext="' + currentValue + '" columanName="' + colName + '">more</a>');
                                            tb += '<td data-label="' + toTitleCase(colName) + '" id="' + colId + '" showFullText="' + currentValue + '">' + _txtLimited3 + '</td>';
                                        } else if (limitedText[i].Column === undefined && currentValue.length > limitedText[i].Limit) {
                                            var _txtLimited4 = currentValue.slice(0, limitedText[i].Limit) + ('...<a id="lDetailText' + colId + '" href="#" class="textLimit" fulltext="' + currentValue + '" columanName="' + colName + '">more</a>');
                                            tb += '<td data-label="' + toTitleCase(colName) + '" id="' + colId + '" showFullText="' + currentValue + '">' + _txtLimited4 + '</td>';
                                        } else {
                                            tb += '<td data-label="' + toTitleCase(colName) + '" id="' + colId + '">' + currentValue + '</td>';
                                        }
                                    }
                                } else if (i === 0 && limitedTextCol.indexOf(colName) <= 0) {
                                    tb += '<td data-label="' + toTitleCase(colName) + '" id="' + colId + '">' + currentValue + '</td>';
                                }

                                if (colName['Visible'] === false) {
                                    tb += '<td data-label="' + toTitleCase(orginalPropertyName) + '" id="' + colId + '" style="display:none;">' + txtLimited + '</td>';
                                } else {
                                    tb += '<td data-label="' + toTitleCase(orginalPropertyName) + '" id="' + colId + '">' + txtLimited + '</td>';
                                }
                            } else {

                                if (newPropertyName !== undefined) {

                                    if (colName['Visible'] === false) {
                                        tb += '<td data-label="' + toTitleCase(orginalPropertyName) + '" id="' + colId + '" style="display:none;">' + txtLimited + '</td>';
                                    } else {
                                        tb += '<td data-label="' + toTitleCase(orginalPropertyName) + '" id="' + colId + '">' + txtLimited + '</td>';
                                    }
                                }
                            }
                            //ADD CUSTOM COLUMN ROW
                            if (customColumnName !== undefined) {

                                var customColumnNameVariables = customColumnValue.match(/([^{{]*?)\w(?=\}})/gmi);

                                if (customColumnNameVariables !== null) {

                                    for (var _i = 0; _i < customColumnNameVariables.length; _i++) {

                                        customColumnValue = privateMethods.findReplaceString(customColumnValue, customColumnNameVariables[_i], rowData[customColumnNameVariables[_i]]);
                                    }
                                    tb += '<td data-label="" id="' + colId + '">' + customColumnValue + '</td>';
                                } else {

                                    tb += '<td data-label="" id="' + colId + '">' + customColumnValue + '</td>';
                                }
                            }
                        });
                    }
                //*********** SELECT ROW *************
                if (selectRows !== false) {

                    var colId = rowIndex + '_' + (calCount + 1);
                    var elementType = selectRows["Type"];
                    var selectRowType = selectRows["SelectRow"];
                    var btnText = selectRows.SelectMeText;

                    if (btnText === undefined) {
                        btnText = "Select";
                    }

                    if (elementType.toLowerCase() === 'checkbox') {
                        tb += '<td data-label id="td_' + colId + '" class="getSelectedRow">\n                                <div>\n                                    <input type="checkbox" id="chk_' + colId + '" name="chkSelectedRow" value="Select"/>\n                                    <label for="chk_' + colId + '">' + btnText + '</label>\n                                </div>\n                              </td>';
                    } else if (elementType.toLowerCase() === 'button') {
                        tb += '<td id="td_' + colId + '" data-label class="getSelectedRow">\n                                <button id="btn_' + colId + '" class="btn btn-primary btn-sm">' + btnText + '</button>\n                             </td>';
                    } else if (elementType.toLowerCase() === 'radio' && selectRowType.toLowerCase() !== 'multiple') {

                        tb += '<td data-label id="td_' + colId + '" class="getSelectedRow"><div>\n                                    <input type="radio" id="rdb_' + colId + '" name="rdbj2HTMLSelectedRow" />\n                                    <label for="rdb_' + colId + '">' + btnText + '</label>\n                                </div></td>';
                    }

                    if (elementType.toLowerCase() === 'radio' && selectRowType.toLowerCase() === 'multiple') {
                        throw Error('Radio button cannot be use for multiple selection');
                    }
                }
                // DELETE ONLY
                if (funDelete !== false) {
                    tb += '<td><button class=\'btn btn-default btn-sm btn-danger j2HTMLDeleteSelectedRow\' j2HTMLTableId="' + tableID + '" selectedRowId="' + rowId + '"><i class="fa fa-trash-o"></i></button></td>';
                }

                //UPDATE ONLY
                if (funUpdate !== false) {
                    tb += '<td><button class=\'btn btn-default btn-sm btn-primary j2HTMLUpdateSelectedRow\' j2HTMLTableId="' + tableID + '" selectedRowId="' + rowId + '"><i class="fa fa-edit"></i></button></td>';
                }

                tb += '</tr>';
            });
            tb += '</tbody></table>';
        },

        /**
         * APPEND OR PREPND VALUES TO COLUMNS
         * @param {APPEND OR PREPND INFORMATION} addToColumn 
         * @param {COLUMN NAME TO APPEND OR PREPAND} colName 
         * @param {CURRENT VALUE} currentValue 
         */
        AppendOrPrependToColumn: function AppendOrPrependToColumn(addToColumn, colName, currentValue) {
            addToColumn.map(function (obj) {

                if (obj['ColumanName'] === colName && obj['Type'].toLowerCase() === 'prepend') {
                    currentValue = obj['Value'] + currentValue;
                }
                if (obj['ColumanName'] === colName && obj['Type'].toLowerCase() === 'append') {
                    currentValue = currentValue + obj['Value'];
                }
            });

            return currentValue;
        },

        /**
         * CREATE CSV FILE
         * @param {Table ID} tableId 
         * @param {File Name} filename 
         */
        toCSV: function toCSV() {

            tableId = tblID;
            filename = pntFileName;

            //let filename = (typeof filename === 'undefined') ? tableId : filename;

            // Generate our CSV string from out HTML Table
            var csv = privateMethods.tableToCSV(tableId);
            // Create a CSV Blob
            var blob = new Blob([csv], {
                type: "text/csv"
            });

            // Determine which approach to take for the download
            if (navigator.msSaveOrOpenBlob) {
                // Works for Internet Explorer and Microsoft Edge
                navigator.msSaveOrOpenBlob(blob, filename + ".csv");
            } else {
                privateMethods.downloadAnchor(filename, URL.createObjectURL(blob), 'csv');
            }
        },

        /**
         * CONVERT TABLE TO CSV
         * @param {table Id} table 
         */
        tableToCSV: function tableToCSV(table) {
            // We'll be co-opting `slice` to create arrays
            var slice = Array.prototype.slice;

            return slice
            //.call(j2HTML.GetElement({ Element: '#tbTest' })[0].rows)
            .call(j2HTML.GetElement({
                Element: table
            })[0].rows).map(function (row) {
                return slice.call(row.cells).map(function (cell) {

                    if (cell.hasAttribute('showfulltext')) {
                        return '"t"'.replace("t", cell.getAttribute('showfulltext'));
                    } else {
                        return '"t"'.replace("t", cell.textContent);
                    }

                    //return '"t"'.replace("t", cell.textContent);
                }).join(",");
            }).join("\r\n");
        },

        /**
         *
         * DOWNLOAD THE FILE
         */
        downloadAnchor: function downloadAnchor(fileName, content, ext) {

            var anchor = document.createElement("a");
            anchor.style = "display:none !important";
            anchor.id = "downloadanchor";
            document.body.appendChild(anchor);

            // If the [download] attribute is supported, try to use it
            if ("download" in anchor) {
                anchor.download = fileName + "." + ext;
            }
            anchor.href = content;
            anchor.click();
            anchor.remove();
        },


        /**
         * 
         * @param {String to replace} string 
         * @param {Variable} find 
         * @param {Replace With} replace 
         */
        findReplaceString: function findReplaceString(string, find, replace) {

            if (/[a-zA-Z\_]+/g.test(string)) {
                return string.replace(new RegExp('\{\{(?:\\s+)?(' + find + ')(?:\\s+)?\}\}'), replace);
            } else {
                throw new Error("Find statement does not match regular expression: /[a-zA-Z\_]+/");
            }
        },

        /**
         * 
         * HIGHLIGHT AND GET SINGLE SELECTED ROW
         * 
         */
        j2HTMLHighlight_And_GetSelectedSingleRowData: function j2HTMLHighlight_And_GetSelectedSingleRowData() {

            var rowId = this.parentNode.getAttribute('id');

            //CHANGE BACKGROUND COLOR
            if (type.toLowerCase() === 'checkbox' || type.toLowerCase() === 'radio') {
                if (this.children[0].children[0].checked) {

                    if (type.toLowerCase() === 'radio') {
                        var elem = j2HTML.GetElement({
                            Element: '.item'
                        });
                        [].slice.call(elem).map(function (row) {

                            row.style.backgroundColor = '';
                            row.style.color = '';
                        });
                    }

                    this.parentNode.style.backgroundColor = selectedRowBackgroundColor;
                    this.parentNode.style.color = selectedRowForecolor;
                    this.parentNode.classList.add("selectedRow");

                    //GET ROW DATA
                    var selectedRowData = {};

                    var element = j2HTML.GetElement({
                        Element: '#' + rowId + ' td'
                    });
                    [].slice.call(element).map(function (td) {

                        if (td.getAttribute('data-label') !== '') {

                            if (td.hasAttribute('showFullText')) {
                                selectedRowData[td.getAttribute('data-label')] = td.getAttribute('showFullText');
                            } else {
                                selectedRowData[td.getAttribute('data-label')] = td.innerText;
                            }

                            //selectedRowData[td.getAttribute('data-label')] = td.innerText;
                        }
                    });

                    if (selectedRowsFunction !== false) {
                        selectedRowsFunction(JSON.stringify(selectedRowData));
                    }
                } else {
                    this.parentNode.style.backgroundColor = '';
                    this.parentNode.classList.remove("selectedRow");
                }
            } else {

                if (this.parentNode.style.backgroundColor === '') {

                    this.parentNode.style.backgroundColor = selectedRowBackgroundColor;
                    this.parentNode.style.color = selectedRowForecolor;
                    this.parentNode.classList.add("selectedRow");
                    //GET ROW DATA
                    var _selectedRowData = {};
                    var _element = j2HTML.GetElement({
                        Element: '#' + rowId + ' td'
                    });
                    [].slice.call(_element).map(function (td) {

                        if (td.getAttribute('data-label') !== '') {
                            _selectedRowData[td.getAttribute('data-label')] = td.innerText;
                        }
                    });

                    if (selectedRowsFunction !== false) {
                        selectedRowsFunction(JSON.stringify(_selectedRowData));
                    }
                } else {
                    this.parentNode.style.backgroundColor = '';
                    this.parentNode.classList.remove("selectedRow");
                }
            }
        },


        /**
         * 
         * HIGHLIGHT(ONLY) MULTIPLE ROWS
         * 
         */
        j2HTMLHighlightMultipleRows: function j2HTMLHighlightMultipleRows() {

            if (type.toLowerCase() === 'checkbox') {

                if (this.children[0].children[0].checked) {

                    this.parentNode.style.backgroundColor = selectedRowBackgroundColor;
                    this.parentNode.style.color = selectedRowForecolor;
                    this.parentNode.classList.add("selectedRow");
                } else {
                    this.parentNode.style.backgroundColor = '';
                    this.parentNode.classList.remove("selectedRow");
                }
            } else {

                if (this.parentNode.style.backgroundColor === '') {

                    this.parentNode.style.backgroundColor = selectedRowBackgroundColor;
                    this.parentNode.style.color = selectedRowForecolor;
                    this.parentNode.classList.add("selectedRow");
                } else {
                    this.parentNode.style.backgroundColor = '';
                    this.parentNode.classList.remove("selectedRow");
                }
            }
        },


        /**
         * 
         * GET MULTIPLE SELECTED ROWS
         * 
         */
        j2HTMLGetSelectedMultipleRowsData: function j2HTMLGetSelectedMultipleRowsData() {

            var rows = j2HTML.GetElement({
                Element: '.selectedRow'
            });
            var arraySelectedRows = [];

            var _loop = function _loop(i) {
                var selectedRowData = {};
                var element = j2HTML.GetElement({
                    Element: '#' + rows[i].getAttribute('id') + ' td'
                });

                [].slice.call(element).map(function (td) {

                    if (td.getAttribute('data-label') !== '') {

                        if (td.hasAttribute('showFullText')) {
                            selectedRowData[td.getAttribute('data-label')] = td.getAttribute('showFullText');
                        } else {
                            selectedRowData[td.getAttribute('data-label')] = td.innerText;
                        }

                        //selectedRowData[td.getAttribute('data-label')] = td.innerText;
                    }
                });

                arraySelectedRows.push(selectedRowData);
            };

            for (var i = 0; i < rows.length; i++) {
                _loop(i);
            }

            if (selectedRowsFunction !== false) {
                selectedRowsFunction(JSON.stringify(arraySelectedRows));
            }
        },


        /**
         * 
         * DELETE TABLE ROW
         * 
         */
        j2HTMLDeleteTableRow: function j2HTMLDeleteTableRow() {

            var rowId = this.getAttribute('selectedRowId');
            var tableID = this.getAttribute('j2HTMLTableId');
            //GET ROW DATA
            var selectedRowData = {};
            var element = j2HTML.GetElement({
                Element: '#' + rowId + ' td'
            });
            [].slice.call(element).map(function (td) {

                if (td.getAttribute('data-label') !== null) {
                    selectedRowData[td.getAttribute('data-label')] = td.innerText;
                }
            });

            var elemTb = document.getElementById(tableID).rows.namedItem(rowId);
            elemTb.parentNode.removeChild(elemTb);

            deleteRowFunction(JSON.stringify(selectedRowData));
        },


        /**
         * 
         * UPDATE TABLE ROW
         * 
         */
        j2HTMLUpdateTableRow: function j2HTMLUpdateTableRow() {

            var rowId = this.getAttribute('selectedRowId');
            var tableID = this.getAttribute('j2HTMLTableId');

            var selectedRowData = {};
            var element = j2HTML.GetElement({
                Element: '#' + rowId + ' td'
            });
            [].slice.call(element).map(function (td) {

                if (td.getAttribute('data-label') !== null) {

                    if (td.hasAttribute('showFullText')) {
                        selectedRowData[td.getAttribute('data-label')] = td.getAttribute('showFullText');
                    } else {
                        selectedRowData[td.getAttribute('data-label')] = td.innerText;
                    }
                }
            });

            data = [];
            data.push(selectedRowData);

            //SHOW MODAL
            j2HTML.Modal({

                Data: data,
                Heading: 'Edit',
                Display: 'Edit',
                UpdateFunction: updateRowFunction,
                TableID: tableID,
                RowID: rowId

            }).ShowModal();
        },
        j2HTMLCreateNewTableRow: function j2HTMLCreateNewTableRow() {

            var tableID = tblID;
            //let customHeader = custColumns;
            var fn = createNewRowFun;

            var selectedRowData = {};
            var element = j2HTML.GetElement({
                Element: tableID + ' tbody tr'
            });
            [].slice.call(element[0].children).map(function (td) {

                if (td.getAttribute('data-label') !== '' && td.style.display !== 'none') {
                    selectedRowData[td.getAttribute('data-label')] = td.innerText;
                } else if (td.getAttribute('data-label') === '' && td.style.display !== 'none') {
                    selectedRowData["AdditionalColumn"] = td.innerHTML;
                }
            });

            data = [];
            data.push(selectedRowData);

            j2HTML.Modal({

                Data: data,
                Heading: 'Insert',
                Display: 'Create',
                TableID: tableID,
                SelectRow: selectRowsForModal,
                CreateFunction: fn

            }).ShowModal();
        },

        /**
         * 
         * TEXT LIMIT
         * 
         */
        showJ2HTMLTableDetailText: function showJ2HTMLTableDetailText() {

            var text = event.target.getAttribute('fulltext');
            var heading = event.target.getAttribute('columanname');
            if (modID !== false) {

                j2HTML.HideModal({
                    ModalID: modID
                });

                var objPopup = {};
                objPopup.Type = 'Confirmation';
                objPopup.Heading = heading;

                if (popupText === false) {
                    objPopup.Message = text + '<br><br><b>Would you like to continue?</b><br>';
                } else {
                    objPopup.Message = text + ('<br><br><b>' + popupText + '</b><br>');
                }
                objPopup.Confirmation = {
                    Function: privateMethods.showTableModalAgain,
                    Parameters: {
                        ModalID: modID
                    }
                };
                j2HTML.PopupMessage(objPopup).ShowPopup();
            } else {
                j2HTML.PopupMessage({

                    Type: 'Information',
                    Heading: 'Detail',
                    Message: text

                }).ShowPopup();
            }
        },

        /**
         * 
         * CLOSE DETAIL MODAL AND SHOW TABLE
         * 
         */
        showTableModalAgain: function showTableModalAgain(param) {
            var modalID = JSON.parse(param).ModalID;
            j2HTML.ShowModal({
                ModalID: modalID
            });
        }
    };
    //endregion
    return table;
}();
function textBox() {
    var args = arguments[0][0];

    var data = false;
    var appendTo = false;
    var propertyName = false;
    var direction = 'vertical';
    var columns = false;

    var error = '';

    if (args.Data !== undefined) {
        data = args.Data;
    } else {
        error += 'Data requred\n';
    }
    if (args.PropertyName) {
        propertyName = args.PropertyName;
    } else {
        error += 'Property name required\n';
    }

    if (error === '') {
        var txt = '';
        $.each(data, function (i, v) {

            $.each(v, function (ii, vv) {

                if (ii === propertyName) {

                    var id = 'txt' + ii;
                    var val = vv;

                    if (direction.toLowerCase() === 'horizontal') {} else {
                        txt += '<div class="row">\n                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">\n                                    <input class="form-control" id=" ' + id + '" value="' + val + '"/>\n                                  </div>\n                                </div>';
                    }
                }
            });
        });

        if (appendTo !== false) {
            $(appendTo).empty();
            $(appendTo).appendTo(txt);
        } else {
            $('#j2HTMLTextBox').remove();
            var div = '<div id="j2HTMLTextBox">' + txt + '</div>';
            $('body').append(div);
        }
    } else {
        alert(error);
    }
}
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var j2HTMLTabs = function () {

    //CREAT CLASS
    var j2HTMLTabs = function () {
        function j2HTMLTabs() {
            _classCallCheck(this, j2HTMLTabs);

            this._data = false;

            this._tabId = false;

            this._selected_tab_index = 1;

            this._defaultTabBackgroundColor = 'black';
            this._defaultTabForecolor = 'white';

            this._selectedTabBackgroundColor = 'gray';
            this._selectedTabForecolor = 'black';
        }

        //#region ************** PROPERTIES ******************

        // PROPERTIES


        _createClass(j2HTMLTabs, [{
            key: 'getTabs',


            //endregion

            /**
             *
             * @memberof j2HTMLTabs
             * 
             */
            value: function getTabs() {

                selectedContentIndex = this._selected_tab_index.toString();

                defaultContentBGColor = this._defaultTabBackgroundColor;
                defaultContentForeColor = this._defaultTabForecolor;

                selectedContentBGColor = this._selectedTabBackgroundColor;
                selectedContentForecolor = this._selectedTabForecolor;

                if (this.tabId !== false) {
                    tab = '#' + this.tabId + '.j2HTMLTab .j2HTMLTabLink';
                } else {
                    tab = '.j2HTMLTab .j2HTMLTabLink';
                }

                if (this.tabId !== false) {
                    tabLink = '#' + this.tabId + '.j2HTMLTab .j2HTMLTabLink button';
                } else {
                    tabLink = '.j2HTMLTab .j2HTMLTabLink button';
                }

                if (this.tabId !== false) {
                    tabContent = '#' + this.tabId + '.j2HTMLTabContent';
                } else {
                    tabContent = '.j2HTMLTabContent';
                }

                //SET TAB BG COLOR
                [].slice.call(j2HTML.GetElement({
                    Element: tab
                })).map(function (tabContent) {
                    tabContent.style.backgroundColor = defaultContentBGColor;
                    tabContent.style.color = defaultContentForeColor;
                });
                //SHOW AND HIDE DEFAULT CONTENT
                [].slice.call(j2HTML.GetElement({
                    Element: tabContent
                })).map(function (tabContent) {

                    if (tabContent.getAttribute('tab-content-index') === selectedContentIndex) {
                        tabContent.style.display = 'block';
                    } else {
                        tabContent.style.display = 'none';
                    }

                    //SET DEFAULT CONTENT COLOR
                    tabContent.style.backgroundColor = selectedContentBGColor;
                    tabContent.style.color = selectedContentForecolor;

                    if (j2HTML.IsElementExsit({
                        Element: '.j2HTMLTab[position="left"]'
                    })) {
                        tabContent.style.left = j2HTML.GetElement({
                            Element: tab
                        })[0].offsetHeight;
                    }
                });
                //SET SELECTED TAB COLOR
                [].slice.call(j2HTML.GetElement({
                    Element: tabLink
                })).map(function (tabLink) {
                    if (tabLink.getAttribute('tab-link-index') === selectedContentIndex) {
                        tabLink.style.backgroundColor = selectedContentBGColor;
                        tabLink.style.color = selectedContentForecolor;
                    } else {
                        tabLink.style.backgroundColor = defaultContentBGColor;
                        tabLink.style.color = defaultContentForeColor;
                    }
                });

                privateMethods.setTabs();
            }
            //endregion


        }, {
            key: 'data',
            get: function get() {
                return this._data;
            },
            set: function set(value) {
                this._data = value;
            }
        }, {
            key: 'tabId',
            get: function get() {
                return this._tabId;
            },
            set: function set(value) {
                this._tabId = value;
            }
        }, {
            key: 'selected_tab_index',
            get: function get() {
                return this._selected_tab_index;
            },
            set: function set(value) {
                this._selected_tab_index = value;
            }
        }, {
            key: 'defaultTabBackgroundColor',
            get: function get() {
                return this._defaultTabBackgroundColor;
            },
            set: function set(value) {
                this._defaultTabBackgroundColor = value;
            }
        }, {
            key: 'defaultTabForecolor',
            get: function get() {
                return this._defaultTabForecolor;
            },
            set: function set(value) {
                this._defaultTabForecolor = value;
            }
        }, {
            key: 'selectedTabBackgroundColor',
            get: function get() {
                return this._selectedTabBackgroundColor;
            },
            set: function set(value) {
                this._selectedTabBackgroundColor = value;
            }
        }, {
            key: 'selectedTabForecolor',
            get: function get() {
                return this._selectedTabForecolor;
            },
            set: function set(value) {
                this._selectedTabForecolor = value;
            }
        }]);

        return j2HTMLTabs;
    }();

    //#region ************** PRIVATE VARIABLE AND METHOD *******

    var tab = void 0;
    var tabContent = void 0;
    var tabButton = void 0;

    var selectedContentIndex = void 0;

    var selectedContentBGColor = void 0;
    var selectedContentForecolor = void 0;

    var defaultContentBGColor = void 0;
    var defaultContentForeColor = void 0;

    //PRIVATE FUNCTIONS
    var privateMethods = {
        setTabs: function setTabs() {

            j2HTML.CreateEvent({ Element: tabLink, Type: 'click' }, function () {

                var contentId = event.target.getAttribute('tab-link-index');

                //SET BUTTON COLOR
                [].slice.call(j2HTML.GetElement({
                    Element: tabLink
                })).map(function (tabLink) {

                    if (tabLink.getAttribute('tab-link-index') === contentId) {
                        tabLink.style.backgroundColor = selectedContentBGColor;
                        tabLink.style.color = selectedContentForecolor;
                    } else {
                        tabLink.style.backgroundColor = defaultContentBGColor;
                        tabLink.style.color = defaultContentForeColor;
                    }
                });

                //SHOW AND HIDE CONTENT
                [].slice.call(j2HTML.GetElement({
                    Element: tabContent
                })).map(function (tabContent) {

                    if (tabContent.getAttribute('tab-content-index') === contentId) {
                        tabContent.style.display = 'block';
                    } else {
                        tabContent.style.display = 'none';
                    }
                });
            });
        },
        privareFunction2: function privareFunction2() {}
    };

    //endregion


    return j2HTMLTabs;
}();
// //****************************************************************************
// //**************************** TOGGLE SWITCH *********************************
// //****************************************************************************

function switchButton() {

    var args = arguments[0][0];

    var error = '';

    var Id = false;
    var texton = false;
    var textoff = false;

    var onColor = false;
    var onbgcolor = false;

    var offColor = false;
    var offbgcolor = false;

    var size = 'md';

    var checked = false;
    var direction = false;

    if (args.Texton !== undefined) {
        texton = args.Texton;
    }
    if (args.Textoff !== undefined) {
        textoff = args.Textoff;
    }

    if (args.Size !== undefined) {
        size = args.Size;
    }

    if (args.CheckedColor !== undefined) {
        checkedColor = args.CheckedColor;
    }
    if (args.UncheckedColor !== undefined) {
        uncheckedColor = args.UncheckedColor;
    }

    if (args.Checked !== undefined) {
        checked = args.Checked;
    }

    if (args.Direction !== undefined) {
        direction = args.Direction;
    }

    //FOR TOGGLE SWITCH FROM JASON
    var data = false;
    var appendTo = false;
    var text = false;

    if (args.ID !== undefined) {
        Id = args.ID;
    }

    if (args.Data !== undefined) {
        data = args.Data;
    }
    if (args.AppendTo !== undefined) {
        appendTo = args.AppendTo;
    }

    if (args.Text !== undefined) {
        text = args.Text;
    }

    //TOGGLE SWITCH FROM JSON
    var chk = '';

    if (data === undefined) {
        error += 'Data is required\n';
    }
    if (appendTo === false) {
        error += 'Append to is required\n';
    }
    if (text === false) {
        error += 'Text to is required\n';
    }
    if (error === '') {

        $.each(data, function (i, v) {

            $.each(data[i], function (ii, vv) {

                if (ii.toLowerCase() === args.Text.toLowerCase()) {

                    var chkId = 'chk' + vv;
                    chk += '<input type="checkbox" id="' + chkId + '" /><label for="' + chkId + '" class="switch switch-' + size + '"';

                    if (texton !== false) {
                        chk += ' texton="' + texton + '"';
                    }
                    if (textoff !== false) {
                        chk += ' textoff="' + textoff + '"';
                    }
                    if (onbgcolor !== false) {
                        chk += ' onbgcolor="' + onbgcolor + '"';
                    }
                    if (offbgcolor !== false) {
                        chk += ' offbgcolor="' + offbgcolor + '"';
                    }
                    if (onColor !== false) {
                        chk += ' oncolor="' + onColor + '"';
                    }
                    if (offColor !== false) {
                        chk += ' offcolor="' + offColor + '"';
                    }

                    if (direction !== false) {
                        chk += '></label><br/>';
                    } else {
                        chk += '></label>';
                    }
                }
            });
        });

        $('#' + args.AppendTo).html(chk);
        toggleSwitch();
    } else {
        alert(error);
    }
}

//ON DOCUMENTS READY
// $(function() {

//     toggleSwitch();
// });


// $(document).on('click', '.switch', function() {

//     var chkId = $(this);

//     if ($(this).prev().is(':Checked')) {

//         switchOff(chkId);

//     } else {

//         switchOn(chkId);

//     }

// });

//TOGGLE SWITCH
// function toggleSwitch(toggle) {

//     $('.switch').each(function() {

//         var chkId = $(this);
//         var text = '';
//         var width = '';

//         $(this).prev().hide();

//         if ($(this).prev().is(':Checked')) {

//             switchOn(chkId);

//         } else {

//             switchOff(chkId);
//         }
//     });
// }

//SET SWITCH ON
// function switchOn(chkId) {


//     var onBGColor = 'blue';
//     var onColor = 'yellow';

//     if ($(chkId).attr('onbgcolor') !== undefined) {
//         onBGColor = $(chkId).attr('onbgcolor');
//         $(chkId).css('background-color', onBGColor);
//     } else {
//         $(chkId).css('background-color', onBGColor);
//     }

//     if ($(chkId).attr('oncolor') !== undefined) {
//         onColor = $(chkId).attr('oncolor');
//         $(chkId).css('color', onColor);
//     } else {
//         $(chkId).css('color', onColor);
//     }
//     //ADJUST WIDTH ACCORDING TO THE TEXT
//     if ($(chkId).attr('texton') !== undefined) {
//         text = (($(chkId).attr('texton').length) / 16);
//         width = text + 3.5 + 1.75 + 'em';
//         $(chkId).css('width', width);
//     }

//     $(chkId).prev().attr('checked', true);


// }

//SET SWITCH OFF
// function switchOff(chkId) {


//     var offBGColor = 'gray';
//     var offColor = 'black';


//     if ($(chkId).attr('offbgcolor') !== undefined) {
//         offBGColor = $(chkId).attr('offbgcolor');
//         $(chkId).css('background-color', offBGColor);
//     } else {
//         $(chkId).css('background-color', offBGColor);
//     }

//     if ($(chkId).attr('offcolor') !== undefined) {
//         offColor = $(chkId).attr('offcolor');
//         $(chkId).css('color', offColor);
//     } else {
//         $(chkId).css('color', offColor);
//     }

//     //ADJUST WIDTH ACCORDING TO THE TEXT
//     if ($(chkId).attr('textoff') !== undefined) {
//         text = (($(chkId).attr('textoff').length) / 16);
//         width = text + 3.5 + 1.75 + 'em';
//         $(chkId).css('width', width);
//     }

//     $(chkId).prev().attr('checked', false);

// }
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var j2HTMLValidation = function () {

    //#region ************** PRIVATE VARIABLE AND METHOD *******

    //CREATE LOCAL VARIABLES
    var Ids = '';
    var valTypes = '';
    var disType = '';
    var errMessages = '';
    //let elementIndx = 0;
    var popoverPosition = '';
    var minOrMaxLength = '';

    //PRIVATE FUNCTIONS
    var privateMethods = {
        validateElement: function validateElement(type, value, length) {
            var isValidate = false;

            if (type.toLowerCase() === 'required') {
                isValidate = privateMethods.requiredRegex(value);
            }
            if (type.toLowerCase() === 'number') {
                isValidate = privateMethods.numberRegex(value);
            }
            if (type.toLocaleLowerCase() === 'minlength') {
                isValidate = privateMethods.minLengthRegex(value, length);
            }

            return isValidate;
        },


        //CUSTOM REGEX
        customRegex: function customRegex(value, strRegex) {
            var pattren = new RegExp(strRegex);
            var pattrenValue = $(value).val();
            var pattrenResult = pattren.test(pattrenValue);
            return pattrenResult;
        },


        //REQUIRED REGEX
        requiredRegex: function requiredRegex(value) {

            var pattren = /\S+/;
            var pattrenValue = value;
            var pattrenResult = pattren.test(pattrenValue);
            return pattrenResult;
        },


        //NUMBER REGEX
        numberRegex: function numberRegex(value) {

            var pattren = /^\d+$/;
            var pattrenValue = value;
            var pattrenResult = pattren.test(pattrenValue);
            return pattrenResult;
        },


        //LETTER REGEX
        letterRegex: function letterRegex(value) {

            var pattren = /^[a-zA-Z]+$/;
            var pattrenValue = value;
            var pattrenResult = pattren.test(pattrenValue);
            return pattrenResult;
        },

        // DECIMAL REGEX
        decimalRegex: function decimalRegex(value) {

            var pattren = /^\d+\.\d+$/;
            var pattrenValue = value;
            var pattrenResult = pattren.test(pattrenValue);
            return pattrenResult;
        },


        //PHONE REGEX
        phoneRegex: function phoneRegex(value) {
            var pattren = /^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$/;
            var pattrenValue = value;
            var pattrenResult = pattren.test(pattrenValue);
            return pattrenResult;
        },


        //DATE REGEX
        dateRegex: function dateRegex(value) {
            var pattren = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
            var pattrenValue = value;
            var pattrenResult = pattren.test(pattrenValue);
            return pattrenResult;
        },


        //EMAIL REGEX
        emailRegex: function emailRegex(value) {
            var pattren = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var pattrenValue = value;
            var pattrenResult = pattren.test(pattrenValue);
            return pattrenResult;
        },


        //ZIP CODE REGEX
        zipcodeRegex: function zipcodeRegex(value) {

            var pattren = /^\d{5}(?:[-\s]\d{4})?$/;
            var pattrenValue = value;
            var pattrenResult = pattren.test(pattrenValue);
            return pattrenResult;
        },


        //URL ADDRESS REGEX
        urlRegex: function urlRegex(value) {
            var pattren = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
            var pattrenValue = value;
            var pattrenResult = pattren.test(pattrenValue);
            return pattrenResult;
        },


        //MAX LENGTH REGEX
        maxLengthRegex: function maxLengthRegex(value, length) {

            if (length !== false) {

                var totalLength = value.length;

                if (totalLength > length) {
                    return false;
                }
            } else {
                alert('length is required');
            }
        },

        //MAX LENGTH REGEX
        minLengthRegex: function minLengthRegex(value, length) {

            if (length !== false) {

                var totalLength = value.length;

                if (totalLength < length) {
                    return false;
                }
            } else {
                alert('length is required');
            }
        },
        showErrorInPopup: function showErrorInPopup(error) {

            j2HTML.PopupMessage({

                Type: 'Error',
                Heading: 'Error',
                Message: error

            }).ShowPopup();
        },
        showErrorWithPopover: function showErrorWithPopover(element, error, position) {

            //CREATE DIV TO HOLD POPOVER (CONTAINER)
            // let elemId = 'po' + element.getAttribute('id').slice(3);
            // let strHTML = `<div id="${elemId}"></div>`;

            // j2HTML.AppendHTMLStringAfter({
            //     Element: '#' + element.getAttribute('id'),
            //     HTMLString: strHTML
            // });


            //ADD ATTRIBUTES TO ELEMENT FOR POPOVER
            element.setAttribute('data-title', 'Error');
            element.setAttribute('data-content', error);
            //element.setAttribute('data-container', '#' + elemId);


            //SHOW POPOVER
            setTimeout(function () {
                new Popover(element, {
                    placement: position
                }).show();
            }, 10);
        }
    };

    //endregion


    //CREAT CLASS

    var j2HTMLValidation = function () {
        function j2HTMLValidation() {
            _classCallCheck(this, j2HTMLValidation);

            this._elementIDs = false;
            this._displayType = false; //TEXT, MODAL, POPOVER
            this._position = false; //TOP OR BOTTOM
            this._errorMessage = false;
            this._validationType = false;
            this._validate = false;
            this._regex = false;
            this._css = false;
            this._length = false;
        }

        //#region ************** PROPERTIES ******************

        // PROPERTIES


        _createClass(j2HTMLValidation, [{
            key: 'setValidation',


            //endregion

            //#region ************** PUBLIC FUNCTIONS ************
            value: function setValidation() {

                var error = '';

                if (this._elementIDs === false) {
                    error += 'Element Ids are required';
                }
                if (this._errorMessage === false) {
                    error += 'Error message is required';
                }
                if (this._elementIDs.length !== this._errorMessage.length) {
                    error += 'Length of Ids must be equal length of error messages';
                }

                if (error === '') {

                    //SET LOCAL VARIABLES
                    Ids = this._elementIDs;
                    valTypes = this._validationType;
                    disType = this._displayType;
                    errMessages = this._errorMessage;
                    cssClass = this._css;
                    minOrMaxLength = this._length;

                    //SET DEFAULT POSITION 
                    if (this._position === false) {

                        popoverPosition = [];
                        [].slice.call(Ids).map(function () {
                            popoverPosition.push('right');
                        });
                    } else {
                        popoverPosition = this._position;
                    }
                    //SET DEFAULT CSS CLASS
                    if (this._css === false) {
                        cssClass = 'errorBackgroundColor';
                    }

                    var v = new j2HTMLValidation();
                    //CREATE ON FOCUS EVENT()
                    [].slice.call(Ids).map(function (elemt) {

                        var Element = j2HTML.GetElement({
                            Element: elemt
                        });

                        //REMOVE PREVIOUSLY ADDED ON-CLICK EVENT LISTENER
                        Element[0].removeEventListener("click", v.onFocusRemoveErrorMessage, false);
                        Element[0].removeEventListener("focus", v.onFocusRemoveErrorMessage, false);

                        //ADD ON-CLICK EVENT LISTENER
                        Element[0].addEventListener("click", v.onFocusRemoveErrorMessage, false);
                        Element[0].addEventListener("focus", v.onFocusRemoveErrorMessage, false);
                    });

                    //CREATE BLUR EVENT (Default)
                    if (this._validate === false) {

                        //CREATE ATTRIBUTE FOR TEXTBOX
                        [].slice.call(Ids).map(function (elemt, index) {

                            var Element = j2HTML.GetElement({
                                Element: elemt
                            });

                            Element[0].setAttribute('Index', index);
                            //Element[0].setAttribute('Type', valTypes[index]);

                            //REMOVE PREVIOUSLY ADDED ON-CLICK EVENT LISTENER
                            Element[0].removeEventListener("blur", v.validateOnBlur, false);

                            //ADD ON-CLICK EVENT LISTENER
                            Element[0].addEventListener("blur", v.validateOnBlur, false);
                        });
                    }

                    //CREATE CLICK EVENT
                    if (this._validate !== false && this._validate.On.toLowerCase() === 'click') {

                        var btnElement = this._validate.ElementID;

                        var btnElemnt = j2HTML.GetElement({
                            Element: btnElement
                        });

                        //REMOVE PREVIOUSLY ADDED ON-CLICK EVENT LISTENER
                        btnElemnt[0].removeEventListener("click", v.validateOnClick, false);

                        //ADD ON-CLICK EVENT LISTENER
                        btnElemnt[0].addEventListener("click", v.validateOnClick, false);
                    }
                } else {
                    throw new Error(error);
                }
            }

            /*********** CLEAR TEXT BOX ON CLICK ***************/

        }, {
            key: 'onFocusRemoveErrorMessage',
            value: function onFocusRemoveErrorMessage() {

                var element = this;

                if (element.classList.contains(cssClass)) {

                    element.classList.remove(cssClass);
                    element.value = '';
                }

                //HIDE POPOVER
                // if (element.hasAttribute('data-title') && element.hasAttribute('data-content')) {

                //     let popover = new Popover(this, {
                //         placement: popoverPosition
                //     });
                //     popover.hide();


                //     element.removeAttribute('data-title');
                //     element.removeAttribute('data-content');

                // }
            }
            /*********** VALIDATE ON CLICK ***************/

        }, {
            key: 'validateOnClick',
            value: function validateOnClick() {

                var errorForPopup = '';
                var previousElement = '';

                [].slice.call(Ids).map(function (elemt, index) {

                    var elem = j2HTML.GetElement({
                        Element: elemt
                    });

                    var val = elem[0].value;
                    var isvalidate = privateMethods.validateElement(valTypes[index], val, minOrMaxLength[index]);

                    //VALIDATE TEXTBOX
                    if (disType === false) {

                        if (isvalidate === false) {

                            elem[0].value = errMessages[index];
                            elem[0].classList.add(cssClass);
                        }
                    }
                    //CONCATENATE ERROR MESSAGE TO SHOW IN POPUP
                    else if (disType !== false && disType.toLocaleLowerCase() === 'popup') {
                            // if (isvalidate === false) {
                            //     errorForPopup += errMessages[index] + ' && ';
                            // }
                            if (isvalidate === false && (previousElement === '' || previousElement === elemt)) {
                                errorForPopup += errMessages[index] + '<br>';
                                previousElement = elemt;
                            } else if (isvalidate === false && (previousElement === '' || previousElement !== elemt)) {

                                // if (errorForPopup.trim().indexOf('&&') >= 0) {
                                //     errorForPopup = errorForPopup.trim().slice(0, -2);
                                // }
                                errorForPopup += errMessages[index] + '<br>';
                            }
                        }
                        //CONCATENATE ERROR MESSAGE TO SHOW IN POPOVER
                        else if (disType !== false && disType.toLocaleLowerCase() === 'popover') {

                                if (isvalidate === false && (previousElement === '' || previousElement === elemt)) {
                                    errorForPopup += errMessages[index] + '<br>';
                                    previousElement = elemt;
                                } else if (isvalidate === false && (previousElement === '' || previousElement !== elemt)) {
                                    errorForPopup = errMessages[index];
                                }

                                //SHOW ERROR IN POPOVER 
                                if (Ids[index] !== Ids[index + 1]) {
                                    elem[0].classList.add(cssClass);
                                    privateMethods.showErrorWithPopover(elem[0], errorForPopup, popoverPosition[index]);
                                }
                            }
                });

                //SHOW ERROR IN POPUP
                if (disType !== false && disType.toLocaleLowerCase() === 'popup') {

                    if (errorForPopup !== '') {
                        privateMethods.showErrorInPopup(errorForPopup);
                    }
                }
            }
            /*********** VALIDATE ON BLUR ***************/

        }, {
            key: 'validateOnBlur',
            value: function validateOnBlur() {

                //let clickedElement = this;
                var errorMsg = '';
                var previousElement = '';

                [].slice.call(Ids).map(function (elemt, index) {

                    var txt = j2HTML.GetElement({
                        Element: elemt
                    });

                    var val = '';
                    var type = valTypes[index];
                    var length = minOrMaxLength[index];
                    if (val.trim() !== errMessages[index]) {
                        val = txt[0].value;
                    } else {
                        val = '';
                    }

                    var isvalidate = privateMethods.validateElement(type, val, length);

                    //CONCATENATE ERROR MESSAGE -- TEXT BOX
                    if (disType === false && isvalidate === false && (previousElement === '' || previousElement === elemt)) {
                        errorMsg += errMessages[index] + ' & ';
                        previousElement = elemt;
                    } else if (disType === false && isvalidate === false && (previousElement === '' || previousElement !== elemt)) {
                        errorMsg = '';
                        errorMsg += errMessages[index];
                    }

                    //SHOW ERROR IN TEXTBOX
                    if (disType === false && isvalidate === false && elemt !== Ids[index + 1]) {

                        if (errorMsg !== '' && val !== errMessages[index]) {

                            if (errorMsg.indexOf('&') >= 0) {
                                errorMsg = errorMsg.trim().slice(0, -1);
                            }

                            txt[0].value = errorMsg;
                            txt[0].classList.add(cssClass);
                        }
                    } else if (disType === false && val.trim() !== errMessages[index]) {
                        txt[0].classList.remove(cssClass);
                    }

                    //CONCATENATE ERROR MESSAGE -- FOR POPUP (TO DO)
                    // if(clickedElement.getAttribute('id')===elemt.slice(1))
                    // {
                    //     if (disType !== false && disType.toLocaleLowerCase() === 'popup') {

                    //         if (isvalidate === false && (previousElement === '' || previousElement === elemt)) {
                    //             errorMsg += errMessages[index] + ' <br> ';
                    //             previousElement = elemt;

                    //         } else if (isvalidate === false && (previousElement === '' || previousElement !== elemt)) {
                    //             errorMsg += errMessages[index] + '<br>';
                    //         }
                    //     }
                    // }

                    //SHOW ERROR WITH POPOVER
                    if (disType !== false && disType.toLocaleLowerCase() === 'popover') {
                        privateMethods.showErrorWithPopover(elemt[0], errMsg, popoverPosition[index]);
                    }
                });

                //SHOW ERROR IN POPUP (TO DO )
                // if (disType !== false && disType.toLocaleLowerCase() === 'popup') {

                //     if (errorMsg !== '') {

                //         //clickedElement.focus();
                //         //document.getElementsByTagName('body').focus();
                //         privateMethods.showErrorInPopup(errorMsg);
                //         //document.getElementsByTagName('body').focus();
                //     }

                // }
            }

            //endregion


        }, {
            key: 'elementIDs',
            get: function get() {
                return this._elementIDs;
            },
            set: function set(value) {
                this._elementIDs = value;
            }
        }, {
            key: 'displayType',
            get: function get() {
                return this._displayType;
            },
            set: function set(value) {
                this._displayType = value;
            }
        }, {
            key: 'position',
            get: function get() {
                return this._position;
            },
            set: function set(value) {
                this._position = value;
            }
        }, {
            key: 'errorMessage',
            get: function get() {
                return this._errorMessage;
            },
            set: function set(value) {
                this._errorMessage = value;
            }
        }, {
            key: 'validationType',
            get: function get() {
                return this._validationType;
            },
            set: function set(value) {
                this._validationType = value;
            }
        }, {
            key: 'validate',
            get: function get() {
                return this._validate;
            },
            set: function set(value) {
                this._validate = value;
            }
        }, {
            key: 'regex',
            get: function get() {
                return this._regex;
            },
            set: function set(value) {
                this._regex = value;
            }
        }, {
            key: 'css',
            get: function get() {
                return this._css;
            },
            set: function set(value) {
                this._css = value;
            }
        }, {
            key: 'length',
            get: function get() {
                return this._length;
            },
            set: function set(value) {
                this._length = value;
            }
        }]);

        return j2HTMLValidation;
    }();

    return j2HTMLValidation;
}();
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

// Native Javascript for Bootstrap 4 v2.0.24 | © dnp_theme | MIT-License
!function (t, e) {
  if ("function" == typeof define && define.amd) define([], e);else if ("object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports) module.exports = e();else {
    var n = e();t.Alert = n.Alert, t.Button = n.Button, t.Carousel = n.Carousel, t.Collapse = n.Collapse, t.Dropdown = n.Dropdown, t.Modal = n.Modal, t.Popover = n.Popover, t.ScrollSpy = n.ScrollSpy, t.Tab = n.Tab, t.Tooltip = n.Tooltip;
  }
}(this, function () {
  "use strict";
  var t = "undefined" != typeof global ? global : this || window,
      e = document,
      n = e.documentElement,
      i = "body",
      o = t.BSN = {},
      a = o.supports = [],
      l = "data-toggle",
      r = "data-dismiss",
      c = "data-spy",
      u = "data-ride",
      s = "Alert",
      f = "Button",
      h = "Carousel",
      d = "Collapse",
      v = "Dropdown",
      p = "Modal",
      m = "Popover",
      g = "ScrollSpy",
      w = "Tab",
      b = "Tooltip",
      y = "data-backdrop",
      T = "data-keyboard",
      x = "data-target",
      C = "data-interval",
      A = "data-height",
      k = "data-pause",
      I = "data-title",
      N = "data-original-title",
      L = "data-dismissible",
      E = "data-trigger",
      S = "data-animation",
      B = "data-container",
      D = "data-placement",
      M = "data-delay",
      P = "backdrop",
      H = "keyboard",
      O = "delay",
      W = "content",
      j = "target",
      q = "interval",
      R = "pause",
      U = "animation",
      z = "placement",
      F = "container",
      X = "offsetTop",
      Y = "offsetLeft",
      G = "scrollTop",
      J = "scrollLeft",
      K = "clientWidth",
      Q = "clientHeight",
      V = "offsetWidth",
      Z = "offsetHeight",
      $ = "innerWidth",
      _ = "innerHeight",
      tt = "scrollHeight",
      et = "height",
      nt = "aria-expanded",
      it = "aria-hidden",
      ot = "click",
      at = "hover",
      lt = "keydown",
      rt = "keyup",
      ct = "resize",
      ut = "scroll",
      st = "show",
      ft = "shown",
      ht = "hide",
      dt = "hidden",
      vt = "close",
      pt = "closed",
      mt = "slid",
      gt = "slide",
      wt = "change",
      bt = "getAttribute",
      yt = "setAttribute",
      Tt = "hasAttribute",
      xt = "createElement",
      Ct = "appendChild",
      At = "innerHTML",
      kt = "getElementsByTagName",
      It = "preventDefault",
      Nt = "getBoundingClientRect",
      Lt = "querySelectorAll",
      Et = "getElementsByClassName",
      St = "getComputedStyle",
      Bt = "indexOf",
      Dt = "parentNode",
      Mt = "length",
      Pt = "toLowerCase",
      Ht = "Transition",
      Ot = "Duration",
      Wt = "Webkit",
      jt = "style",
      qt = "push",
      Rt = "tabindex",
      Ut = "contains",
      zt = "active",
      Ft = "show",
      Xt = "collapsing",
      Yt = "left",
      Gt = "right",
      Jt = "top",
      Kt = "bottom",
      Qt = "onmouseleave" in e ? ["mouseenter", "mouseleave"] : ["mouseover", "mouseout"],
      Vt = /\b(top|bottom|left|right)+/,
      Zt = 0,
      $t = "fixed-top",
      _t = "fixed-bottom",
      te = Wt + Ht in n[jt] || Ht[Pt]() in n[jt],
      ee = Wt + Ht in n[jt] ? Wt[Pt]() + Ht + "End" : Ht[Pt]() + "end",
      ne = Wt + Ot in n[jt] ? Wt[Pt]() + Ht + Ot : Ht[Pt]() + Ot,
      ie = function ie(t) {
    t.focus ? t.focus() : t.setActive();
  },
      oe = function oe(t, e) {
    t.classList.add(e);
  },
      ae = function ae(t, e) {
    t.classList.remove(e);
  },
      le = function le(t, e) {
    return t.classList[Ut](e);
  },
      re = function re(t, e) {
    return [].slice.call(t[Et](e));
  },
      ce = function ce(t, n) {
    var i = n ? n : e;return "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) ? t : i.querySelector(t);
  },
      ue = function ue(t, n) {
    var i = n.charAt(0),
        o = n.substr(1);if ("." === i) {
      for (; t && t !== e; t = t[Dt]) {
        if (null !== ce(n, t[Dt]) && le(t, o)) return t;
      }
    } else if ("#" === i) for (; t && t !== e; t = t[Dt]) {
      if (t.id === o) return t;
    }return !1;
  },
      se = function se(t, e, n) {
    t.addEventListener(e, n, !1);
  },
      fe = function fe(t, e, n) {
    t.removeEventListener(e, n, !1);
  },
      he = function he(t, e, n) {
    se(t, e, function i(o) {
      n(o), fe(t, e, i);
    });
  },
      de = function de(e) {
    var n = t[St](e)[ne];return n = parseFloat(n), n = "number" != typeof n || isNaN(n) ? 0 : 1e3 * n, n + 50;
  },
      ve = function ve(t, e) {
    var n = 0,
        i = de(t);te && he(t, ee, function (t) {
      e(t), n = 1;
    }), setTimeout(function () {
      !n && e();
    }, i);
  },
      pe = function pe(t, e, n) {
    var i = new CustomEvent(t + ".bs." + e);i.relatedTarget = n, this.dispatchEvent(i);
  },
      me = function me() {
    return { y: t.pageYOffset || n[G], x: t.pageXOffset || n[J] };
  },
      ge = function ge(t, o, a, l) {
    var r,
        c,
        u,
        s,
        f,
        h,
        d = { w: o[V], h: o[Z] },
        v = n[K] || e[i][K],
        p = n[Q] || e[i][Q],
        m = t[Nt](),
        g = l === e[i] ? me() : { x: l[Y] + l[J], y: l[X] + l[G] },
        w = { w: m[Gt] - m[Yt], h: m[Kt] - m[Jt] },
        b = le(o, "popover"),
        y = ce(".arrow", o),
        T = m[Jt] + w.h / 2 - d.h / 2 < 0,
        x = m[Yt] + w.w / 2 - d.w / 2 < 0,
        C = m[Yt] + d.w / 2 + w.w / 2 >= v,
        A = m[Jt] + d.h / 2 + w.h / 2 >= p,
        k = m[Jt] - d.h < 0,
        I = m[Yt] - d.w < 0,
        N = m[Jt] + d.h + w.h >= p,
        L = m[Yt] + d.w + w.w >= v;a = (a === Yt || a === Gt) && I && L ? Jt : a, a = a === Jt && k ? Kt : a, a = a === Kt && N ? Jt : a, a = a === Yt && I ? Gt : a, a = a === Gt && L ? Yt : a, o.className[Bt](a) === -1 && (o.className = o.className.replace(Vt, a)), f = y[V], h = y[Z], a === Yt || a === Gt ? (c = a === Yt ? m[Yt] + g.x - d.w - (b ? f : 0) : m[Yt] + g.x + w.w, T ? (r = m[Jt] + g.y, u = w.h / 2 - f) : A ? (r = m[Jt] + g.y - d.h + w.h, u = d.h - w.h / 2 - f) : (r = m[Jt] + g.y - d.h / 2 + w.h / 2, u = d.h / 2 - (b ? .9 * h : h / 2))) : a !== Jt && a !== Kt || (r = a === Jt ? m[Jt] + g.y - d.h - (b ? h : 0) : m[Jt] + g.y + w.h, x ? (c = 0, s = m[Yt] + w.w / 2 - f) : C ? (c = v - 1.01 * d.w, s = d.w - (v - m[Yt]) + w.w / 2 - f / 2) : (c = m[Yt] + g.x - d.w / 2 + w.w / 2, s = d.w / 2 - f / 2)), o[jt][Jt] = r + "px", o[jt][Yt] = c + "px", u && (y[jt][Jt] = u + "px"), s && (y[jt][Yt] = s + "px");
  };o.version = "2.0.24";var we = function we(t) {
    t = ce(t);var e = this,
        n = "alert",
        i = ue(t, "." + n),
        o = function o() {
      le(i, "fade") ? ve(i, l) : l();
    },
        a = function a(o) {
      i = ue(o[j], "." + n), t = ce("[" + r + '="' + n + '"]', i), t && i && (t === o[j] || t[Ut](o[j])) && e.close();
    },
        l = function l() {
      pe.call(i, pt, n), fe(t, ot, a), i[Dt].removeChild(i);
    };this.close = function () {
      i && t && le(i, Ft) && (pe.call(i, vt, n), ae(i, Ft), i && o());
    }, s in t || se(t, ot, a), t[s] = e;
  };a[qt]([s, we, "[" + r + '="alert"]']);var be = function be(t) {
    t = ce(t);var n = !1,
        i = "button",
        o = "checked",
        a = "LABEL",
        l = "INPUT",
        r = function r(t) {
      var n = t.which || t.keyCode;32 === n && t[j] === e.activeElement && u(t);
    },
        c = function c(t) {
      var e = t.which || t.keyCode;32 === e && t[It]();
    },
        u = function u(e) {
      var r = e[j].tagName === a ? e[j] : e[j][Dt].tagName === a ? e[j][Dt] : null;if (r) {
        var c = e[j],
            u = re(c[Dt], "btn"),
            s = r[kt](l)[0];if (s) {
          if ("checkbox" === s.type && (s[o] ? (ae(r, zt), s[bt](o), s.removeAttribute(o), s[o] = !1) : (oe(r, zt), s[bt](o), s[yt](o, o), s[o] = !0), n || (n = !0, pe.call(s, wt, i), pe.call(t, wt, i))), "radio" === s.type && !n && !s[o]) {
            oe(r, zt), s[yt](o, o), s[o] = !0, pe.call(s, wt, i), pe.call(t, wt, i), n = !0;for (var f = 0, h = u[Mt]; f < h; f++) {
              var d = u[f],
                  v = d[kt](l)[0];d !== r && le(d, zt) && (ae(d, zt), v.removeAttribute(o), v[o] = !1, pe.call(v, wt, i));
            }
          }setTimeout(function () {
            n = !1;
          }, 50);
        }
      }
    };f in t || (se(t, ot, u), ce("[" + Rt + "]", t) && se(t, rt, r), se(t, lt, c));for (var s = re(t, "btn"), h = s[Mt], d = 0; d < h; d++) {
      !le(s[d], zt) && ce("input:checked", s[d]) && oe(s[d], zt);
    }t[f] = this;
  };a[qt]([f, be, "[" + l + '="buttons"]']);var ye = function ye(i, o) {
    i = ce(i), o = o || {};var a = i[bt](C),
        l = o[q],
        r = "false" === a ? 0 : parseInt(a),
        c = i[bt](k) === at || !1,
        u = "true" === i[bt](T) || !1,
        s = "carousel",
        f = "paused",
        d = "direction",
        v = "carousel-item",
        p = "data-slide-to";this[H] = o[H] === !0 || u, this[R] = !(o[R] !== at && !c) && at, this[q] = "number" == typeof l ? l : l === !1 || 0 === r || r === !1 ? 0 : 5e3;var m = this,
        g = i.index = 0,
        w = i.timer = 0,
        b = !1,
        y = re(i, v),
        x = y[Mt],
        A = this[d] = Yt,
        I = re(i, s + "-control-prev")[0],
        N = re(i, s + "-control-next")[0],
        L = ce("." + s + "-indicators", i),
        E = L && L[kt]("LI") || [],
        S = function S() {
      m[q] === !1 || le(i, f) || (oe(i, f), !b && clearInterval(w));
    },
        B = function B() {
      m[q] !== !1 && le(i, f) && (ae(i, f), !b && clearInterval(w), !b && m.cycle());
    },
        D = function D(t) {
      if (t[It](), !b) {
        var e = t[j];if (!e || le(e, zt) || !e[bt](p)) return !1;g = parseInt(e[bt](p), 10), m.slideTo(g);
      }
    },
        M = function M(t) {
      if (t[It](), !b) {
        var e = t.currentTarget || t.srcElement;e === N ? g++ : e === I && g--, m.slideTo(g);
      }
    },
        P = function P(t) {
      if (!b) {
        switch (t.which) {case 39:
            g++;break;case 37:
            g--;break;default:
            return;}m.slideTo(g);
      }
    },
        O = function O() {
      var e = i[Nt](),
          o = t[_] || n[Q];return e[Jt] <= o && e[Kt] >= 0;
    },
        W = function W(t) {
      for (var e = 0, n = E[Mt]; e < n; e++) {
        ae(E[e], zt);
      }E[t] && oe(E[t], zt);
    };this.cycle = function () {
      w = setInterval(function () {
        O() && (g++, m.slideTo(g));
      }, this[q]);
    }, this.slideTo = function (t) {
      if (!b) {
        var n,
            o = this.getActiveIndex();o !== t && (o < t || 0 === o && t === x - 1 ? A = m[d] = Yt : (o > t || o === x - 1 && 0 === t) && (A = m[d] = Gt), t < 0 ? t = x - 1 : t === x && (t = 0), g = t, n = A === Yt ? "next" : "prev", pe.call(i, gt, s, y[t]), b = !0, clearInterval(w), W(t), te && le(i, "slide") ? (oe(y[t], v + "-" + n), y[t][V], oe(y[t], v + "-" + A), oe(y[o], v + "-" + A), he(y[t], ee, function (a) {
          var l = a[j] !== y[t] ? 1e3 * a.elapsedTime + 100 : 20;b && setTimeout(function () {
            b = !1, oe(y[t], zt), ae(y[o], zt), ae(y[t], v + "-" + n), ae(y[t], v + "-" + A), ae(y[o], v + "-" + A), pe.call(i, mt, s, y[t]), e.hidden || !m[q] || le(i, f) || m.cycle();
          }, l);
        })) : (oe(y[t], zt), y[t][V], ae(y[o], zt), setTimeout(function () {
          b = !1, m[q] && !le(i, f) && m.cycle(), pe.call(i, mt, s, y[t]);
        }, 100)));
      }
    }, this.getActiveIndex = function () {
      return y[Bt](re(i, v + " active")[0]) || 0;
    }, h in i || (m[R] && m[q] && (se(i, Qt[0], S), se(i, Qt[1], B), se(i, "touchstart", S), se(i, "touchend", B)), N && se(N, ot, M), I && se(I, ot, M), L && se(L, ot, D), m[H] === !0 && se(t, lt, P)), m.getActiveIndex() < 0 && (y[Mt] && oe(y[0], zt), E[Mt] && W(0)), m[q] && m.cycle(), i[h] = m;
  };a[qt]([h, ye, "[" + u + '="carousel"]']);var Te = function Te(t, e) {
    t = ce(t), e = e || {};var n,
        i,
        o = null,
        a = null,
        r = this,
        c = t[bt]("data-parent"),
        u = "collapse",
        s = "collapsed",
        f = "isAnimating",
        h = function h(t, e) {
      pe.call(t, st, u), t[f] = !0, oe(t, Xt), ae(t, u), t[jt][et] = t[tt] + "px", ve(t, function () {
        t[f] = !1, t[yt](nt, "true"), e[yt](nt, "true"), ae(t, Xt), oe(t, u), oe(t, Ft), t[jt][et] = "", pe.call(t, ft, u);
      });
    },
        v = function v(t, e) {
      pe.call(t, ht, u), t[f] = !0, t[jt][et] = t[tt] + "px", ae(t, u), ae(t, Ft), oe(t, Xt), t[V], t[jt][et] = "0px", ve(t, function () {
        t[f] = !1, t[yt](nt, "false"), e[yt](nt, "false"), ae(t, Xt), oe(t, u), t[jt][et] = "", pe.call(t, dt, u);
      });
    },
        p = function p() {
      var e = t.href && t[bt]("href"),
          n = t[bt](x),
          i = e || n && "#" === n.charAt(0) && n;return i && ce(i);
    };this.toggle = function (t) {
      t[It](), le(a, Ft) ? r.hide() : r.show();
    }, this.hide = function () {
      a[f] || (v(a, t), oe(t, s));
    }, this.show = function () {
      o && (n = ce("." + u + "." + Ft, o), i = n && (ce("[" + l + '="' + u + '"][' + x + '="#' + n.id + '"]', o) || ce("[" + l + '="' + u + '"][href="#' + n.id + '"]', o))), (!a[f] || n && !n[f]) && (i && n !== a && (v(n, i), oe(i, s)), h(a, t), ae(t, s));
    }, d in t || se(t, ot, r.toggle), a = p(), a[f] = !1, o = ce(e.parent) || c && ue(t, c), t[d] = r;
  };a[qt]([d, Te, "[" + l + '="collapse"]']);var xe = function xe(t, n) {
    t = ce(t), this.persist = n === !0 || "true" === t[bt]("data-persist") || !1;var i = this,
        o = "children",
        a = t[Dt],
        l = "dropdown",
        r = "open",
        c = null,
        u = ce(".dropdown-menu", a),
        s = function () {
      for (var t = u[o], e = [], n = 0; n < t[Mt]; n++) {
        t[n][o][Mt] && "A" === t[n][o][0].tagName && e[qt](t[n][o][0]), "A" === t[n].tagName && e[qt](t[n]);
      }return e;
    }(),
        f = function f(t) {
      (t.href && "#" === t.href.slice(-1) || t[Dt] && t[Dt].href && "#" === t[Dt].href.slice(-1)) && this[It]();
    },
        h = function h() {
      var n = t[r] ? se : fe;n(e, ot, d), n(e, lt, m), n(e, rt, g);
    },
        d = function d(e) {
      var n = e[j],
          o = n && (v in n || v in n[Dt]);(n !== u && !u[Ut](n) || !i.persist && !o) && (c = n === t || t[Ut](n) ? t : null, b(), f.call(e, n));
    },
        p = function p(e) {
      c = t, w(), f.call(e, e[j]);
    },
        m = function m(t) {
      var e = t.which || t.keyCode;38 !== e && 40 !== e || t[It]();
    },
        g = function g(n) {
      var o = n.which || n.keyCode,
          a = e.activeElement,
          l = s[Bt](a),
          f = a === t,
          h = u[Ut](a),
          d = a[Dt] === u || a[Dt][Dt] === u;(d || f) && (l = f ? 0 : 38 === o ? l > 1 ? l - 1 : 0 : 40 === o && l < s[Mt] - 1 ? l + 1 : l, s[l] && ie(s[l])), (s[Mt] && d || !s[Mt] && (h || f) || !h) && t[r] && 27 === o && (i.toggle(), c = null);
    },
        w = function w() {
      pe.call(a, st, l, c), oe(u, Ft), oe(a, Ft), u[yt](nt, !0), pe.call(a, ft, l, c), t[r] = !0, fe(t, ot, p), setTimeout(function () {
        ie(u[kt]("INPUT")[0] || t), h();
      }, 1);
    },
        b = function b() {
      pe.call(a, ht, l, c), ae(u, Ft), ae(a, Ft), u[yt](nt, !1), pe.call(a, dt, l, c), t[r] = !1, h(), ie(t), setTimeout(function () {
        se(t, ot, p);
      }, 1);
    };t[r] = !1, this.toggle = function () {
      le(a, Ft) && t[r] ? b() : w();
    }, v in t || (!Rt in u && u[yt](Rt, "0"), se(t, ot, p)), t[v] = i;
  };a[qt]([v, xe, "[" + l + '="dropdown"]']);var Ce = function Ce(o, a) {
    o = ce(o);var l,
        c = o[bt](x) || o[bt]("href"),
        u = ce(c),
        s = le(o, "modal") ? o : u,
        f = "modal",
        h = "static",
        d = "paddingLeft",
        v = "paddingRight",
        m = "modal-backdrop";if (le(o, "modal") && (o = null), s) {
      a = a || {}, this[H] = a[H] !== !1 && "false" !== s[bt](T), this[P] = a[P] !== h && s[bt](y) !== h || h, this[P] = a[P] !== !1 && "false" !== s[bt](y) && this[P], this[W] = a[W];var g,
          w,
          b,
          C,
          A = this,
          k = null,
          I = re(n, $t).concat(re(n, _t)),
          N = function N() {
        var e = n[Nt]();return t[$] || e[Gt] - Math.abs(e[Yt]);
      },
          L = function L() {
        var n,
            o = t[St](e[i]),
            a = parseInt(o[v], 10);if (g && (e[i][jt][v] = a + b + "px", I[Mt])) for (var l = 0; l < I[Mt]; l++) {
          n = t[St](I[l])[v], I[l][jt][v] = parseInt(n) + b + "px";
        }
      },
          E = function E() {
        if (e[i][jt][v] = "", I[Mt]) for (var t = 0; t < I[Mt]; t++) {
          I[t][jt][v] = "";
        }
      },
          S = function S() {
        var t,
            n = e[xt]("div");return n.className = f + "-scrollbar-measure", e[i][Ct](n), t = n[V] - n[K], e[i].removeChild(n), t;
      },
          B = function B() {
        g = e[i][K] < N(), w = s[tt] > n[Q], b = S();
      },
          D = function D() {
        s[jt][d] = !g && w ? b + "px" : "", s[jt][v] = g && !w ? b + "px" : "";
      },
          M = function M() {
        s[jt][d] = "", s[jt][v] = "";
      },
          O = function O() {
        Zt = 1;var t = e[xt]("div");C = ce("." + m), null === C && (t[yt]("class", m + " fade"), C = t, e[i][Ct](C));
      },
          q = function q() {
        C = ce("." + m), C && null !== C && "object" == (typeof C === "undefined" ? "undefined" : _typeof(C)) && (Zt = 0, e[i].removeChild(C), C = null), pe.call(s, dt, f);
      },
          R = function R() {
        le(s, Ft) ? se(e, lt, G) : fe(e, lt, G);
      },
          U = function U() {
        le(s, Ft) ? se(t, ct, A.update) : fe(t, ct, A.update);
      },
          z = function z() {
        le(s, Ft) ? se(s, ot, J) : fe(s, ot, J);
      },
          F = function F() {
        ie(s), pe.call(s, ft, f, k);
      },
          X = function X() {
        s[jt].display = "", o && ie(o), function () {
          re(e, f + " " + Ft)[0] || (M(), E(), ae(e[i], f + "-open"), C && le(C, "fade") ? (ae(C, Ft), ve(C, q)) : q(), U(), z(), R());
        }();
      },
          Y = function Y(t) {
        var e = t[j];e = e[Tt](x) || e[Tt]("href") ? e : e[Dt], e !== o || le(s, Ft) || (s.modalTrigger = o, k = o, A.show(), t[It]());
      },
          G = function G(t) {
        A[H] && 27 == t.which && le(s, Ft) && A.hide();
      },
          J = function J(t) {
        var e = t[j];le(s, Ft) && (e[Dt][bt](r) === f || e[bt](r) === f || e === s && A[P] !== h) && (A.hide(), k = null, t[It]());
      };this.toggle = function () {
        le(s, Ft) ? this.hide() : this.show();
      }, this.show = function () {
        pe.call(s, st, f, k);var t = re(e, f + " " + Ft)[0];t && t !== s && t.modalTrigger[p].hide(), this[P] && !Zt && O(), C && Zt && !le(C, Ft) && (C[V], l = de(C), oe(C, Ft)), setTimeout(function () {
          s[jt].display = "block", B(), L(), D(), oe(e[i], f + "-open"), oe(s, Ft), s[yt](it, !1), U(), z(), R(), le(s, "fade") ? ve(s, F) : F();
        }, te && C ? l : 0);
      }, this.hide = function () {
        pe.call(s, ht, f), C = ce("." + m), l = C && de(C), ae(s, Ft), s[yt](it, !0), setTimeout(function () {
          le(s, "fade") ? ve(s, X) : X();
        }, te && C ? l : 0);
      }, this.setContent = function (t) {
        ce("." + f + "-content", s)[At] = t;
      }, this.update = function () {
        le(s, Ft) && (B(), L(), D());
      }, !o || p in o || se(o, ot, Y), A[W] && A.setContent(A[W]), !!o && (o[p] = A);
    }
  };a[qt]([p, Ce, "[" + l + '="modal"]']);var Ae = function Ae(n, o) {
    n = ce(n), o = o || {};var a = n[bt](E),
        l = n[bt](S),
        r = n[bt](D),
        c = n[bt](L),
        u = n[bt](M),
        s = n[bt](B),
        f = "popover",
        h = "template",
        d = "trigger",
        v = "class",
        p = "div",
        g = "fade",
        w = "data-content",
        b = "dismissible",
        y = '<button type="button" class="close">×</button>',
        T = ce(o[F]),
        x = ce(s),
        C = ue(n, ".modal"),
        A = ue(n, "." + $t),
        k = ue(n, "." + _t);this[h] = o[h] ? o[h] : null, this[d] = o[d] ? o[d] : a || at, this[U] = o[U] && o[U] !== g ? o[U] : l || g, this[z] = o[z] ? o[z] : r || Jt, this[O] = parseInt(o[O] || u) || 200, this[b] = !(!o[b] && "true" !== c), this[F] = T ? T : x ? x : A ? A : k ? k : C ? C : e[i];var N = this,
        P = n[bt](I) || null,
        H = n[bt](w) || null;if (H || this[h]) {
      var W = null,
          q = 0,
          R = this[z],
          X = function X(t) {
        null !== W && t[j] === ce(".close", W) && N.hide();
      },
          Y = function Y() {
        N[F].removeChild(W), q = null, W = null;
      },
          G = function G() {
        P = n[bt](I), H = n[bt](w), W = e[xt](p);var t = e[xt](p);if (t[yt](v, "arrow"), W[Ct](t), null !== H && null === N[h]) {
          if (W[yt]("role", "tooltip"), null !== P) {
            var i = e[xt]("h3");i[yt](v, f + "-header"), i[At] = N[b] ? P + y : P, W[Ct](i);
          }var o = e[xt](p);o[yt](v, f + "-body"), o[At] = N[b] && null === P ? H + y : H, W[Ct](o);
        } else {
          var a = e[xt](p);a[At] = N[h], W[At] = a.firstChild[At];
        }N[F][Ct](W), W[jt].display = "block", W[yt](v, f + " bs-" + f + "-" + R + " " + N[U]);
      },
          J = function J() {
        !le(W, Ft) && oe(W, Ft);
      },
          K = function K() {
        ge(n, W, R, N[F]);
      },
          Q = function Q(i) {
        ot != N[d] && "focus" != N[d] || !N[b] && i(n, "blur", N.hide), N[b] && i(e, ot, X), i(t, ct, N.hide);
      },
          V = function V() {
        Q(se), pe.call(n, ft, f);
      },
          Z = function Z() {
        Q(fe), Y(), pe.call(n, dt, f);
      };this.toggle = function () {
        null === W ? N.show() : N.hide();
      }, this.show = function () {
        clearTimeout(q), q = setTimeout(function () {
          null === W && (R = N[z], G(), K(), J(), pe.call(n, st, f), N[U] ? ve(W, V) : V());
        }, 20);
      }, this.hide = function () {
        clearTimeout(q), q = setTimeout(function () {
          W && null !== W && le(W, Ft) && (pe.call(n, ht, f), ae(W, Ft), N[U] ? ve(W, Z) : Z());
        }, N[O]);
      }, m in n || (N[d] === at ? (se(n, Qt[0], N.show), N[b] || se(n, Qt[1], N.hide)) : ot != N[d] && "focus" != N[d] || se(n, N[d], N.toggle)), n[m] = N;
    }
  };a[qt]([m, Ae, "[" + l + '="popover"]']);var ke = function ke(e, n) {
    e = ce(e);var i = ce(e[bt](x)),
        o = e[bt]("data-offset");if (n = n || {}, n[j] || i) {
      for (var a, l = this, r = n[j] && ce(n[j]) || i, c = r && r[kt]("A"), u = parseInt(o || n.offset) || 10, s = [], f = [], h = e[Z] < e[tt] ? e : t, d = h === t, v = 0, p = c[Mt]; v < p; v++) {
        var m = c[v][bt]("href"),
            w = m && "#" === m.charAt(0) && "#" !== m.slice(-1) && ce(m);w && (s[qt](c[v]), f[qt](w));
      }var b = function b(t) {
        var n = s[t],
            i = f[t],
            o = n[Dt][Dt],
            l = le(o, "dropdown") && o[kt]("A")[0],
            r = d && i[Nt](),
            c = le(n, zt) || !1,
            h = (d ? r[Jt] + a : i[X]) - u,
            v = d ? r[Kt] + a - u : f[t + 1] ? f[t + 1][X] - u : e[tt],
            p = a >= h && v > a;if (!c && p) le(n, zt) || (oe(n, zt), l && !le(l, zt) && oe(l, zt), pe.call(e, "activate", "scrollspy", s[t]));else if (p) {
          if (!p && !c || c && p) return;
        } else le(n, zt) && (ae(n, zt), l && le(l, zt) && !re(n[Dt], zt).length && ae(l, zt));
      },
          y = function y() {
        a = d ? me().y : e[G];for (var t = 0, n = s[Mt]; t < n; t++) {
          b(t);
        }
      };this.refresh = function () {
        y();
      }, g in e || (se(h, ut, l.refresh), se(t, ct, l.refresh)), l.refresh(), e[g] = l;
    }
  };a[qt]([g, ke, "[" + c + '="scroll"]']);var Ie = function Ie(t, e) {
    t = ce(t);var n = t[bt](A),
        i = "tab",
        o = "height",
        a = "float",
        r = "isAnimating";e = e || {}, this[o] = !!te && (e[o] || "true" === n);var c,
        u,
        s,
        f,
        h,
        d,
        v,
        p = this,
        m = ue(t, ".nav"),
        g = !1,
        b = m && ce(".dropdown-toggle", m),
        y = function y() {
      g[jt][o] = "", ae(g, Xt), m[r] = !1;
    },
        T = function T() {
      g ? d ? y() : setTimeout(function () {
        g[jt][o] = v + "px", g[V], ve(g, y);
      }, 50) : m[r] = !1, pe.call(c, ft, i, u);
    },
        x = function x() {
      g && (s[jt][a] = Yt, f[jt][a] = Yt, h = s[tt]), oe(f, zt), pe.call(c, st, i, u), ae(s, zt), pe.call(u, dt, i, c), g && (v = f[tt], d = v === h, oe(g, Xt), g[jt][o] = h + "px", g[Z], s[jt][a] = "", f[jt][a] = ""), le(f, "fade") ? setTimeout(function () {
        oe(f, Ft), ve(f, T);
      }, 20) : T();
    };if (m) {
      m[r] = !1;var C = function C() {
        var t,
            e = re(m, zt);return 1 !== e[Mt] || le(e[0][Dt], "dropdown") ? e[Mt] > 1 && (t = e[e[Mt] - 1]) : t = e[0], t;
      },
          k = function k() {
        return ce(C()[bt]("href"));
      },
          I = function I(t) {
        var e = t[j][bt]("href");t[It](), c = t[j][bt](l) === i || e && "#" === e.charAt(0) ? t[j] : t[j][Dt], !m[r] && !le(c, zt) && p.show();
      };this.show = function () {
        c = c || t, f = ce(c[bt]("href")), u = C(), s = k(), m[r] = !0, ae(u, zt), oe(c, zt), b && (le(t[Dt], "dropdown-menu") ? le(b, zt) || oe(b, zt) : le(b, zt) && ae(b, zt)), pe.call(u, ht, i, c), le(s, "fade") ? (ae(s, Ft), ve(s, x)) : x();
      }, w in t || se(t, ot, I), p[o] && (g = k()[Dt]), t[w] = p;
    }
  };a[qt]([w, Ie, "[" + l + '="tab"]']);var Ne = function Ne(n, o) {
    n = ce(n), o = o || {};var a = n[bt](S),
        l = n[bt](D),
        r = n[bt](M),
        c = n[bt](B),
        u = "tooltip",
        s = "class",
        f = "title",
        h = "fade",
        d = "div",
        v = ce(o[F]),
        p = ce(c),
        m = ue(n, ".modal"),
        g = ue(n, "." + $t),
        w = ue(n, "." + _t);this[U] = o[U] && o[U] !== h ? o[U] : a || h, this[z] = o[z] ? o[z] : l || Jt, this[O] = parseInt(o[O] || r) || 200, this[F] = v ? v : p ? p : g ? g : w ? w : m ? m : e[i];var y = this,
        T = 0,
        x = this[z],
        C = null,
        A = n[bt](f) || n[bt](I) || n[bt](N);if (A && "" != A) {
      var k = function k() {
        y[F].removeChild(C), C = null, T = null;
      },
          L = function L() {
        if (A = n[bt](f) || n[bt](I) || n[bt](N), !A || "" == A) return !1;C = e[xt](d), C[yt]("role", u);var t = e[xt](d);t[yt](s, "arrow"), C[Ct](t);var i = e[xt](d);i[yt](s, u + "-inner"), C[Ct](i), i[At] = A, y[F][Ct](C), C[yt](s, u + " bs-" + u + "-" + x + " " + y[U]);
      },
          E = function E() {
        ge(n, C, x, y[F]);
      },
          P = function P() {
        !le(C, Ft) && oe(C, Ft);
      },
          H = function H() {
        se(t, ct, y.hide), pe.call(n, ft, u);
      },
          W = function W() {
        fe(t, ct, y.hide), k(), pe.call(n, dt, u);
      };this.show = function () {
        clearTimeout(T), T = setTimeout(function () {
          if (null === C) {
            if (x = y[z], 0 == L()) return;E(), P(), pe.call(n, st, u), y[U] ? ve(C, H) : H();
          }
        }, 20);
      }, this.hide = function () {
        clearTimeout(T), T = setTimeout(function () {
          C && le(C, Ft) && (pe.call(n, ht, u), ae(C, Ft), y[U] ? ve(C, W) : W());
        }, y[O]);
      }, this.toggle = function () {
        C ? y.hide() : y.show();
      }, b in n || (n[yt](N, A), n.removeAttribute(f), se(n, Qt[0], y.show), se(n, Qt[1], y.hide)), n[b] = y;
    }
  };a[qt]([b, Ne, "[" + l + '="tooltip"]']);var Le = function Le(t, e) {
    for (var n = 0, i = e[Mt]; n < i; n++) {
      new t(e[n]);
    }
  },
      Ee = o.initCallback = function (t) {
    t = t || e;for (var n = 0, i = a[Mt]; n < i; n++) {
      Le(a[n][1], t[Lt](a[n][2]));
    }
  };return e[i] ? Ee() : se(e, "DOMContentLoaded", function () {
    Ee();
  }), { Alert: we, Button: be, Carousel: ye, Collapse: Te, Dropdown: xe, Modal: Ce, Popover: Ae, ScrollSpy: ke, Tab: Ie, Tooltip: Ne };
});
// Native JavaScript for Bootstrap 3/4 Polyfill
(function () {
  var F = "Document",
      i = document,
      g = this[F] || this.HTMLDocument,
      l = "Window",
      E = window,
      p = this.constructor || this[l] || Window,
      u = "HTMLElement",
      k = "documentElement",
      D = Element,
      J = "className",
      d = "add",
      c = "classList",
      x = "remove",
      z = "contains",
      s = "class",
      e = "setAttribute",
      A = "getAttribute",
      t = "prototype",
      o = "indexOf",
      r = "length",
      y = "split",
      b = "trim",
      f = "Event",
      I = "CustomEvent",
      C = "_events",
      n = "type",
      a = "target",
      m = "currentTarget",
      B = "relatedTarget",
      v = "cancelable",
      q = "bubbles",
      w = "cancelBubble",
      H = "cancelImmediate",
      K = "detail",
      L = "addEventListener",
      h = "removeEventListener",
      j = "dispatchEvent";if (!E[u]) {
    E[u] = E[D];
  }if (!Array[t][o]) {
    Array[t][o] = function (O) {
      if (this === undefined || this === null) {
        throw new TypeError(this + " is not an object");
      }var N = this instanceof String ? this[y]("") : this,
          P = Math.max(Math.min(N[r], 9007199254740991), 0) || 0,
          M = Number(arguments[1]) || 0;M = (M < 0 ? Math.max(P + M, 0) : M) - 1;while (++M < P) {
        if (M in N && N[M] === O) {
          return M;
        }
      }return -1;
    };
  }if (!String[t][b]) {
    String[t][b] = function () {
      return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
    };
  }if (!(c in D[t])) {
    var G = function G(M) {
      var N = (M[A](s) || "").replace(/^\s+|\s+$/g, "")[y](/\s+/) || [];hasClass = this[z] = function (O) {
        return N[o](O) > -1;
      }, addClass = this[d] = function (O) {
        if (!hasClass(O)) {
          N.push(O);M[e](s, N.join(" "));
        }
      }, removeClass = this[x] = function (O) {
        if (hasClass(O)) {
          N.splice(N[o](O), 1);M[e](s, N.join(" "));
        }
      }, toggleClass = this.toggle = function (O) {
        if (hasClass(O)) {
          removeClass(O);
        } else {
          addClass(O);
        }
      };
    };Object.defineProperty(D[t], c, { get: function get() {
        return new G(this);
      } });
  }if (!E[f] || !p[t][f]) {
    E[f] = p[t][f] = g[t][f] = D[t][f] = function (O, Q) {
      if (!O) {
        throw new Error("Not enough arguments");
      }var P,
          N = Q && Q[q] !== undefined ? Q[q] : false,
          M = Q && Q[v] !== undefined ? Q[v] : false;if ("createEvent" in i) {
        P = i.createEvent(f);P.initEvent(O, N, M);
      } else {
        P = i.createEventObject();P[n] = O;P[q] = N;P[v] = M;
      }return P;
    };
  }if (!(I in E) || !(I in p[t])) {
    E[I] = p[t][I] = g[t][I] = Element[t][I] = function (M, O) {
      if (!M) {
        throw Error("CustomEvent TypeError: An event name must be provided.");
      }var N = new Event(M, O);N[K] = O && O[K] || null;return N;
    };
  }if (!E[L] || !p[t][L]) {
    E[L] = p[t][L] = g[t][L] = D[t][L] = function () {
      var M = this,
          N = arguments[0],
          O = arguments[1];if (!M[C]) {
        M[C] = {};
      }if (!M[C][N]) {
        M[C][N] = function (T) {
          var U = M[C][T[n]].list,
              R = U.slice(),
              Q = -1,
              S = R[r],
              P;T.preventDefault = function () {
            if (T[v] !== false) {
              T.returnValue = false;
            }
          };T.stopPropagation = function () {
            T[w] = true;
          };T.stopImmediatePropagation = function () {
            T[w] = true;T[H] = true;
          };T[m] = M;T[B] = T[B] || T.fromElement || null;T[a] = T[a] || T.srcElement || M;T.timeStamp = new Date().getTime();if (T.clientX) {
            T.pageX = T.clientX + i[k].scrollLeft;T.pageY = T.clientY + i[k].scrollTop;
          }while (++Q < S && !T[H]) {
            if (Q in R) {
              P = R[Q];if (U[o](P) !== -1 && typeof P === "function") {
                P.call(M, T);
              }
            }
          }
        };M[C][N].list = [];if (M.attachEvent) {
          M.attachEvent("on" + N, M[C][N]);
        }
      }M[C][N].list.push(O);
    };E[h] = p[t][h] = g[t][h] = D[t][h] = function () {
      var N = this,
          O = arguments[0],
          P = arguments[1],
          M;if (N[C] && N[C][O] && N[C][O].list) {
        M = N[C][O].list[o](P);if (M !== -1) {
          N[C][O].list.splice(M, 1);if (!N[C][O].list[r]) {
            if (N.detachEvent) {
              N.detachEvent("on" + O, N[C][O]);
            }delete N[C][O];
          }
        }
      }
    };
  }if (!E[j] || !p[t][j] || !g[t][j] || !D[t][j]) {
    E[j] = p[t][j] = g[t][j] = D[t][j] = function (Q) {
      if (!arguments[r]) {
        throw new Error("Not enough arguments");
      }if (!Q || typeof Q[n] !== "string") {
        throw new Error("DOM Events Exception 0");
      }var N = this,
          P = Q[n];try {
        if (!Q[q]) {
          Q[w] = true;var O = function O(R) {
            R[w] = true;(N || E).detachEvent("on" + P, O);
          };this.attachEvent("on" + P, O);
        }this.fireEvent("on" + P, Q);
      } catch (M) {
        Q[a] = N;do {
          Q[m] = N;if (C in N && typeof N[C][P] === "function") {
            N[C][P].call(N, Q);
          }if (typeof N["on" + P] === "function") {
            N["on" + P].call(N, Q);
          }N = N.nodeType === 9 ? N.parentWindow : N.parentNode;
        } while (N && !Q[w]);
      }return true;
    };
  }
})();
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

// //****************************************************************************
// //*********************************** SORT ***********************************
// //****************************************************************************
function j2HTMLSort(id, sel, sortvalue) {

    var a, b, i, ii, y, bytt, v1, v2, cc, j;

    if ((typeof id === "undefined" ? "undefined" : _typeof(id)) == "object") {
        a = [id];
    } else {
        a = document.querySelectorAll(id);
    }
    for (i = 0; i < a.length; i++) {
        for (j = 0; j < 2; j++) {
            cc = 0;
            y = 1;
            while (y == 1) {
                y = 0;
                b = a[i].querySelectorAll(sel);
                for (ii = 0; ii < b.length - 1; ii++) {
                    bytt = 0;
                    if (sortvalue) {
                        v1 = b[ii].querySelector(sortvalue).innerHTML.toLowerCase();
                        v2 = b[ii + 1].querySelector(sortvalue).innerHTML.toLowerCase();
                    } else {
                        v1 = b[ii].innerHTML.toLowerCase();
                        v2 = b[ii + 1].innerHTML.toLowerCase();
                    }
                    if (j === 0 && v1 > v2 || j == 1 && v1 < v2) {
                        bytt = 1;
                        break;
                    }
                }
                if (bytt == 1) {
                    b[ii].parentNode.insertBefore(b[ii + 1], b[ii]);
                    y = 1;
                    cc++;
                }
            }
            if (cc > 0) {
                break;
            }
        }
    }
}

// //****************************************************************************
// //******************************** FILTER ************************************
// //****************************************************************************
function j2HTMLFilter(id, sel, filter) {
    var a, b, c, i, ii, iii, hit;
    a = getElements(id);
    for (i = 0; i < a.length; i++) {
        b = getElements(sel);
        for (ii = 0; ii < b.length; ii++) {
            hit = 0;
            if (b[ii].innerHTML.toUpperCase().indexOf(filter.toUpperCase()) > -1) {
                hit = 1;
            }
            c = b[ii].getElementsByTagName("*");
            for (iii = 0; iii < c.length; iii++) {
                if (c[iii].innerHTML.toUpperCase().indexOf(filter.toUpperCase()) > -1) {
                    hit = 1;
                }
            }
            if (hit == 1) {
                b[ii].style.display = "";
            } else {
                b[ii].style.display = "none";
            }
        }
    }
}

//GET ELEMENT
function getElements(id) {
    if ((typeof id === "undefined" ? "undefined" : _typeof(id)) == "object") {
        return [id];
    } else {
        return document.querySelectorAll(id);
    }
}

//CONVERT TO CAMEL CASE
function toTitleCase(str) {
    return str.replace(/(?:^|\s)\w/g, function (match) {
        return match.toUpperCase();
    });
}

function getSelected() {

    var args = arguments[0];

    var groupName = 'chkGroupName';
    var elementType = false;
    var getSelected = false;
    var error = '';

    if (args.GroupName === undefined && (args.ElementType.toLowerCase() === "checkbox" || args.ElementType.toLowerCase() === "radiobutton")) {
        error += 'Group name is required\n';
    } else {

        if (args.GroupName !== undefined) {

            var elem = getElementByName(args.GroupName);

            if (elem === null) {
                error += 'GroupName not found\n';
            } else {
                groupName = args.GroupName;
            }

            // if ($('input[name=' + args.GroupName + ']').length === 0) {
            //     error += 'GroupName not found\n';
            // } else {
            //     groupName = args.GroupName;
            // }
        }
    }

    if (args.ElementType === undefined) {
        error += 'Type is required\n';
    } else {

        if (args.ElementType.toLowerCase() != "checkbox" && args.ElementType.toLowerCase() != "radiobutton" && args.ElementType.toLowerCase() != "dropdown") {
            error += "ElementType must be Checkbox or Radiobutoon or Dropdown\n";
        } else {
            elementType = args.ElementType;
        }
    }
    if (args.GetSelected === undefined) {
        error += 'Get is required';
    } else {

        if (args.GetSelected.toLowerCase() != "text" && args.GetSelected.toLowerCase() != "value" && args.GetSelected.toLowerCase() != "both") {
            error += "GetSelected must be Text or Value or Both\n";
        } else {
            getSelected = args.GetSelected;
        }
    }

    if (error === '') {

        var selectedVal;
        if (elementType.toLowerCase() === 'checkbox' || elementType.toLowerCase() === 'radiobutton') {

            selectedVal = GetSelectedCheckBoxOrRadioButton(groupName, elementType, getSelected);
        }

        return selectedVal;
    } else {
        alert(error);
    }
}

//GET CHECKBOX VAULE and/or TEXT
function GetSelectedCheckBoxOrRadioButton(groupName, elementType, get) {

    var selectedResult = [];
    //GET THE VALUE
    if (get.toLowerCase() === 'value') {

        $('input[name=' + groupName + ']').each(function () {
            if ($(this).is(":checked")) {

                var objSelectedValue = {};
                objSelectedValue.Value = $(this).val();
                //objSelectedResult.Value = $(this).val();
                selectedResult.push(objSelectedValue);
            }
        });
    }
    //GET THE TEXT
    if (get.toLowerCase() === 'text') {

        $('input[name=' + groupName + ']').each(function () {
            if ($(this).is(":checked")) {

                //objSelectedResult.Text = $(this).parent().text();
                var objSelectedText = {};
                objSelectedText.Text = $(this).parent().text();
                selectedResult.push(objSelectedText);
            }
        });
    }
    //GET BOTH VALUE AND TEXT
    if (get.toLowerCase() === 'both') {

        $('input[name=' + groupName + ']').each(function () {
            if ($(this).is(":checked")) {

                var objSelectedBoth = {};
                objSelectedBoth.Value = $(this).val();
                objSelectedBoth.Text = $(this).parent().text();
                selectedResult.push(objSelectedBoth);
            }
        });
    }

    return selectedResult;
}
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*--------------------------------------------------------------------------
* linq.js - LINQ for JavaScript
* ver 2.2.0.2 (Jan. 21th, 2011)
*
* created and maintained by neuecc <ils@neue.cc>
* licensed under Microsoft Public License(Ms-PL)
* http://neue.cc/
* http://linqjs.codeplex.com/
*--------------------------------------------------------------------------*/
Enumerable = function () {
  var m = "Single:sequence contains more than one element.",
      e = true,
      b = null,
      a = false,
      c = function c(a) {
    this.GetEnumerator = a;
  };c.Choice = function () {
    var a = arguments[0] instanceof Array ? arguments[0] : arguments;return new c(function () {
      return new f(g.Blank, function () {
        return this.Yield(a[Math.floor(Math.random() * a.length)]);
      }, g.Blank);
    });
  };c.Cycle = function () {
    var a = arguments[0] instanceof Array ? arguments[0] : arguments;return new c(function () {
      var b = 0;return new f(g.Blank, function () {
        if (b >= a.length) b = 0;return this.Yield(a[b++]);
      }, g.Blank);
    });
  };c.Empty = function () {
    return new c(function () {
      return new f(g.Blank, function () {
        return a;
      }, g.Blank);
    });
  };c.From = function (j) {
    if (j == b) return c.Empty();if (j instanceof c) return j;if ((typeof j === "undefined" ? "undefined" : _typeof(j)) == i.Number || (typeof j === "undefined" ? "undefined" : _typeof(j)) == i.Boolean) return c.Repeat(j, 1);if ((typeof j === "undefined" ? "undefined" : _typeof(j)) == i.String) return new c(function () {
      var b = 0;return new f(g.Blank, function () {
        return b < j.length ? this.Yield(j.charAt(b++)) : a;
      }, g.Blank);
    });if ((typeof j === "undefined" ? "undefined" : _typeof(j)) != i.Function) {
      if (_typeof(j.length) == i.Number) return new h(j);if (!(j instanceof Object) && d.IsIEnumerable(j)) return new c(function () {
        var c = e,
            b;return new f(function () {
          b = new Enumerator(j);
        }, function () {
          if (c) c = a;else b.moveNext();return b.atEnd() ? a : this.Yield(b.item());
        }, g.Blank);
      });
    }return new c(function () {
      var b = [],
          c = 0;return new f(function () {
        for (var a in j) {
          !(j[a] instanceof Function) && b.push({ Key: a, Value: j[a] });
        }
      }, function () {
        return c < b.length ? this.Yield(b[c++]) : a;
      }, g.Blank);
    });
  }, c.Return = function (a) {
    return c.Repeat(a, 1);
  };c.Matches = function (h, e, d) {
    if (d == b) d = "";if (e instanceof RegExp) {
      d += e.ignoreCase ? "i" : "";d += e.multiline ? "m" : "";e = e.source;
    }if (d.indexOf("g") === -1) d += "g";return new c(function () {
      var b;return new f(function () {
        b = new RegExp(e, d);
      }, function () {
        var c = b.exec(h);return c ? this.Yield(c) : a;
      }, g.Blank);
    });
  };c.Range = function (e, d, a) {
    if (a == b) a = 1;return c.ToInfinity(e, a).Take(d);
  };c.RangeDown = function (e, d, a) {
    if (a == b) a = 1;return c.ToNegativeInfinity(e, a).Take(d);
  };c.RangeTo = function (d, e, a) {
    if (a == b) a = 1;return d < e ? c.ToInfinity(d, a).TakeWhile(function (a) {
      return a <= e;
    }) : c.ToNegativeInfinity(d, a).TakeWhile(function (a) {
      return a >= e;
    });
  };c.Repeat = function (d, a) {
    return a != b ? c.Repeat(d).Take(a) : new c(function () {
      return new f(g.Blank, function () {
        return this.Yield(d);
      }, g.Blank);
    });
  };c.RepeatWithFinalize = function (a, e) {
    a = d.CreateLambda(a);e = d.CreateLambda(e);return new c(function () {
      var c;return new f(function () {
        c = a();
      }, function () {
        return this.Yield(c);
      }, function () {
        if (c != b) {
          e(c);c = b;
        }
      });
    });
  };c.Generate = function (a, e) {
    if (e != b) return c.Generate(a).Take(e);a = d.CreateLambda(a);return new c(function () {
      return new f(g.Blank, function () {
        return this.Yield(a());
      }, g.Blank);
    });
  };c.ToInfinity = function (d, a) {
    if (d == b) d = 0;if (a == b) a = 1;return new c(function () {
      var b;return new f(function () {
        b = d - a;
      }, function () {
        return this.Yield(b += a);
      }, g.Blank);
    });
  };c.ToNegativeInfinity = function (d, a) {
    if (d == b) d = 0;if (a == b) a = 1;return new c(function () {
      var b;return new f(function () {
        b = d + a;
      }, function () {
        return this.Yield(b -= a);
      }, g.Blank);
    });
  };c.Unfold = function (h, b) {
    b = d.CreateLambda(b);return new c(function () {
      var d = e,
          c;return new f(g.Blank, function () {
        if (d) {
          d = a;c = h;return this.Yield(c);
        }c = b(c);return this.Yield(c);
      }, g.Blank);
    });
  };c.prototype = { CascadeBreadthFirst: function CascadeBreadthFirst(g, b) {
      var h = this;g = d.CreateLambda(g);b = d.CreateLambda(b);return new c(function () {
        var i,
            k = 0,
            j = [];return new f(function () {
          i = h.GetEnumerator();
        }, function () {
          while (e) {
            if (i.MoveNext()) {
              j.push(i.Current());return this.Yield(b(i.Current(), k));
            }var f = c.From(j).SelectMany(function (a) {
              return g(a);
            });if (!f.Any()) return a;else {
              k++;j = [];d.Dispose(i);i = f.GetEnumerator();
            }
          }
        }, function () {
          d.Dispose(i);
        });
      });
    }, CascadeDepthFirst: function CascadeDepthFirst(g, b) {
      var h = this;g = d.CreateLambda(g);b = d.CreateLambda(b);return new c(function () {
        var j = [],
            i;return new f(function () {
          i = h.GetEnumerator();
        }, function () {
          while (e) {
            if (i.MoveNext()) {
              var f = b(i.Current(), j.length);j.push(i);i = c.From(g(i.Current())).GetEnumerator();return this.Yield(f);
            }if (j.length <= 0) return a;d.Dispose(i);i = j.pop();
          }
        }, function () {
          try {
            d.Dispose(i);
          } finally {
            c.From(j).ForEach(function (a) {
              a.Dispose();
            });
          }
        });
      });
    }, Flatten: function Flatten() {
      var h = this;return new c(function () {
        var j,
            i = b;return new f(function () {
          j = h.GetEnumerator();
        }, function () {
          while (e) {
            if (i != b) if (i.MoveNext()) return this.Yield(i.Current());else i = b;if (j.MoveNext()) if (j.Current() instanceof Array) {
              d.Dispose(i);i = c.From(j.Current()).SelectMany(g.Identity).Flatten().GetEnumerator();continue;
            } else return this.Yield(j.Current());return a;
          }
        }, function () {
          try {
            d.Dispose(j);
          } finally {
            d.Dispose(i);
          }
        });
      });
    }, Pairwise: function Pairwise(b) {
      var e = this;b = d.CreateLambda(b);return new c(function () {
        var c;return new f(function () {
          c = e.GetEnumerator();c.MoveNext();
        }, function () {
          var d = c.Current();return c.MoveNext() ? this.Yield(b(d, c.Current())) : a;
        }, function () {
          d.Dispose(c);
        });
      });
    }, Scan: function Scan(i, g, j) {
      if (j != b) return this.Scan(i, g).Select(j);var h;if (g == b) {
        g = d.CreateLambda(i);h = a;
      } else {
        g = d.CreateLambda(g);h = e;
      }var k = this;return new c(function () {
        var b,
            c,
            j = e;return new f(function () {
          b = k.GetEnumerator();
        }, function () {
          if (j) {
            j = a;if (!h) {
              if (b.MoveNext()) return this.Yield(c = b.Current());
            } else return this.Yield(c = i);
          }return b.MoveNext() ? this.Yield(c = g(c, b.Current())) : a;
        }, function () {
          d.Dispose(b);
        });
      });
    }, Select: function Select(b) {
      var e = this;b = d.CreateLambda(b);return new c(function () {
        var c,
            g = 0;return new f(function () {
          c = e.GetEnumerator();
        }, function () {
          return c.MoveNext() ? this.Yield(b(c.Current(), g++)) : a;
        }, function () {
          d.Dispose(c);
        });
      });
    }, SelectMany: function SelectMany(g, e) {
      var h = this;g = d.CreateLambda(g);if (e == b) e = function e(b, a) {
        return a;
      };e = d.CreateLambda(e);return new c(function () {
        var j,
            i = undefined,
            k = 0;return new f(function () {
          j = h.GetEnumerator();
        }, function () {
          if (i === undefined) if (!j.MoveNext()) return a;do {
            if (i == b) {
              var f = g(j.Current(), k++);i = c.From(f).GetEnumerator();
            }if (i.MoveNext()) return this.Yield(e(j.Current(), i.Current()));d.Dispose(i);i = b;
          } while (j.MoveNext());return a;
        }, function () {
          try {
            d.Dispose(j);
          } finally {
            d.Dispose(i);
          }
        });
      });
    }, Where: function Where(b) {
      b = d.CreateLambda(b);var e = this;return new c(function () {
        var c,
            g = 0;return new f(function () {
          c = e.GetEnumerator();
        }, function () {
          while (c.MoveNext()) {
            if (b(c.Current(), g++)) return this.Yield(c.Current());
          }return a;
        }, function () {
          d.Dispose(c);
        });
      });
    }, OfType: function OfType(c) {
      var a;switch (c) {case Number:
          a = i.Number;break;case String:
          a = i.String;break;case Boolean:
          a = i.Boolean;break;case Function:
          a = i.Function;break;default:
          a = b;}return a === b ? this.Where(function (a) {
        return a instanceof c;
      }) : this.Where(function (b) {
        return (typeof b === "undefined" ? "undefined" : _typeof(b)) === a;
      });
    }, Zip: function Zip(e, b) {
      b = d.CreateLambda(b);var g = this;return new c(function () {
        var i,
            h,
            j = 0;return new f(function () {
          i = g.GetEnumerator();h = c.From(e).GetEnumerator();
        }, function () {
          return i.MoveNext() && h.MoveNext() ? this.Yield(b(i.Current(), h.Current(), j++)) : a;
        }, function () {
          try {
            d.Dispose(i);
          } finally {
            d.Dispose(h);
          }
        });
      });
    }, Join: function Join(m, i, h, k, j) {
      i = d.CreateLambda(i);h = d.CreateLambda(h);k = d.CreateLambda(k);j = d.CreateLambda(j);var l = this;return new c(function () {
        var n,
            q,
            o = b,
            p = 0;return new f(function () {
          n = l.GetEnumerator();q = c.From(m).ToLookup(h, g.Identity, j);
        }, function () {
          while (e) {
            if (o != b) {
              var c = o[p++];if (c !== undefined) return this.Yield(k(n.Current(), c));c = b;p = 0;
            }if (n.MoveNext()) {
              var d = i(n.Current());o = q.Get(d).ToArray();
            } else return a;
          }
        }, function () {
          d.Dispose(n);
        });
      });
    }, GroupJoin: function GroupJoin(l, h, e, j, i) {
      h = d.CreateLambda(h);e = d.CreateLambda(e);j = d.CreateLambda(j);i = d.CreateLambda(i);var k = this;return new c(function () {
        var m = k.GetEnumerator(),
            n = b;return new f(function () {
          m = k.GetEnumerator();n = c.From(l).ToLookup(e, g.Identity, i);
        }, function () {
          if (m.MoveNext()) {
            var b = n.Get(h(m.Current()));return this.Yield(j(m.Current(), b));
          }return a;
        }, function () {
          d.Dispose(m);
        });
      });
    }, All: function All(b) {
      b = d.CreateLambda(b);var c = e;this.ForEach(function (d) {
        if (!b(d)) {
          c = a;return a;
        }
      });return c;
    }, Any: function Any(c) {
      c = d.CreateLambda(c);var b = this.GetEnumerator();try {
        if (arguments.length == 0) return b.MoveNext();while (b.MoveNext()) {
          if (c(b.Current())) return e;
        }return a;
      } finally {
        d.Dispose(b);
      }
    }, Concat: function Concat(e) {
      var g = this;return new c(function () {
        var i, h;return new f(function () {
          i = g.GetEnumerator();
        }, function () {
          if (h == b) {
            if (i.MoveNext()) return this.Yield(i.Current());h = c.From(e).GetEnumerator();
          }return h.MoveNext() ? this.Yield(h.Current()) : a;
        }, function () {
          try {
            d.Dispose(i);
          } finally {
            d.Dispose(h);
          }
        });
      });
    }, Insert: function Insert(h, b) {
      var g = this;return new c(function () {
        var j,
            i,
            l = 0,
            k = a;return new f(function () {
          j = g.GetEnumerator();i = c.From(b).GetEnumerator();
        }, function () {
          if (l == h && i.MoveNext()) {
            k = e;return this.Yield(i.Current());
          }if (j.MoveNext()) {
            l++;return this.Yield(j.Current());
          }return !k && i.MoveNext() ? this.Yield(i.Current()) : a;
        }, function () {
          try {
            d.Dispose(j);
          } finally {
            d.Dispose(i);
          }
        });
      });
    }, Alternate: function Alternate(a) {
      a = c.Return(a);return this.SelectMany(function (b) {
        return c.Return(b).Concat(a);
      }).TakeExceptLast();
    }, Contains: function Contains(f, b) {
      b = d.CreateLambda(b);var c = this.GetEnumerator();try {
        while (c.MoveNext()) {
          if (b(c.Current()) === f) return e;
        }return a;
      } finally {
        d.Dispose(c);
      }
    }, DefaultIfEmpty: function DefaultIfEmpty(b) {
      var g = this;return new c(function () {
        var c,
            h = e;return new f(function () {
          c = g.GetEnumerator();
        }, function () {
          if (c.MoveNext()) {
            h = a;return this.Yield(c.Current());
          } else if (h) {
            h = a;return this.Yield(b);
          }return a;
        }, function () {
          d.Dispose(c);
        });
      });
    }, Distinct: function Distinct(a) {
      return this.Except(c.Empty(), a);
    }, Except: function Except(e, b) {
      b = d.CreateLambda(b);var g = this;return new c(function () {
        var h, i;return new f(function () {
          h = g.GetEnumerator();i = new n(b);c.From(e).ForEach(function (a) {
            i.Add(a);
          });
        }, function () {
          while (h.MoveNext()) {
            var b = h.Current();if (!i.Contains(b)) {
              i.Add(b);return this.Yield(b);
            }
          }return a;
        }, function () {
          d.Dispose(h);
        });
      });
    }, Intersect: function Intersect(e, b) {
      b = d.CreateLambda(b);var g = this;return new c(function () {
        var h, i, j;return new f(function () {
          h = g.GetEnumerator();i = new n(b);c.From(e).ForEach(function (a) {
            i.Add(a);
          });j = new n(b);
        }, function () {
          while (h.MoveNext()) {
            var b = h.Current();if (!j.Contains(b) && i.Contains(b)) {
              j.Add(b);return this.Yield(b);
            }
          }return a;
        }, function () {
          d.Dispose(h);
        });
      });
    }, SequenceEqual: function SequenceEqual(h, f) {
      f = d.CreateLambda(f);var g = this.GetEnumerator();try {
        var b = c.From(h).GetEnumerator();try {
          while (g.MoveNext()) {
            if (!b.MoveNext() || f(g.Current()) !== f(b.Current())) return a;
          }return b.MoveNext() ? a : e;
        } finally {
          d.Dispose(b);
        }
      } finally {
        d.Dispose(g);
      }
    }, Union: function Union(e, b) {
      b = d.CreateLambda(b);var g = this;return new c(function () {
        var j, h, i;return new f(function () {
          j = g.GetEnumerator();i = new n(b);
        }, function () {
          var b;if (h === undefined) {
            while (j.MoveNext()) {
              b = j.Current();if (!i.Contains(b)) {
                i.Add(b);return this.Yield(b);
              }
            }h = c.From(e).GetEnumerator();
          }while (h.MoveNext()) {
            b = h.Current();if (!i.Contains(b)) {
              i.Add(b);return this.Yield(b);
            }
          }return a;
        }, function () {
          try {
            d.Dispose(j);
          } finally {
            d.Dispose(h);
          }
        });
      });
    }, OrderBy: function OrderBy(b) {
      return new j(this, b, a);
    }, OrderByDescending: function OrderByDescending(a) {
      return new j(this, a, e);
    }, Reverse: function Reverse() {
      var b = this;return new c(function () {
        var c, d;return new f(function () {
          c = b.ToArray();d = c.length;
        }, function () {
          return d > 0 ? this.Yield(c[--d]) : a;
        }, g.Blank);
      });
    }, Shuffle: function Shuffle() {
      var b = this;return new c(function () {
        var c;return new f(function () {
          c = b.ToArray();
        }, function () {
          if (c.length > 0) {
            var b = Math.floor(Math.random() * c.length);return this.Yield(c.splice(b, 1)[0]);
          }return a;
        }, g.Blank);
      });
    }, GroupBy: function GroupBy(i, h, e, g) {
      var j = this;i = d.CreateLambda(i);h = d.CreateLambda(h);if (e != b) e = d.CreateLambda(e);g = d.CreateLambda(g);return new c(function () {
        var c;return new f(function () {
          c = j.ToLookup(i, h, g).ToEnumerable().GetEnumerator();
        }, function () {
          while (c.MoveNext()) {
            return e == b ? this.Yield(c.Current()) : this.Yield(e(c.Current().Key(), c.Current()));
          }return a;
        }, function () {
          d.Dispose(c);
        });
      });
    }, PartitionBy: function PartitionBy(j, i, g, h) {
      var l = this;j = d.CreateLambda(j);i = d.CreateLambda(i);h = d.CreateLambda(h);var k;if (g == b) {
        k = a;g = function g(b, a) {
          return new o(b, a);
        };
      } else {
        k = e;g = d.CreateLambda(g);
      }return new c(function () {
        var b,
            n,
            o,
            m = [];return new f(function () {
          b = l.GetEnumerator();if (b.MoveNext()) {
            n = j(b.Current());o = h(n);m.push(i(b.Current()));
          }
        }, function () {
          var d;while ((d = b.MoveNext()) == e) {
            if (o === h(j(b.Current()))) m.push(i(b.Current()));else break;
          }if (m.length > 0) {
            var f = k ? g(n, c.From(m)) : g(n, m);if (d) {
              n = j(b.Current());o = h(n);m = [i(b.Current())];
            } else m = [];return this.Yield(f);
          }return a;
        }, function () {
          d.Dispose(b);
        });
      });
    }, BufferWithCount: function BufferWithCount(e) {
      var b = this;return new c(function () {
        var c;return new f(function () {
          c = b.GetEnumerator();
        }, function () {
          var b = [],
              d = 0;while (c.MoveNext()) {
            b.push(c.Current());if (++d >= e) return this.Yield(b);
          }return b.length > 0 ? this.Yield(b) : a;
        }, function () {
          d.Dispose(c);
        });
      });
    }, Aggregate: function Aggregate(c, b, a) {
      return this.Scan(c, b, a).Last();
    }, Average: function Average(a) {
      a = d.CreateLambda(a);var c = 0,
          b = 0;this.ForEach(function (d) {
        c += a(d);++b;
      });return c / b;
    }, Count: function Count(a) {
      a = a == b ? g.True : d.CreateLambda(a);var c = 0;this.ForEach(function (d, b) {
        if (a(d, b)) ++c;
      });return c;
    }, Max: function Max(a) {
      if (a == b) a = g.Identity;return this.Select(a).Aggregate(function (a, b) {
        return a > b ? a : b;
      });
    }, Min: function Min(a) {
      if (a == b) a = g.Identity;return this.Select(a).Aggregate(function (a, b) {
        return a < b ? a : b;
      });
    }, MaxBy: function MaxBy(a) {
      a = d.CreateLambda(a);return this.Aggregate(function (b, c) {
        return a(b) > a(c) ? b : c;
      });
    }, MinBy: function MinBy(a) {
      a = d.CreateLambda(a);return this.Aggregate(function (b, c) {
        return a(b) < a(c) ? b : c;
      });
    }, Sum: function Sum(a) {
      if (a == b) a = g.Identity;return this.Select(a).Aggregate(0, function (a, b) {
        return a + b;
      });
    }, ElementAt: function ElementAt(d) {
      var c,
          b = a;this.ForEach(function (g, f) {
        if (f == d) {
          c = g;b = e;return a;
        }
      });if (!b) throw new Error("index is less than 0 or greater than or equal to the number of elements in source.");return c;
    }, ElementAtOrDefault: function ElementAtOrDefault(f, d) {
      var c,
          b = a;this.ForEach(function (g, d) {
        if (d == f) {
          c = g;b = e;return a;
        }
      });return !b ? d : c;
    }, First: function First(c) {
      if (c != b) return this.Where(c).First();var f,
          d = a;this.ForEach(function (b) {
        f = b;d = e;return a;
      });if (!d) throw new Error("First:No element satisfies the condition.");return f;
    }, FirstOrDefault: function FirstOrDefault(c, d) {
      if (d != b) return this.Where(d).FirstOrDefault(c);var g,
          f = a;this.ForEach(function (b) {
        g = b;f = e;return a;
      });return !f ? c : g;
    }, Last: function Last(c) {
      if (c != b) return this.Where(c).Last();var f,
          d = a;this.ForEach(function (a) {
        d = e;f = a;
      });if (!d) throw new Error("Last:No element satisfies the condition.");return f;
    }, LastOrDefault: function LastOrDefault(c, d) {
      if (d != b) return this.Where(d).LastOrDefault(c);var g,
          f = a;this.ForEach(function (a) {
        f = e;g = a;
      });return !f ? c : g;
    }, Single: function Single(d) {
      if (d != b) return this.Where(d).Single();var f,
          c = a;this.ForEach(function (a) {
        if (!c) {
          c = e;f = a;
        } else throw new Error(m);
      });if (!c) throw new Error("Single:No element satisfies the condition.");return f;
    }, SingleOrDefault: function SingleOrDefault(d, f) {
      if (f != b) return this.Where(f).SingleOrDefault(d);var g,
          c = a;this.ForEach(function (a) {
        if (!c) {
          c = e;g = a;
        } else throw new Error(m);
      });return !c ? d : g;
    }, Skip: function Skip(e) {
      var b = this;return new c(function () {
        var c,
            g = 0;return new f(function () {
          c = b.GetEnumerator();while (g++ < e && c.MoveNext()) {}
        }, function () {
          return c.MoveNext() ? this.Yield(c.Current()) : a;
        }, function () {
          d.Dispose(c);
        });
      });
    }, SkipWhile: function SkipWhile(b) {
      b = d.CreateLambda(b);var g = this;return new c(function () {
        var c,
            i = 0,
            h = a;return new f(function () {
          c = g.GetEnumerator();
        }, function () {
          while (!h) {
            if (c.MoveNext()) {
              if (!b(c.Current(), i++)) {
                h = e;return this.Yield(c.Current());
              }continue;
            } else return a;
          }return c.MoveNext() ? this.Yield(c.Current()) : a;
        }, function () {
          d.Dispose(c);
        });
      });
    }, Take: function Take(e) {
      var b = this;return new c(function () {
        var c,
            g = 0;return new f(function () {
          c = b.GetEnumerator();
        }, function () {
          return g++ < e && c.MoveNext() ? this.Yield(c.Current()) : a;
        }, function () {
          d.Dispose(c);
        });
      });
    }, TakeWhile: function TakeWhile(b) {
      b = d.CreateLambda(b);var e = this;return new c(function () {
        var c,
            g = 0;return new f(function () {
          c = e.GetEnumerator();
        }, function () {
          return c.MoveNext() && b(c.Current(), g++) ? this.Yield(c.Current()) : a;
        }, function () {
          d.Dispose(c);
        });
      });
    }, TakeExceptLast: function TakeExceptLast(e) {
      if (e == b) e = 1;var g = this;return new c(function () {
        if (e <= 0) return g.GetEnumerator();var b,
            c = [];return new f(function () {
          b = g.GetEnumerator();
        }, function () {
          while (b.MoveNext()) {
            if (c.length == e) {
              c.push(b.Current());return this.Yield(c.shift());
            }c.push(b.Current());
          }return a;
        }, function () {
          d.Dispose(b);
        });
      });
    }, TakeFromLast: function TakeFromLast(e) {
      if (e <= 0 || e == b) return c.Empty();var g = this;return new c(function () {
        var j,
            h,
            i = [];return new f(function () {
          j = g.GetEnumerator();
        }, function () {
          while (j.MoveNext()) {
            i.length == e && i.shift();i.push(j.Current());
          }if (h == b) h = c.From(i).GetEnumerator();return h.MoveNext() ? this.Yield(h.Current()) : a;
        }, function () {
          d.Dispose(h);
        });
      });
    }, IndexOf: function IndexOf(c) {
      var a = b;this.ForEach(function (d, b) {
        if (d === c) {
          a = b;return e;
        }
      });return a !== b ? a : -1;
    }, LastIndexOf: function LastIndexOf(b) {
      var a = -1;this.ForEach(function (d, c) {
        if (d === b) a = c;
      });return a;
    }, ToArray: function ToArray() {
      var a = [];this.ForEach(function (b) {
        a.push(b);
      });return a;
    }, ToLookup: function ToLookup(c, b, a) {
      c = d.CreateLambda(c);b = d.CreateLambda(b);a = d.CreateLambda(a);var e = new n(a);this.ForEach(function (g) {
        var f = c(g),
            a = b(g),
            d = e.Get(f);if (d !== undefined) d.push(a);else e.Add(f, [a]);
      });return new q(e);
    }, ToObject: function ToObject(b, a) {
      b = d.CreateLambda(b);a = d.CreateLambda(a);var c = {};this.ForEach(function (d) {
        c[b(d)] = a(d);
      });return c;
    }, ToDictionary: function ToDictionary(c, b, a) {
      c = d.CreateLambda(c);b = d.CreateLambda(b);a = d.CreateLambda(a);var e = new n(a);this.ForEach(function (a) {
        e.Add(c(a), b(a));
      });return e;
    }, ToJSON: function ToJSON(a, b) {
      return JSON.stringify(this.ToArray(), a, b);
    }, ToString: function ToString(a, c) {
      if (a == b) a = "";if (c == b) c = g.Identity;return this.Select(c).ToArray().join(a);
    }, Do: function Do(b) {
      var e = this;b = d.CreateLambda(b);return new c(function () {
        var c,
            g = 0;return new f(function () {
          c = e.GetEnumerator();
        }, function () {
          if (c.MoveNext()) {
            b(c.Current(), g++);return this.Yield(c.Current());
          }return a;
        }, function () {
          d.Dispose(c);
        });
      });
    }, ForEach: function ForEach(c) {
      c = d.CreateLambda(c);var e = 0,
          b = this.GetEnumerator();try {
        while (b.MoveNext()) {
          if (c(b.Current(), e++) === a) break;
        }
      } finally {
        d.Dispose(b);
      }
    }, Write: function Write(c, f) {
      if (c == b) c = "";f = d.CreateLambda(f);var g = e;this.ForEach(function (b) {
        if (g) g = a;else document.write(c);document.write(f(b));
      });
    }, WriteLine: function WriteLine(a) {
      a = d.CreateLambda(a);this.ForEach(function (b) {
        document.write(a(b));document.write("<br />");
      });
    }, Force: function Force() {
      var a = this.GetEnumerator();try {
        while (a.MoveNext()) {}
      } finally {
        d.Dispose(a);
      }
    }, Let: function Let(b) {
      b = d.CreateLambda(b);var e = this;return new c(function () {
        var g;return new f(function () {
          g = c.From(b(e)).GetEnumerator();
        }, function () {
          return g.MoveNext() ? this.Yield(g.Current()) : a;
        }, function () {
          d.Dispose(g);
        });
      });
    }, Share: function Share() {
      var e = this,
          d;return new c(function () {
        return new f(function () {
          if (d == b) d = e.GetEnumerator();
        }, function () {
          return d.MoveNext() ? this.Yield(d.Current()) : a;
        }, g.Blank);
      });
    }, MemoizeAll: function MemoizeAll() {
      var h = this,
          e,
          d;return new c(function () {
        var c = -1;return new f(function () {
          if (d == b) {
            d = h.GetEnumerator();e = [];
          }
        }, function () {
          c++;return e.length <= c ? d.MoveNext() ? this.Yield(e[c] = d.Current()) : a : this.Yield(e[c]);
        }, g.Blank);
      });
    }, Catch: function Catch(b) {
      b = d.CreateLambda(b);var e = this;return new c(function () {
        var c;return new f(function () {
          c = e.GetEnumerator();
        }, function () {
          try {
            return c.MoveNext() ? this.Yield(c.Current()) : a;
          } catch (d) {
            b(d);return a;
          }
        }, function () {
          d.Dispose(c);
        });
      });
    }, Finally: function Finally(b) {
      b = d.CreateLambda(b);var e = this;return new c(function () {
        var c;return new f(function () {
          c = e.GetEnumerator();
        }, function () {
          return c.MoveNext() ? this.Yield(c.Current()) : a;
        }, function () {
          try {
            d.Dispose(c);
          } finally {
            b();
          }
        });
      });
    }, Trace: function Trace(c, a) {
      if (c == b) c = "Trace";a = d.CreateLambda(a);return this.Do(function (b) {
        console.log(c, ":", a(b));
      });
    } };var g = { Identity: function Identity(a) {
      return a;
    }, True: function True() {
      return e;
    }, Blank: function Blank() {} },
      i = { Boolean: typeof e === "undefined" ? "undefined" : _typeof(e), Number: _typeof(0), String: _typeof(""), Object: _typeof({}), Undefined: typeof undefined === "undefined" ? "undefined" : _typeof(undefined), Function: _typeof(function () {}) },
      d = { CreateLambda: function CreateLambda(a) {
      if (a == b) return g.Identity;if ((typeof a === "undefined" ? "undefined" : _typeof(a)) == i.String) if (a == "") return g.Identity;else if (a.indexOf("=>") == -1) return new Function("$,$$,$$$,$$$$", "return " + a);else {
        var c = a.match(/^[(\s]*([^()]*?)[)\s]*=>(.*)/);return new Function(c[1], "return " + c[2]);
      }return a;
    }, IsIEnumerable: function IsIEnumerable(b) {
      if ((typeof Enumerator === "undefined" ? "undefined" : _typeof(Enumerator)) != i.Undefined) try {
        new Enumerator(b);return e;
      } catch (c) {}return a;
    }, Compare: function Compare(a, b) {
      return a === b ? 0 : a > b ? 1 : -1;
    }, Dispose: function Dispose(a) {
      a != b && a.Dispose();
    } },
      k = { Before: 0, Running: 1, After: 2 },
      f = function f(d, _f, g) {
    var c = new p(),
        b = k.Before;this.Current = c.Current;this.MoveNext = function () {
      try {
        switch (b) {case k.Before:
            b = k.Running;d();case k.Running:
            if (_f.apply(c)) return e;else {
              this.Dispose();return a;
            }case k.After:
            return a;}
      } catch (g) {
        this.Dispose();throw g;
      }
    };this.Dispose = function () {
      if (b != k.Running) return;try {
        g();
      } finally {
        b = k.After;
      }
    };
  },
      p = function p() {
    var a = b;this.Current = function () {
      return a;
    };this.Yield = function (b) {
      a = b;return e;
    };
  },
      j = function j(f, b, c, e) {
    var a = this;a.source = f;a.keySelector = d.CreateLambda(b);a.descending = c;a.parent = e;
  };j.prototype = new c();j.prototype.CreateOrderedEnumerable = function (a, b) {
    return new j(this.source, a, b, this);
  };j.prototype.ThenBy = function (b) {
    return this.CreateOrderedEnumerable(b, a);
  };j.prototype.ThenByDescending = function (a) {
    return this.CreateOrderedEnumerable(a, e);
  };j.prototype.GetEnumerator = function () {
    var h = this,
        d,
        c,
        e = 0;return new f(function () {
      d = [];c = [];h.source.ForEach(function (b, a) {
        d.push(b);c.push(a);
      });var a = l.Create(h, b);a.GenerateKeys(d);c.sort(function (b, c) {
        return a.Compare(b, c);
      });
    }, function () {
      return e < c.length ? this.Yield(d[c[e++]]) : a;
    }, g.Blank);
  };var l = function l(c, d, e) {
    var a = this;a.keySelector = c;a.descending = d;a.child = e;a.keys = b;
  };l.Create = function (a, d) {
    var c = new l(a.keySelector, a.descending, d);return a.parent != b ? l.Create(a.parent, c) : c;
  };l.prototype.GenerateKeys = function (d) {
    var a = this;for (var f = d.length, g = a.keySelector, e = new Array(f), c = 0; c < f; c++) {
      e[c] = g(d[c]);
    }a.keys = e;a.child != b && a.child.GenerateKeys(d);
  };l.prototype.Compare = function (e, f) {
    var a = this,
        c = d.Compare(a.keys[e], a.keys[f]);if (c == 0) {
      if (a.child != b) return a.child.Compare(e, f);c = d.Compare(e, f);
    }return a.descending ? -c : c;
  };var h = function h(a) {
    this.source = a;
  };h.prototype = new c();h.prototype.Any = function (a) {
    return a == b ? this.source.length > 0 : c.prototype.Any.apply(this, arguments);
  };h.prototype.Count = function (a) {
    return a == b ? this.source.length : c.prototype.Count.apply(this, arguments);
  };h.prototype.ElementAt = function (a) {
    return 0 <= a && a < this.source.length ? this.source[a] : c.prototype.ElementAt.apply(this, arguments);
  };h.prototype.ElementAtOrDefault = function (a, b) {
    return 0 <= a && a < this.source.length ? this.source[a] : b;
  };h.prototype.First = function (a) {
    return a == b && this.source.length > 0 ? this.source[0] : c.prototype.First.apply(this, arguments);
  };h.prototype.FirstOrDefault = function (a, d) {
    return d != b ? c.prototype.FirstOrDefault.apply(this, arguments) : this.source.length > 0 ? this.source[0] : a;
  };h.prototype.Last = function (d) {
    var a = this;return d == b && a.source.length > 0 ? a.source[a.source.length - 1] : c.prototype.Last.apply(a, arguments);
  };h.prototype.LastOrDefault = function (d, e) {
    var a = this;return e != b ? c.prototype.LastOrDefault.apply(a, arguments) : a.source.length > 0 ? a.source[a.source.length - 1] : d;
  };h.prototype.Skip = function (d) {
    var b = this.source;return new c(function () {
      var c;return new f(function () {
        c = d < 0 ? 0 : d;
      }, function () {
        return c < b.length ? this.Yield(b[c++]) : a;
      }, g.Blank);
    });
  };h.prototype.TakeExceptLast = function (a) {
    if (a == b) a = 1;return this.Take(this.source.length - a);
  };h.prototype.TakeFromLast = function (a) {
    return this.Skip(this.source.length - a);
  };h.prototype.Reverse = function () {
    var b = this.source;return new c(function () {
      var c;return new f(function () {
        c = b.length;
      }, function () {
        return c > 0 ? this.Yield(b[--c]) : a;
      }, g.Blank);
    });
  };h.prototype.SequenceEqual = function (d, e) {
    return (d instanceof h || d instanceof Array) && e == b && c.From(d).Count() != this.Count() ? a : c.prototype.SequenceEqual.apply(this, arguments);
  };h.prototype.ToString = function (a, d) {
    if (d != b || !(this.source instanceof Array)) return c.prototype.ToString.apply(this, arguments);if (a == b) a = "";return this.source.join(a);
  };h.prototype.GetEnumerator = function () {
    var b = this.source,
        c = 0;return new f(g.Blank, function () {
      return c < b.length ? this.Yield(b[c++]) : a;
    }, g.Blank);
  };var n = function () {
    var h = function h(a, b) {
      return Object.prototype.hasOwnProperty.call(a, b);
    },
        d = function d(a) {
      return a === b ? "null" : a === undefined ? "undefined" : _typeof(a.toString) === i.Function ? a.toString() : Object.prototype.toString.call(a);
    },
        l = function l(d, c) {
      var a = this;a.Key = d;a.Value = c;a.Prev = b;a.Next = b;
    },
        j = function j() {
      this.First = b;this.Last = b;
    };j.prototype = { AddLast: function AddLast(c) {
        var a = this;if (a.Last != b) {
          a.Last.Next = c;c.Prev = a.Last;a.Last = c;
        } else a.First = a.Last = c;
      }, Replace: function Replace(c, a) {
        if (c.Prev != b) {
          c.Prev.Next = a;a.Prev = c.Prev;
        } else this.First = a;if (c.Next != b) {
          c.Next.Prev = a;a.Next = c.Next;
        } else this.Last = a;
      }, Remove: function Remove(a) {
        if (a.Prev != b) a.Prev.Next = a.Next;else this.First = a.Next;if (a.Next != b) a.Next.Prev = a.Prev;else this.Last = a.Prev;
      } };var k = function k(c) {
      var a = this;a.count = 0;a.entryList = new j();a.buckets = {};a.compareSelector = c == b ? g.Identity : c;
    };k.prototype = { Add: function Add(i, j) {
        var a = this,
            g = a.compareSelector(i),
            f = d(g),
            c = new l(i, j);if (h(a.buckets, f)) {
          for (var b = a.buckets[f], e = 0; e < b.length; e++) {
            if (a.compareSelector(b[e].Key) === g) {
              a.entryList.Replace(b[e], c);b[e] = c;return;
            }
          }b.push(c);
        } else a.buckets[f] = [c];a.count++;a.entryList.AddLast(c);
      }, Get: function Get(i) {
        var a = this,
            c = a.compareSelector(i),
            g = d(c);if (!h(a.buckets, g)) return undefined;for (var e = a.buckets[g], b = 0; b < e.length; b++) {
          var f = e[b];if (a.compareSelector(f.Key) === c) return f.Value;
        }return undefined;
      }, Set: function Set(k, m) {
        var b = this,
            g = b.compareSelector(k),
            j = d(g);if (h(b.buckets, j)) for (var f = b.buckets[j], c = 0; c < f.length; c++) {
          if (b.compareSelector(f[c].Key) === g) {
            var i = new l(k, m);b.entryList.Replace(f[c], i);f[c] = i;return e;
          }
        }return a;
      }, Contains: function Contains(j) {
        var b = this,
            f = b.compareSelector(j),
            i = d(f);if (!h(b.buckets, i)) return a;for (var g = b.buckets[i], c = 0; c < g.length; c++) {
          if (b.compareSelector(g[c].Key) === f) return e;
        }return a;
      }, Clear: function Clear() {
        this.count = 0;this.buckets = {};this.entryList = new j();
      }, Remove: function Remove(g) {
        var a = this,
            f = a.compareSelector(g),
            e = d(f);if (!h(a.buckets, e)) return;for (var b = a.buckets[e], c = 0; c < b.length; c++) {
          if (a.compareSelector(b[c].Key) === f) {
            a.entryList.Remove(b[c]);b.splice(c, 1);if (b.length == 0) delete a.buckets[e];a.count--;return;
          }
        }
      }, Count: function Count() {
        return this.count;
      }, ToEnumerable: function ToEnumerable() {
        var d = this;return new c(function () {
          var c;return new f(function () {
            c = d.entryList.First;
          }, function () {
            if (c != b) {
              var d = { Key: c.Key, Value: c.Value };c = c.Next;return this.Yield(d);
            }return a;
          }, g.Blank);
        });
      } };return k;
  }(),
      q = function q(a) {
    var b = this;b.Count = function () {
      return a.Count();
    };b.Get = function (b) {
      return c.From(a.Get(b));
    };b.Contains = function (b) {
      return a.Contains(b);
    };b.ToEnumerable = function () {
      return a.ToEnumerable().Select(function (a) {
        return new o(a.Key, a.Value);
      });
    };
  },
      o = function o(b, a) {
    this.Key = function () {
      return b;
    };h.call(this, a);
  };o.prototype = new h();return c;
}();
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var j2HTMLForm = function () {

    //CREAT CLASS
    var j2HTMLForm = function () {
        function j2HTMLForm() {
            _classCallCheck(this, j2HTMLForm);

            this._data = false;
            this._ID = false;
            this._appendTo = false;
            this._appendPaginationTo = false;
            this._type = 'Empty';
            this._exclude = false;
            this._include = false;
            this._getData = false;
            this._customElement = false;
            this._textLimit = 100;
            // //REQUIRED ONLY FOR DATE PICKER IF FORM IS INSIDE MODAL
            // this._modalID = false;
        }
        //#region ************** PROPERTIES ******************

        // PROPERTY -- DATA


        _createClass(j2HTMLForm, [{
            key: 'getForm',


            // get modalID() {
            //     return this._modalID;
            // }
            // set modalID(value) {
            //     this._modalID = value;
            // }

            //endregion

            //#region ************** PUBLIC FUNCTIONS ************
            value: function getForm() {
                var _this = this;

                var error = '';
                var createPreviousNextButton = false;

                if (this._data === false) {
                    error += 'Data is required \n';
                }

                if (error === '') {

                    modalID = this._modalID;
                    formID = this._ID.slice(1);
                    maxTextLimit = this._textLimit;

                    var str = '';
                    if (this._type === 'Empty') {
                        str = privateMethods.empty(this._data, this._exclude, this._include, this._customElement);
                    } else {
                        str = privateMethods.list(this._data, this._exclude, this._include, this._customElement);
                        if (this._data.length > 1) {
                            createPreviousNextButton = true;
                        }
                    }
                    if (this._appendTo !== false) {
                        j2HTML.EmptyElement({
                            Element: this._appendTo
                        });
                        j2HTML.AppendHTMLString({
                            Element: this._appendTo,
                            HTMLString: str
                        });
                    } else {
                        if (j2HTML.IsElementExsit({
                            Element: '#divJson2HTMLForm'
                        })) {
                            j2HTML.RemoveElement({
                                Element: '#divJson2HTMLForm'
                            });
                            j2HTML.AppendHTMLString({
                                Element: 'body',
                                HTMLString: '<div id="divJson2HTMLForm"></div>'
                            });
                            j2HTML.AppendHTMLString({
                                Element: '#divJson2HTMLForm',
                                HTMLString: str
                            });
                        } else {
                            j2HTML.AppendHTMLString({
                                Element: 'body',
                                HTMLString: '<div id="divJson2HTMLForm"></div>'
                            });
                            j2HTML.AppendHTMLString({
                                Element: '#divJson2HTMLForm',
                                HTMLString: tb
                            });
                        }
                    }

                    if (this._customElement !== false) {
                        custElement = this._customElement;
                    }

                    //SET PLACE-HOLDER
                    j2HTML.Placeholder({
                        ElementIDs: '.txt' + formID
                    });

                    //CREATE PREVIOUS AND NEXT BUTTONS
                    if (createPreviousNextButton === true) {

                        totalForms = this._data.length;
                        //<button id="btn${formID}First" class="btn btn-sm btn-primary disabled" onclick="j2HTMLFirstForm('${formID}','${this._data.length}')">First</button>

                        var buttons = '<div id="frmPaginationButtons">\n                                      <button id="btn' + formID + 'First" class="btn btn-sm btn-primary disabled">First</button>\n                                      <button id="btn' + formID + 'Previous" class="btn btn-sm btn-primary disabled" title="Previous"><<</button>\n                                      <button id="btn' + formID + 'Next" class="btn btn-sm btn-primary" title="Next">>></button>\n                                      <button id="btn' + formID + 'Last" class="btn btn-sm btn-primary">Last</button>\n                                   </div>';

                        //FORM NUMBERING

                        var totalNumbers = '<div id="frmNumners">\n                                            <h4>Showing 1 of ' + this._data.length + '</h4>\n                                        </div>';
                        //REMOVE BUTTONS IF EXSIT                    
                        if (j2HTML.IsElementExsit({
                            Element: '#frmPaginationButtons'
                        })) {
                            j2HTML.RemoveElement({
                                Element: '#frmPaginationButtons'
                            });
                        }
                        //REMOVE NUMBERS DIV IF EXSIT 
                        if (j2HTML.IsElementExsit({
                            Element: '#frmNumners'
                        })) {
                            j2HTML.RemoveElement({
                                Element: '#frmNumners'
                            });
                        }
                        if (this._appendTo === false && this._appendPaginationTo === false) {

                            j2HTML.AppendHTMLStringAfter({
                                Element: '#divJson2HTMLForm',
                                HTMLString: buttons
                            });
                            j2HTML.AppendHTMLStringAfter({
                                Element: '#divJson2HTMLForm',
                                HTMLString: totalNumbers
                            });
                        } else if (this._appendTo !== false && this._appendPaginationTo === false) {

                            j2HTML.AppendHTMLStringAfter({
                                Element: this._appendTo,
                                HTMLString: buttons
                            });

                            j2HTML.AppendHTMLStringAfter({
                                Element: this._appendTo,
                                HTMLString: totalNumbers
                            });
                        } else if (this._appendTo === false && this._appendPaginationTo !== false) {

                            j2HTML.AppendHTMLStringAfter({
                                Element: this._appendPaginationTo,
                                HTMLString: buttons
                            });

                            j2HTML.AppendHTMLStringAfter({
                                Element: this._appendPaginationTo,
                                HTMLString: totalNumbers
                            });
                        } else if (this._appendTo !== false && this._appendPaginationTo !== false) {

                            j2HTML.AppendHTMLStringAfter({
                                Element: this._appendPaginationTo,
                                HTMLString: buttons
                            });

                            j2HTML.AppendHTMLStringAfter({
                                Element: this._appendPaginationTo,
                                HTMLString: totalNumbers
                            });
                        }

                        var frm = new j2HTMLForm();

                        //BUTTON FIRST -- REMOVE AND ADD EVENT LISTERNER
                        var btnFirst = j2HTML.GetElement({
                            Element: '#btn' + formID + 'First'
                        });
                        btnFirst[0].removeEventListener('click', frm.j2HTMLFirstForm, false);
                        btnFirst[0].addEventListener('click', frm.j2HTMLFirstForm, false);

                        //BUTTON NEXT -- REMOVE AND ADD EVENT LISTERNER
                        var btnNext = j2HTML.GetElement({
                            Element: '#btn' + formID + 'Next'
                        });
                        btnNext[0].removeEventListener('click', frm.j2HTMLNextForm, false);
                        btnNext[0].addEventListener('click', frm.j2HTMLNextForm, false);

                        //BUTTON PREVIOUS -- REMOVE AND ADD EVENT LISTERNER
                        var btnPrevious = j2HTML.GetElement({
                            Element: '#btn' + formID + 'Previous'
                        });
                        btnPrevious[0].removeEventListener('click', frm.j2HTMLPreviousForm, false);
                        btnPrevious[0].addEventListener('click', frm.j2HTMLPreviousForm, false);

                        //BUTTON LAST -- REMOVE AND ADD EVENT LISTERNER
                        var btnLast = j2HTML.GetElement({
                            Element: '#btn' + formID + 'Last'
                        });
                        btnLast[0].removeEventListener('click', frm.j2HTMLLastForm, false);
                        btnLast[0].addEventListener('click', frm.j2HTMLLastForm, false);
                    }

                    //ADD CUSTOMER ELEMENT
                    if (this._customElement !== false) {
                        var _loop = function _loop(i) {

                            var option = {};

                            if (_this._customElement[i].Data !== undefined) {
                                option.Data = _this._customElement[i].Data;
                            }
                            if (_this._customElement[i].Text !== undefined) {
                                option.Text = _this._customElement[i].Text;
                            }
                            if (_this._customElement[i].Value !== undefined) {
                                option.Value = _this._customElement[i].Value;
                            }
                            if (_this._customElement[i].Direction !== undefined) {
                                option.Direction = _this._customElement[i].Direction;
                            }
                            if (_this._customElement[i].Title !== undefined) {
                                option.Title = _this._customElement[i].Title;
                            }

                            //ADDED DROPDOWN 
                            if (_this._customElement[i].Type.toLowerCase() === "dropdown" && _this._type.toLowerCase() !== 'list') {

                                var appendTo = '#ddl' + _this._customElement[i].Column.replace(/\s+/g, '');
                                var ddl = '<select id="ddl' + _this._customElement[i].Column.replace(/\s+/g, '') + '" class="form-control"></select>';
                                option.AppendTo = appendTo;

                                j2HTML.AppendHTMLString({
                                    Element: '#div' + _this._customElement[i].Column.replace(/\s+/g, ''),
                                    HTMLString: ddl
                                });

                                j2HTML.Dropdown(option);
                            } else if (_this._customElement[i].Type.toLowerCase() === "dropdown" && _this._type.toLowerCase() === 'list') {
                                var frms = j2HTML.GetElement({
                                    Element: '[id^="' + formID + '"]'
                                });
                                [].slice.call(frms).map(function (frm, index) {

                                    var appendTo = '#ddl' + _this._customElement[i].Column.replace(/\s+/g, '') + index;
                                    var ddl = '<select id="ddl' + _this._customElement[i].Column.replace(/\s+/g, '') + index + '" class="form-control"></select>';
                                    option.AppendTo = appendTo;

                                    j2HTML.AppendHTMLString({
                                        Element: '#div' + _this._customElement[i].Column.replace(/\s+/g, '') + index,
                                        HTMLString: ddl
                                    });

                                    j2HTML.Dropdown(option);
                                });
                            }

                            //ADD RADIO BUTTON
                            if (_this._customElement[i].Type.toLowerCase() === "radio" && _this._type.toLowerCase() !== 'list') {

                                var _appendTo = '#div' + _this._customElement[i].Column.replace(/\s+/g, '');
                                option.AppendTo = _appendTo;
                                j2HTML.Radio(option);
                            } else if (_this._customElement[i].Type.toLowerCase() === "radio" && _this._type.toLowerCase() === 'list') {
                                var _frms = j2HTML.GetElement({
                                    Element: '[id^="' + formID + '"]'
                                });
                                [].slice.call(_frms).map(function (frm, index) {

                                    var appendTo = '#div' + _this._customElement[i].Column.replace(/\s+/g, '') + index;
                                    option.AppendTo = appendTo;
                                    j2HTML.Radio(option);
                                });
                            }

                            //ADD CHECKBOX
                            if (_this._customElement[i].Type.toLowerCase() === "checkbox" && _this._type.toLowerCase() !== 'list') {

                                var _appendTo2 = '#div' + _this._customElement[i].Column.replace(/\s+/g, '');
                                option.AppendTo = _appendTo2;
                                j2HTML.Checkbox(option);
                            } else if (_this._customElement[i].Type.toLowerCase() === "checkbox" && _this._type.toLowerCase() === 'list') {

                                var _frms2 = j2HTML.GetElement({
                                    Element: '[id^="' + formID + '"]'
                                });
                                [].slice.call(_frms2).map(function (frm, index) {

                                    var appendTo = '#div' + _this._customElement[i].Column.replace(/\s+/g, '') + index;
                                    option.AppendTo = appendTo;
                                    j2HTML.Checkbox(option);
                                });
                            }

                            //ADD DATA LIST
                            if (_this._customElement[i].Type.toLowerCase() === "datalist" && _this._type.toLowerCase() !== 'list') {

                                option.DataList = _this._customElement[i].DataList;
                                var _appendTo3 = '#div' + _this._customElement[i].Column.replace(/\s+/g, '');
                                option.AppendTo = _appendTo3;
                                j2HTML.List(option);
                            } else if (_this._customElement[i].Type.toLowerCase() === "datalist" && _this._type.toLowerCase() === 'list') {

                                var _frms3 = j2HTML.GetElement({
                                    Element: '[id^="' + formID + '"]'
                                });

                                var datalistId = _this._customElement[0].DataList.ID;
                                [].slice.call(_frms3).map(function (frm, index) {

                                    //this._customElement[0].DataList.ID =this._customElement[0].DataList.ID +index; 
                                    _this._customElement[0].DataList.ID = datalistId + index;
                                    var appendTo = '#div' + _this._customElement[i].Column.replace(/\s+/g, '') + index;

                                    option.AppendTo = appendTo;
                                    option.DataList = _this._customElement[0].DataList;

                                    j2HTML.List(option);
                                });
                            }
                        };

                        for (var i = 0; i < this._customElement.length; i++) {
                            _loop(i);
                        }
                    }
                    //************ CREATE DATE PICKER EVENTS ************************/
                    var datePickers = j2HTML.GetElement({
                        Element: '.j2HTMLDatePicker'
                    });

                    if (datePickers.length !== 0) {

                        var _frm = new j2HTMLForm();

                        [].slice.call(datePickers).map(function (datePicker) {

                            var btnDatePickerID = datePicker.getAttribute('id');

                            document.getElementById(btnDatePickerID).addEventListener('click', _frm.j2HTMLFormGetDatePicker, false);
                        });
                    }

                    //************ GET FORM DATA ********************/ 
                    if (this._getData !== false) {
                        var _frm2 = new j2HTMLForm();

                        funFormData = this._getData.Function;

                        var btn = j2HTML.GetElement({
                            Element: this._getData.ButtonID
                        });
                        //REMOVE PREVIOUSLY ADDED CLICK EVENT LISTENER
                        if (btn.length !== 0) {
                            btn[0].removeEventListener("click", _frm2.getFormData, false);
                            //ADD CLICK EVENT LISTENER
                            btn[0].addEventListener("click", _frm2.getFormData, false);
                        }
                        // btn[0].removeEventListener("click", frm.getFormData, false);
                        // //ADD CLICK EVENT LISTENER
                        // btn[0].addEventListener("click", frm.getFormData, false);
                    }
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }
        }, {
            key: 'getFormData',
            value: function getFormData() {

                var frm = document.querySelector('[id^="' + formID + '"][visible="true"]');
                var elems = frm.elements;

                var frmData = {};
                var selectedValue = [];
                var selectedText = [];

                [].slice.call(elems).map(function (elem) {

                    if (elem.nodeName === 'INPUT' && (elem.type === 'text' || elem.type === 'date')) {

                        var id = elem.getAttribute('id').slice(3);
                        var text = elem.value;
                        frmData[id] = text;
                    } else if (elem.nodeName.toLowerCase() === 'textarea' && elem.type.toLowerCase() === 'textarea') {

                        var _id = elem.getAttribute('id').slice(3);
                        var _text = elem.innerHTML;
                        frmData[_id] = _text;
                    } else if (elem.nodeName === 'SELECT' && elem.type === 'select-one') {

                        var _id2 = elem.getAttribute('id');

                        var _selectedValue = j2HTML.GetSelectedValueAndText({
                            Element: '#' + _id2,
                            Type: 'dropdown'
                        });

                        frmData[elem.getAttribute('id').slice(3)] = _selectedValue;
                    } else if (elem.nodeName === 'INPUT' && elem.type === 'radio') {

                        var _id3 = document.getElementsByName(elem.getAttribute('name'))[0].parentElement.parentElement.getAttribute('id').slice(3);
                        if (elem.checked) {
                            var obj = {
                                Value: elem.value,
                                Text: elem.getAttribute('text')
                            };
                            frmData[_id3] = obj;
                        }
                    } else if (elem.nodeName === 'INPUT' && elem.type === 'checkbox') {

                        var _id4 = document.getElementsByName(elem.getAttribute('name'))[0].parentElement.parentElement.getAttribute('id').slice(3);

                        if (elem.checked) {
                            selectedValue.push(elem.value);
                            selectedText.push(elem.getAttribute('text'));
                        }

                        var _obj = {
                            Value: selectedValue,
                            Text: selectedText
                        };
                        frmData[_id4] = _obj;
                    }
                });

                funFormData(frmData);
            }
        }, {
            key: 'j2HTMLFirstForm',
            value: function j2HTMLFirstForm() {

                var frmID = formID;
                var id = frmID + 0;
                var forms = document.querySelectorAll('.' + frmID);

                [].slice.call(forms).map(function (form) {
                    form.style.display = 'none';
                    form.setAttribute('visible', 'false');
                });

                document.getElementById(id).style.display = '';
                document.getElementById(id).setAttribute('visible', 'true');

                var totalNumbers = '<h4>Showing 1 of ' + totalForms + '</h4>';
                j2HTML.EmptyElement({
                    Element: '#frmNumners'
                });
                j2HTML.AppendHTMLString({
                    Element: '#frmNumners',
                    HTMLString: totalNumbers
                });

                document.getElementById('btn' + frmID + 'First').classList.add('disabled');
                document.getElementById('btn' + frmID + 'Previous').classList.add('disabled');

                document.getElementById('btn' + frmID + 'Last').classList.remove('disabled');
                document.getElementById('btn' + frmID + 'Next').classList.remove('disabled');
            }
        }, {
            key: 'j2HTMLNextForm',
            value: function j2HTMLNextForm() {

                var frmID = formID;
                var id = document.querySelectorAll('.' + frmID + '[visible="true"]')[0].getAttribute('id');
                var currentId = id.substring(frmID.length, id.length);
                var nextId = frmID + (parseInt(currentId) + 1);

                if (parseInt(currentId) !== parseInt(totalForms) - 1) {

                    var forms = document.querySelectorAll('.' + frmID);

                    [].slice.call(forms).map(function (form) {
                        form.style.display = 'none';
                        form.setAttribute('visible', 'false');
                    });

                    document.getElementById(nextId).style.display = '';
                    document.getElementById(nextId).setAttribute('visible', 'true');

                    var totalNumbers = '<h4>Showing ' + (parseInt(currentId) + 2) + ' of ' + totalForms + '</h4>';
                    j2HTML.EmptyElement({
                        Element: '#frmNumners'
                    });
                    j2HTML.AppendHTMLString({
                        Element: '#frmNumners',
                        HTMLString: totalNumbers
                    });

                    document.getElementById('btn' + frmID + 'First').classList.remove('disabled');
                    document.getElementById('btn' + frmID + 'Previous').classList.remove('disabled');
                } else if (parseInt(currentId) === parseInt(totalForms) - 1) {
                    document.getElementById('btn' + frmID + 'Last').classList.add('disabled');
                    document.getElementById('btn' + frmID + 'Next').classList.add('disabled');
                }
            }
        }, {
            key: 'j2HTMLPreviousForm',
            value: function j2HTMLPreviousForm() {

                var frmID = formID;
                var id = document.querySelectorAll('.' + frmID + '[visible="true"]')[0].getAttribute('id');
                var currentId = id.substring(frmID.length, id.length);
                var previousId = frmID + (parseInt(currentId) - 1);

                if (parseInt(currentId) !== 0) {

                    var forms = document.querySelectorAll('.' + frmID);

                    [].slice.call(forms).map(function (form) {
                        form.style.display = 'none';
                        form.setAttribute('visible', 'false');
                    });

                    document.getElementById(previousId).style.display = '';
                    document.getElementById(previousId).setAttribute('visible', 'true');

                    var totalNumbers = '<h4>Showing ' + (parseInt(currentId) - 1 + 1) + ' of ' + totalForms + '</h4>';
                    j2HTML.EmptyElement({
                        Element: '#frmNumners'
                    });
                    j2HTML.AppendHTMLString({
                        Element: '#frmNumners',
                        HTMLString: totalNumbers
                    });
                }
                if (parseInt(currentId) - 1 === 0) {
                    document.getElementById('btn' + frmID + 'First').classList.add('disabled');
                    document.getElementById('btn' + frmID + 'Previous').classList.add('disabled');
                }
            }
        }, {
            key: 'j2HTMLLastForm',
            value: function j2HTMLLastForm() {

                var frmID = formID;
                var id = frmID + (parseInt(totalForms) - 1);
                var forms = document.querySelectorAll('.' + frmID);

                [].slice.call(forms).map(function (form) {
                    form.style.display = 'none';
                    form.setAttribute('visible', 'false');
                });

                document.getElementById(id).style.display = '';
                document.getElementById(id).setAttribute('visible', 'true');

                var totalNumbers = '<h4>Showing ' + parseInt(totalForms) + ' of ' + totalForms + '</h4>';
                j2HTML.EmptyElement({
                    Element: '#frmNumners'
                });
                j2HTML.AppendHTMLString({
                    Element: '#frmNumners',
                    HTMLString: totalNumbers
                });

                document.getElementById('btn' + frmID + 'Last').classList.add('disabled');
                document.getElementById('btn' + frmID + 'Next').classList.add('disabled');

                document.getElementById('btn' + frmID + 'First').classList.remove('disabled');
                document.getElementById('btn' + frmID + 'Previous').classList.remove('disabled');
            }
        }, {
            key: 'j2HTMLFormGetDatePicker',
            value: function j2HTMLFormGetDatePicker() {

                var btnDatePickerID = void 0;

                if (event.target.tagName.toLowerCase() === 'button') {
                    btnDatePickerID = event.target.getAttribute('id');
                } else if (event.target.tagName.toLowerCase() === 'i') {
                    btnDatePickerID = event.target.parentElement.getAttribute('id');
                }

                var txtDatePickerID = '#txt' + btnDatePickerID.slice(3);

                j2HTML.DatePicker({
                    AssignSelectedDateTo: txtDatePickerID
                });
            }
        }, {
            key: 'Data',
            get: function get() {
                return this._data;
            },
            set: function set(value) {
                this._data = value;
            }
        }, {
            key: 'ID',
            get: function get() {
                return this._ID;
            },
            set: function set(value) {
                this._ID = value;
            }
        }, {
            key: 'AppendTo',
            get: function get() {
                return this._appendTo;
            },
            set: function set(value) {
                this._appendTo = value;
            }
        }, {
            key: 'AppendPaginationTo',
            get: function get() {
                return this._appendPaginationTo;
            },
            set: function set(value) {
                this._appendPaginationTo = value;
            }
        }, {
            key: 'Type',
            get: function get() {
                return this._type;
            },
            set: function set(value) {
                this._type = value;
            }
        }, {
            key: 'Exclude',
            get: function get() {
                return this._exclude;
            },
            set: function set(value) {
                this._exclude = value;
            }
        }, {
            key: 'Include',
            get: function get() {
                return this._include;
            },
            set: function set(value) {
                this._include = value;
            }
        }, {
            key: 'GetData',
            get: function get() {
                return this._getData;
            },
            set: function set(value) {
                this._getData = value;
            }
        }, {
            key: 'customElement',
            get: function get() {
                return this._customElement;
            },
            set: function set(value) {
                this._customElement = value;
            }
        }, {
            key: 'textLimit',
            get: function get() {
                return this._textLimit;
            },
            set: function set(value) {
                this._textLimit = value;
            }
        }]);

        return j2HTMLForm;
    }();

    //#region ************** PRIVATE VARIABLE AND METHOD *******

    var formID = '';
    var funFormData = '';
    var totalForms = '';
    var maxTextLimit = '';

    //PRIVATE FUNCTIONS
    var privateMethods = {
        empty: function empty(data, exclude, include, customElemt) {

            var strFrom = '<form id="' + formID + '" class="' + formID + '" visible="true" style="display">';

            var vals = data[0];

            for (var val in vals) {

                //IF CUSTOM ELEMENT IS NOT FALSE
                if (customElemt !== false) {
                    for (var i = 0; i < customElemt.length; i++) {

                        if (customElemt[i].Column.replace(/\s+/g, '') === val.replace(/\s+/g, '')) {
                            strFrom += '<div id="div' + customElemt[i].Column.replace(/\s+/g, '') + '" class="placeholder"></div>';
                        }
                    }
                }
                if (exclude !== false) {
                    if (exclude.indexOf(val) < 0) {
                        if (privateMethods.isThisDate(vals[val])) {
                            currentValue = vals[val].split('T')[0];

                            strFrom += '<div>\n                                            <div class="input-group placeholder">\n                                                <input id="txt' + val.replace(/\s+/g, '') + '" type="text" class="form-control txt' + formID + '" placeholder="' + val + '">\n                                                <div class="input-group-append">\n                                                    <button type=\'button\' id="btn' + val.replace(/\s+/g, '') + '" class="btn btn-default btn-sm j2HTMLDatePicker"><i class="fa fa-calendar"></i></button>\n                                                </div>\n                                            </div>\n                                        </div>';
                        } else {

                            strFrom += '<div class="placeholder">\n                                            <input type="text" id="txt' + val.replace(/\s+/g, '') + '" class="form-control txt' + formID + '" placeholder="' + val + '">\n                                        </div>';
                        }
                    }
                } else if (exclude === false) {
                    if (privateMethods.isThisDate(vals[val])) {

                        strFrom += '<div>\n                                        <div class="input-group placeholder">\n                                            <input id="txt' + val.replace(/\s+/g, '') + '" type="text" class="form-control txt' + formID + '" placeholder="' + val + '">\n                                            <div class="input-group-append">\n                                                <button type=\'button\' id="btn' + val.replace(/\s+/g, '') + '" class="btn btn-default btn-sm j2HTMLDatePicker"><i class="fa fa-calendar"></i></button>\n                                            </div>\n                                        </div>\n                                    </div>';
                    } else {

                        strFrom += '<div class="placeholder">\n                                        <input type="text" id="txt' + val.replace(/\s+/g, '') + '" class="form-control txt' + formID + '" placeholder="' + val + '">\n                                    </div>';
                    }
                }
            }
            if (include !== false) {

                for (var key in include) {

                    var name = include[key].Name;
                    strFrom += '<div class="placeholder">\n                                    <input type="text" id="txt' + name.replace(/\s+/g, '') + '" class="form-control txt' + formID + '" placeholder="' + name + '">\n                                </div>';
                }
            }

            strFrom += '</form>';

            return strFrom;
        },
        list: function list(data, exclude, include, customElemt) {

            var strFrom = '';
            for (var key in data) {

                if (key === "0") {
                    if (data.length > 1) {
                        strFrom += '<form id="' + (formID + key) + '" visible="true" class="' + formID + '" style="display">';
                    } else {
                        strFrom += '<form id="' + formID + '" visible="true" class="' + formID + '" style="display">';
                    }
                } else {
                    strFrom += '<form id="' + (formID + key) + '" visible="false" class="' + formID + '" style="display:none">';
                }

                var vals = data[key];
                for (var val in vals) {

                    //IF CUSTOM ELEMENT IS NOT FALSE
                    if (customElemt !== false) {
                        for (var i = 0; i < customElemt.length; i++) {

                            //SET DIV FOR DROPDOWN, LIST, AND RADIO BUTTON
                            if (customElemt[i].Column.replace(/\s+/g, '') === val.replace(/\s+/g, '') && customElemt[i].Type.toLowerCase() !== 'image') {
                                strFrom += '<div id="div' + (customElemt[i].Column.replace(/\s+/g, '') + key) + '" class="placeholder"></div>';
                            }
                            //SET IMAGE FROM PATH
                            else if (customElemt[i].Column.replace(/\s+/g, '') === val.replace(/\s+/g, '') && customElemt[i].Type.toLowerCase() === 'image' && customElemt[i].ImageType.toLowerCase() === 'path') {
                                    var imgWidth = false;
                                    var imgHeight = false;

                                    if (customElemt[i].Width !== undefined) {
                                        imgWidth = customElemt[i].Width + 'px';
                                    }
                                    if (customElemt[i].Height !== undefined) {
                                        imgHeight = customElemt[i].Height + 'px';
                                    }

                                    if (imgWidth !== false && imgHeight !== false) {
                                        strFrom += '<div class="placeholder">\n                                                    <input type="text" id="txt' + val.replace(/\s+/g, '') + '" class="form-control txt' + formID + '" value="' + vals[val] + '" placeholder="' + val + '">\n                                                </div>    \n                                                <img id="img' + (customElemt[i].Column.replace(/\s+/g, '') + i) + '" src="' + vals[val] + '" width="' + imgWidth + '" height="' + imgHeight + '" / >';
                                    } else if (imgWidth !== false && imgHeight === false) {
                                        strFrom += '<div class="placeholder">\n                                                    <input type="text" id="txt' + val.replace(/\s+/g, '') + '" class="form-control txt' + formID + '"  value="' + vals[val] + '" placeholder="' + val + '">\n                                                </div>    \n                                                <img id="img' + (customElemt[i].Column.replace(/\s+/g, '') + i) + '" src="' + vals[val] + '" width="' + imgWidth + '" / >';
                                    } else if (imgWidth === false && imgHeight !== false) {
                                        strFrom += '<div class="placeholder">\n                                                    <input type="text" id="txt' + val.replace(/\s+/g, '') + '" class="form-control txt' + formID + '"  value="' + vals[val] + '" placeholder="' + val + '">\n                                                </div>     \n                                                <img id="img' + (customElemt[i].Column.replace(/\s+/g, '') + i) + '" src="' + vals[val] + '" height="' + imgHeight + '" / >';
                                    } else if (imgWidth === false && imgHeight === false) {
                                        strFrom += '<div class="placeholder">\n                                                    <input type="text" id="txt' + val.replace(/\s+/g, '') + '" class="form-control txt' + formID + '"  value="' + vals[val] + '" placeholder="' + val + '">\n                                                </div>    \n                                                <img id="img' + (customElemt[i].Column.replace(/\s+/g, '') + i) + '" src="' + vals[val] + '"/ >';
                                    }
                                }
                        }
                    }
                    if (exclude !== false) {
                        if (exclude.indexOf(val) < 0) {
                            if (privateMethods.isThisDate(vals[val])) {
                                currentValue = vals[val].split('T')[0];

                                strFrom += '<div>\n                                                <div class="input-group placeholder">\n                                                    <input type="text" id="txt' + val.replace(/\s+/g, '') + '"  class="form-control txt' + formID + '" placeholder="' + val + '" value="' + currentValue + '">\n                                                    <div class="input-group-append">\n                                                        <button type=\'button\' id="btn' + val.replace(/\s+/g, '') + '" class="btn btn-default btn-sm j2HTMLDatePicker"><i class="fa fa-calendar"></i></button>\n                                                    </div>\n                                                </div>\n                                            </div>';
                            } else {
                                if (vals[val] !== null) {
                                    currentValue = vals[val];
                                } else {
                                    currentValue = '';
                                }

                                //TEXT AREA
                                if (currentValue.length > maxTextLimit) {
                                    strFrom += '<div class="placeholder">\n                                    <textarea class="form-control txt' + formID + '" rows="5" id="txt' + val.replace(/\s+/g, '') + '" value="Yes" placeholder="' + val + '" style="resize: none;">' + currentValue + '</textarea>\n                                </div>';
                                } else {
                                    strFrom += '<div class="placeholder">\n                                                <input type="text" id="txt' + val.replace(/\s+/g, '') + '" class="form-control txt' + formID + '" placeholder="' + val + '" value="' + currentValue + '">\n                                            </div>';
                                }

                                // strFrom += `<div class="placeholder">
                                //                 <input type="text" id="txt${val.replace(/\s+/g, '')}" class="form-control txt${formID}" placeholder="${val}" value="${currentValue}">
                                //             </div>`;
                            }
                        }
                    } else if (exclude === false) {
                        if (privateMethods.isThisDate(vals[val])) {
                            currentValue = vals[val].split('T')[0];

                            strFrom += '<div>\n                                            <div class="input-group placeholder">\n                                                <input type="text" id="txt' + val.replace(/\s+/g, '') + '"  class="form-control txt' + formID + '" placeholder="' + val + '" value="' + currentValue + '">\n                                                <div class="input-group-append">\n                                                    <button type=\'button\' id="btn' + val.replace(/\s+/g, '') + '" class="btn btn-default btn-sm j2HTMLDatePicker"><i class="fa fa-calendar"></i></button>\n                                                </div>\n                                            </div>\n                                        </div>';
                        } else {
                            if (vals[val] !== null) {
                                currentValue = vals[val];
                            } else {
                                currentValue = '';
                            }
                            strFrom += '<div class="placeholder">\n                                            <input type="text" id="txt' + val.replace(/\s+/g, '') + '" class="form-control txt' + formID + '" placeholder="' + val + '" value="' + currentValue + '">\n                                        </div>';
                        }
                    }
                }
                if (include !== false) {

                    for (var _key in include) {

                        var name = include[_key].Name;
                        var value = include[_key].Value;

                        strFrom += '<div class="placeholder">\n                                        <input type="text" id="txt' + name.replace(/\s+/g, '') + '" class="form-control txt' + formID + '" placeholder="' + name + '" value="' + value + '">\n                                    </div>';
                    }
                }

                strFrom += '</form>';
            }
            return strFrom;
        },
        isThisDate: function isThisDate(value) {
            var isDate = false;
            if (typeof value === 'string') {

                var reISO = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*))(?:Z|(\+|-)([\d|:]*))?$/;
                var a = reISO.exec(value);
                if (a) {

                    isDate = true;
                }
            }
            return isDate;
        }
    };

    //endregion

    return j2HTMLForm;
}();
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var httpVerbs = function () {

    //CREAT CLASS
    var httpVerbs = function () {
        function httpVerbs() {
            _classCallCheck(this, httpVerbs);

            this._url = false;
            this._headers = false;
            this._body = false;
        }

        //#region ************** PROPERTIES ******************

        // PROPERTY -- URL


        _createClass(httpVerbs, [{
            key: 'GetData',


            // PROPERTY -- Callback
            // get callback() { return this._callback; }
            // set callback(value) { this._callback = value; }


            //endregion

            //#region ************** PUBLIC FUNCTIONS ************
            /**
             * GET DATA (Retrieves a resource or list of resources)
             */
            value: function GetData(fun) {

                var error = '';

                paramHeaders = this._headers;

                if (this._url === false) {
                    error = 'url is required';
                }

                if (error === '') {

                    //CREATE REQUEST OBJECT
                    var reqObj = {
                        method: 'GET',
                        mode: 'cors',
                        cache: 'default'
                    };
                    //SETTING HEADER
                    if (this._headers !== false) {

                        reqObj.headers = new Headers(paramHeaders);
                    }

                    //CREATE REQUEST
                    var request = new Request(this._url, reqObj);

                    //FETCH DATA FROM DATABASE
                    privateMethods.fetchData(request, function (error, data) {
                        fun(error, data);
                    });
                } else {
                    alert(error);
                    console.log(error);
                    throw new Error(error);
                }
            }
        }, {
            key: 'PostData',
            value: function PostData(fun) {

                var error = '';

                if (this._url === false) {
                    error += 'url is required\n';
                }
                if (this._body === false) {
                    error += 'body is required\n';
                }

                if (error === '') {
                    paramBody = this._body;
                    paramHeaders = this._headers;

                    //CREATE REQUEST OBJECT
                    var reqObj = {
                        method: 'POST',
                        mode: 'cors',
                        cache: 'default'
                    };

                    //SETTING HEADER
                    if (this._headers !== false) {

                        reqObj.headers = new Headers(paramHeaders);
                    }

                    //SETTING BODY
                    if (this._body !== false) {
                        reqObj.body = JSON.stringify(paramBody);
                    }

                    //CREATE REQUEST
                    var request = new Request(this._url, reqObj);

                    //FETCH DATA FROM DATABASE
                    privateMethods.fetchData(request, function (error, data) {
                        fun(error, data);
                    });
                } else {

                    alert(error);
                    console.log(error);
                    throw new Error(error);
                }
            }
        }, {
            key: 'PutData',
            value: function PutData(fun) {

                var error = '';

                if (this._url === false) {
                    error += 'url is required\n';
                }
                if (this._body === false) {
                    error += 'body is required\n';
                }

                if (error === '') {
                    paramBody = this._body;
                    paramHeaders = this._headers;

                    //CREATE REQUEST OBJECT
                    var reqObj = {
                        method: 'PUT',
                        mode: 'cors',
                        cache: 'default'
                    };

                    //SETTING HEADER
                    if (this._headers !== undefined) {

                        //reqObj.headers = new Headers(JSON.parse(JSON.stringify(paramHeaders)));
                        reqObj.headers = new Headers(paramHeaders);
                    }

                    //SETTING BODY
                    if (this._body !== false) {
                        reqObj.body = JSON.stringify(paramBody);
                    }

                    //CREATE REQUEST
                    var request = new Request(this._url, reqObj);
                    privateMethods.fetchData(request, function (error, data) {
                        fun(error, data);
                    });
                } else {
                    alert(error);
                    console.log(error);
                    throw new Error(error);
                }
            }
        }, {
            key: 'DeleteData',
            value: function DeleteData(fun) {

                var error = '';

                if (this._url === false) {
                    error += 'url is required\n';
                }

                if (error === '') {
                    paramHeaders = this._headers;
                    paramBody = this._body;

                    //CREATE REQUEST OBJECT
                    var reqObj = {
                        method: 'DELETE',
                        mode: 'cors',
                        cache: 'default'
                    };

                    //SETTING HEADER
                    if (this._headers !== false) {

                        reqObj.headers = new Headers(paramHeaders);
                    }

                    //SETTING BODY
                    if (this._body !== false) {
                        reqObj.body = JSON.stringify(paramBody);
                    }

                    //CREATE REQUEST
                    var request = new Request(this._url, reqObj);

                    privateMethods.fetchData(request, function (error, data) {
                        fun(error, data);
                    });
                } else {
                    alert(error);
                    console.log(error);
                    throw new Error(error);
                }
            }
            //endregion


        }, {
            key: 'url',
            get: function get() {
                return this._url;
            },
            set: function set(value) {
                this._url = value;
            }

            // PROPERTY -- Parameters
            // get parameters() { return this._parameters; }
            // set parameters(value) { this._parameters = value; }

            // PROPERTY -- Header

        }, {
            key: 'headers',
            get: function get() {
                return this._headers;
            },
            set: function set(value) {
                this._headers = value;
            }

            // PROPERTY -- body

        }, {
            key: 'body',
            get: function get() {
                return this._body;
            },
            set: function set(value) {
                this._body = value;
            }
        }]);

        return httpVerbs;
    }();

    //#region ************** PRIVATE VARIABLE AND METHOD *******

    var paramHeaders = '';
    var paramBody = '';

    //PRIVATE FUNCTIONS
    var privateMethods = {

        /**
         * 
         * @param {*} request 
         * @param {*} callback 
         */
        fetchData: function fetchData(request, callback) {

            fetch(request).then(function (response) {

                if (response.status !== 200) {

                    console.log('Looks like there was a problem. Status Code: ' + response.status);
                    if (response.status === 204) {
                        return 'The request was successful but there is no content to return (i.e. the response is empty)';
                    } else {
                        return response.json();
                    }

                    //return response.json();
                }
                var contentType = response.headers.get("content-type");
                if (contentType && contentType.indexOf("application/json") >= 0) {
                    return response.json();
                } else {
                    throw new TypeError("Oops, we haven't got JSON!");
                }
            }).then(function (data) {
                callback(null, data);
            }).catch(function (err) {
                console.log(err);
                callback(err, null);
            });
        },
        privareFunction2: function privareFunction2() {}
    };

    //endregion


    return httpVerbs;
}();
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

(function webpackUniversalModuleDefinition(root, factory) {
	if ((typeof exports === 'undefined' ? 'undefined' : _typeof(exports)) === 'object' && (typeof module === 'undefined' ? 'undefined' : _typeof(module)) === 'object') module.exports = factory();else if (typeof define === 'function' && define.amd) define([], factory);else if ((typeof exports === 'undefined' ? 'undefined' : _typeof(exports)) === 'object') exports["Datepickk"] = factory();else root["Datepickk"] = factory();
})(this, function () {
	return (/******/function (modules) {
			// webpackBootstrap
			/******/ // The module cache
			/******/var installedModules = {};
			/******/
			/******/ // The require function
			/******/function __webpack_require__(moduleId) {
				/******/
				/******/ // Check if module is in cache
				/******/if (installedModules[moduleId]) {
					/******/return installedModules[moduleId].exports;
					/******/
				}
				/******/ // Create a new module (and put it into the cache)
				/******/var module = installedModules[moduleId] = {
					/******/i: moduleId,
					/******/l: false,
					/******/exports: {}
					/******/ };
				/******/
				/******/ // Execute the module function
				/******/modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
				/******/
				/******/ // Flag the module as loaded
				/******/module.l = true;
				/******/
				/******/ // Return the exports of the module
				/******/return module.exports;
				/******/
			}
			/******/
			/******/
			/******/ // expose the modules object (__webpack_modules__)
			/******/__webpack_require__.m = modules;
			/******/
			/******/ // expose the module cache
			/******/__webpack_require__.c = installedModules;
			/******/
			/******/ // define getter function for harmony exports
			/******/__webpack_require__.d = function (exports, name, getter) {
				/******/if (!__webpack_require__.o(exports, name)) {
					/******/Object.defineProperty(exports, name, {
						/******/configurable: false,
						/******/enumerable: true,
						/******/get: getter
						/******/ });
					/******/
				}
				/******/
			};
			/******/
			/******/ // getDefaultExport function for compatibility with non-harmony modules
			/******/__webpack_require__.n = function (module) {
				/******/var getter = module && module.__esModule ?
				/******/function getDefault() {
					return module['default'];
				} :
				/******/function getModuleExports() {
					return module;
				};
				/******/__webpack_require__.d(getter, 'a', getter);
				/******/return getter;
				/******/
			};
			/******/
			/******/ // Object.prototype.hasOwnProperty.call
			/******/__webpack_require__.o = function (object, property) {
				return Object.prototype.hasOwnProperty.call(object, property);
			};
			/******/
			/******/ // __webpack_public_path__
			/******/__webpack_require__.p = "";
			/******/
			/******/ // Load entry module and return exports
			/******/return __webpack_require__(__webpack_require__.s = 0);
			/******/
		}(
		/************************************************************************/
		/******/[
		/* 0 */
		/***/function (module, exports, __webpack_require__) {

			"use strict";

			Object.defineProperty(exports, "__esModule", {
				value: true
			});

			__webpack_require__(1);

			function Datepickk(args) {
				Datepickk.numInstances = (Datepickk.numInstances || 0) + 1;
				var that = this;
				var eventName = 'click';
				var selectedDates = [];

				var currentYear = new Date().getFullYear();
				var currentMonth = new Date().getMonth() + 1;

				var languages = {
					no: {
						monthNames: ['Januar', 'Februar', 'Mars', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Desember'],
						dayNames: ['sø', 'ma', 'ti', 'on', 'to', 'fr', 'lø'],
						weekStart: 1
					},
					se: {
						monthNames: ['januari', 'februari', 'mars', 'april', 'maj', 'juni', 'juli', 'augusti', 'september', 'oktober', 'november', 'december'],
						dayNames: ['sö', 'må', 'ti', 'on', 'to', 'fr', 'lö'],
						weekStart: 1
					},
					ru: {
						monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
						dayNames: ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'],
						weekStart: 1
					},
					en: {
						monthNames: ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'],
						dayNames: ['su', 'mo', 'tu', 'we', 'th', 'fr', 'sa'],
						weekStart: 0
					},
					de: {
						monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
						dayNames: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
						weekStart: 1
					}
				};

				/*Language aliases*/
				languages.nb = languages.no;
				languages.nn = languages.no;

				var range = false;
				var maxSelections = null;
				var container = document.body;
				var opened = false;
				var months = 1;
				var closeOnSelect = false;
				var button = null;
				var title = null;
				var onNavigation = null;
				var onClose = null;
				var onConfirm = null;
				var closeOnClick = true;
				var inline = false;
				var lang = 'en';
				var onSelect = null;
				var disabledDates = [];
				var disabledDays = [];
				var highlight = [];
				var tooltips = {};
				var daynames = true;
				var today = true;
				var startDate = null;
				var minDate = null;
				var maxDate = null;
				var weekStart = null;
				var locked = false;

				function generateDaynames() {
					that.el.days.innerHTML = '';
					var ws = weekStart !== null ? weekStart : languages[lang].weekStart;
					if (daynames) {
						for (var x = 0; x < months && x < 3; x++) {
							var weekEl = document.createElement('div');
							weekEl.setAttribute('class', 'd-week');
							for (var i = 0; i < 7; i++) {
								var dayNameIndex = i + ws > languages[lang].dayNames.length - 1 ? i + ws - languages[lang].dayNames.length : i + ws;

								var dayEl = document.createElement('div');
								var dayTextEl = document.createElement('p');
								dayTextEl.innerHTML = languages[lang].dayNames[dayNameIndex];

								dayEl.appendChild(dayTextEl);
								weekEl.appendChild(dayEl);
							}

							that.el.days.appendChild(weekEl);
						}
					}
				}

				function generateYears() {
					[].slice.call(that.el.yearPicker.childNodes).forEach(function (node, index) {
						node.innerHTML = "'" + (currentYear + parseInt(node.getAttribute('data-year'))).toString().substring(2, 4);
					});
				}

				function generateInputs() {
					that.el.tables.innerHTML = '';
					for (var x = 0; x < months; x++) {
						var container = document.createElement('div');
						container.setAttribute('class', 'd-table');
						for (var i = 0; i < 42; i++) {
							var input = document.createElement('input');
							input.type = 'checkbox';
							input.id = Datepickk.numInstances + '-' + x + '-d-day-' + i;
							var label = document.createElement('label');
							label.setAttribute("for", Datepickk.numInstances + '-' + x + '-d-day-' + i);

							var text = document.createElement('text');

							var tooltip = document.createElement('span');
							tooltip.setAttribute('class', 'd-tooltip');

							container.appendChild(input);
							container.appendChild(label);

							label.appendChild(text);
							label.appendChild(tooltip);

							input.addEventListener(eventName, function (event) {
								if (locked) {
									event.preventDefault();
								}
							});
							input.addEventListener('change', inputChange);
						}

						that.el.tables.appendChild(container);
					}

					that.el.tables.addEventListener('mouseover', highlightLegend);
					that.el.tables.addEventListener('mouseout', highlightLegend);

					function highlightLegend(e) {
						if (e.target.nodeName !== 'LABEL') return;

						var legendIds = e.target.getAttribute('data-legend-id') ? e.target.getAttribute('data-legend-id').split(' ') : [];
						if (!legendIds.length) return;

						legendIds.forEach(function (legendId) {
							var element = that.el.legend.querySelector('[data-legend-id="' + legendId + '"]');
							if (e.type == 'mouseover' && element) {
								var color = element.getAttribute('data-color') ? hexToRgb(element.getAttribute('data-color')) : null;
								element.setAttribute('style', 'background-color:rgba(' + color.r + ',' + color.g + ',' + color.b + ',0.35);');
							} else if (element) {
								element.removeAttribute('style');
							}
						});

						function hexToRgb(hex) {
							var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
							return result ? {
								r: parseInt(result[1], 16),
								g: parseInt(result[2], 16),
								b: parseInt(result[3], 16)
							} : null;
						}
					}
				}

				function generateLegends() {
					var start = new Date(that.el.tables.childNodes[0].childNodes[0].getAttribute('data-date'));
					var end = new Date(that.el.tables.childNodes[months - 1].childNodes[82].getAttribute('data-date'));
					var _highlights = highlight.filter(function (x) {
						for (var m = 0; m < x.dates.length; m++) {
							if (x.dates[m].start < end && x.dates[m].end > start) {
								return true;
							}
						}
						return false;
					});
					var legends = [];
					for (var l = 0; l < _highlights.length; l++) {
						if ('legend' in _highlights[l] && _highlights[l].legend) {
							var oldLegend = container.querySelector('.d-legend-item[data-legend="' + _highlights[l].legend + '"][data-color="' + _highlights[l].backgroundColor + '"]');
							if (oldLegend == null) {
								var legendItem = document.createElement('p');
								legendItem.setAttribute('class', 'd-legend-item');
								legendItem.setAttribute('data-legend', _highlights[l].legend);
								legendItem.setAttribute('data-legend-id', highlight.indexOf(_highlights[l]));
								legendItem.setAttribute('data-color', _highlights[l].backgroundColor);
								var legendItemPoint = document.createElement('span');
								legendItemPoint.setAttribute('style', 'background-color:' + _highlights[l].backgroundColor);

								legendItem.appendChild(legendItemPoint);

								that.el.legend.appendChild(legendItem);
								legendItem.addEventListener('mouseover', hoverLegend);
								legendItem.addEventListener('mouseout', hoverLegend);
								legends.push(legendItem);
							} else {
								legends.push(oldLegend);
							}
						}
					}

					[].slice.call(that.el.legend.querySelectorAll('.d-legend-item')).forEach(function (item) {
						if (legends.indexOf(item) < 0) {
							item.removeEventListener('mouseover', hoverLegend);
							item.removeEventListener('mouseout', hoverLegend);
							that.el.legend.removeChild(item);
						}
					});

					function hoverLegend(e) {
						[].slice.call(that.el.tables.querySelectorAll('[data-legend-id*="' + this.getAttribute('data-legend-id') + '"]')).forEach(function (element) {
							if (e.type == 'mouseover') element.classList.add('legend-hover');else element.classList.remove('legend-hover');
						});
					}
				}

				function parseMonth(month) {
					if (month > 11) month -= 12;else if (month < 0) month += 12;
					return month;
				}

				function generateDates(year, month) {
					var monthElements = that.el.querySelectorAll('.d-table');
					var ws = weekStart !== null ? weekStart : languages[lang].weekStart;

					[].slice.call(that.el.querySelectorAll('.d-table')).forEach(function (element, index) {
						var days = new Date(year, month + index, 0).getDate();
						var daysLast = new Date(year, month + index - 1, 0).getDate();
						var startDay = new Date(year, month + index - 1, 1).getDay();
						var startDate = null;
						var endDate = null;
						if (startDay - ws < 0) {
							startDay = 7 - ws;
						} else {
							startDay -= ws;
						}
						var monthText = languages[lang].monthNames[parseMonth(month - 1 + index)];
						element.setAttribute('data-month', monthText);

						[].slice.call(element.querySelectorAll('.d-table input')).forEach(function (inputEl, i) {
							var labelEl = inputEl.nextSibling;

							inputEl.checked = false;
							inputEl.removeAttribute('disabled');
							labelEl.removeAttribute('style');
							labelEl.removeAttribute('data-legend-id');
							labelEl.className = '';

							var date = null;
							if (i < startDay) {
								labelEl.childNodes[0].innerHTML = daysLast - (startDay - i - 1);
								if (index == 0) {
									date = new Date(year, month + index - 2, daysLast - (startDay - i - 1));
									labelEl.className = 'prev';
								} else {
									date = '';
									labelEl.className = 'd-hidden';
									inputEl.setAttribute('disabled', true);
								}
							} else if (i < days + startDay) {
								date = new Date(year, month + index - 1, i - startDay + 1);
								labelEl.childNodes[0].innerHTML = i - startDay + 1;
								labelEl.className = '';
							} else {
								labelEl.childNodes[0].innerHTML = i - days - startDay + 1;
								if (index == monthElements.length - 1) {
									date = new Date(year, month + index, i - days - startDay + 1);
									labelEl.className = 'next';
								} else {
									date = '';
									labelEl.className = 'd-hidden';
									inputEl.setAttribute('disabled', true);
								}
							}

							if (date instanceof Date) {
								inputEl.setAttribute('data-date', date.toJSON());

								if (disabledDates.indexOf(date.getTime()) != -1 || disabledDays.indexOf(date.getDay()) != -1) {
									inputEl.setAttribute('disabled', true);
								}

								if (minDate && date < minDate || maxDate && date > maxDate) {
									inputEl.setAttribute('disabled', true);
									labelEl.className = 'd-hidden';
								}

								if (today && date.getTime() == new Date().setHours(0, 0, 0, 0)) {
									labelEl.classList.add('today');
								} else {
									labelEl.classList.remove('today');
								}

								if (tooltips[date.getTime()]) {
									labelEl.childNodes[0].setAttribute('data-tooltip', true);
									labelEl.childNodes[1].innerHTML = tooltips[date.getTime()];
								} else {
									labelEl.childNodes[0].removeAttribute('data-tooltip');
									labelEl.childNodes[1].innerHTML = '';
								}

								var _highlights = highlight.filter(function (x) {
									for (var m = 0; m < x.dates.length; m++) {
										if (date.getTime() >= x.dates[m].start.getTime() && date.getTime() <= x.dates[m].end.getTime()) {
											return true;
										}
									}
									return false;
								});

								if (_highlights.length > 0) {
									var bgColor = '';
									var legendIds = '';

									if (_highlights.length > 1) {
										var percent = Math.round(100 / _highlights.length);
										bgColor = 'background: linear-gradient(-45deg,';
										for (var z = 0; z < _highlights.length; z++) {
											legendIds += highlight.indexOf(_highlights[z]);
											if (z !== _highlights.length - 1) {
												legendIds += ' ';
											}
											bgColor += _highlights[z].backgroundColor + ' ' + percent * z + '%';
											if (z != _highlights.length - 1) {
												bgColor += ',';
												bgColor += _highlights[z].backgroundColor + ' ' + percent * (z + 1) + '%,';
											}
										}
										bgColor += ');';
									} else {
										bgColor = _highlights[0].backgroundColor ? 'background:' + _highlights[0].backgroundColor + ';' : '';
										legendIds += highlight.indexOf(_highlights[0]);
									}
									var Color = _highlights[0].color ? 'color:' + _highlights[0].color + ';' : '';
									labelEl.setAttribute('style', bgColor + Color);
									labelEl.setAttribute('data-legend-id', legendIds);
								}
							}
						});
					});

					generateLegends();
				};

				function setDate() {
					if (!that.el.tables.childNodes.length || !that.el.tables.childNodes[0].childNodes.length) return;

					resetCalendar();

					if (currentMonth > 12 || currentMonth < 1) {
						if (currentMonth > 12) {
							currentYear += 1;
							currentMonth -= 12;
						} else {
							currentYear -= 1;
							currentMonth += 12;
						}
					}

					if (maxDate && new Date(currentYear, currentMonth - 1 + months - 1, 1) >= new Date(maxDate).setDate(1)) {
						currentYear = maxDate.getFullYear();
						currentMonth = maxDate.getMonth() + 1 - months + 1;
						that.el.header.childNodes[2].setAttribute('style', 'visibility:hidden');
					} else {
						that.el.header.childNodes[2].removeAttribute('style');
					}
					if (minDate && new Date(currentYear, currentMonth - 1, 1) <= new Date(minDate).setDate(1)) {
						currentYear = minDate.getFullYear();
						currentMonth = minDate.getMonth() + 1;
						that.el.header.childNodes[0].setAttribute('style', 'visibility:hidden');
					} else {
						that.el.header.childNodes[0].removeAttribute('style');
					}

					for (var c = 0; c < months; c++) {
						var index = currentMonth - 1 + c;
						if (index > 11) {
							index -= 12;
						} else if (index < 0) {
							index += 12;
						}

						that.el.monthPicker.childNodes[index].classList.add('current');
					}

					generateDates(currentYear, currentMonth);
					generateYears();
					var startmonth = languages[lang].monthNames[currentMonth - 1];
					var endmonth = '';
					if (months > 1) {
						endmonth += ' - ';
						var monthint = currentMonth - 1 + months - 1;
						if (monthint > 11) {
							monthint -= 12;
						} else if (monthint < 0) {
							monthint += 12;
						}
						endmonth += languages[lang].monthNames[monthint];
					}
					var yearname = currentMonth - 1 + months - 1 > 11 ? currentYear.toString().substring(2, 4) + '/' + (currentYear + 1).toString().substring(2, 4) : currentYear.toString().substring(2, 4);
					that.el.header.childNodes[1].childNodes[0].innerHTML = startmonth + endmonth;
					that.el.header.childNodes[1].childNodes[1].innerHTML = yearname;

					that.el.yearPicker.querySelector('[data-year="0"]').classList.add('current');
					if (currentMonth - 1 + months - 1 > 11) {
						that.el.yearPicker.querySelector('[data-year="1"]').classList.add('current');
					}

					renderSelectedDates();
					if (onNavigation) onNavigation.call(that);
				};

				function renderSelectedDates() {
					selectedDates.forEach(function (date) {
						var el = that.el.querySelector('[data-date="' + date.toJSON() + '"]');
						if (el) {
							el.checked = true;
						}
					});

					that.el.tables.classList.remove('before');
					if (range && selectedDates.length > 1) {
						var currentDate = new Date(currentYear, currentMonth - 1, 1);
						var sorted = selectedDates.sort(function (a, b) {
							return a.getTime() - b.getTime();
						});
						var first = that.el.querySelector('[data-date="' + sorted[0].toJSON() + '"]');
						if (!first && currentDate >= new Date(sorted[0].getFullYear(), sorted[0].getMonth(), 1) && currentDate <= new Date(sorted[1].getFullYear(), sorted[1].getMonth(), 1)) {
							that.el.tables.classList.add('before');
						}
					}
				};

				function resetCalendar() {
					[].slice.call(that.el.querySelectorAll('.d-table input')).forEach(function (inputEl) {
						inputEl.checked = false;
					});

					[].slice.call(that.el.monthPicker.querySelectorAll('.current')).forEach(function (monthPickEl) {
						monthPickEl.classList.remove('current');
					});

					[].slice.call(that.el.yearPicker.querySelectorAll('.current')).forEach(function (yearPickEl) {
						yearPickEl.classList.remove('current');
					});
				};

				function nextMonth() {
					currentMonth += months;
					setDate();
				};

				function prevMonth() {
					currentMonth -= months;
					setDate();
				};

				function selectDate(date, ignoreOnSelect) {
					date = new Date(date);
					date.setHours(0, 0, 0, 0);
					var el = that.el.querySelector('[data-date="' + date.toJSON() + '"]');

					if (range && el && el.checked) {
						el.classList.add('single');
					}

					if (el && !el.checked) {
						el.checked = true;
					}

					selectedDates.push(date);

					if (onSelect && !ignoreOnSelect) {
						onSelect.apply(date, [true]);
					}
				};

				function unselectDate(date, ignoreOnSelect) {
					date = new Date(date);
					date.setHours(0, 0, 0, 0);
					var el = that.el.querySelector('[data-date="' + date.toJSON() + '"]');
					if (el) {
						el.classList.remove('single');
						if (el.checked) {
							el.checked = false;
						}
					}

					selectedDates = selectedDates.filter(function (x) {
						return x.getTime() != date.getTime();
					});

					if (onSelect && !ignoreOnSelect) {
						onSelect.call(date, false);
					}
				};

				function unselectAll(ignoreOnSelect) {
					selectedDates.forEach(function (date) {
						unselectDate(date, ignoreOnSelect);
					});
				};

				function inputChange(e) {
					var input = this;
					var date = new Date(input.getAttribute('data-date'));
					input.classList.remove('single');
					if (locked) {
						return;
					}
					if (range) {
						that.el.tables.classList.remove('before');
					}
					if (input.checked) {
						if (maxSelections && selectedDates.length > maxSelections - 1) {
							var length = selectedDates.length;
							for (length; length > maxSelections - 1; length--) {
								unselectDate(selectedDates[0]);
							}
						}

						if (range && selectedDates.length) {
							var first = that.el.querySelector('[data-date="' + selectedDates[0].toJSON() + '"]');
							if (!first && date > selectedDates[0]) {
								that.el.tables.classList.add('before');
							}
						}

						selectedDates.push(date);

						if (closeOnSelect) {
							that.hide();
						}
					} else {
						if (range && selectedDates.length == 1 && selectedDates[0].getTime() == date.getTime()) {
							selectDate(date);
							input.classList.add('single');
						} else {
							selectedDates = selectedDates.filter(function (x) {
								return x.getTime() != date.getTime();
							});
						}
					}

					if (onSelect) {
						onSelect.call(date, input.checked);
					}
				};

				function setRange(val) {
					if (val) {
						range = true;
						that.el.tables.classList.add('range');
					} else {
						range = false;
						that.el.tables.classList.remove('range');
					}
				};

				function show(properties) {
					if (!that.inline && that.container === document.body) {
						document.body.classList.add('d-noscroll');
					}
					setArgs(properties);
					var handler = function handler() {
						that.el.classList.remove('d-show');
						that.el.calendar.removeEventListener(whichAnimationEvent(), handler);
					};
					that.el.calendar.addEventListener(whichAnimationEvent(), handler);
					that.el.classList.add('d-show');
					container.appendChild(that.el);
					opened = true;
					if (startDate) {
						currentMonth = startDate.getMonth() + 1;
						currentYear = startDate.getFullYear();
					}
					setDate();
				};

				function hide() {
					document.body.classList.remove('d-noscroll');
					var handler = function handler() {
						that.el.parentNode.removeChild(that.el);
						opened = false;
						that.el.classList.remove('d-hide');
						if (typeof onClose == 'function') {
							onClose.apply(that);
						}
						that.el.removeEventListener(whichAnimationEvent(), handler);
					};
					that.el.addEventListener(whichAnimationEvent(), handler);
					that.el.classList.add('d-hide');
				};

				function bindEvents() {
					that.el.header.childNodes[0].addEventListener(eventName, prevMonth);
					that.el.header.childNodes[2].addEventListener(eventName, nextMonth);
					that.el.header.childNodes[1].childNodes[0].addEventListener(eventName, function () {
						if (that.el.monthPicker.classList.contains('d-show')) {
							that.el.monthPicker.classList.remove('d-show');
						} else {
							that.el.monthPicker.classList.add('d-show');
						}
						that.el.yearPicker.classList.remove('d-show');
					});
					that.el.header.childNodes[1].childNodes[1].addEventListener(eventName, function () {
						generateYears();
						if (that.el.yearPicker.classList.contains('d-show')) {
							that.el.yearPicker.classList.remove('d-show');
						} else {
							that.el.yearPicker.classList.add('d-show');
						}
						that.el.monthPicker.classList.remove('d-show');
					});
					that.el.button.addEventListener(eventName, hide);

					that.el.overlay.addEventListener(eventName, function () {
						if (closeOnClick) {
							that.hide();
						}
					});

					[].slice.call(that.el.monthPicker.childNodes).forEach(function (monthPicker) {
						monthPicker.addEventListener(eventName, function () {
							currentMonth = parseInt(this.getAttribute('data-month'));
							setDate();
							that.el.monthPicker.classList.remove('d-show');
						});
					});

					[].slice.call(that.el.yearPicker.childNodes).forEach(function (yearPicker) {
						yearPicker.addEventListener(eventName, function () {
							currentYear += parseInt(this.getAttribute('data-year'));
							setDate();
							that.el.yearPicker.classList.remove('d-show');
						});
					});

					var startX = 0;
					var distance = 0;
					that.el.calendar.addEventListener('touchstart', function (e) {
						startX = e.changedTouches[0].clientX || e.originalEvent.changedTouches[0].clientX;
						//e.preventDefault();
					});

					that.el.calendar.addEventListener('touchmove', function (e) {
						distance = e.changedTouches[0].clientX - startX || e.originalEvent.changedTouches[0].clientX - startX;
						e.preventDefault();
					});

					that.el.calendar.addEventListener('touchend', function (e) {
						if (distance > 50) {
							prevMonth();
						} else if (distance < -50) {
							nextMonth();
						}
						distance = 0;
					});
				};

				function setArgs(x) {
					for (var key in x) {
						if (key in that) {
							that[key] = x[key];
						}
					};
				};

				function init() {
					that.el = document.createElement('div');
					that.el.id = 'Datepickk';
					that.el.classList.add(getBrowserVersion().type);
					that.el.innerHTML = template;
					that.el.calendar = that.el.childNodes[1];
					that.el.titleBox = that.el.childNodes[0];
					that.el.button = that.el.childNodes[3];
					that.el.header = that.el.calendar.childNodes[0];
					that.el.monthPicker = that.el.calendar.childNodes[1];
					that.el.yearPicker = that.el.calendar.childNodes[2];
					that.el.tables = that.el.calendar.childNodes[4];
					that.el.days = that.el.calendar.childNodes[3];
					that.el.overlay = that.el.childNodes[4];
					that.el.legend = that.el.childNodes[2];

					setArgs(args);

					generateInputs();
					generateDaynames();
					bindEvents();

					if (inline) {
						show();
					}
				}

				that.show = show;
				that.hide = hide;
				that.selectDate = selectDate;
				that.unselectAll = unselectAll;
				that.unselectDate = unselectDate;

				function currentDateGetter() {
					return new Date(currentYear, currentMonth - 1, 1);
				}
				function currentDateSetter(x) {
					x = new Date(x);
					currentMonth = x.getMonth() + 1;
					currentYear = x.getFullYear();
					setDate();
				}

				Object.defineProperties(that, {
					"selectedDates": {
						get: function get() {
							return selectedDates.sort(function (a, b) {
								return a.getTime() - b.getTime();
							});
						}
					},
					"range": {
						get: function get() {
							return range;
						},
						set: function set(x) {
							setRange(x);
							if (x) {
								maxSelections = 2;
							}
						}
					},
					"button": {
						get: function get() {
							return button;
						},
						set: function set(x) {
							if (typeof x == 'string') {
								button = x;
							} else {
								button = null;
							}
							that.el.button.innerHTML = button ? button : '';
						}
					},
					"title": {
						get: function get() {
							return title;
						},
						set: function set(x) {
							if (typeof x == 'string') {
								title = x;
							} else {
								title = null;
							}
							that.el.titleBox.innerText = title ? title : '';
						}
					},
					"lang": {
						get: function get() {
							return lang;
						},
						set: function set(x) {
							if (x in languages) {
								lang = x;
								generateDaynames();
								setDate();
							} else {
								console.error('Language not found');
							}
						}
					},
					"weekStart": {
						get: function get() {
							return weekStart !== null ? weekStart : languages[lang].weekStart;
						},
						set: function set(x) {
							if (typeof x == 'number' && x > -1 && x < 7) {
								weekStart = x;
								generateDaynames();
								setDate();
							} else {
								console.error('weekStart must be a number between 0 and 6');
							}
						}
					},
					"months": {
						get: function get() {
							return months;
						},
						set: function set(x) {
							if (typeof x == 'number' && x > 0) {
								months = x;
								generateDaynames();
								generateInputs();
								setDate();

								if (months == 1) {
									that.el.classList.remove('multi');
								} else {
									that.el.classList.add('multi');
								}
							} else {
								console.error('months must be a number > 0');
							}
						}
					},
					"isOpen": {
						get: function get() {
							return opened;
						}
					},
					"closeOnSelect": {
						get: function get() {
							return closeOnSelect;
						},
						set: function set(x) {
							if (x) {
								closeOnSelect = true;
							} else {
								closeOnSelect = false;
							}
						}
					},
					"disabledDays": {
						get: function get() {
							return disabledDays;
						},
						set: function set(x) {
							if (x instanceof Array) {
								for (var i = 0; i < x.length; i++) {
									if (typeof x[i] == 'number') {
										disabledDays.push(x[i]);
									}
								}
							} else if (typeof x == 'number') {
								disabledDays = [x];
							} else if (!x) {
								disabledDays = [];
							}
							setDate();
						}
					},
					"disabledDates": {
						get: function get() {
							return disabledDates.map(function (x) {
								return new Date(x);
							});
						},
						set: function set(x) {
							if (x instanceof Array) {
								x.forEach(function (date) {
									if (date instanceof Date) {
										disabledDates.push(new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime());
									}
								});
							} else if (x instanceof Date) {
								disabledDates = [new Date(x.getFullYear(), x.getMonth(), x.getDate()).getTime()];
							} else if (!x) {
								disabledDates = [];
							}
							setDate();
						}
					},
					"highlight": {
						get: function get() {
							return highlight;
						},
						set: function set(x) {
							if (x instanceof Array) {
								x.forEach(function (hl) {
									if (hl instanceof Object) {
										var highlightObj = {};
										highlightObj.dates = [];

										if ('start' in hl) {
											highlightObj.dates.push({
												start: new Date(hl.start.getFullYear(), hl.start.getMonth(), hl.start.getDate()),
												end: 'end' in hl ? new Date(hl.end.getFullYear(), hl.end.getMonth(), hl.end.getDate()) : new Date(hl.start.getFullYear(), hl.start.getMonth(), hl.start.getDate())
											});
										} else if ('dates' in hl && hl.dates instanceof Array) {
											hl.dates.forEach(function (hlDate) {
												highlightObj.dates.push({
													start: new Date(hlDate.start.getFullYear(), hlDate.start.getMonth(), hlDate.start.getDate()),
													end: 'end' in hlDate ? new Date(hlDate.end.getFullYear(), hlDate.end.getMonth(), hlDate.end.getDate()) : new Date(hlDate.start.getFullYear(), hlDate.start.getMonth(), hlDate.start.getDate())
												});
											});
										}

										highlightObj.color = hl.color;
										highlightObj.backgroundColor = hl.backgroundColor;
										highlightObj.legend = 'legend' in hl ? hl.legend : null;

										highlight.push(highlightObj);
									}
								});
							} else if (x instanceof Object) {
								var highlightObj = {};
								highlightObj.dates = [];

								if ('start' in x) {
									highlightObj.dates.push({
										start: new Date(x.start.getFullYear(), x.start.getMonth(), x.start.getDate()),
										end: 'end' in x ? new Date(x.end.getFullYear(), x.end.getMonth(), x.end.getDate()) : new Date(x.start.getFullYear(), x.start.getMonth(), x.start.getDate())
									});
								} else if ('dates' in x && x.dates instanceof Array) {
									x.dates.forEach(function (hlDate) {
										highlightObj.dates.push({
											start: new Date(hlDate.start.getFullYear(), hlDate.start.getMonth(), hlDate.start.getDate()),
											end: 'end' in hlDate ? new Date(hlDate.end.getFullYear(), hlDate.end.getMonth(), hlDate.end.getDate()) : new Date(hlDate.start.getFullYear(), hlDate.start.getMonth(), hlDate.start.getDate())
										});
									});
								}

								highlightObj.color = x.color;
								highlightObj.backgroundColor = x.backgroundColor;
								highlightObj.legend = 'legend' in x ? x.legend : null;

								highlight.push(highlightObj);
							} else if (!x) {
								highlight = [];
							}

							setDate();
						}
					},
					"onClose": {
						set: function set(callback) {
							onClose = callback;
						}
					},
					"onSelect": {
						set: function set(callback) {
							onSelect = callback;
						}
					},
					"today": {
						get: function get() {
							return today;
						},
						set: function set(x) {
							if (x) {
								today = true;
							} else {
								today = false;
							}
						}
					},
					"daynames": {
						get: function get() {
							return daynames;
						},
						set: function set(x) {
							if (x) {
								daynames = true;
							} else {
								daynames = false;
							}
							generateDaynames();
						}
					},
					"fullscreen": {
						get: function get() {
							return that.el.classList.contains('fullscreen');
						},
						set: function set(x) {
							if (x) {
								that.el.classList.add('fullscreen');
							} else {
								that.el.classList.remove('fullscreen');
							}
						}
					},
					"locked": {
						get: function get() {
							return locked;
						},
						set: function set(x) {
							if (x) {
								locked = true;
								that.el.tables.classList.add('locked');
							} else {
								locked = false;
								that.el.tables.classList.remove('locked');
							}
						}
					},
					"maxSelections": {
						get: function get() {
							return maxSelections;
						},
						set: function set(x) {
							if (typeof x == 'number' && !range) {
								maxSelections = x;
							} else {
								if (range) {
									maxSelections = 2;
								} else {
									maxSelections = null;
								}
							}
						}
					},
					"onConfirm": {
						set: function set(callback) {
							if (typeof callback == 'function') {
								onConfirm = callback.bind(that);
								that.el.button.addEventListener(eventName, onConfirm);
							} else if (!callback) {
								that.el.button.removeEventListener(eventName, onConfirm);
								onConfirm = null;
							}
						}
					},
					"onNavigation": {
						set: function set(callback) {
							if (typeof callback == 'function') {
								onNavigation = callback.bind(that);
							} else if (!callback) {
								onNavigation = null;
							}
						}
					},
					"closeOnClick": {
						get: function get() {
							return closeOnClick;
						},
						set: function set(x) {
							if (x) {
								closeOnClick = true;
							} else {
								closeOnClick = false;
							}
						}
					},
					"tooltips": {
						get: function get() {
							var ret = [];
							for (key in tooltips) {
								ret.push({
									date: new Date(parseInt(key)),
									text: tooltips[key]
								});
							}
							return ret;
						},
						set: function set(x) {
							if (x instanceof Array) {
								x.forEach(function (item) {
									if (item.date && item.text && item.date instanceof Date) {
										tooltips[new Date(item.date.getFullYear(), item.date.getMonth(), item.date.getDate()).getTime()] = item.text;
									}
								});
							} else if (x instanceof Object) {
								if (x.date && x.text && x.date instanceof Date) {
									tooltips[new Date(x.date.getFullYear(), x.date.getMonth(), x.date.getDate()).getTime()] = x.text;
								}
							} else if (!x) {
								tooltips = [];
							}
							setDate();
						}
					},
					"currentDate": {
						get: currentDateGetter,
						set: currentDateSetter
					},
					"setDate": {
						set: currentDateSetter
					},
					"startDate": {
						get: function get() {
							return startDate;
						},
						set: function set(x) {
							if (x) {
								startDate = new Date(x);
							} else {
								startDate = null;
								currentYear = new Date().getFullYear();
								currentMonth = new Date().getMonth() + 1;
							}
							setDate();
						}
					},
					"minDate": {
						get: function get() {
							return minDate;
						},
						set: function set(x) {
							minDate = x ? new Date(x) : null;
							setDate();
						}
					},
					"maxDate": {
						get: function get() {
							return maxDate;
						},
						set: function set(x) {
							maxDate = x ? new Date(x) : null;
							setDate();
						}
					},
					"container": {
						get: function get() {
							return container;
						},
						set: function set(x) {
							if (x instanceof String) {
								var y = document.querySelector(x);
								if (y) {
									container = y;
									if (container != document.body) {
										that.el.classList.add('wrapped');
									} else {
										that.el.classList.remove('wrapped');
									}
								} else {
									console.error("Container doesn't exist");
								}
							} else if (x instanceof HTMLElement) {
								container = x;
								if (container != document.body) {
									that.el.classList.add('wrapped');
								} else {
									that.el.classList.remove('wrapped');
								}
							} else {
								console.error("Invalid type");
							}
						}
					},
					"inline": {
						get: function get() {
							return inline;
						},
						set: function set(x) {
							if (x) {
								inline = true;
								that.el.classList.add('inline');
							} else {
								inline = false;
								that.el.classList.remove('inline');
							}
						}
					}

				});

				init();
				setDate();

				return Object.freeze(that);
			} /*!
     * Datepickk
     * Docs & License: https://crsten.github.com/datepickk
     * (c) 2017 Carsten Jacobsen
     */

			;

			function whichAnimationEvent() {
				var t;
				var el = document.createElement('fakeelement');
				var transitions = {
					'animation': 'animationend',
					'OAnimation': 'oanimationend',
					'MozAnimation': 'animationend',
					'WebkitAnimation': 'webkitAnimationEnd',
					'': 'MSAnimationEnd'
				};

				for (t in transitions) {
					if (el.style[t] !== undefined) {
						return transitions[t];
					}
				}
			}

			var template = '<div class="d-title"></div>' + '<div class="d-calendar">' + '<div class="d-header">' + '<i id="d-previous"></i>' + '<p><span class="d-month"></span><span class="d-year"></span></p>' + '<i id="d-next"></i>' + '</div>' + '<div class="d-month-picker">' + '<div data-month="1">1</div>' + '<div data-month="2">2</div>' + '<div data-month="3">3</div>' + '<div data-month="4">4</div>' + '<div data-month="5">5</div>' + '<div data-month="6">6</div>' + '<div data-month="7">7</div>' + '<div data-month="8">8</div>' + '<div data-month="9">9</div>' + '<div data-month="10">10</div>' + '<div data-month="11">11</div>' + '<div data-month="12">12</div>' + '</div>' + '<div class="d-year-picker">' + '<div data-year="-5"></div>' + '<div data-year="-4"></div>' + '<div data-year="-3"></div>' + '<div data-year="-2"></div>' + '<div data-year="-1"></div>' + '<div data-year="0"></div>' + '<div data-year="1"></div>' + '<div data-year="2"></div>' + '<div data-year="3"></div>' + '<div data-year="4"></div>' + '<div data-year="5"></div>' + '</div>' + '<div class="d-weekdays"></div>' + '<div class="d-tables"></div>' + '</div>' + '<div class="d-legend"></div>' + '<button class="d-confirm"></button>' + '<div class="d-overlay"></div>';

			var getBrowserVersion = function getBrowserVersion() {
				var browser = {
					type: null,
					version: null
				};

				var ua = navigator.userAgent,
				    tem,
				    ios,
				    M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
				ios = ua.match(/(iphone|ipad)\s+OS\s+([\d+_]+\d+)/i) || [];
				if (/trident/i.test(M[1])) {
					tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
					browser.type = 'MSIE';
					browser.version = parseInt(tem[1]);
					return browser;
				}
				if (M[1] === 'Chrome') {
					tem = ua.match(/\bOPR\/(\d+)/);
					if (tem != null) return 'Opera ' + tem[1];
				}
				if (ios[1]) {
					return browser = {
						type: 'iOS',
						version: ios[2]
					};
				}
				M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
				if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
				browser.type = M[0];
				browser.version = parseInt(M[1]);

				return browser;
			};

			exports.default = Datepickk;

			/***/
		},
		/* 1 */
		/***/function (module, exports) {

			// removed by extract-text-webpack-plugin

			/***/}]
		/******/)["default"]
	);
});
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var j2HTMLCarousel = function () {

    //#region ************** PRIVATE VARIABLE *******

    var apptTo = '';

    //CREAT CLASS

    var j2HTMLCarousel = function () {
        function j2HTMLCarousel() {
            _classCallCheck(this, j2HTMLCarousel);

            this._Id = false;
            this._appendTo = false;
            this._data = false;
            this._caption = false;
            this._image = false;
            this._start = 0;
            this._option = false;
        }

        //#region ************** PROPERTIES ******************

        // PROPERTY -- DATA

        _createClass(j2HTMLCarousel, [{
            key: 'getCarousel',


            //endregion

            //#region ************** PUBLIC FUNCTIONS ************
            value: function getCarousel() {

                var error = false;

                if (this._data === false) {
                    error += 'Data is required for carousel';
                }

                if (error === false) {

                    var strCarousel = void 0;
                    var carouselId = void 0;

                    if (this._Id === false) {
                        carouselId = 'j2HTMLCarousel';
                    } else {
                        carouselId = this._Id;
                    }

                    strCarousel = '<div id="' + carouselId + '" class="carousel slide" style="width:' + this._image.Width + 'px !important; height:' + this.image.Height + 'px !important;">';
                    var strUL = '<ul class="carousel-indicators">';

                    //CREATE CAROUSEL UNORDER LIST (UL)
                    for (var i = 0; i < this._data.length; i++) {

                        if (this._start === i) {
                            strUL += '<li data-target="#' + carouselId + '" data-slide-to="' + i + '" class="active"></li>';
                        } else {
                            strUL += '<li data-target="#' + carouselId + '" data-slide-to="' + i + '"></li>';
                        }
                    }

                    strUL += '</ul>';

                    strCarousel += strUL;

                    //CREATE INNER CAROUSEL
                    var strInnerCarousel = '<div class="carousel-inner">';

                    for (var i = 0; i < this._data.length; i++) {

                        var strCarouselItem = '<div class="carousel-item">\n                                            <img src="' + this._data[i][this._image.Column] + '" alt="' + this._data[i][this._image.Column] + '" width="' + this._image.Width + '" height="' + this.image.Height + '">';
                        if (this._caption !== false) {
                            strCarouselCaption = '<div class="carousel-caption">\n                                                 <h3>' + this._data[i][this._caption] + '</h3>\n                                              </div>';
                            strCarouselItem += strCarouselCaption;
                        }

                        strInnerCarousel += strCarouselItem + '</div>';
                    }

                    strInnerCarousel += '</div>';

                    var strCarouselControl = '<a class="carousel-control-prev" href="#' + carouselId + '" data-slide="prev">\n                                            <span class="carousel-control-prev-icon"></span>\n                                         </a>\n                                         <a class="carousel-control-next" href="#' + carouselId + '" data-slide="next">\n                                            <span class="carousel-control-next-icon"></span>\n                                          </a>';

                    strCarousel += strInnerCarousel + strCarouselControl + '</div>';

                    if (this._appendTo === false) {

                        j2HTML.AppendHTMLString({
                            Element: 'body',
                            HTMLString: strCarousel
                        });
                    } else {

                        j2HTML.EmptyElement({
                            Element: '' + this._appendTo
                        });

                        j2HTML.AppendHTMLString({
                            Element: this._appendTo,
                            HTMLString: strCarousel
                        });
                    }

                    var myCarousel = document.getElementById('' + carouselId);

                    if (this._option === false) {

                        this._option = {};
                        this._option.keyboard = true;
                    } else if (this._option !== false) {
                        this._option.keyboard = true;
                    }
                    var myCarouselInit = new Carousel(myCarousel, this._option);

                    if (this._start !== false) {
                        myCarouselInit.slideTo(this._start);
                    }
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }

            //endregion


        }, {
            key: 'Id',
            get: function get() {
                return this._Id;
            },
            set: function set(value) {
                this._Id = value;
            }
        }, {
            key: 'appendTo',
            get: function get() {
                return this._appendTo;
            },
            set: function set(value) {
                this._appendTo = value;
            }
        }, {
            key: 'data',
            get: function get() {
                return this._data;
            },
            set: function set(value) {
                this._data = value;
            }
        }, {
            key: 'caption',
            get: function get() {
                return this._caption;
            },
            set: function set(value) {
                this._caption = value;
            }
        }, {
            key: 'image',
            get: function get() {
                return this._image;
            },
            set: function set(value) {
                this._image = value;
            }
        }, {
            key: 'start',
            get: function get() {
                return this._start;
            },
            set: function set(value) {
                this._start = value;
            }
        }, {
            key: 'option',
            get: function get() {
                return this._option;
            },
            set: function set(value) {
                this._option = value;
            }
        }]);

        return j2HTMLCarousel;
    }();

    //#region ************** PRIVATE METHODS *******


    //PRIVATE FUNCTIONS


    var privateMethods = {
        privareFunction1: function privareFunction1() {},
        privareFunction2: function privareFunction2() {}
    };

    //endregion


    return j2HTMLCarousel;
}();
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var j2HTMLDatePicker = function () {

    //#region ************** PRIVATE VARIABLE *******

    var btnText = '';
    var assignToElement = '';

    //CREAT CLASS

    var j2HTMLDatePicker = function () {
        function j2HTMLDatePicker() {
            _classCallCheck(this, j2HTMLDatePicker);

            this._title = false;
            this._startDate = false;
            this.setDate = false;
            this._minDate = false;
            this._maxDate = false;
            this._closeButtonText = false;
            this._assignSelectedDateTo = false;
            this._weekStartOn = false;
            this._showMonths = false;
            this._tooltip = false;
            this._maxDatesToSelect = false;
            this._appendTo = false;
            this._disabledDays = false;
            this._disabledDates = false;
            this._hideDayNames = false;
            this._showFullScreen = false;
            this._lockCalendar = false;
            this._getMultipleSelectedDates = false;
            this._highlight = false;
        }

        //#region ************** PROPERTIES ******************

        // PROPERTY -- DATA


        _createClass(j2HTMLDatePicker, [{
            key: 'getDatePicker',


            //endregion

            //#region ************** PUBLIC FUNCTIONS ************
            value: function getDatePicker() {

                var datepicker = new Datepickk();
                assignToElement = this._assignSelectedDateTo;

                /**
                 * 
                 * DATE PICKER SETTING
                 * 
                 */
                //SET TITLE
                if (this._title !== false) {
                    datepicker.title = this._title;
                }
                //BY DEFALUT CURRENT DATE IS SET
                if (this._setDate !== false) {
                    var _d = new Date(this._setDate);
                    var year = _d.getFullYear();
                    var _date = _d.getDate();
                    var month = _d.getMonth();
                    datepicker.setDate = new Date(year, month, _date);
                } else {
                    var d = new Date();
                    var _year = d.getFullYear();
                    var _date2 = d.getDate();
                    var _month = d.getMonth();
                    datepicker.currentDate = new Date(_year, _month, _date2);
                    datepicker.today = true;
                }
                //StartDate
                if (this._startDate !== false) {
                    var _d2 = new Date(this._startDate);
                    var _year2 = _d2.getFullYear();
                    var _date3 = _d2.getDate();
                    var _month2 = _d2.getMonth();
                    datepicker.startDate = new Date(_year2, _month2, _date3);
                }
                //MaxDate
                if (this._maxDate !== false) {
                    var _d3 = new Date(this._maxDate);
                    var _year3 = _d3.getFullYear();
                    var _date4 = _d3.getDate();
                    var _month3 = _d3.getMonth();
                    datepicker.maxDate = new Date(_year3, _month3, _date4);
                }
                //MinDate
                if (this._minDate !== false) {
                    var _d4 = new Date(this._minDate);
                    var _year4 = _d4.getFullYear();
                    var _date5 = _d4.getDate();
                    var _month4 = _d4.getMonth();
                    datepicker.minDate = new Date(_year4, _month4, _date5);
                }

                // You can change when the week should start by setting the weekStart property to a number between 0 - 6 where 0 is sunday and 6 is saturday.
                //The languages have the weekStart predefined.So
                // if you choose norwegian it will automatically start the week on monday
                // if no weekStart has been set.
                if (this._weekStartOn !== false) {
                    datepicker.weekStart = this._weekStartOn;
                }
                //NUMBER OF MONTHS TO SHOW
                if (this._showMonths !== false) {
                    datepicker.months = this._showMonths;
                }
                //ADD TOOLTIP TO DATE
                if (this._tooltip !== false) {

                    var arrayToolTips = [];

                    for (var _i = 0; _i < this._tooltip.length; _i++) {

                        var _d5 = new Date(this._tooltip[_i].Date);
                        var _year5 = _d5.getFullYear();
                        var _date6 = _d5.getDate();
                        var _month5 = _d5.getMonth();

                        var tooptip = {};
                        tooptip.date = new Date(_year5, _month5, _date6);
                        tooptip.text = this._tooltip[_i].Text;

                        arrayToolTips.push(tooptip);
                    }

                    datepicker.tooltips = arrayToolTips;
                }
                //ALLOW MAXIMUM DATES TO SELECT
                if (this._maxDatesToSelect !== false) {
                    datepicker.maxSelections = this._maxDatesToSelect;
                }
                //SHOW CALENDAR IN DIV (NO MODAL)
                if (this._appendTo !== false) {
                    datepicker.container = document.querySelector(this._appendTo);
                    datepicker.inline = true;
                }
                //DISBALED SPECIFIC DAYS 
                //ARRAY OF int, Example: [0,1]
                if (this._disabledDays !== false) {
                    datepicker.disabledDays = this._disabledDays;
                }
                //DISABLED SPECIFIC DATES
                //ARRAY OF DATES, Example: ['06/23/2019', '07/23/2019']
                if (this._disabledDates !== false) {
                    var dates = [];
                    for (var i = 0; this._disabledDates.length; i++) {
                        var _d6 = new Date(this._disabledDates[i]);
                        var _year6 = _d6.getFullYear();
                        var _date7 = _d6.getDate();
                        var _month6 = _d6.getMonth();

                        dates.push(new Date(_year6, _month6, _date7));
                    }

                    datepicker.disabledDates = dates;
                }
                //HIDE DAYS NAME
                if (this._hideDayNames === true) {
                    datepicker.daynames = false;
                }

                //SHOW CALENDAR IN FULL SCREEN
                if (this._showFullScreen !== false) {
                    datepicker.fullscreen = true;
                }

                //Locks the Calendar (view only mode)
                if (this._lockCalendar !== false) {
                    datepicker.locked = true;
                }

                //HIGHLIGHT DATES
                if (this._highlight !== undefined) {

                    var hlight = {};
                    var arrayDates = [];
                    var bgColor = void 0;
                    var foreColor = void 0;

                    if (this._highlight !== false) {

                        for (var i = 0; i < this._highlight.dates.length; i++) {

                            var objDate = {};

                            var sD = new Date(this._highlight.dates[i].Start);
                            var sYear = sD.getFullYear();
                            var sDate = sD.getDate();
                            var sMonth = sD.getMonth();
                            objDate.start = new date(sYear, sMonth, sDate);

                            var eD = new Date(this._highlight.dates[i].End);
                            var eYear = eD.getFullYear();
                            var eDate = eD.getDate();
                            var eMonth = eD.getMonth();

                            objDate.end = new date(eYear, eMonth, eDate);

                            arrayDates.push(objDate);
                        }

                        if (highlight.BGColor === undefined) {
                            bgColor = '#E99C00';
                        } else {
                            bgColor = highlight.BGColor;
                        }
                        if (highlight.ForeColor === undefined) {
                            foreColor = '#ffffff';
                        } else {
                            foreColor = highlight.ForeColor;
                        }

                        if (highlight.Legend !== undefined) {
                            hlight.legend = highlight.Legend;
                        }

                        hlight.dates = arrayDates;
                        hlight.backgroundColor = bgColor;
                        hlight.color = foreColor;

                        //SET DATE HIGH LIGHT
                        datepicker.highlight = hlight;
                    }
                }

                /**
                 * 
                 * FUNCTIONS CALL
                 * 
                 */

                //CLOSE DATE PICKET ON SELECT (BY DEFAULT)
                if (this._closeButtonText === false && this._assignSelectedDateTo === false) {
                    privateMethods.closeOnSelect(datepicker, this._modalToHideID);
                }
                //CLOSE DATE PICKET ON BUTTON CLICK
                if (this._closeButtonText !== false && this._assignSelectedDateTo === false) {
                    datepicker.button = this._closeButtonText;
                    privateMethods.closeOnButtonClick(datepicker);
                }

                //ASSIGN SELECTED DATE TO ELEMENT ON SELECT
                if (this._assignSelectedDateTo !== false && this._closeButtonText === false) {
                    privateMethods.assignSelectedDateToElement(datepicker);
                }
                //ASSIGN SELECTED DATE TO ELEMENT ON CLOSE
                if (this._assignSelectedDateTo !== false && this._closeButtonText !== false) {
                    datepicker.button = this._closeButtonText;
                    privateMethods.assignSelectedDateToElementOnButtonClick(datepicker);
                }

                //GET MULTIPLE SELECTED DATES
                if (this._getMultipleSelectedDates !== false) {
                    if (this._closeButtonText === false) {
                        console.log('Text for button is requred');
                    } else {
                        privateMethods.getSelectedDatesOnButtonClick(datepicker);
                    }
                }
            }
            //endregion


        }, {
            key: 'title',
            get: function get() {
                return this._title;
            },
            set: function set(value) {
                this._title = value;
            }
        }, {
            key: 'startDate',
            get: function get() {
                return this._startDate;
            },
            set: function set(value) {
                this._startDate = value;
            }
        }, {
            key: 'setDate',
            get: function get() {
                return this._setDate;
            },
            set: function set(value) {
                this._setDate = value;
            }
        }, {
            key: 'minDate',
            get: function get() {
                return this._minDate;
            },
            set: function set(value) {
                this._minDate = value;
            }
        }, {
            key: 'maxDate',
            get: function get() {
                return this._maxDate;
            },
            set: function set(value) {
                this._maxDate = value;
            }
        }, {
            key: 'closeButtonText',
            get: function get() {
                return this._closeButtonText;
            },
            set: function set(value) {
                this._closeButtonText = value;
            }
        }, {
            key: 'assignSelectedDateTo',
            get: function get() {
                return this._assignSelectedDateTo;
            },
            set: function set(value) {
                this._assignSelectedDateTo = value;
            }
        }, {
            key: 'weekStartOn',
            get: function get() {
                return this._weekStartOn;
            },
            set: function set(value) {
                this._weekStartOn = value;
            }
        }, {
            key: 'showMonths',
            get: function get() {
                return this._showMonths;
            },
            set: function set(value) {
                this._showMonths = value;
            }
        }, {
            key: 'toolTips',
            get: function get() {
                return this._tooltip;
            },
            set: function set(value) {
                this._tooltip = value;
            }
        }, {
            key: 'modalToHideID',
            get: function get() {
                return this._modalToHideID;
            },
            set: function set(value) {
                this._modalToHideID = value;
            }
        }, {
            key: 'maxDatesToSelect',
            get: function get() {
                return this._maxDatesToSelect;
            },
            set: function set(value) {
                this._maxDatesToSelect = value;
            }
        }, {
            key: 'appendTo',
            get: function get() {
                return this._appendTo;
            },
            set: function set(value) {
                this._appendTo = value;
            }
        }, {
            key: 'disabledDays',
            get: function get() {
                return this._disabledDays;
            },
            set: function set(value) {
                this._disabledDays = value;
            }
        }, {
            key: 'disabledDates',
            get: function get() {
                return this._disabledDates;
            },
            set: function set(value) {
                this._disabledDates = value;
            }
        }, {
            key: 'hideDayNames',
            get: function get() {
                return this._hideDayNames;
            },
            set: function set(value) {
                this._hideDayNames = value;
            }
        }, {
            key: 'showFullScreen',
            get: function get() {
                return this._showFullScreen;
            },
            set: function set(value) {
                this._showFullScreen = value;
            }
        }, {
            key: 'loclCalendar',
            get: function get() {
                return this._lockCalendar;
            },
            set: function set(value) {
                this._lockCalendar = value;
            }
        }, {
            key: 'getMultipleSelectedDates',
            get: function get() {
                return this._getMultipleSelectedDates;
            },
            set: function set(value) {
                this._getMultipleSelectedDates = value;
            }
        }, {
            key: 'highlight',
            get: function get() {
                return this._highlight;
            },
            set: function set(value) {
                this._highlight = value;
            }
        }]);

        return j2HTMLDatePicker;
    }();

    //#region ************** PRIVATE METHODS *******
    //PRIVATE FUNCTIONS


    var privateMethods = {
        closeOnSelect: function closeOnSelect(datepicker, modalID) {

            if (j2HTML.IsElementExsit({
                Element: '#Datepickk'
            })) {
                j2HTML.RemoveElement({
                    Element: '#Datepickk'
                });
            }

            datepicker.unselectAll();

            datepicker.closeOnSelect = true;

            datepicker.onClose = function () {
                datepicker.closeOnSelect = false;
                datepicker.onClose = null;
            };

            datepicker.show();

            j2HTML.GetElement({
                Element: '.d-table'
            })[0].style.cssText += ';display:flex !important;';
        },
        closeOnButtonClick: function closeOnButtonClick(datepicker) {

            if (j2HTML.IsElementExsit({
                Element: '#Datepickk'
            })) {
                j2HTML.RemoveElement({
                    Element: '#Datepickk'
                });
            }

            datepicker.unselectAll();
            datepicker.closeOnClick = false;
            datepicker.onClose = function () {
                datepicker.closeOnClick = true;
                datepicker.button = null;
                datepicker.onClose = null;
            };
            datepicker.show();
            j2HTML.GetElement({
                Element: '.d-table'
            })[0].style.cssText += ';display:flex !important;';
        },
        assignSelectedDateToElement: function assignSelectedDateToElement(datepicker) {

            if (j2HTML.IsElementExsit({
                Element: '#Datepickk'
            })) {
                j2HTML.RemoveElement({
                    Element: '#Datepickk'
                });
            }

            datepicker.unselectAll();

            datepicker.closeOnSelect = true;

            //CLOSE THE DATE PICKER 
            datepicker.onClose = function () {

                datepicker.closeOnSelect = false;

                datepicker.onClose = null;

                //SHOW PLACE HOLDER IF IT IS EXSIT
                if (j2HTML.IsElementExsit({
                    Element: '#lbl' + assignToElement.slice(1)
                })) {
                    j2HTML.ShowElement({
                        Element: '#lbl' + assignToElement.slice(1)
                    });
                    j2HTML.AddClass({
                        Element: '#' + assignToElement.slice(1),
                        ClassName: 'active'
                    });
                    j2HTML.AddClass({
                        Element: '#' + assignToElement.slice(1),
                        ClassName: 'focusIn'
                    });
                }
            };

            //SET VALUE OF INPUT ELEMENT
            datepicker.onSelect = function (checked) {

                j2HTML.SetElementValue({
                    Element: assignToElement,
                    Value: this.toLocaleDateString()
                });
            };

            datepicker.show();
            j2HTML.GetElement({
                Element: '.d-table'
            })[0].style.cssText += ';display:flex !important;';
        },
        assignSelectedDateToElementOnButtonClick: function assignSelectedDateToElementOnButtonClick(datepicker) {

            if (j2HTML.IsElementExsit({
                Element: '#Datepickk'
            })) {
                j2HTML.RemoveElement({
                    Element: '#Datepickk'
                });
            }

            datepicker.unselectAll();
            datepicker.closeOnClick = false;

            //CLOSE THE DATE PICKER 
            datepicker.onClose = function () {

                datepicker.closeOnClick = true;
                datepicker.closeOnSelect = false;
                datepicker.onClose = null;

                //SHOW PLACE HOLDER IF IT IS EXSIT
                if (j2HTML.IsElementExsit({
                    Element: '#lbl' + assignToElement.slice(1)
                })) {
                    j2HTML.ShowElement({
                        Element: '#lbl' + assignToElement.slice(1)
                    });
                    j2HTML.AddClass({
                        Element: '#' + assignToElement.slice(1),
                        ClassName: 'active'
                    });
                    j2HTML.AddClass({
                        Element: '#' + assignToElement.slice(1),
                        ClassName: 'focusIn'
                    });
                }
            };

            //SET VALUE OF INPUT ELEMENT
            datepicker.onSelect = function (checked) {

                j2HTML.SetElementValue({
                    Element: assignToElement,
                    Value: this.toLocaleDateString()
                });
            };

            datepicker.show();

            j2HTML.GetElement({
                Element: '.d-table'
            })[0].style.cssText += ';display:flex !important;';
        },
        getSelectedDatesOnButtonClick: function getSelectedDatesOnButtonClick(datepicker) {

            if (j2HTML.IsElementExsit({
                Element: '#Datepickk'
            })) {
                j2HTML.RemoveElement({
                    Element: '#Datepickk'
                });
            }

            datepicker.unselectAll();
            datepicker.closeOnClick = false;
            datepicker.onClose = function () {
                datepicker.closeOnClick = true;
                datepicker.button = null;
                datepicker.onClose = null;

                var dates = datepicker.selectedDates;
                var formatedDates = [];

                for (var i = 0; i < dates.length; i++) {
                    formatedDates.push(dates[i].toLocaleDateString());
                }

                getMultipleSelectedDates = formatedDates;
            };
            datepicker.show();
            j2HTML.GetElement({
                Element: '.d-table'
            })[0].style.cssText += ';display:flex !important;';
        }
    };

    //endregion
    return j2HTMLDatePicker;
}();
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var checkbox = function () {

    //*********** CLASS ***************/
    var checkbox = function () {
        function checkbox() {
            _classCallCheck(this, checkbox);

            this._data = false;
            this._groupName = 'chkj2HTMLGroupName';
            this._value = false;
            this._text = false;
            this._selectedValue = false;
            this._direction = 'Horizontal';
            this._appendTo = false;

            //ONLY FOR MODAL
            this._isModal = false;
        }

        //#region PROPERTIES

        //PROPERTY -- DATA


        _createClass(checkbox, [{
            key: 'getCheckbox',


            //endregion 


            //#region PUBLIC FUNCTIONS
            value: function getCheckbox() {

                var error = '';

                if (this._data === false) {
                    error += 'Data is required \n';
                } else if (this._appendTo === false && this._isModal === false) {
                    error += 'Control ID is required to append checkbox\n';
                } else if (this._text === false) {
                    error += 'Text is required\n';
                }

                if (this._appendTo !== false) {

                    if (this._isModal === true) {

                        this._appendTo = '#' + this._appendTo.slice(1) + 'body';

                        this._groupName = 'chk' + this._appendTo.slice(1).slice(0, -4);
                        groupName = 'chk' + this._appendTo.slice(1).slice(0, -4);
                        isModalShowing = this._isModal;
                    }
                } else {
                    if (this._isModal === true) {

                        this._appendTo = '#j2HTMLModalbody';
                        this._groupName = 'chkj2HTMLModal';
                        groupName = 'chkj2HTMLModal';
                        isModalShowing = this._isModal;
                    }
                }

                if (this._groupName !== false) {
                    groupName = this._groupName;
                } else {
                    groupName = false;
                }

                if (this._isModal === true && this._selectedValue === false) {
                    j2HTML.HideElement({ Element: '#btnJ2HTMLModalSubmitData' });
                }

                if (error === '') {
                    var checkboxArray = this._data.map(privateMethods.createCheckBox.bind(null, this._text, this._value, this._direction, this._groupName));

                    //EMPTY THE APPEND TO ELELEMNT 
                    j2HTML.EmptyElement({
                        Element: this._appendTo
                    });

                    apptTo = this._appendTo;
                    //APPEND VAUE TO ELEMENT
                    checkboxArray.map(function (obj) {

                        j2HTML.AppendElementToElement({
                            AppendTo: apptTo,
                            NewElement: obj
                        });
                    });

                    if (this._selectedValue !== false) {
                        //getSelectedValueFun= this._selectedValue;
                        sendData = this._data;
                        functionName = this._selectedValue.Function;
                        OnEvent = this._selectedValue.On.toLowerCase();
                        var chk = new checkbox();

                        if (this._selectedValue.On.toLowerCase() === "change") {

                            var elems = j2HTML.GetElementByName({ Name: this._groupName });

                            [].slice.call(elems).map(function (checkbox) {

                                //REMOVE PREVIOUSLY ADDED ON-CHANGE EVENT LISTENER
                                checkbox.removeEventListener("change", chk.setj2HTMLCheckboxSelectedValue, false);

                                //ADD ON-CHANGE EVENT LISTENER
                                checkbox.addEventListener("change", chk.setj2HTMLCheckboxSelectedValue, false);
                            });
                        }
                        if (this._selectedValue.On.toLowerCase() === "click" && this._isModal === false) {

                            //REMOVE ON-CHANGE EVENT LISTENER
                            var ddlElem = j2HTML.GetElement({ Element: this._appendTo });
                            ddlElem[0].removeEventListener("change", chk.setj2HTMLCheckboxSelectedValue, false);

                            //REMOVE PREVIOUSLY ADDED ON-CLICK EVENT LISTENER
                            var elem = j2HTML.GetElement({ Element: this._selectedValue.ButtonID });
                            elem[0].removeEventListener("click", chk.setj2HTMLCheckboxSelectedValue, false);

                            //ADD ON-CLICK EVENT LISTENER
                            elem[0].addEventListener("click", chk.setj2HTMLCheckboxSelectedValue, false);
                        }
                        if (this._selectedValue.On.toLowerCase() === "click" && this._isModal === true) {

                            //REMOVE ON-CHANGE EVENT LISTENER
                            var _ddlElem = j2HTML.GetElement({ Element: this._appendTo });
                            _ddlElem[0].removeEventListener("change", chk.setj2HTMLCheckboxSelectedValue, false);

                            var _elem = j2HTML.GetElement({ Element: '#btnJ2HTMLModalSubmitData' });
                            //REMOVE PREVIOUSLY ADDED ON-CLICK EVENT LISTENER
                            _elem[0].removeEventListener("click", chk.setj2HTMLCheckboxSelectedValue, false);

                            //ADD ON-CLICK EVENT LISTENER
                            _elem[0].addEventListener("click", chk.setj2HTMLCheckboxSelectedValue, false);
                        }
                    }
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }
        }, {
            key: 'setj2HTMLCheckboxSelectedValue',
            value: function setj2HTMLCheckboxSelectedValue() {

                //j2HTMLCheckboxSelectedValue(apptTo, sendData, functionName);
                //let name = apptTo.slice(1);

                var selectedValue = [];
                var selectedText = [];

                var chks = '';

                if (groupName !== false) {
                    chks = document.querySelectorAll('[name="' + groupName + '"]');
                } else {
                    chks = document.querySelectorAll('[name="chk' + apptTo.slice(1) + '"]');
                }

                [].slice.call(chks).map(function (chk) {

                    if (chk.checked) {

                        if (chk.value !== undefined) {
                            selectedValue.push(chk.value);
                        }
                        selectedText.push(chk.getAttribute('text'));
                    }
                });

                functionName(selectedValue, selectedText, sendData);
                if (isModalShowing === true) {
                    if (apptTo === false) {
                        j2HTML.HideModal({ ModalID: '#j2HTMLModal' });
                        j2HTML.RemoveElement({ Element: '#j2HTMLModal' });
                    } else {
                        j2HTML.HideModal({ ModalID: apptTo.slice(0, -4) });
                        j2HTML.RemoveElement({ Element: apptTo.slice(0, -4) });
                    }
                }
            }

            //endregion

        }, {
            key: 'data',
            get: function get() {
                return this._data;
            },
            set: function set(value) {
                this._data = value;
            }

            //PROPERTY -- GROUP NAME 

        }, {
            key: 'groupName',
            get: function get() {
                return this._groupName;
            },
            set: function set(value) {
                this._groupName = value;
            }

            //PROPERTY -- VALUE

        }, {
            key: 'value',
            get: function get() {
                return this._value;
            },
            set: function set(value) {
                this._value = value;
            }

            //PROPERTY -- TEXT

        }, {
            key: 'text',
            get: function get() {
                return this._text;
            },
            set: function set(value) {
                this._text = value;
            }

            //SELECTED VALUE

        }, {
            key: 'selectedValue',
            get: function get() {
                return this._selectedValue;
            },
            set: function set(value) {
                this._selectedValue = value;
            }

            //PROPERTY -- DIRECTION

        }, {
            key: 'direction',
            get: function get() {
                return this._direction;
            },
            set: function set(value) {
                this._direction = value;
            }

            //PROPERTY -- APPEND TO

        }, {
            key: 'appendTo',
            get: function get() {
                return this._appendTo;
            },
            set: function set(value) {
                this._appendTo = value;
            }
        }, {
            key: 'isModal',
            get: function get() {
                return this._isModal;
            },
            set: function set(value) {
                this._isModal = value;
            }
        }]);

        return checkbox;
    }();

    //#region PRIVATE VARIABLES AND FUNCTIONS


    var apptTo = '';
    var sendData = '';
    var functionName = '';
    var groupName = '';
    var isModalShowing = '';

    //ADD ALL PRIVATE METHODS INSIDE privateMethods
    var privateMethods = {
        createCheckBox: function createCheckBox(text, value, direction, groupName, obj) {

            //CREATE LABEL
            var lbl = document.createElement('label');
            //MARGIN top right left bottom
            lbl.style.margin = '0px 0px 10px,opx';
            var lblText = document.createTextNode(obj[text]);

            //CREATE CHECKBOX
            var chk = document.createElement('input');
            chk.setAttribute('type', 'checkbox');

            //************* HORIZONTAL *****************
            //HORIZONTAL WITH VALUE
            if (value !== false && direction === 'Horizontal') {

                chk.setAttribute('name', groupName);
                chk.setAttribute('value', obj[value]);
                chk.setAttribute('text', obj[text]);
            }
            //HORIZONTAL WITH NO VALUE
            else if (value === false && direction === 'Horizontal') {

                    chk.setAttribute('name', groupName);
                    chk.setAttribute('text', obj[text]);
                }

            //************* VERTICAL ******************* */
            //VERTICAL WITH VALUE
            if (value !== false && direction !== 'Horizontal') {

                lbl.style.display = 'block';
                chk.setAttribute('name', groupName);
                chk.setAttribute('value', obj[value]);
                chk.setAttribute('text', obj[text]);
            }
            //VERTICAL WITH NO VALUE
            else if (value === false && direction !== 'Horizontal') {
                    lbl.style.display = 'block';
                    chk.setAttribute('name', groupName);
                    chk.setAttribute('text', obj[text]);
                }

            lbl.appendChild(chk);
            lbl.appendChild(lblText);
            return lbl;
        }
    };
    //endregion

    return checkbox;
}();
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var dropdown = function () {
    var dropdown = function () {
        function dropdown() {
            _classCallCheck(this, dropdown);

            this._data = false;
            this._appendTo = false;
            this._ddlTitle = false;
            this._ddlvalue = false;
            this._text = false;
            this._selectedValue = false;

            //ONLY FOR MODAL
            this._isModal = false;
        }

        //#region PROPERTIES

        //PROPERTY -- DATAF


        _createClass(dropdown, [{
            key: 'getDropdown',
            value: function getDropdown() {
                var error = '';

                if (this._data === false) {
                    error += 'Data is required \n';
                } else if (this._appendTo === false && this._isModal === false) {
                    error += 'Element ID is required to append dropdown button(AppendTo) \n';
                } else if (this._text === false) {
                    error += 'Text is required\n';
                }

                if (this._appendTo !== false) {

                    if (this._isModal === true) {

                        var ddlModalId = this._appendTo.slice(1);
                        this._appendTo = '#ddl' + ddlModalId + 'body';
                        isModalShowing = this._isModal;
                    }
                } else {
                    if (this._isModal === true) {

                        this._appendTo = '#ddlj2HTMLModalbody';
                        isModalShowing = this._isModal;
                    }
                }

                if (this._isModal === true && this._selectedValue === false) {
                    j2HTML.HideElement({
                        Element: '#btnJ2HTMLModalSubmitData'
                    });
                }

                if (error === '') {
                    var dropdownArray = this._data.map(privateMethods.createDropdown.bind(null, this._text, this._ddvalue));

                    //ADD 'Select...' at 0 index  of the Array
                    if (this._ddlTitle !== false) {

                        var selectOption = document.createElement('option');
                        selectOption.setAttribute('value', '-1');
                        selectOption.appendChild(document.createTextNode(this._ddlTitle));
                        dropdownArray.splice(0, 0, selectOption);

                        // if (this._ddlTitle.Show === true && this._ddlTitle.Text === undefined) {

                        //     let selectOption = document.createElement('option');
                        //     selectOption.setAttribute('value', '-1');
                        //     selectOption.appendChild(document.createTextNode('Select...'));
                        //     dropdownArray.splice(0, 0, selectOption);

                        // } else if (this._ddlTitle.Show === true && this._ddlTitle.Text !== undefined) {

                        //     let selectOption = document.createElement('option');
                        //     selectOption.setAttribute('value', '-1');
                        //     selectOption.appendChild(document.createTextNode(this._ddlTitle.Text));
                        //     dropdownArray.splice(0, 0, selectOption);
                        // }
                    }
                    // let selectOption = document.createElement('option');
                    // selectOption.setAttribute('value', '-1');


                    // selectOption.appendChild(document.createTextNode('Select...'));
                    // dropdownArray.splice(0, 0, selectOption);


                    //EMPTY THE APPEND TO ELELEMNT 
                    j2HTML.EmptyElement({
                        Element: this._appendTo
                    });

                    apptTo = this._appendTo;
                    //APPEND VAUE TO ELEMENT
                    dropdownArray.map(function (obj) {

                        j2HTML.AppendElementToElement({
                            AppendTo: apptTo,
                            NewElement: obj
                        });
                    });

                    if (this._selectedValue !== false) {

                        sendData = this._data;
                        functionName = this._selectedValue.Function;
                        var ddl = new dropdown();

                        if (this._selectedValue.On.toLowerCase() === "change") {

                            var elem = j2HTML.GetElement({
                                Element: this._appendTo
                            });

                            //REMOVE PREVIOUSLY ADDED ON-CHANGE EVENT LISTENER
                            elem[0].removeEventListener("change", ddl.setj2HTMLDropdownSelectedValue, false);

                            //ADD ON-CHANGE EVENT LISTENER
                            elem[0].addEventListener("change", ddl.setj2HTMLDropdownSelectedValue, false);
                        }
                        if (this._selectedValue.On.toLowerCase() === "click" && this._isModal === false) {

                            //REMOVE ON-CHANGE EVENT LISTENER
                            var ddlElem = j2HTML.GetElement({
                                Element: this._appendTo
                            });
                            ddlElem[0].removeEventListener("change", ddl.setj2HTMLDropdownSelectedValue, false);

                            //REMOVE PREVIOUSLY ADDED ON-CLICK EVENT LISTENER
                            var _elem = j2HTML.GetElement({
                                Element: this._selectedValue.ButtonID
                            });
                            _elem[0].removeEventListener("click", ddl.setj2HTMLDropdownSelectedValue, false);

                            //ADD ON-CLICK EVENT LISTENER
                            _elem[0].addEventListener("click", ddl.setj2HTMLDropdownSelectedValue, false);
                        }

                        if (this._selectedValue.On.toLowerCase() === "click" && this._isModal === true) {

                            //REMOVE ON-CHANGE EVENT LISTENER
                            var _ddlElem = j2HTML.GetElement({
                                Element: this._appendTo
                            });
                            _ddlElem[0].removeEventListener("change", ddl.setj2HTMLDropdownSelectedValue, false);

                            //REMOVE PREVIOUSLY ADDED ON-CLICK EVENT LISTENER
                            var _elem2 = j2HTML.GetElement({
                                Element: '#btnJ2HTMLModalSubmitData'
                            });
                            _elem2[0].removeEventListener("click", ddl.setj2HTMLDropdownSelectedValue, false);

                            //ADD ON-CLICK EVENT LISTENER
                            _elem2[0].addEventListener("click", ddl.setj2HTMLDropdownSelectedValue, false);
                        }
                    }
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }
        }, {
            key: 'setj2HTMLDropdownSelectedValue',
            value: function setj2HTMLDropdownSelectedValue() {

                var ddl = document.querySelectorAll(apptTo)[0];
                var selectedValue = '';
                if (ddl.value !== undefined) {
                    selectedValue = ddl.value;
                }
                //selectedValue = ddl.value;
                var selectedText = ddl.options[ddl.selectedIndex].text;
                functionName(selectedText, selectedValue, JSON.stringify(sendData));

                if (isModalShowing === true) {
                    if (apptTo === false) {
                        j2HTML.HideModal({
                            ModalID: '#j2HTMLModal'
                        });
                        j2HTML.RemoveElement({
                            Element: '#j2HTMLModal'
                        });
                    } else {
                        j2HTML.HideModal({
                            ModalID: '#' + apptTo.slice(4).slice(0, -4)
                        });
                        j2HTML.RemoveElement({
                            Element: '#' + apptTo.slice(4).slice(0, -4)
                        });
                    }
                }
            }
        }, {
            key: 'data',
            get: function get() {
                return this._data;
            },
            set: function set(value) {
                this._data = value;
            }

            //PROPERTY -- APPEND TO

        }, {
            key: 'appendTo',
            get: function get() {
                return this._appendTo;
            },
            set: function set(value) {
                this._appendTo = value;
            }

            //PROPERTY -- SELECTION TEXT

        }, {
            key: 'ddlTitle',
            get: function get() {
                return this._ddlTitle;
            },
            set: function set(value) {
                this._ddlTitle = value;
            }

            //PROPERTY -- VALUE

        }, {
            key: 'ddlValue',
            get: function get() {
                return this._ddvalue;
            },
            set: function set(value) {
                this._ddvalue = value;
            }

            //PROPERTY -- TEXT

        }, {
            key: 'text',
            get: function get() {
                return this._text;
            },
            set: function set(value) {
                this._text = value;
            }

            //SELECTED VALUE

        }, {
            key: 'selectedValue',
            get: function get() {
                return this._selectedValue;
            },
            set: function set(value) {
                this._selectedValue = value;
            }
        }, {
            key: 'isModal',
            get: function get() {
                return this._isModal;
            },
            set: function set(value) {
                this._isModal = value;
            }
        }]);

        return dropdown;
    }();

    //#region PRIVATE VARIABLES AND FUNCTIONS


    var apptTo = '';
    var sendData = '';
    var functionName = '';
    var isModalShowing = '';

    //ADD ALL PRIVATE METHODS INSIDE privateMethods
    var privateMethods = {
        createDropdown: function createDropdown(text, value, obj) {

            //CREATE LABEL
            var ddlOption = document.createElement('option');
            if (value !== false) {
                ddlOption.setAttribute('value', obj[value]);
                ddlOption.appendChild(document.createTextNode(obj[text]));
            } else {
                ddlOption.appendChild(document.createTextNode(obj[text]));
            }

            return ddlOption;
        }
    };
    //endregion
    return dropdown;
}();

// function dropdown() {
//     var args = arguments[0][0];


//     var data = false;
//     var appendTo = false;
//     var value = false;
//     var text = false;

//     //ONLY FOR MODAL
//     var isModal = false;

//     var error = '';

//     if (args.Data !== undefined) {
//         data = args.Data;
//     } else {
//         error += 'Data is required';
//     }

//     // if (args.AppendTo !== undefined) {
//     //     appendTo = args.AppendTo;
//     // }

//     if (args.isModal !== undefined) {
//         isModal = args.isModal;
//     }

//     if (args.AppendTo !== undefined) {
//         appendTo = args.AppendTo;

//         if (isModal === true) {

//             var chkModalId = appendTo.replace('#', '');
//             appendTo = '#ddl' + chkModalId;
//         }
//     } else {
//         if (isModal === true) {

//             appendTo = '#ddlj2HTMLModal';
//         }
//     }


//     if (args.Value !== undefined) {
//         value = args.Value;

//         if (data !== false) {
//             if (data[0].hasOwnProperty(value) === false) {
//                 error += 'Value "' + value + '" not found\n';
//             }
//         }
//     }
//     if (args.Text !== undefined) {

//         text = args.Text;

//         if (data !== false) {
//             if (data[0].hasOwnProperty(text) === false) {
//                 error += 'Text "' + text + '" not found\n';
//             }
//         }
//     } else {
//         error += 'Text is required';
//     }

//     var ddl = '';

//     if (error === '') {

//         //IF OBJECT
//         //if (data.length > 0) {

//         $.each(data, function (i, v) {

//             var count = 0;
//             var ddlID = '';
//             var ddlValue = '';
//             $.each(data[i], function (ii, vv) {

//                 if (value !== undefined && ii === value) {
//                     ddlValue = vv;
//                 }
//                 // else {
//                 //     error += value + 'not found';
//                 // }

//                 if (ii === text) {
//                     ddlText = vv;
//                 }
//                 // else {
//                 //     error += text + 'not found';
//                 // }

//                 //if (error === '') {

//                 count = count + 1;

//                 if (Object.keys(data[0]).length == count) {

//                     if (value !== false) {
//                         ddl += '<option value="' + ddlValue + '">' + ddlText + '</option>';
//                     } else {
//                         ddl += '<option>' + ddlText + '</option>';
//                     }
//                 }
//                 // } else {
//                 //     alert(error);
//                 //     return false;
//                 // }
//             });
//         });

//         $(appendTo).append(ddl);

//     } else {
//         alert(error);
//     }

// }
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var list = function () {

    //CREATING CLASS
    var list = function () {

        //CLASS CONSTRUCTOR
        function list() {
            _classCallCheck(this, list);

            this._data = false;
            this._text = false;
            this._dataListvalue = false; //VALUE
            this._dataList = false;
            this._type = false;
            this._appendTo = false;
        }

        //#region *************** PROPERTIES **********************

        //PROPERTY -- DATA


        _createClass(list, [{
            key: 'getList',

            //endregion


            //#region *************** PUBLIC FUNCTIONS *****************
            value: function getList() {
                var error = '';

                if (this._data === false) {
                    error += 'Data is required \n';
                } else if (this._appendTo === false) {
                    error += '<ul> ID is required to append list \n';
                } else if (this._text === false) {
                    error += 'Text is required\n';
                }

                if (error === '') {

                    apptTo = this._appendTo;
                    dataListData = this._data;

                    //IF IT IS NOT A DATALST
                    if (this._dataList === false) {
                        var liList = this._data.map(privateMethods.createList.bind(null, this._text));

                        //EMPTY APPEND TO ELELEMNT 
                        j2HTML.EmptyElement({
                            Element: this._appendTo
                        });

                        //APPEND VAUE TO ELEMENT
                        liList.map(function (obj) {

                            j2HTML.AppendElementToElement({
                                AppendTo: apptTo,
                                NewElement: obj
                            });
                        });
                    } else {

                        //IF IT IS DATALST
                        var options = this._data.map(privateMethods.createDataList.bind(null, this._text, this._dataListvalue));
                        dataListFun = this._dataList.Function;

                        if (this._dataList.ID === undefined) {
                            datalistID = 'j2HTMLDataList';
                        } else {
                            datalistID = this._dataList.ID;
                        }

                        var input = document.createElement('input');
                        input.setAttribute("list", '' + (datalistID + 'list'));
                        input.classList.add('form-control');
                        input.id = 'txt' + datalistID;
                        inputID = 'txt' + datalistID;

                        if (this._dataList.Title === undefined) {
                            input.setAttribute('placeholder', 'Select...');
                        } else {
                            input.setAttribute('placeholder', '' + this._dataList.Title);
                        }

                        if (this._dataList.On !== undefined) {
                            if (this._dataList.On.toLowerCase() === 'change') {

                                var l = new list();
                                input.addEventListener('input', l.getDataListSelectedValue, false);
                            } else if (this._dataList.On.toLowerCase() === 'click') {
                                var _l = new list();

                                input.removeEventListener('input', _l.getDataListSelectedValue, false);

                                var element = j2HTML.GetElement({
                                    Element: this._dataList.ButtonID
                                });
                                element[0].addEventListener('click', _l.getDataListSelectedValue, false);
                            }
                        }

                        var datalist = document.createElement('datalist');
                        datalist.id = '' + (datalistID + 'list');

                        options.map(function (option) {

                            datalist.appendChild(option);
                        });
                        //EMPTY APPEND TO ELELEMNT 
                        j2HTML.EmptyElement({
                            Element: this._appendTo
                        });

                        //APPEND INPUT
                        j2HTML.AppendElementToElement({
                            AppendTo: apptTo,
                            NewElement: input
                        });

                        //APPEND DATA-LIST
                        j2HTML.AppendElementToElement({
                            AppendTo: apptTo,
                            NewElement: datalist
                        });

                        //SET PLACE-HOLDER
                        j2HTML.Placeholder({
                            ElementIDs: ['#txt' + datalistID]
                        });
                    }
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }
        }, {
            key: 'getDataListSelectedValue',
            value: function getDataListSelectedValue() {

                var val = document.getElementById('' + inputID).value;
                var opts = document.getElementById('' + datalistID).childNodes;
                for (var i = 0; i < opts.length; i++) {
                    if (opts[i].value === val) {

                        var obj = {};
                        obj.Value = opts[i].value;
                        obj.Text = opts[i].innerText;

                        dataListFun(opts[i].innerText, opts[i].value, JSON.stringify(dataListData));
                        break;
                    }
                }
            }

            //endregion

        }, {
            key: 'data',
            get: function get() {
                return this._data;
            },
            set: function set(value) {
                this._data = value;
            }

            //PROPERTY -- TEXT

        }, {
            key: 'text',
            get: function get() {
                return this._text;
            },
            set: function set(value) {
                this._text = value;
            }

            //PROPERTY -- DATA LIST VALUE

        }, {
            key: 'dataListValue',
            get: function get() {
                return this._dataListvalue;
            },
            set: function set(value) {
                this._dataListvalue = value;
            }

            //PROPERTY -- DATA LIST VALUE

        }, {
            key: 'dataList',
            get: function get() {
                return this._dataList;
            },
            set: function set(value) {
                this._dataList = value;
            }

            //PROPERTY -- DATA LIST VALUE

        }, {
            key: 'type',
            get: function get() {
                return this._type;
            },
            set: function set(value) {
                this._type = value;
            }

            //PROPERTY -- APPEND TO

        }, {
            key: 'appendTo',
            get: function get() {
                return this._appendTo;
            },
            set: function set(value) {
                this._appendTo = value;
            }
        }]);

        return list;
    }();

    //#region ************* PRIVATE VARIABLES AND FUNCTIONS ***********


    var apptTo = '';
    var inputID = void 0;
    var datalistID = void 0;
    var dataListFun = void 0;
    var dataListData = void 0;

    var privateMethods = {
        createList: function createList(text, obj) {

            var li = document.createElement('li');
            li.appendChild(document.createTextNode(obj[text]));

            return li;
        },
        createDataList: function createDataList(text, value, obj) {

            var option = document.createElement('option');
            if (value !== false) {
                option.value = obj[value];
                option.appendChild(document.createTextNode(obj[text]));
            } else {
                option.appendChild(document.createTextNode(obj[text]));
            }
            return option;
        }
    };
    //endregion


    return list;
}();
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var j2HTMLModal = function () {

    //CREAT CLASS
    var j2HTMLModal = function () {
        function j2HTMLModal() {
            _classCallCheck(this, j2HTMLModal);

            this._modalData = false;
            this._modalID = false;
            this._appendTo = false;
            this._modalFooter = true;
            this._modalHeader = true;
            this._modalSize = false;
            this._headingText = false;
            this._display = false;
            this._exclude = false;
            this._include = false;
            this._customColumns = false;

            //FOR LOADING MODAL
            this._loadingModalText = false;
            this._parentId = false;
            this._image = false;
            this._class = false;

            //ONLY WHEN CREATING MODAL FROM TABLE
            //INSERT ROW, UPDATE ROW AND DELETE ROW
            this._tableID = false;
            this._rowID = false;
            this._updateFunction = false;
            this._createFunction = false;
            this._selectRows = false;
        }

        //#region ************** PROPERTIES ******************

        //MODAL DATA


        _createClass(j2HTMLModal, [{
            key: 'getModal',


            //endregion

            //#region ************** PUBLIC FUNCTIONS ************
            value: function getModal() {

                var error = '';
                if (this._display !== false && this._display.toLowerCase() !== 'empty' && this._display.toLowerCase() !== 'radiobutton' && this._display.toLowerCase() !== 'checkbox' && this._display.toLowerCase() !== 'dropdown' && this._modalData === false) {

                    error += 'Data is required.';
                }
                if (this._modalID === false) {
                    this._modalID = '#j2HTMLModal';
                }
                if (this._display === false) {
                    this._display = defaultDisplay;
                }
                if (this._headingText === false) {
                    this._headingText = 'Model Heading';
                }

                //ONLY WHEN CREATING MODAL FROM TABLE
                //INSERT ROW, UPDATE ROW AND DELETE ROW
                if (this._tableID !== false) {
                    tableID = this._tableID;
                }
                if (this._updateButton !== false) {
                    updateButton = true;
                }
                if (this._deleteButton !== false) {
                    deleteButton = true;
                }

                if (this._rowID !== false) {
                    rowId = this._rowID;
                }

                if (this._updateFunction !== false) {
                    updateFun = this._updateFunction;
                }

                if (this._createFunction !== false) {
                    createFun = this._createFunction;
                } else {
                    createFun = false;
                }

                if (this._tableID !== false) {
                    tableId = this._tableID;
                } else {
                    tableId = false;
                }

                //CREATE MODAL
                if (error === '') {
                    count = 0;

                    modalID = this._modalID;
                    modalBodyId = this._modalID.replace('#', '') + 'body';
                    modalHeadingId = this._modalID.replace('#', '') + 'heading';
                    modalFooterId = this._modalID.replace('#', '') + 'footer';

                    // //CHECK IF MODAL WITH SAME NAME IS EXIST DELETE IT, BEFORE CREATE NEW MODAL WITH SAME NAME
                    var elemModal = j2HTML.IsElementExsit({
                        Element: this._modalID
                    });
                    if (elemModal === true) {
                        j2HTML.RemoveElement({
                            Element: this._modalID
                        });
                    }

                    var heading = '';
                    if (this._modalHeader === true) {
                        heading = '<div class="modal-header">\n                                    <h4  id="' + modalHeadingId + '" class="modal-title">' + this._headingText + '</h4>\n                                    <button id="btn' + modalID.slice(1) + 'HeadingClose" type="button" class="close">&times;</button>  \n                                </div>';
                    }

                    var footer = '';
                    if (this._modalFooter === true && this._updateFunction === false && this._createFunction === false) {
                        footer = '<div id="' + modalFooterId + '" class="modal-footer">\n                                <div class="float-right">\n                                    <button id="btn' + modalID.slice(1) + 'FooterClose" type="button" class="btn btn-sm btn-primary">Close</button>\n                                </div>\n                              </div>';
                    }
                    if (this._modalFooter === true && this._updateFunction !== false && this._createFunction === false) {
                        footer = '<div id="' + modalFooterId + '" class=\'modal-footer\'>\n                                <div class="float-right">\n                                    <button id="btnJ2HTMLModalUpdateData" type=\'button\' class=\'btn btn-sm btn-primary updateData\'>Update</button>\n                                    <button id="btn' + modalID.slice(1) + 'FooterClose" type =\'button\' class=\'btn btn-sm btn-primary\'>Close</button>\n                                </div>\n                              </div>';
                    }
                    if (this._modalFooter === true && (this._display.toLowerCase() === 'radiobutton' || this._display.toLowerCase() === 'checkbox' || this._display.toLowerCase() === 'dropdown') && this._createFunction === false) {
                        footer = '<div id="' + modalFooterId + '" class=\'modal-footer\'>\n                                <div class="float-right">\n                                    <button id="btnJ2HTMLModalSubmitData" type=\'button\' class=\'btn btn-sm btn-primary updateData\'>Submit</button>\n                                    <button id="btn' + modalID.slice(1) + 'FooterClose" type =\'button\' class=\'btn btn-sm btn-primary\'>Close</button>\n                                </div>\n                              </div>';
                    }

                    if (this._modalFooter === true && this._updateFunction === false && this._createFunction !== false) {
                        footer = '<div id="' + modalFooterId + '" class=\'modal-footer\'>\n                                <div class="float-right">\n                                    <button type=\'button\' class=\'btn btn-sm btn-primary createNewDataRow\'">Create</button>\n                                    <button id="btn' + modalID.slice(1) + 'FooterClose" type =\'button\' class=\'btn btn-sm btn-primary\'> Close</button>\n                                </div>\n                              </div>';
                    }

                    //// ************ EMPTY MODAL ******************** 
                    if (this._display.toLowerCase() === 'Empty'.toLowerCase() || this._display.toLowerCase() === 'Form'.toLowerCase()) {
                        modalBody = document.createElement('div');
                        modalBody.innerHTML = 'This is modal body';
                        modalBody.id = modalBodyId;
                        Object.assign(modalBody.style, {
                            padding: "10px"
                        });
                    }

                    //// ************ RADIO BUTTON ******************** 
                    if (this._display.toLowerCase() === 'RadioButton'.toLowerCase() && this._updateFunction === false && this._createFunction === false) {

                        modalBody = document.createElement('div');
                        modalBody.id = modalBodyId;
                        Object.assign(modalBody.style, {
                            padding: "10px",
                            maxHeight: "600px",
                            overflowY: "auto"
                        });
                    }

                    //// ************ CHECKBOX ********************
                    if (this._display.toLowerCase() === 'Checkbox'.toLowerCase() && this._updateFunction === false && this._createFunction === false) {

                        //modalBody = '<div id="chk' + modalBodyId + '"class="modal-body"></div>';
                        modalBody = document.createElement('div');
                        modalBody.id = modalBodyId;
                        Object.assign(modalBody.style, {
                            padding: "10px",
                            maxHeight: "600px",
                            overflowY: "auto"
                        });
                    }

                    //// ************ DROPDOWN ********************
                    if (this._display.toLowerCase() === 'Dropdown'.toLowerCase() && this._updateFunction === false && this._createFunction === false) {

                        modalBody = document.createElement('div');
                        Object.assign(modalBody.style, {
                            padding: "10px"
                        });
                        modalBody.innerHTML = '<select id="ddl' + modalBodyId + '" class="form-control" ><option value="-1">Select...</option></select>';
                        Object.assign(modalBody.style, {
                            padding: "10px"
                        });
                    }

                    //// ************ READ ONLY ********************
                    if (this._display.toLowerCase() === 'readonly' && this._updateFunction === false && this._createFunction === false) {

                        modalBody = document.createElement('div');
                        modalBody.id = modalBodyId;
                        modalBody.classList.add('modal-body');
                        Object.assign(modalBody.style, {
                            maxHeight: "600px"
                        });

                        excludeItemFromModal = this._exclude;
                        includeItemToModal = this._include;

                        var modalArray = this._modalData.map(function (item, index) {

                            //INCLUDE ITEM TO DATA
                            if (includeItemToModal !== false) {
                                var _iteratorNormalCompletion = true;
                                var _didIteratorError = false;
                                var _iteratorError = undefined;

                                try {
                                    for (var _iterator = includeItemToModal[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                                        var includeItem = _step.value;

                                        item[includeItem] = '';
                                    }
                                } catch (err) {
                                    _didIteratorError = true;
                                    _iteratorError = err;
                                } finally {
                                    try {
                                        if (!_iteratorNormalCompletion && _iterator.return) {
                                            _iterator.return();
                                        }
                                    } finally {
                                        if (_didIteratorError) {
                                            throw _iteratorError;
                                        }
                                    }
                                }
                            }

                            //EXCULDE ITEM FROM THE DATA
                            if (excludeItemFromModal !== false) {
                                var _iteratorNormalCompletion2 = true;
                                var _didIteratorError2 = false;
                                var _iteratorError2 = undefined;

                                try {
                                    for (var _iterator2 = excludeItemFromModal[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                                        var excludeItem = _step2.value;

                                        delete item[excludeItem];
                                    }
                                } catch (err) {
                                    _didIteratorError2 = true;
                                    _iteratorError2 = err;
                                } finally {
                                    try {
                                        if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                            _iterator2.return();
                                        }
                                    } finally {
                                        if (_didIteratorError2) {
                                            throw _iteratorError2;
                                        }
                                    }
                                }
                            }

                            return privateMethods.getReadonlyModal(modalID, item);
                        });

                        modalArray.map(function (elem) {
                            modalBody.appendChild(elem);
                        });
                    }
                    //// ************ EDIT ********************
                    if (this._display.toLowerCase() === 'edit' || this._display.toLowerCase() === 'create') {

                        modalBody = document.createElement('div');
                        modalBody.id = modalBodyId;
                        modalBody.classList.add('modal-body');
                        Object.assign(modalBody.style, {
                            maxHeight: "600px"
                        });

                        updateFun = this._updateFunction;
                        createFun = this._createFunction;

                        modalID = this._modalID;
                        objCustomColumns = this._customColumns;

                        excludeItemFromModal = this._exclude;
                        includeItemToModal = this._include;

                        var _modalArray = this._modalData.map(function (item) {

                            //INCLUDE ITEM TO DATA
                            if (includeItemToModal !== false) {
                                var _iteratorNormalCompletion3 = true;
                                var _didIteratorError3 = false;
                                var _iteratorError3 = undefined;

                                try {
                                    for (var _iterator3 = includeItemToModal[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                                        var includeItem = _step3.value;

                                        item[includeItem] = '';
                                    }
                                } catch (err) {
                                    _didIteratorError3 = true;
                                    _iteratorError3 = err;
                                } finally {
                                    try {
                                        if (!_iteratorNormalCompletion3 && _iterator3.return) {
                                            _iterator3.return();
                                        }
                                    } finally {
                                        if (_didIteratorError3) {
                                            throw _iteratorError3;
                                        }
                                    }
                                }
                            }

                            //EXCULDE ITEM FROM THE DATA
                            if (excludeItemFromModal !== false) {
                                var _iteratorNormalCompletion4 = true;
                                var _didIteratorError4 = false;
                                var _iteratorError4 = undefined;

                                try {
                                    for (var _iterator4 = excludeItemFromModal[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                                        var excludeItem = _step4.value;

                                        delete item[excludeItem];
                                    }
                                } catch (err) {
                                    _didIteratorError4 = true;
                                    _iteratorError4 = err;
                                } finally {
                                    try {
                                        if (!_iteratorNormalCompletion4 && _iterator4.return) {
                                            _iterator4.return();
                                        }
                                    } finally {
                                        if (_didIteratorError4) {
                                            throw _iteratorError4;
                                        }
                                    }
                                }
                            }

                            if (createFun === false && updateFun !== false) {
                                return privateMethods.getEditableModal(modalID, item, null, true);
                            } else if (createFun !== false && updateFun === false) {
                                return privateMethods.getEditableModal(modalID, item, null, false);
                            } else if (createFun === false && updateFun === false) {
                                return privateMethods.getEditableModal(modalID, item, null, true);
                            }
                        });

                        _modalArray.map(function (elem) {
                            modalBody.appendChild(elem);
                        });
                    } else if ((this._display.toLowerCase() === 'edit' || this._display.toLowerCase() === 'create') && this._updateFunction === false && this._createFunction !== false) {
                        alert('create function fired');
                    }

                    //*************** SET MODAL SIZE****************
                    var modSize = void 0;
                    if (this._modalSize === false) {
                        modSize = '<div class="modal-dialog">';
                    } else {
                        modSize = '<div class="modal-dialog ' + this._modalSize + '">';
                    }

                    //*************** APPEND MODAL ****************
                    var strModal = '<div id="' + this._modalID.replace('#', '') + '" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">\n                                    ' + modSize + '\n                                    <!-- Modal content-->\n                                    <div class="modal-content">\n                                        ' + heading + ' \n                                        ' + (modalBody.outerHTML || new XMLSerializer().serializeToString(modalBody)) + ' \n                                        ' + footer + ' \n                                    </div >\n                                    </div>\n                                </div>';

                    if (this._appendTo === false) {

                        var elem = j2HTML.GetElement({
                            Element: 'body'
                        });
                        elem[0].insertAdjacentHTML('beforeend', strModal);
                    } else {

                        j2HTML.AppendHTMLString({
                            Element: this._appendTo,
                            HTMLString: strModal
                        });
                        //IF DROPDOWN
                        if (this._display.toLowerCase() === 'Dropdown'.toLowerCase() && updateFun === false && createFun === false) {

                            modalBody = document.createElement('div');
                            Object.assign(modalBody.style, {
                                padding: "10px"
                            });
                            modalBody.innerHTML = '<select id="ddl' + modalID.slice(1) + '" class="form-control" ><option value="-1">Select...</option></select>';
                        }
                    }

                    var m = new j2HTMLModal();
                    //CREATE EVENT FOR CLOSE BUTTON
                    if (j2HTML.IsElementExsit({
                        Element: '#btn' + modalID.slice(1) + 'HeadingClose'
                    })) {
                        var closeHeaderbtn = j2HTML.GetElement({
                            Element: '#btn' + modalID.slice(1) + 'HeadingClose'
                        });

                        //REMOVE PREVIOUSLY ADDED CLICK EVENT LISTENER
                        closeHeaderbtn[0].removeEventListener("click", m.closeModal, false);

                        //ADD ON-CHANGE EVENT LISTENER
                        closeHeaderbtn[0].addEventListener("click", m.closeModal, false);
                    }
                    if (j2HTML.IsElementExsit({
                        Element: '#btn' + modalID.slice(1) + 'FooterClose'
                    })) {
                        var closeFooterbtn = j2HTML.GetElement({
                            Element: '#btn' + modalID.slice(1) + 'FooterClose'
                        });

                        //REMOVE PREVIOUSLY ADDED CLICK EVENT LISTENER
                        closeFooterbtn[0].removeEventListener("click", m.closeModal, false);

                        //ADD ON-CHANGE EVENT LISTENER
                        closeFooterbtn[0].addEventListener("click", m.closeModal, false);
                    }

                    if (this._modalData.length > 1 && (this._display.toLowerCase() === 'readonly' || this._display.toLowerCase() === 'edit')) {

                        var buutons = '<div class="mr-auto">\n                                      <button id="btn' + modalID.slice(1) + 'First" class="btn btn-sm btn-primary disabled">First</button>\n                                      <button id="btn' + modalID.slice(1) + 'Previous" class="btn btn-sm btn-primary disabled" title="Previous"><<</button>\n                                      <button id="btn' + modalID.slice(1) + 'Next" class="btn btn-sm btn-primary" title="Next">>></button>\n                                      <button id="btn' + modalID.slice(1) + 'Last" class="btn btn-sm btn-primary">Last</button>\n                                    </div>';

                        //FORM NUMBERING
                        totalModals = this._modalData.length;
                        var totalNumbers = '<br><span id="modalNumners" style="font-size:medium">Showing 1 of ' + this._modalData.length + '</span>';

                        j2HTML.AppendHTMLString({
                            Element: '#' + modalHeadingId,
                            HTMLString: totalNumbers
                        });
                        j2HTML.PrependHTMLString({
                            Element: '#' + modalFooterId,
                            HTMLString: buutons
                        });

                        //let m = new j2HTMLModal();

                        //BUTTON FIRST -- REMOVE AND ADD EVENT LISTERNER
                        var btnFirst = j2HTML.GetElement({
                            Element: '#btn' + modalID.slice(1) + 'First'
                        });
                        btnFirst[0].removeEventListener('click', m.j2HTMLFirstModal, false);
                        btnFirst[0].addEventListener('click', m.j2HTMLFirstModal, false);

                        //BUTTON NEXT -- REMOVE AND ADD EVENT LISTERNER
                        var btnNext = j2HTML.GetElement({
                            Element: '#btn' + modalID.slice(1) + 'Next'
                        });
                        btnNext[0].removeEventListener('click', m.j2HTMLNextModal, false);
                        btnNext[0].addEventListener('click', m.j2HTMLNextModal, false);

                        //BUTTON PREVIOUS -- REMOVE AND ADD EVENT LISTERNER
                        var btnPrevious = j2HTML.GetElement({
                            Element: '#btn' + modalID.slice(1) + 'Previous'
                        });
                        btnPrevious[0].removeEventListener('click', m.j2HTMLPreviousModal, false);
                        btnPrevious[0].addEventListener('click', m.j2HTMLPreviousModal, false);

                        //BUTTON LAST -- REMOVE AND ADD EVENT LISTERNER
                        var btnLast = j2HTML.GetElement({
                            Element: '#btn' + modalID.slice(1) + 'Last'
                        });
                        btnLast[0].removeEventListener('click', m.j2HTMLLastModal, false);
                        btnLast[0].addEventListener('click', m.j2HTMLLastModal, false);
                    }

                    //CREATE UPDATE EVENT
                    if (this._updateFunction !== false) {

                        modalID = this._modalID;
                        modalData = this._modalData;
                        rowID = this._rowID;
                        updatefn = this._updateFunction;

                        var _m = new j2HTMLModal();
                        var elems = j2HTML.GetElement({
                            Element: '.updateData'
                        });

                        [].slice.call(elems).map(function (elem) {

                            //REMOVE PREVIOUSLY EXIST EVENT
                            elem.removeEventListener("click", _m.updateModal, false);

                            //ADD EVENT
                            elem.addEventListener("click", _m.updateModal, false);
                        });
                    }

                    //CEREATE CREATE NEW DATA ROW EVENT
                    if (this._createFunction !== false) {

                        pramSelectRows = this._selectRows;

                        var _m2 = new j2HTMLModal();
                        var _elems = j2HTML.GetElement({
                            Element: '.createNewDataRow'
                        });

                        [].slice.call(_elems).map(function (elem) {

                            //REMOVE PREVIOUSLY EXIST EVENT
                            elem.removeEventListener("click", _m2.createModal, false);

                            //ADD EVENT
                            elem.addEventListener("click", _m2.createModal, false);
                        });
                    }

                    //ADD PLACE HOLDER
                    if (this._display.toLowerCase() === 'edit' || this._display.toLowerCase() === 'create') {
                        j2HTML.Placeholder({
                            ElementIDs: '.J2HTMLModalTextBox'
                        });
                    }
                } else {
                    alert(error);
                    throw new Error(error);
                }
                return false;
            }
        }, {
            key: 'closeModal',
            value: function closeModal() {
                j2HTML.HideModal({
                    ModalID: modalID
                });
                j2HTML.RemoveElement({
                    Element: modalID
                });
            }
        }, {
            key: 'showWaitingModal',
            value: function showWaitingModal() {

                var txt = void 0;
                var spinner = void 0;
                var modal = void 0;
                var centerClass = void 0;
                var error = '';

                if (this._image !== false && this._class !== false) {
                    error = 'Either image or class required';
                }
                if (error === '') {
                    if (this._loadingModalText !== false) {
                        txt = '<h3>' + this._loadingModalText + '</h3>';
                    } else {
                        txt = '<h3>Wait...</h3>';
                    }

                    if (this._image !== false) {
                        spinner = '<img src="' + this._image + '" style="width:100px; height:100px;"';
                    } else if (this._class !== false) {
                        spinner = '<i class="' + this._class + '"></i>';
                    } else {
                        spinner = '<i class="fa fa-spinner fa-5x fa-spin"></i>';
                    }

                    if (this._parentId !== false) {

                        centerClass = "centerSpinner";

                        var parentElemnt = j2HTML.GetElement({
                            Element: this._parentId
                        });
                        var w = parentElemnt[0].clientWidth;
                        var h = parentElemnt[0].clientHeight;

                        parentElemnt[0].style.position = 'relative';

                        modal = '<div id="divj2HTMLWaitingModal" class="hideWaiting" style="width:' + w + 'px; height:' + h + 'px">';
                    } else {
                        centerClass = "center";
                        modal = '<div id="divj2HTMLWaitingModal" class="hideWaiting">';
                    }
                    var str = modal + '\n                        <div class="' + centerClass + '">\n                            ' + txt + '\n                            ' + spinner + '\n                        </div>\n                      </div>';

                    //IF PARENT IS AVAIBLE APPEND TO PARENT OTHERWISE APPEND TO BODY      
                    var elem = void 0;
                    if (this._parentId !== false) {
                        elem = j2HTML.GetElement({
                            Element: this._parentId
                        });
                    } else {
                        elem = j2HTML.GetElement({
                            Element: 'body'
                        });
                    }

                    elem[0].insertAdjacentHTML('beforeend', str);

                    var waitingModal = j2HTML.GetElement({
                        Element: '#divj2HTMLWaitingModal'
                    });
                    waitingModal[0].classList.remove('hideWaiting');
                    if (this._parentId !== false) {
                        waitingModal[0].classList.add('showWaitingInParent');
                    } else {
                        waitingModal[0].classList.add('showWaiting');
                    }
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }
        }, {
            key: 'hideWaitingModal',
            value: function hideWaitingModal() {

                var waitingModal = j2HTML.GetElement({
                    Element: '#divj2HTMLWaitingModal'
                });
                waitingModal[0].classList.remove('showWaiting');
                waitingModal[0].classList.add('hideWaiting');

                j2HTML.RemoveElement({
                    Element: '#divj2HTMLWaitingModal'
                });
            }
            // FUNCTION WILL BE INVOVED WHEN SUBN+MIT BUTTON CLICKED

        }, {
            key: 'updateModal',
            value: function updateModal() {

                var isContentUpdated = false;
                var oldData = modalData;
                var newData = j2HTML.GetElement({
                    Element: '.tb' + modalID.slice(1) + '[visible="true"]'
                });

                var arraynewObj = [];
                var newObj = {};

                //GET ALL TABLES OF MODALES --- TABLE = #s of ROWS
                [].slice.call(newData).map(function (ndata, index) {

                    //let test = 'Hello';

                    //GET ALL THE COLUMNS OF THE TABLE IN MODAL
                    [].slice.call(j2HTML.GetElement({
                        Element: '#' + ndata.getAttribute('id') + ' tbody tr'
                    })).map(function (col, colIndex) {

                        var propName = col.children[0].id.substring(2).split('_')[0];
                        var newValue = document.getElementById('txt' + col.children[0].id.substring(2)).value;

                        if (oldData[index][propName] !== null && newValue !== null && oldData[index][propName].toString() !== newValue.toString()) {

                            var colId = void 0;

                            if (rowID !== false) {
                                colId = '#td_' + rowID.substring(2) + '_' + colIndex;
                                j2HTML.SetElementText({
                                    Element: colId,
                                    Value: newValue
                                });
                            }

                            isContentUpdated = true;

                            //j2HTML.SetElementText({ Element: colId, Value: newValue });
                            newObj[propName] = newValue;
                        } else {
                            newObj[propName] = newValue;
                        }
                    });
                    arraynewObj.push(newObj);
                    newObj = {};
                });

                if (isContentUpdated === true) {

                    updatefn(JSON.stringify(arraynewObj));

                    j2HTML.HideModal({
                        ModalID: modalID
                    });
                    j2HTML.RemoveElement({
                        Element: modalID
                    });
                } else {
                    updatefn('No data has been updated');
                }
            }
        }, {
            key: 'createModal',
            value: function createModal() {

                var tblId = tableId;
                var fn = createFun;

                //INSERT ROW INTO TABLE WHEN USING CREATE MODAL WITH TABLE
                if (tableId !== false) {

                    var element = j2HTML.GetElement({
                        Element: '.tbj2HTMLModal tbody tr td'
                    });
                    var totalRows = j2HTML.GetElement({
                        Element: tblId + ' tbody tr'
                    }).length;
                    var newDataArray = [];
                    var newDataObj = {};

                    var tr = '<tr id="tr' + totalRows + '" class="item" align="left">';

                    [].slice.call(element).map(function (col, colIndex) {

                        if (!col.getAttribute('id').includes('tdAdditionalColumn')) {
                            var propName = col.children[0].children[0].innerText;
                            var propValue = col.children[0].children[1].value;
                            var td = '<td data-label="' + propName + '" id="td_' + totalRows + '_' + colIndex + '">' + propValue + '</td>';
                            tr += td;
                            newDataObj[propName] = propValue;
                        } else {
                            tr += '<td data-label="" id="td_' + totalRows + '_' + colIndex + '" class="getSelectedRow">' + j2HTML.GetElement({ Element: '#' + col.getAttribute('id') })[0].children[0].outerHTML + '</td>';
                        }
                    });
                    tr += '</tr>';

                    var appendAfter = '#tr' + (totalRows - 1);
                    j2HTML.AppendHTMLStringAfter({
                        Element: appendAfter,
                        HTMLString: tr
                    });

                    var t = new table();
                    t.addeventForSelectedRows(pramSelectRows);

                    //j2HTML.CreateEvent('.getSelectedRow', 'click', privateMethods.j2HTMLHighlight_And_GetSelectedSingleRowData);

                    newDataArray.push(newDataObj);
                    //fn(JSON.stringify(newDataArray));
                    //REMOVE MODAL
                    j2HTML.HideModal({
                        ModalID: modalID
                    });
                    j2HTML.RemoveElement({
                        Element: modalID
                    });
                } else {
                    var objCreateData = {};

                    var dataElemt = j2HTML.GetElement({
                        Element: '.J2HTMLModalTextBox'
                    });
                    [].slice.call(dataElemt).map(function (elemt) {

                        var Id = elemt.getAttribute('id').slice(3).slice(0, -4);
                        var val = elemt.value;
                        objCreateData[Id] = val;
                    });

                    //fn(objCreateData);

                    //REMOVE MODAL
                    j2HTML.HideModal({
                        ModalID: modalID
                    });
                    j2HTML.RemoveElement({
                        Element: modalID
                    });
                }
            }

            // PAGINATION BUTTONS

        }, {
            key: 'j2HTMLFirstModal',
            value: function j2HTMLFirstModal() {

                //let frmID = formID;
                var id = 'tb' + modalID.slice(1) + '_0';
                var modals = document.querySelectorAll('.' + 'tb' + modalID.slice(1));

                [].slice.call(modals).map(function (modal) {
                    modal.style.display = 'none';
                    modal.setAttribute('visible', 'false');
                });

                document.getElementById(id).style.display = '';
                document.getElementById(id).setAttribute('visible', 'true');

                var totalNumbers = 'Showing 1 of ' + totalModals;
                j2HTML.EmptyElement({
                    Element: '#modalNumners'
                });
                j2HTML.AppendHTMLString({
                    Element: '#modalNumners',
                    HTMLString: totalNumbers
                });

                document.getElementById('btn' + modalID.slice(1) + 'First').classList.add('disabled');
                document.getElementById('btn' + modalID.slice(1) + 'Previous').classList.add('disabled');

                document.getElementById('btn' + modalID.slice(1) + 'Last').classList.remove('disabled');
                document.getElementById('btn' + modalID.slice(1) + 'Next').classList.remove('disabled');
            }
        }, {
            key: 'j2HTMLNextModal',
            value: function j2HTMLNextModal() {

                var id = document.querySelectorAll('.' + 'tb' + modalID.slice(1) + '[visible="true"]')[0].getAttribute('id');
                var currentId = id.substring(modalID.slice(1).length + 3, id.length);
                var nextId = 'tb' + modalID.slice(1) + '_' + (parseInt(currentId) + 1);

                if (parseInt(currentId) !== parseInt(totalModals) - 1) {

                    var modals = document.querySelectorAll('.' + 'tb' + modalID.slice(1));

                    [].slice.call(modals).map(function (modal) {
                        modal.style.display = 'none';
                        modal.setAttribute('visible', 'false');
                    });

                    document.getElementById(nextId).style.display = '';
                    document.getElementById(nextId).setAttribute('visible', 'true');

                    var totalNumbers = 'Showing ' + (parseInt(currentId) + 2) + ' of ' + totalModals;
                    j2HTML.EmptyElement({
                        Element: '#modalNumners'
                    });
                    j2HTML.AppendHTMLString({
                        Element: '#modalNumners',
                        HTMLString: totalNumbers
                    });

                    document.getElementById('btn' + modalID.slice(1) + 'First').classList.remove('disabled');
                    document.getElementById('btn' + modalID.slice(1) + 'Previous').classList.remove('disabled');
                } else if (parseInt(currentId) === parseInt(totalModals) - 1) {
                    document.getElementById('btn' + modalID.slice(1) + 'Last').classList.add('disabled');
                    document.getElementById('btn' + modalID.slice(1) + 'Next').classList.add('disabled');
                }
            }
        }, {
            key: 'j2HTMLPreviousModal',
            value: function j2HTMLPreviousModal() {

                var id = document.querySelectorAll('.' + 'tb' + modalID.slice(1) + '[visible="true"]')[0].getAttribute('id');
                var currentId = id.substring(modalID.slice(1).length + 3, id.length);
                var previousId = 'tb' + modalID.slice(1) + '_' + (parseInt(currentId) - 1);

                if (parseInt(currentId) !== 0) {

                    var modals = document.querySelectorAll('.' + 'tb' + modalID.slice(1));

                    [].slice.call(modals).map(function (modal) {
                        modal.style.display = 'none';
                        modal.setAttribute('visible', 'false');
                    });

                    document.getElementById(previousId).style.display = '';
                    document.getElementById(previousId).setAttribute('visible', 'true');

                    var totalNumbers = 'Showing ' + (parseInt(currentId) - 1 + 1) + ' of ' + totalModals;
                    j2HTML.EmptyElement({
                        Element: '#modalNumners'
                    });
                    j2HTML.AppendHTMLString({
                        Element: '#modalNumners',
                        HTMLString: totalNumbers
                    });
                }
                if (parseInt(currentId) - 1 === 0) {
                    document.getElementById('btn' + modalID.slice(1) + 'First').classList.add('disabled');
                    document.getElementById('btn' + modalID.slice(1) + 'Previous').classList.add('disabled');
                }
            }
        }, {
            key: 'j2HTMLLastModal',
            value: function j2HTMLLastModal() {

                //let frmID = formID;
                var id = 'tb' + modalID.slice(1) + '_' + (parseInt(totalModals) - 1);
                var modals = document.querySelectorAll('.' + 'tb' + modalID.slice(1));

                [].slice.call(modals).map(function (modal) {
                    modal.style.display = 'none';
                    modal.setAttribute('visible', 'false');
                });

                document.getElementById(id).style.display = '';
                document.getElementById(id).setAttribute('visible', 'true');

                var totalNumbers = 'Showing ' + parseInt(totalModals) + ' of ' + totalModals;
                j2HTML.EmptyElement({
                    Element: '#modalNumners'
                });
                j2HTML.AppendHTMLString({
                    Element: '#modalNumners',
                    HTMLString: totalNumbers
                });

                document.getElementById('btn' + modalID.slice(1) + 'Last').classList.add('disabled');
                document.getElementById('btn' + modalID.slice(1) + 'Next').classList.add('disabled');

                document.getElementById('btn' + modalID.slice(1) + 'First').classList.remove('disabled');
                document.getElementById('btn' + modalID.slice(1) + 'Previous').classList.remove('disabled');
            }
            //endregion

        }, {
            key: 'modalData',
            get: function get() {
                return this._modalData;
            },
            set: function set(value) {
                this._modalData = value;
            }

            //MODAL DATA

        }, {
            key: 'modalID',
            get: function get() {
                return this._modalID;
            },
            set: function set(value) {
                this._modalID = value;
            }

            //APPEND TO

        }, {
            key: 'appendTo',
            get: function get() {
                return this._appendTo;
            },
            set: function set(value) {
                this._appendTo = value;
            }

            //MODAL HEADER

        }, {
            key: 'modalHeader',
            get: function get() {
                return this._modalHeader;
            },
            set: function set(value) {
                this._modalHeader = value;
            }

            //MODAL FOOTER

        }, {
            key: 'modalFooter',
            get: function get() {
                return this._modalFooter;
            },
            set: function set(value) {
                this._modalFooter = value;
            }

            //MODAL SIZE

        }, {
            key: 'modalSize',
            get: function get() {
                return this._modalSize;
            },
            set: function set(value) {
                this._modalSize = value;
            }

            //HEADING (TEXT)

        }, {
            key: 'modalHeading',
            get: function get() {
                return this._headingText;
            },
            set: function set(value) {
                this._headingText = value;
            }

            //DISPLAY

        }, {
            key: 'display',
            get: function get() {
                return this._display;
            },
            set: function set(value) {
                this._display = value;
            }

            //EXCLUDE

        }, {
            key: 'exclude',
            get: function get() {
                return this._exclude;
            },
            set: function set(value) {
                this._exclude = value;
            }
        }, {
            key: 'include',
            get: function get() {
                return this._include;
            },
            set: function set(value) {
                this._include = value;
            }

            //CUSTOM COLUMNS

        }, {
            key: 'customColumns',
            get: function get() {
                return this._customColumns;
            },
            set: function set(value) {
                this._customColumns = value;
            }

            //FOR LOADING MODAL

        }, {
            key: 'loadingModalText',
            get: function get() {
                return this._loadingModalText;
            },
            set: function set(value) {
                this._loadingModalText = value;
            }
        }, {
            key: 'parentID',
            get: function get() {
                return this._parentId;
            },
            set: function set(value) {
                this._parentId = value;
            }
        }, {
            key: 'image',
            get: function get() {
                return this._image;
            },
            set: function set(value) {
                this._image = value;
            }
        }, {
            key: 'class',
            get: function get() {
                return this._class;
            },
            set: function set(value) {
                this._class = value;
            }

            //ONLY WHEN CREATING MODAL FROM TABLE
            //INSERT ROW, UPDATE ROW AND DELETE ROW

            //TABLE ID

        }, {
            key: 'tableID',
            get: function get() {
                return this._tableID;
            },
            set: function set(value) {
                this._tableID = value;
            }

            //ROW ID

        }, {
            key: 'rowID',
            get: function get() {
                return this._rowID;
            },
            set: function set(value) {
                this._rowID = value;
            }

            //UPDATE FUNCTION

        }, {
            key: 'updateFunction',
            get: function get() {
                return this._updateFunction;
            },
            set: function set(value) {
                this._updateFunction = value;
            }

            //CREATE FUNCTION

        }, {
            key: 'createFunction',
            get: function get() {
                return this._createFunction;
            },
            set: function set(value) {
                this._createFunction = value;
            }
        }, {
            key: 'selectRows',
            get: function get() {
                return this._selectRows;
            },
            set: function set(value) {
                this._selectRows = value;
            }
        }]);

        return j2HTMLModal;
    }();

    //#region ************** PRIVATE VARIABLE AND METHOD *******

    var updateFun = false;
    var createFun = false;
    var modalID = void 0;
    var modalBody = "This is body";
    var modalBodyId = void 0;
    var modalHeadingId = void 0;
    var modalFooterId = void 0;
    var defaultDisplay = 'readonly';
    //let objCustomColumns = false;
    var totalModals = void 0;
    var excludeItemFromModal = void 0;
    var includeItemToModal = void 0;
    var tableId = void 0;
    var pramSelectRows = void 0;

    //********** FOR UPDATE MODAL */
    var modalData = void 0;
    var rowID = void 0;
    var updatefn = void 0;

    //let re_arrange_data_array=[];
    //let re_arrange_data_obj={};

    //ONLY WHEN CREATING MODAL FROM TABLE
    //INSERT ROW, UPDATE ROW AND DELETE ROW
    var count = void 0;

    //PRIVATE FUNCTIONS
    var privateMethods = {

        /**
         * CREATE LABLE
         * @param {ID of Modal} ID 
         * @param {data} obj 
         */
        getReadonlyModal: function getReadonlyModal(ID, obj) {

            var tb = void 0;
            tb = document.createElement('table');
            tb.style = 'width:95%';
            tb.id = 'tb' + ID.slice(1) + '_' + count;
            tb.classList.add('tb' + ID.replace('#', ''));

            if (count === 0) {
                tb.style.display = '';
                tb.setAttribute('visible', 'true');
            } else {
                tb.style.display = 'none';
                tb.setAttribute('visible', 'false');
            }

            count = count + 1;

            Object.keys(obj).map(function (objProperty, index) {

                //CREATE ROW
                var tr = document.createElement('tr');
                //CREATE COLUMN-1
                var td1 = document.createElement('td');
                td1.style = 'width:auto';
                //CREATE BOLD
                var b1 = document.createElement('b');
                //ADD b1 to td
                b1.appendChild(document.createTextNode(objProperty));
                //APPEND b1 to td1
                td1.appendChild(b1);
                //APPEND td1 to row
                tr.appendChild(td1);

                //CREATE COLUMN-2
                var td2 = document.createElement('td');
                td2.style = 'width:auto';
                //APPEND TEXT TO ELEMENT
                td2.appendChild(document.createTextNode(obj[objProperty]));
                //APPEND td2 to ROW
                tr.appendChild(td2);

                //APPEND ROW TO TABLE
                tb.appendChild(tr);
            });

            return tb;
        },


        /**
         * 
         * @param {Modal ID} ID 
         * @param {Custom Column(s) Object} customObj 
         * @param {Orginal Column(s) Object} orginalObj 
         * @param {is this for update} isUpdated 
         */
        getEditableModal: function getEditableModal(ID, customObj) {
            var orginalObj = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            var isUpdated = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;


            var tb = void 0;
            var oldj = orginalObj;
            var newj = customObj;

            tb = document.createElement('table');
            tb.style = 'width:95%';
            tb.id = 'tb' + ID.slice(1) + '_' + count;
            tb.classList.add('tb' + ID.replace('#', ''));

            if (count === 0) {
                tb.style.display = '';
                tb.setAttribute('visible', 'true');
            } else {
                tb.style.display = 'none';
                tb.setAttribute('visible', 'false');
            }

            Object.keys(customObj).map(function (objProperty, index) {

                //CREATE ROW
                var tr = document.createElement('tr');
                //CREATE COLUMN-1
                var td1 = document.createElement('td');

                if (oldj === null) {
                    td1.id = 'td' + Object.keys(newj)[index] + '_' + count + '_' + index;
                } else {
                    td1.id = 'td' + Object.keys(oldj)[index] + '_' + count + '_' + index;
                }

                td1.style = 'width:auto; padding:0px; margin:0px';

                //***************************************** */

                //CREATE DIV
                var div = document.createElement('div');
                div.classList.add('placeholder');

                if (Object.keys(newj)[index] !== 'AdditionalColumn') {

                    //CREATE TEXTBOX
                    var txt = document.createElement('input');
                    txt.type = 'text';

                    if (oldj === null) {
                        txt.id = 'txt' + Object.keys(newj)[index] + '_' + count + '_' + index;
                    } else {
                        txt.id = 'txt' + Object.keys(oldj)[index] + '_' + count + '_' + index;
                    }

                    txt.classList.add('form-control');
                    txt.classList.add('J2HTMLModalTextBox');

                    if (oldj === null) {
                        txt.classList.add(Object.keys(newj)[index]);
                    } else {
                        txt.classList.add(Object.keys(oldj)[index]);
                    }
                    //APPEND JSON PROPERTY TO TEXTBOX
                    if (isUpdated === true) {
                        txt.setAttribute('value', customObj[objProperty]);
                    }

                    txt.setAttribute('placeholder', objProperty);

                    //APPEND TXT TO DIV
                    div.appendChild(txt);

                    //APPEND DIV TO td1
                    td1.appendChild(div);
                } else {
                    //td1.appendChild(customObj[objProperty]);
                    td1.insertAdjacentHTML('beforeend', customObj[objProperty]);
                    td1.style.display = 'none';
                }

                //APPEND td1 to ROW
                tr.appendChild(td1);

                //APPEND ROW TO TABLE
                tb.appendChild(tr);
            });

            count = count + 1;
            return tb;
        }
    };

    return j2HTMLModal;
}();
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

//**************  USE ICON IF IT IS ICON, USER img IF IT IS IMAGE ******************/

/**
 *                      Extra small	    Small	    Medium	   Large	      Extra Large
        Class prefix	.col-	        .col-sm-    .col-md-   .col-lg-	     .col-xl-
        Screen width	<576px	        >=576px	    >=768px	   >=992px	     >=1200px
 * 
 */

//**  TEXT ON TOP */
//<a class="j2HTMLNavBarLink " href="#"><icon class="fa fa-fw fa-home"></icon><br>Element</a>

//**  TEXT ON RIGHT */
//<a class="j2HTMLNavBarLink " href="#">Element <icon class="fa fa-fw fa-home"></icon></a>

//**  TEXT ON LEFT */
//<a class="j2HTMLNavBarLink " href="#"><icon class="fa fa-fw fa-home"></icon> Element</a>

var j2HTMLNavbar = function () {

        //CREAT CLASS
        var j2HTMLNavbar = function () {
                function j2HTMLNavbar() {
                        _classCallCheck(this, j2HTMLNavbar);

                        this._data = false;
                        this._menuId = false;
                        this._position = false;
                        this._appendTo = false;
                        this._text = false;
                        this._link = false;

                        this._titlebgcolor = false;
                        this._titleforecolor = false;

                        this._navbarbgcolor = false;
                        this._navbarforecolor = false;

                        this._selectedmneubgcolor = false;
                        this._selectedmenuforecolor = false;

                        this._hoverbgcolor = false;
                        this._hoverforecolor = false;

                        this._largemenubgcolor = false;
                        this._largemenuforecolor = false;

                        this._largemenuhoverbgcolor = false;
                        this._largemenuhoverforecolor = false;

                        this._submenubgcolor = false;
                        this._submenuforecolor = false;

                        this._headingcolor = false;
                        this._subheadingcolor = false;
                }

                //#region ************** PROPERTIES ******************

                _createClass(j2HTMLNavbar, [{
                        key: 'getnavbar',


                        //endregion

                        //#region ************** PUBLIC FUNCTIONS ************
                        value: function getnavbar() {

                                //SET LOCAL VARIABLE VALUES
                                navbarId = this._menuId;

                                if (this._position === false) {
                                        navbarPosition = 'top';
                                } else {
                                        navbarPosition = this._position;
                                }
                                //navbarPosition = this._position;
                                navbarTitleColor = this._titleforecolor;
                                navbarTitleBGColor = this._titlebgcolor;

                                //GET NAVBAR
                                if (navbarId !== false) {
                                        navbarObj = j2HTML.GetElement({
                                                Element: navbarId
                                        });
                                } else if (navbarId === false) {
                                        //else if (navbarPosition.toLowerCase() === 'top' && navbarId === false) {
                                        navbarObj = j2HTML.GetElement({
                                                Element: 'nav[position="' + navbarPosition + '"]'
                                        });
                                }

                                //SET NAVBAR CSS ACCORDING TO POSITION
                                if (navbarPosition.toLowerCase() === 'top') {
                                        navbarObj[0].classList.add('j2HTMLTopNavbar');
                                } else if (navbarPosition.toLowerCase() === 'left') {
                                        navbarObj[0].classList.add('j2HTMLLeftNavbar');
                                }

                                //SET NAVBAR BACKGROUND AND FORECOLOR
                                navbarObj[0].style.backgroundColor = this._navbarbgcolor;
                                navbarObj[0].style.color = this._navbarforecolor;

                                //SET NAVBAR TITLE COLOR
                                if (j2HTML.IsElementExsit({
                                        Element: 'nav[position="' + navbarPosition + '"] .nav-title'
                                })) {
                                        var _menuTitle = j2HTML.GetElement({
                                                Element: 'nav[position="' + navbarPosition + '"] .nav-title'
                                        });
                                        _menuTitle[0].style.backgroundColor = this._titlebgcolor;
                                        _menuTitle[0].style.color = this._titleforecolor;
                                }

                                //ADD HUM BURGER ICON FOR SMALL DEVICE (JAVASCRIPT MEDIA QUERY)
                                window.addEventListener("resize", function () {

                                        privateMethods.navbarMediaQuery();
                                });
                                privateMethods.navbarMediaQuery();

                                // if (this._data !== false) {

                                //     let navBarPosition = this._position;

                                //     let nav = `<nav class="j2HTMLNavBar navBar-BgColor" position="${navBarPosition}"> <ul>`;

                                //     data.map((menu) => {

                                //         nav += `<li><a class="j2HTMLNavBarLink" href="# ">${menu.text}</a>`;
                                //     });
                                //     privateMethods.setNavBar(this.menuId);

                                // } else {
                                //     privateMethods.setNavBar(this.menuId);
                                // }

                                // ////SET NAVBAR COLOR
                                // let setMenuColor = '';
                                // if (this.menuId !== false) {
                                //     setMenuColor = `.j2HTMLNavBar[id="${this.menuId}"]`;
                                // } else {
                                //     setMenuColor = `.j2HTMLNavBar:not([customcolor="true"])`;
                                // }
                                // [].slice.call(j2HTML.GetElement({ Element: setMenuColor })).map((element) => {

                                //     let position = element.getAttribute('position');
                                //     //GET SPECIFIC MENU COLRS AND APPEND TO HIDDEN ELEMENT(navbarcolor) 
                                //     let strHTML = `<navbarcolor 
                                //                     navbarbgcolor="${this.navbarbgcolor}" 
                                //                     navbarforecolor="${this.navbarforecolor}" 
                                //                     activebgcolor="${this.selectedmneubgcolor}"
                                //                     activeforecolor="${this.selectedmenuforecolor}" 
                                //                     hoverbgcolor="${this.hoverbgcolor}"
                                //                     hoverforecolor="${this.hoverforecolor}"
                                //                     largemenubgcolor="${this.largemenubgcolor}"
                                //                     largemenuforecolor="${this.largemenuforecolor}"
                                //                     largemenuhoverbgcolor="${this.largemenuhoverbgcolor}"
                                //                     largemenuhoverforecolor="${this.largemenuhoverforecolor}"
                                //                     submenubgcolor="${this.submenubgcolor}"
                                //                     submenuforecolor="${this.submenuforecolor}"
                                //                     headingcolor="${this.headingcolor}"
                                //                     subheadingcolor="${this.subheadingcolor}"
                                //                     style="display:hidden">
                                //                  </navbarcolor>`;
                                //     j2HTML.AppendHTMLString({ Element: `.j2HTMLNavBar[position="${position}"]`, HTMLString: strHTML });

                                //     if (this.menuId !== false) {
                                //         element.setAttribute('customcolor', true);
                                //         privateMethods.SetNavBarColor(element, this.navbarbgcolor, this.navbarforecolor);
                                //     } else {
                                //         privateMethods.SetNavBarColor(element, this.navbarbgcolor, this.navbarforecolor);
                                //     }
                                // });


                                // /**
                                //  * 
                                //  * //SET MENU LINK COLOR
                                //  * 
                                //  */
                                // let setMenuULColor = '';
                                // if (this.menuId !== false) {

                                //     setMenuULColor = `.j2HTMLNavBar[id="${this.menuId}"] .j2HTMLNavBarLink`;
                                // } else {
                                //     setMenuULColor = `.j2HTMLNavBar:not([customcolor="true"]) .j2HTMLNavBarLink`;
                                // }

                                // [].slice.call(j2HTML.GetElement({ Element: setMenuULColor })).map((element) => {

                                //     let navBar = j2HTML.GetClosestParentElement({ Element: element, Selector: '.j2HTMLNavBar' });
                                //     if (navBar.getAttribute('position') === 'top') {

                                //         if (j2HTML.GetElement({ Element: '.j2HTMLNavBar ul li:first-child .brandSubText' }).length !== 0) {
                                //             if (j2HTML.GetClosestParentElement({ Element: element, Selector: '.submenu' }) === undefined) {
                                //                 element.style.height = j2HTML.GetElement({ Element: '.j2HTMLNavBar ul li:first-child' })[0].offsetHeight + 'px';
                                //                 element.parentElement.style.height = j2HTML.GetElement({ Element: '.j2HTMLNavBar ul li:first-child' })[0].offsetHeight + 'px';
                                //             }
                                //         }
                                //     }
                                //     privateMethods.SetNavBarColor(element, this.navbarbgcolor, this.navbarforecolor);
                                // });

                                // //SET ACTIVE COLOR
                                // let setActiveColor = '';
                                // if (this.menuId !== false) {

                                //     setActiveColor = `.j2HTMLNavBar[id="${this.menuId}"] li.active`;
                                // } else {
                                //     setActiveColor = `.j2HTMLNavBar:not([customcolor="true"]) li.active`;
                                // }
                                // [].slice.call(j2HTML.GetElement({ Element: setActiveColor })).map((element) => {

                                //     menuActiveBGColor = j2HTML.GetClosestParentElement({ Element: element, Selector: '.j2HTMLNavBar' }).children[1].getAttribute('activebgcolor');
                                //     menuActiveForeColor = j2HTML.GetClosestParentElement({ Element: element, Selector: '.j2HTMLNavBar' }).children[1].getAttribute('activeforecolor');

                                //     privateMethods.SetNavBarColor(element.children[0], this.selectedmneubgcolor, this.selectedmenuforecolor);
                                // });

                                // //******** SET HOVER COLOR (NAVBAR) *********
                                // /**
                                //  * 
                                //  * MOUSE OVER EVENT (NAVBAR)
                                //  * 
                                //  */
                                // let mouseOverEvent;
                                // if (this.menuId !== false) {
                                //     mouseOverEvent = `.j2HTMLNavBar[id="${this.menuId}"] .j2HTMLNavBarLink`;
                                // } else {
                                //     mouseOverEvent = `.j2HTMLNavBar:not([customcolor="true"]) .j2HTMLNavBarLink`;
                                // }
                                // j2HTML.CreateEvent(mouseOverEvent, 'mouseover', () => {

                                //     menuHoverBGColor = j2HTML.GetClosestParentElement({ Element: event.target, Selector: '.j2HTMLNavBar' }).children[1].getAttribute('hoverbgcolor');
                                //     menuHoveForeColor = j2HTML.GetClosestParentElement({ Element: event.target, Selector: '.j2HTMLNavBar' }).children[1].getAttribute('hoverforecolor');
                                //     if (event.target.children.length !== 0) {
                                //         event.target.children[0].style.backgroundColor = '';
                                //         event.target.children[0].style.color = '';
                                //     }
                                //     privateMethods.SetNavBarColor(event.target, menuHoverBGColor, menuHoveForeColor);

                                // });

                                // /**
                                //  * 
                                //  * MOUSE OUT EVENT (NAVBAR)
                                //  * 
                                //  */
                                // let mouseOutEvent;
                                // if (this.menuId !== false) {
                                //     mouseOutEvent = `.j2HTMLNavBar[id="${this.menuId}"] .j2HTMLNavBarLink`;
                                // } else {
                                //     mouseOutEvent = `.j2HTMLNavBar:not([customcolor="true"]) .j2HTMLNavBarLink`;
                                // }
                                // j2HTML.CreateEvent(mouseOutEvent, 'mouseout', () => {

                                //     menuBGColor = j2HTML.GetClosestParentElement({ Element: event.target, Selector: '.j2HTMLNavBar' }).children[1].getAttribute('navbarbgcolor');
                                //     menuForeColor = j2HTML.GetClosestParentElement({ Element: event.target, Selector: '.j2HTMLNavBar' }).children[1].getAttribute('navbarforecolor');

                                //     submenuGBColor = j2HTML.GetClosestParentElement({ Element: event.target, Selector: '.j2HTMLNavBar' }).children[1].getAttribute('submenubgcolor');
                                //     submenuForeColor = j2HTML.GetClosestParentElement({ Element: event.target, Selector: '.j2HTMLNavBar' }).children[1].getAttribute('submenuforecolor');

                                //     let isSubmenu = j2HTML.GetClosestParentElement({ Element: event.target, Selector: '.submenu' });

                                //     if (event.target.parentElement.getAttribute('keepselected') === 'true' || event.target.parentElement.classList.contains('active')) {
                                //         event.target.style.backgroundColor = menuActiveBGColor;
                                //         event.target.style.color = menuActiveForeColor;
                                //     } else {

                                //         if (isSubmenu === undefined) {
                                //             event.target.style.backgroundColor = menuBGColor;
                                //             event.target.style.color = menuForeColor;
                                //         } else {
                                //             event.target.style.backgroundColor = submenuGBColor;
                                //             event.target.style.color = submenuForeColor;
                                //         }
                                //     }
                                // });


                                // //******** SET HOVER COLOR (LARGEMENU)*********
                                // /**
                                //  * 
                                //  * MOUSE OVER EVENT (LARGEMENU)
                                //  * 
                                //  */
                                // let largemenuMouseOverEvent;
                                // if (this.menuId !== false) {
                                //     largemenuMouseOverEvent = `.j2HTMLNavBar[id="${this.menuId}"] .largemenu a`;
                                // } else {
                                //     largemenuMouseOverEvent = `.j2HTMLNavBar:not([customcolor="true"]) .largemenu a`;
                                // }
                                // j2HTML.CreateEvent(largemenuMouseOverEvent, 'mouseover', () => {

                                //     menuLargemenuHoverBGColor = j2HTML.GetClosestParentElement({ Element: event.target, Selector: '.j2HTMLNavBar' }).children[1].getAttribute('largemenuhoverbgcolor');
                                //     menuLargemenuHoverForecolor = j2HTML.GetClosestParentElement({ Element: event.target, Selector: '.j2HTMLNavBar' }).children[1].getAttribute('largemenuhoverforecolor');
                                //     event.target.style.backgroundColor = menuLargemenuHoverBGColor;
                                //     event.target.style.color = menuLargemenuHoverForecolor;
                                //     event.target.style.display = 'block';
                                //     event.target.style.textDecoration = 'none';
                                // });
                                // /**
                                //  * 
                                //  * MOUSE OUT EVENT (LARGEMENU)
                                //  * 
                                //  */
                                // let largemenuMouseOutEvent;
                                // if (this.menuId !== false) {
                                //     largemenuMouseOutEvent = `.j2HTMLNavBar[id="${this.menuId}"] .largemenu a`;
                                // } else {
                                //     largemenuMouseOutEvent = `.j2HTMLNavBar:not([customcolor="true"]) .largemenu a`;
                                // }
                                // j2HTML.CreateEvent(largemenuMouseOutEvent, 'mouseout', () => {

                                //     menuLargemenuBGColor = j2HTML.GetClosestParentElement({ Element: event.target, Selector: '.j2HTMLNavBar' }).children[1].getAttribute('largemenubgcolor');
                                //     menuLargemenuForecolor = j2HTML.GetClosestParentElement({ Element: event.target, Selector: '.j2HTMLNavBar' }).children[1].getAttribute('largemenuforecolor');
                                //     event.target.style.backgroundColor = menuLargemenuBGColor;
                                //     event.target.style.color = menuLargemenuForecolor;
                                //     //event.target.style.display = 'block';
                                //     //event.target.style.textDecoration = 'none';
                                // });

                                // //SET SUBMENU COLOR
                                // let submenuColor = '';
                                // if (this.menuId !== false) {
                                //     submenuColor = `.j2HTMLNavBar[id="${this.menuId}"] .submenu li a`;
                                // } else {
                                //     submenuColor = `.j2HTMLNavBar:not([customcolor="true"]) .submenu li a`;
                                // }
                                // [].slice.call(j2HTML.GetElement({ Element: submenuColor })).map((element) => {

                                //     element.style.backgroundColor = this.submenubgcolor;
                                //     element.style.color = this.submenuforecolor;

                                // });

                                // //SET HEADING COLOR
                                // let setHeadingColor = '';
                                // if (this.menuId !== false) {
                                //     setHeadingColor = `.j2HTMLNavBar[id="${this.menuId}"] .heading`;
                                // } else {
                                //     setHeadingColor = `.j2HTMLNavBar:not([customcolor="true"]) .heading`;
                                // }
                                // [].slice.call(j2HTML.GetElement({ Element: setHeadingColor })).map((element) => {

                                //     menuHeadingColor = j2HTML.GetClosestParentElement({ Element: element, Selector: '.j2HTMLNavBar' }).children[1].getAttribute('headingcolor');
                                //     element.style.color = menuHeadingColor;
                                // });

                                // //SET SUB-HEADING COLOR
                                // let setSubHeadingColor = '';
                                // if (this.menuId !== false) {
                                //     setSubHeadingColor = `.j2HTMLNavBar[id="${this.menuId}"] .subheading`;
                                // } else {
                                //     setSubHeadingColor = `.j2HTMLNavBar:not([customcolor="true"]) .subheading`;
                                // }
                                // [].slice.call(j2HTML.GetElement({ Element: setSubHeadingColor })).map((element) => {

                                //     menuSubheadingColor = j2HTML.GetClosestParentElement({ Element: element, Selector: '.j2HTMLNavBar' }).children[1].getAttribute('subheadingcolor');
                                //     element.style.color = menuSubheadingColor;
                                // });

                        }

                        //endregion


                }, {
                        key: 'data',
                        get: function get() {
                                return this._data;
                        },
                        set: function set(value) {
                                this._data = value;
                        }
                }, {
                        key: 'menuId',
                        get: function get() {
                                return this._menuId;
                        },
                        set: function set(value) {
                                this._menuId = value;
                        }
                }, {
                        key: 'position',
                        get: function get() {
                                return this._position;
                        },
                        set: function set(value) {
                                this._position = value;
                        }
                }, {
                        key: 'appendTo',
                        get: function get() {
                                return this._appendTo;
                        },
                        set: function set(value) {
                                this._appendTo = value;
                        }
                }, {
                        key: 'text',
                        get: function get() {
                                return this._text;
                        },
                        set: function set(value) {
                                this._text = value;
                        }
                }, {
                        key: 'link',
                        get: function get() {
                                return this._link;
                        },
                        set: function set(value) {
                                this._link = value;
                        }
                }, {
                        key: 'titlebgcolor',
                        get: function get() {
                                return this._titlebgcolor;
                        },
                        set: function set(value) {
                                this._titlebgcolor = value;
                        }
                }, {
                        key: 'titleforecolor',
                        get: function get() {
                                return this._titleforecolor;
                        },
                        set: function set(value) {
                                this._titleforecolor = value;
                        }
                }, {
                        key: 'navbarbgcolor',
                        get: function get() {
                                return this._navbarbgcolor;
                        },
                        set: function set(value) {
                                this._navbarbgcolor = value;
                        }
                }, {
                        key: 'navbarforecolor',
                        get: function get() {
                                return this._navbarforecolor;
                        },
                        set: function set(value) {
                                this._navbarforecolor = value;
                        }
                }, {
                        key: 'selectedmneubgcolor',
                        get: function get() {
                                return this._selectedmneubgcolor;
                        },
                        set: function set(value) {
                                this._selectedmneubgcolor = value;
                        }
                }, {
                        key: 'selectedmenuforecolor',
                        get: function get() {
                                return this._selectedmenuforecolor;
                        },
                        set: function set(value) {
                                this._selectedmenuforecolor = value;
                        }
                }, {
                        key: 'hoverbgcolor',
                        get: function get() {
                                return this._hoverbgcolor;
                        },
                        set: function set(value) {
                                this._hoverbgcolor = value;
                        }
                }, {
                        key: 'hoverforecolor',
                        get: function get() {
                                return this._hoverforecolor;
                        },
                        set: function set(value) {
                                this._hoverforecolor = value;
                        }
                }, {
                        key: 'largemenubgcolor',
                        get: function get() {
                                return this._largemenubgcolor;
                        },
                        set: function set(value) {
                                this._largemenubgcolor = value;
                        }
                }, {
                        key: 'largemenuforecolor',
                        get: function get() {
                                return this._largemenuforecolor;
                        },
                        set: function set(value) {
                                this._largemenuforecolor = value;
                        }
                }, {
                        key: 'largemenuhoverbgcolor',
                        get: function get() {
                                return this._largemenuhoverbgcolor;
                        },
                        set: function set(value) {
                                this._largemenuhoverbgcolor = value;
                        }
                }, {
                        key: 'largemenuhoverforecolor',
                        get: function get() {
                                return this._largemenuhoverforecolor;
                        },
                        set: function set(value) {
                                this._largemenuhoverforecolor = value;
                        }
                }, {
                        key: 'submenubgcolor',
                        get: function get() {
                                return this._submenubgcolor;
                        },
                        set: function set(value) {
                                this._submenubgcolor = value;
                        }
                }, {
                        key: 'submenuforecolor',
                        get: function get() {
                                return this._submenuforecolor;
                        },
                        set: function set(value) {
                                this._submenuforecolor = value;
                        }
                }, {
                        key: 'headingcolor',
                        get: function get() {
                                return this._headingcolor;
                        },
                        set: function set(value) {
                                this._headingcolor = value;
                        }
                }, {
                        key: 'subheadingcolor',
                        get: function get() {
                                return this._subheadingcolor;
                        },
                        set: function set(value) {
                                this._subheadingcolor = value;
                        }
                }]);

                return j2HTMLNavbar;
        }();

        //#region ************** PRIVATE VARIABLE AND METHOD *******


        var navbarId = void 0;
        var navbarPosition = void 0;
        var navbarObj = void 0;
        var menuTitle = void 0;

        //// COLOR
        var navbarTitleColor = void 0;
        var navbarTitleBGColor = void 0;

        // let mobileMenuLink;
        // let submenus = 0;
        // let mobileMenuButtons = '<ul style="list-style-type: none; padding-left:5px; padding-top:8px;">';
        // let isMobileNavBarEventCreated = false;
        // let isMenuClickEventCreated = false;

        // //COLOR
        // let menuBGColor = '';
        // let menuForeColor = '';
        // let menuActiveBGColor = '';
        // let menuActiveForeColor = '';
        // let menuHoverBGColor = '';
        // let menuHoveForeColor = '';
        // let menuLargeBGColor = '';
        // let menuLargeForeColor = '';
        // let menuSubmenuBGColor = '';
        // let menuSubmenuForeColor = '';
        // let menuLargemenuHoverBGColor = '';
        // let menuLargemenuHoverForecolor = '';
        //let isCustomColor = false;


        //PRIVATE FUNCTIONS
        var privateMethods = {
                toggleNavbar: function toggleNavbar() {

                        var navPosition = navbarPosition;

                        j2HTML.ToggleElement({
                                Element: 'nav[position="' + navPosition + '"] .nav-menu'
                        });
                },
                navbarMediaQuery: function navbarMediaQuery() {

                        if (navbarId !== false) {
                                menuTitle = j2HTML.GetElement({
                                        Element: 'nav[id="' + navbarId + '"] .nav-title'
                                });
                        } else if (navbarId === false) {
                                //else if (navbarPosition.toLowerCase() === 'top' && navbarId === false) {
                                menuTitle = j2HTML.GetElement({
                                        Element: 'nav[position=' + navbarPosition + '] .nav-title'
                                });
                        }

                        var titleHeading = j2HTML.GetElement({
                                Element: 'nav[position=' + navbarPosition + '] .nav-title h1'
                        });

                        var x = window.matchMedia("(max-width:500px)");

                        if (x.matches) {

                                //HIDE Navbar-MENU
                                var navMenu = j2HTML.GetElement({ Element: 'nav[position="' + navbarPosition + '"] .nav-menu' });
                                navMenu[0].style.display = 'none';

                                if (j2HTML.IsElementExsit({
                                        Element: 'nav[position="' + navbarPosition + '"] .nav-title h1'
                                })) {
                                        //FLOAT LEFT (nav-title)
                                        titleHeading[0].classList.add('float-left');
                                        titleHeading[0].style.marginLeft = '10px';
                                }

                                //FLOAT LEFT (nav-title)
                                //titleHeading[0].classList.add('float-left');
                                //titleHeading[0].style.marginLeft = '10px';

                                if (menuTitle.length === 0) {

                                        var strDiv = '<div class="nav-title">\n                                    <h2>Menu</h2>\n                                    <button id="btnHamburrgerMenu" class="float-right" style="background-color:' + navbarTitleBGColor + '; color:' + navbarTitleColor + ';">\n                                        <i class="fa fa-bars fa-lg"></i>\n                                    </button>\n                                  </div>';

                                        if (j2HTML.IsElementExsit({
                                                Element: '#btnHamburrgerMenu'
                                        }) === false) {
                                                j2HTML.AppendHTMLString({
                                                        Element: navbar[0],
                                                        HTMLString: strDiv
                                                });
                                        }
                                } else {
                                        if (navbarPosition.toLowerCase() === 'top' || navbarPosition.toLowerCase() === 'bottom') {

                                                var _strDiv = '<button id="btnHamburrgerMenu" class="btn btn-sm float-right" style="background-color:' + navbarTitleBGColor + '; color:' + navbarTitleColor + ';">\n                                        <i class="fa fa-bars fa-lg"></i>\n                                      </button>';
                                                if (j2HTML.IsElementExsit({
                                                        Element: '#btnHamburrgerMenu'
                                                }) === false) {
                                                        j2HTML.AppendHTMLString({
                                                                Element: '.nav-title',
                                                                HTMLString: _strDiv
                                                        });
                                                }
                                        }
                                }

                                //CREATE EVENT FOR MENU BUTTON 
                                [].slice.call(j2HTML.GetElement({
                                        Element: '#btnHamburrgerMenu'
                                })).map(function (element) {
                                        element.addEventListener('click', privateMethods.toggleNavbar, false);
                                });
                        } else {

                                //REMOVE FLOAT LEFT (nav-title)
                                titleHeading[0].classList.remove('float-left');
                                titleHeading[0].style.marginLeft = '0px';

                                //SHOW NAV-MENU
                                var _navMenu = j2HTML.GetElement({ Element: 'nav[position="' + navbarPosition + '"] .nav-menu' });
                                _navMenu[0].style.display = '';

                                //HIDE HAM BURGER MENU
                                if (j2HTML.IsElementExsit({
                                        Element: '#btnHamburrgerMenu'
                                })) {

                                        [].slice.call(j2HTML.GetElement({
                                                Element: '#btnHamburrgerMenu'
                                        })).map(function (element) {
                                                element.removeEventListener('click', privateMethods.toggleNavbar, false);
                                        });
                                        j2HTML.RemoveElement({
                                                Element: '#btnHamburrgerMenu'
                                        });
                                }
                        }
                }
        };

        //endregion


        return j2HTMLNavbar;
}();
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var paging = function () {

    //CREAT CLASS
    var paging = function () {
        function paging() {
            _classCallCheck(this, paging);

            this._data = false;

            this._paginationAppendTo = false;
            this._rowsPerPage = 10;
            this._tableID = '#tbJsonToHtml';
            this._startPage = 1;

            this._showPages = false;

            //this._startPageNumber = false;
            //this._endPageNumber = false;
        }

        //#region ************** PROPERTIES ******************

        _createClass(paging, [{
            key: 'getPaging',


            //endregion

            //#region ************** PUBLIC FUNCTIONS ************
            value: function getPaging() {
                var tableID = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this._tableID;
                var rowsPerPage = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this._rowsPerPage;
                var startPage = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : this._startPage;
                var showPages = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : this._showPages;
                var paginationAppendTo = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : this._paginationAppendTo;


                totalRows = j2HTML.GetElement({
                    Element: tableID + ' tbody tr'
                }).length; //$(tableName).find('tbody tr:has(td)').length;
                totalPages = Math.ceil(totalRows / rowsPerPage);

                if (totalPages < parseInt(startPage)) {
                    alert('There are total ' + totalPages + ' pages, therefore start page cannot be ' + startPage);
                } else {

                    if (showPages === false && startPage === 1) {
                        startPageNumber = 0;
                        endPageNumber = totalPages;
                    }

                    if (showPages !== false && startPage === 1) {
                        startPageNumber = 0;
                        endPageNumber = showPages;
                    }

                    if (showPages === false && startPage !== 1) {

                        startPageNumber = 0;
                        endPageNumber = totalPages;
                    }

                    if (showPages !== false && startPage !== 1) {

                        if (startPageNumber === false && endPageNumber === false) {
                            if (startPage > showPages) {

                                startPageNumber = Math.floor(parseInt(startPage) / parseInt(showPages)) * parseInt(showPages);
                                endPageNumber = parseInt(startPageNumber) + parseInt(showPages);
                            } else if (startPage <= showPages) {

                                startPageNumber = 0;
                                endPageNumber = showPages;
                            }
                        }
                    }

                    //CREATE PAGINATION
                    var pagination = privateMethods.createPagination(startPage, totalPages, showPages, startPageNumber, endPageNumber);

                    if (paginationAppendTo !== false) {

                        j2HTML.EmptyElement({
                            Element: paginationAppendTo
                        });
                        j2HTML.AppendHTMLString({
                            Element: paginationAppendTo,
                            HTMLString: pagination
                        });

                        if (totalPages <= 1) {
                            j2HTML.HideElement({
                                Element: paginationAppendTo
                            });
                        } else {
                            j2HTML.ShowElement({
                                Element: paginationAppendTo
                            });
                        }
                    } else {

                        if (j2HTML.IsElementExsit({
                            Element: '#pages'
                        }) === false) {
                            var div = '<div id="pages">' + pagination + '</div>';
                            j2HTML.AppendHTMLStringAfter({
                                Element: tableID,
                                HTMLString: div
                            });
                            if (totalPages <= 1) {
                                j2HTML.HideElement({
                                    Element: '#pages'
                                });
                            } else {
                                j2HTML.ShowElement({
                                    Element: '#pages'
                                });
                            }
                        } else {
                            j2HTML.EmptyElement({
                                Element: '#pages'
                            });
                            j2HTML.AppendHTMLString({
                                Element: '#pages',
                                HTMLString: pagination
                            });

                            if (totalPages <= 1) {
                                j2HTML.HideElement({
                                    Element: '#pages'
                                });
                            } else {
                                j2HTML.ShowElement({
                                    Element: '#pages'
                                });
                            }
                        }
                    }

                    //SHOW DATA
                    if (startPage !== undefined) {

                        if (endPageNumber > parseInt(startPage)) {
                            privateMethods.showPaginationData(tableID, startPage, rowsPerPage);
                        } else {
                            privateMethods.showPaginationData(tableID, startPageNumber + 1, rowsPerPage);
                        }
                    } else {
                        privateMethods.showPaginationData(tableID, startPageNumber + 1, rowsPerPage);
                    }
                }

                //SHOW SELECTED PAGE DATA
                privateMethods.showSelectedPage(tableID, rowsPerPage, paginationAppendTo);

                //REMOVE EVENT LISTERNER FROM PRVIOUS BUTTON IF IT IS PAGE ONE ELSE SHOW DATA
                var isPreviusBtnExsit = j2HTML.IsElementExsit({
                    Element: '#liPreviousPages'
                });

                if (startPageNumber !== 0) {
                    privateMethods.showPreviousPages(tableID, rowsPerPage, startPage, showPages, paginationAppendTo);
                } else {
                    if (isPreviusBtnExsit) {
                        var elemt = j2HTML.GetElement({
                            Element: '#liPreviousPages'
                        });
                        elemt[0].classList.add('disbaled');
                        elemt[0].removeEventListener("click", privateMethods.previousPage, false);
                    }
                }

                //REMOVE EVENT LISTERNER FROM NEXT BUTTON IF IT IS LAST PAGE ELSE SHOW DATA
                var isNextBtnExsit = j2HTML.IsElementExsit({
                    Element: '#liNextPages'
                });
                if (totalPages !== showPages) {

                    privateMethods.showNextPages(tableID, rowsPerPage, startPage, showPages, paginationAppendTo);
                } else {

                    if (isNextBtnExsit) {
                        var _elemt = j2HTML.GetElement({
                            Element: '#liNextPages'
                        });
                        _elemt[0].classList.add('disabled');
                        _elemt[0].removeEventListener("click", privateMethods.nextPage, false);
                    }
                }
            }

            //endregion


        }, {
            key: 'paginationAppendTo',
            get: function get() {
                return this._paginationAppendTo;
            },
            set: function set(value) {
                this._paginationAppendTo = value;
            }
        }, {
            key: 'rowsPerPage',
            get: function get() {
                return this._rowsPerPage;
            },
            set: function set(value) {
                this._rowsPerPage = value;
            }
        }, {
            key: 'tableID',
            get: function get() {
                return this._tableID;
            },
            set: function set(value) {
                this._tableID = value;
            }
        }, {
            key: 'startPage',
            get: function get() {
                return this._startPage;
            },
            set: function set(value) {
                this._startPage = value;
            }
        }, {
            key: 'showPages',
            get: function get() {
                return this._showPages;
            },
            set: function set(value) {
                this._showPages = value;
            }
        }]);

        return paging;
    }();

    //#region ************** PRIVATE VARIABLE AND METHOD *******

    var totalRows = void 0;
    var totalPages = void 0;

    var startPageNumber = false;
    var endPageNumber = false;

    var _getPaging = void 0;
    var tblID = void 0;
    var perPageRows = void 0;
    var pageAppendTo = void 0;
    //let strPage;
    var pagesShow = void 0;

    //PRIVATE FUNCTIONS
    var privateMethods = {

        // CREATE PAGINATION
        createPagination: function createPagination(startPage, totalPages, showPages, startPageNumber, endPageNumber) {

            if (startPageNumber === false && endPageNumber === false) {
                startPageNumber = 0;
                endPageNumber = totalPages;
            } else {
                if (endPageNumber > totalPages) {
                    endPageNumber = totalPages;
                }
            }
            var pagination = '';
            if (startPage === false || startPage === 1) {
                pagination += '<span id="paginationTotalPages">Showing 1 of ' + totalPages + ' pages</span><br>';
            } else {
                pagination += '<span id="paginationTotalPages">Showing ' + startPage + ' of ' + totalPages + ' pages</span><br>';
            }
            pagination += '<ul class="pagination pagination-sm">';
            if (showPages !== false && startPage === 1 && totalPages !== showPages) {
                pagination += '<li id="liPreviousPages" class="page-item disabled"><a class="page-link" href="#"><<</a></li>';
            }
            if (showPages !== false && startPage !== 1 && totalPages !== showPages) {
                pagination += '<li id="liPreviousPages" class="page-item"><a class="page-link" href="#"><<</a></li>';
            }

            if (totalPages > 1 && showPages === false) {

                for (i = startPageNumber; i < endPageNumber; i++) {

                    if (i + 1 === parseInt(startPage)) {
                        pagination += '<li class="page-item active"><a href="#" class="page-link pageNumber">' + (i + 1) + '</a></li>';
                    } else {
                        pagination += '<li class="page-item"><a href="#" class="page-link pageNumber">' + (i + 1) + '</a></li>';
                    }
                }
            }
            if (totalPages > 1 && showPages !== false) {

                for (i = startPageNumber; i < endPageNumber; i++) {

                    if (i + 1 === parseInt(startPage)) {
                        pagination += '<li class="page-item active"><a href="#" class="page-link pageNumber">' + (i + 1) + '</a></li>';
                    } else {
                        pagination += '<li class="page-item"><a href="#" class="page-link pageNumber">' + (i + 1) + '</a></li>';
                    }
                }
            }

            if (showPages === false) {
                pagination += '</ul>';
            } else {

                if (totalPages !== showPages) {
                    if (totalPages === endPageNumber) {
                        pagination += '<li id="liNextPages" class="page-item disabled"><a href="#" class="page-link">>></a></li></ul>';
                    } else {
                        pagination += '<li id="liNextPages" class="page-item"><a href="#" class="page-link">>></a></li></ul>';
                    }
                }
            }
            return pagination;
        },

        //SHOW PAGE DATA ACCORDING TO START PAGE
        showPaginationData: function showPaginationData(tableID, startNumber, rowsPerPage) {

            j2HTML.HideElement({
                Element: tableID + ' tbody tr'
            });
            var tr = j2HTML.GetElement({
                Element: tableID + ' tbody tr'
            });

            var nBegin = (startNumber - 1) * rowsPerPage;

            var nEnd = void 0;

            if (tr.length >= parseInt(startNumber) * rowsPerPage) {
                nEnd = startNumber * rowsPerPage;
            } else {
                nEnd = tr.length;
            }

            for (var i = nBegin; i < nEnd; i++) {
                j2HTML.ShowElement({
                    Element: '#' + tr[i].getAttribute('id')
                });
            }

            // //ENABLE OR DISABLE PREVIOUS BUTTON
            // if (nBegin !== 0) {
            //     let prevLink = j2HTML.GetElement({
            //         Element: '#liPreviousPages'
            //     });
            //     prevLink[0].classList.remove('disabled');
            // } else if (nBegin === 0) {
            //     let prevLink = j2HTML.GetElement({
            //         Element: '#liPreviousPages'
            //     });
            //     prevLink[0].classList.add('disabled');
            // }


            // // //ENABLE OR DISABLE LAST BUTTON
            // if (tr.length >= parseInt(startNumber) * rowsPerPage) {
            //     let nextLink = j2HTML.GetElement({
            //         Element: '#liNextPages'
            //     });
            //     nextLink[0].classList.remove('disabled');
            // }
            // if (tr.length <= parseInt(startNumber) * rowsPerPage) {
            //     let nextLink = j2HTML.GetElement({
            //         Element: '#liNextPages'
            //     });
            //     nextLink[0].classList.add('disabled');
            // }
        },


        //SHOW PAGE DATA ACCORDING TO PAGE NUMBER
        showSelectedPage: function showSelectedPage(tableID, rowsPerPage, paginationAppendTo) {

            tblID = tableID;
            perPageRows = rowsPerPage;
            pageAppendTo = paginationAppendTo;

            var elemts = j2HTML.GetElement({
                Element: '.pageNumber'
            });
            [].slice.call(elemts).map(function (elem) {

                //REMOVE PREVIOUSLY ADDED EVENT LISTENER
                elem.removeEventListener("click", privateMethods.selectedPage, false);

                //ADD EVENT LISTENER
                elem.addEventListener("click", privateMethods.selectedPage, false);
            });
        },


        //SHOW SET OF PREVIOUS PAGES
        showPreviousPages: function showPreviousPages(tableID, rowsPerPage, startPage, showPages, paginationAppendTo) {

            tblID = tableID;
            perPageRows = rowsPerPage;
            pageAppendTo = paginationAppendTo;
            strPage = startPage;
            pagesShow = showPages;

            var elemt = j2HTML.GetElement({
                Element: '#liPreviousPages'
            });
            //REMOVE PREVIOUSLY ADDED EVENT LISTENER
            elemt[0].removeEventListener("click", privateMethods.previousPage, false);

            //ADD EVENT LISTENER
            elemt[0].addEventListener("click", privateMethods.previousPage, false);
        },


        //SHOW SET OF NEXT PAGES
        showNextPages: function showNextPages(tableID, rowsPerPage, startPage, showPages, paginationAppendTo) {

            tblID = tableID;
            perPageRows = rowsPerPage;
            strPage = startPage;
            pagesShow = showPages;
            pageAppendTo = paginationAppendTo;

            var elemt = j2HTML.GetElement({
                Element: '#liNextPages'
            });
            //REMOVE PREVIOUSLY ADDED EVENT LISTENER
            elemt[0].removeEventListener("click", privateMethods.nextPage, false);

            //ADD EVENT LISTENER
            elemt[0].addEventListener("click", privateMethods.nextPage, false);
        },
        selectedPage: function selectedPage() {

            var endPage = void 0;
            //REMOVE CLASS
            var elem = j2HTML.GetElement({
                Element: '.pageNumber'
            });
            [].slice.call(elem).map(function (elemt) {

                elemt.parentNode.classList.remove('active');
            });

            //ADD ACTIVE CLASS
            this.parentNode.classList.add('active');
            startPageNumber = this.innerHTML;

            privateMethods.showPaginationData(tblID, startPageNumber, perPageRows);
            j2HTML.EmptyElement({
                Element: '#paginationTotalPages'
            });
            j2HTML.AppendHTMLString({
                Element: '#paginationTotalPages',
                HTMLString: 'Showing ' + startPageNumber + ' of ' + totalPages + ' pages'
            });
        },
        previousPage: function previousPage() {
            var _this = this;

            var setStartPage = 1;
            var setEndPage = void 0;
            var Id = void 0;

            var pStart = void 0;
            var pEnd = void 0;

            if (pageAppendTo === false) {
                Id = '#pages';
            } else {
                Id = pageAppendTo;
            }
            if (j2HTML.GetElement({
                Element: Id + ' ul li'
            }).length - 2 === pagesShow) {
                setEndPage = parseInt(pagesShow);
            } else {

                setEndPage = j2HTML.GetElement({
                    Element: Id + ' ul li'
                }).length - 2;
            }

            pStart = parseInt(j2HTML.GetElement({
                Element: Id + ' ul li'
            })[setStartPage].innerText);
            pEnd = parseInt(j2HTML.GetElement({
                Element: Id + ' ul li'
            })[setEndPage].innerText);

            if (pEnd == totalPages) {
                pEnd = pEnd + (pagesShow - pEnd % pagesShow);
            }

            startPageNumber = pStart - parseInt(pagesShow) - 1;
            endPageNumber = pEnd - parseInt(pagesShow);

            if (startPageNumber < 0) {
                startPageNumber = 0;
            }

            startPage = startPageNumber + 1;

            var p = new paging();
            p.tableID = tblID;
            p.rowsPerPage = perPageRows;
            p.startPage = startPage;
            p.showPages = pagesShow;
            p.paginationAppendTo = pageAppendTo;
            p.getPaging();

            //SHOW DATA
            if (startPageNumber === 0) {
                this.classList.add('disabled');
            }

            //REMOVE CLASS
            var elem = j2HTML.GetElement({
                Element: '.pageNumber'
            });
            [].slice.call(elem).map(function (elemt, index) {

                elemt.parentNode.classList.remove('active');
                if (index === 0 && _this.getAttribute('id') === 'liPreviousPages') {
                    elemt.parentNode.classList.add('active');
                } else {
                    _this.parentNode.classList.add('active');
                }

                //elemt.parentNode.classList.remove('active');
            });
            //this.parentNode.classList.add('active');
        },
        nextPage: function nextPage() {
            var _this2 = this;

            var setStartPage = 1;
            var setEndPage = parseInt(pagesShow);

            var Id;

            if (pageAppendTo === false) {
                Id = '#pages';
            } else {
                Id = pageAppendTo;
            }

            pStart = parseInt(j2HTML.GetElement({
                Element: Id + ' ul li'
            })[setStartPage].innerText);
            pEnd = parseInt(j2HTML.GetElement({
                Element: Id + ' ul li'
            })[setEndPage].innerText);

            startPageNumber = pStart + parseInt(pagesShow) - 1;
            endPageNumber = pEnd + parseInt(pagesShow);

            if (endPageNumber > totalPages) {
                endPageNumber = totalPages;
            }

            startPage = startPageNumber + 1;

            var p = new paging();
            p.tableID = tblID;
            p.rowsPerPage = perPageRows;
            p.startPage = startPage;
            p.showPages = pagesShow;
            p.paginationAppendTo = pageAppendTo;
            p.getPaging();

            if (endPageNumber === totalPages) {

                this.classList.add('disabled');
            }

            //REMOVE CLASS
            var elem = j2HTML.GetElement({
                Element: '.pageNumber'
            });
            [].slice.call(elem).map(function (elemt, index) {

                elemt.parentNode.classList.remove('active');

                if (index === 0 && _this2.getAttribute('id') === 'liNextPages') {
                    elemt.parentNode.classList.add('active');
                } else {
                    _this2.parentNode.classList.add('active');
                }
            });

            //this.parentNode.classList.add('active');
        }
    };

    //endregion
    return paging;
}();
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var placeholder = function () {

    //CREAT CLASS
    var placeholder = function () {
        function placeholder() {
            _classCallCheck(this, placeholder);

            this._elementIDs = false;
        }

        //#region ************** PROPERTIES ******************

        // PROPERTY -- DATA


        _createClass(placeholder, [{
            key: 'getPlaceholder',


            //endregion

            //#region ************** PUBLIC FUNCTIONS ************
            value: function getPlaceholder() {

                var error = '';

                if (this._elementIDs === false) {
                    error += 'Element id(s) required';
                }

                if (error === '') {

                    if (_typeof(this._elementIDs) !== 'object' && this._elementIDs.charAt(0) === '.') {
                        var elementIds = [].slice.call(j2HTML.GetElement({
                            Element: this._elementIDs
                        }));
                        privateMethods.animation(elementIds);
                    } else {
                        privateMethods.animation(this._elementIDs);
                    }
                } else {
                    alert(error);
                    throw new Error(error);
                }
            }
            //endregion


        }, {
            key: 'elementIDs',
            get: function get() {
                return this._elementIDs;
            },
            set: function set(value) {
                this._elementIDs = value;
            }
        }]);

        return placeholder;
    }();

    //#region ************** PRIVATE VARIABLE AND METHOD *******


    //PRIVATE FUNCTIONS


    var privateMethods = {
        animation: function animation(elementIDs) {

            var placeholder = '';
            var label = '';

            elementIDs.map(function (element) {

                var placeholderValue = void 0;
                var elemValue = '';

                if (typeof elementIDs[0] === 'string') {

                    var elem = [].slice.call(j2HTML.GetElement({
                        Element: element
                    }));

                    elem.map(function (elm) {

                        var id = 'lbl' + elm.getAttribute('id');
                        var forId = elm.getAttribute('id');
                        placeholderValue = elm.getAttribute('placeholder');
                        var lbl = '<label style="display:none" id="' + id + '" class="lblPlaceholder" for="' + forId + '">' + placeholderValue + '</label>';
                        elm.insertAdjacentHTML('beforebegin', lbl);

                        if (elm.getAttribute('value') !== undefined && elm.getAttribute('value') !== '' && elm.getAttribute('value') !== null) {
                            j2HTML.ShowElement({
                                Element: '#lbl' + elm.getAttribute('id')
                            });
                            j2HTML.AddClass({
                                Element: '#' + elm.getAttribute('id'),
                                ClassName: 'active'
                            });
                            j2HTML.AddClass({
                                Element: '#' + elm.getAttribute('id'),
                                ClassName: 'focusIn'
                            });
                        }
                        elm.addEventListener('focus', function () {

                            j2HTML.ShowElement({
                                Element: '#lbl' + elm.getAttribute('id')
                            });
                            elem[0].setAttribute('placeholder', '');

                            j2HTML.AddClass({
                                Element: '#' + elm.getAttribute('id'),
                                ClassName: 'active'
                            });
                            j2HTML.AddClass({
                                Element: '#' + elm.getAttribute('id'),
                                ClassName: 'focusIn'
                            });
                        });
                        elm.addEventListener('focusout', function () {

                            if (elm.value === '') {

                                j2HTML.RemoveClass({
                                    Element: '#' + elm.getAttribute('id'),
                                    ClassName: 'active'
                                });
                                j2HTML.HideElement({
                                    Element: '#lbl' + elm.getAttribute('id')
                                });
                                elem[0].setAttribute('placeholder', placeholderValue);
                            }
                            j2HTML.RemoveClass({
                                Element: '#' + elm.getAttribute('id'),
                                ClassName: 'active'
                            });
                        });
                    });
                } else {

                    var id = 'lbl' + element.getAttribute('id');
                    var forId = element.getAttribute('id');
                    placeholderValue = element.getAttribute('placeholder');
                    var lbl = '<label style="display:none" id="' + id + '" class="lblPlaceholder" for="' + forId + '">' + placeholderValue + '</label>';
                    element.insertAdjacentHTML('beforebegin', lbl);

                    if (element.getAttribute('value') !== undefined && element.getAttribute('value') !== '' && element.getAttribute('value') !== null) {
                        j2HTML.ShowElement({
                            Element: '#lbl' + element.getAttribute('id')
                        });
                        j2HTML.AddClass({
                            Element: '#' + element.getAttribute('id'),
                            ClassName: 'active'
                        });
                        j2HTML.AddClass({
                            Element: '#' + element.getAttribute('id'),
                            ClassName: 'focusIn'
                        });
                    }

                    element.addEventListener('focus', function () {

                        j2HTML.ShowElement({
                            Element: '#lbl' + element.getAttribute('id')
                        });
                        element.setAttribute('placeholder', '');

                        j2HTML.AddClass({
                            Element: '#' + element.getAttribute('id'),
                            ClassName: 'active'
                        });
                        j2HTML.AddClass({
                            Element: '#' + element.getAttribute('id'),
                            ClassName: 'focusIn'
                        });
                    });
                    element.addEventListener('focusout', function () {

                        if (element.value === '') {

                            j2HTML.RemoveClass({
                                Element: '#' + element.getAttribute('id'),
                                ClassName: 'active'
                            });
                            j2HTML.HideElement({
                                Element: '#lbl' + element.getAttribute('id')
                            });
                            element.setAttribute('placeholder', placeholderValue);
                        }
                        j2HTML.RemoveClass({
                            Element: '#' + element.getAttribute('id'),
                            ClassName: 'active'
                        });
                    });
                }
            });

            // $(elementIDs).each((i, v) => {


            //     var id = 'lbl' + $(v).attr('id');
            //     var forId = $(v).attr('id');
            //     var placeholderValue = $(v).attr('placeholder')
            //     var lbl = `<label style="display:none" id="${id}" class="lblPlaceholder" for="${forId}">${placeholderValue}</label>`;

            //     $(v).before(lbl);

            //     label = $('#' + 'lbl' + $(v).attr('id'));

            //     $(v).on('focus', function() {

            //         var id = 'lbl' + $(v).attr('id');

            //         $('#' + id).show();
            //         $(v).attr('placeholder', '');
            //         $('#' + id).addClass('active, focusIn');


            //     });

            //     $(v).on('focusout', function() {

            //         if ($(v).val() === '') {
            //             var id = 'lbl' + $(v).attr('id');
            //             $('#' + id).removeClass('active');
            //             $('#' + id).val('');
            //             $('#' + id).hide();
            //             $(v).attr('placeholder', placeholderValue);
            //         }
            //         $('#' + id).removeClass('active');


            //     });
            // });
        }
    };

    //endregion


    return placeholder;
}();

// function placeholder() {

//     var elementIDs = false;
//     var error = '';
//     var args = arguments[0][0];

//     if (args.ElementIDs === undefined) {
//         error += 'Element Id is required';
//     } else {

//         elementIDs = args.ElementIDs;

//     }
//     if (error === '') {
//         animation(elementIDs);
//     }
// }

// function makeid() {
//     var text = "";
//     var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
//     for (var i = 0; i < 5; i++)
//         text += possible.charAt(Math.floor(Math.random() * possible.length));
//     return text;
// }


// function animation(elementIDs) {

//     var placeholder = '';
//     var label = '';


//     $(elementIDs).each((i, v) => {


//         var id = 'lbl' + $(v).attr('id');
//         var forId = $(v).attr('id');
//         var placeholderValue = $(v).attr('placeholder')
//         var lbl = `<label style="display:none" id="${id}" class="lblPlaceholder" for="${forId}">${placeholderValue}</label>`;

//         $(v).before(lbl);

//         label = $('#' + 'lbl' + $(v).attr('id'));

//         $(v).on('focus', function() {

//             var id = 'lbl' + $(v).attr('id');

//             $('#' + id).show();
//             $(v).attr('placeholder', '');
//             $('#' + id).addClass('active, focusIn');


//         });

//         $(v).on('focusout', function() {

//             if ($(v).val() === '') {
//                 var id = 'lbl' + $(v).attr('id');
//                 $('#' + id).removeClass('active');
//                 $('#' + id).val('');
//                 $('#' + id).hide();
//                 $(v).attr('placeholder', placeholderValue);
//             }
//             $('#' + id).removeClass('active');


//         });
//     });
// }

function checkBoxExample() {


    j2HTML.ShowLoadingModal({

        Text: 'Getting Data from API'

    });

    var domain = 'https://api.wahidtechnology.com/Northwind/api';
    var URL = domain + "/Customer";

    j2HTML.Get({
            url: URL
        },
        function (error, data) {

            j2HTML.Checkbox({
                Data: data[0],
                AppendTo: '#divCheckbox',
                Text: 'Company',
                Value: 'ID',
                Direction: 'Vertical'
            });

            //ADDED LEFT SPACE TO EACH CHECK BOX
            var chkBox = j2HTML.GetElement({
                Element: 'input[name="chkdivCheckbox"]'
            });

            [].slice.call(chkBox).map(function (chk) {

                chk.style.marginLeft = "5px";

            });

            j2HTML.HideLoadingModal();

        });







}


function carsouelExample() {

    j2HTML.ShowLoadingModal({

        Text: 'Getting Data from API for carsouel' 

    });

    var domain = 'https://api.wahidtechnology.com/Northwind/api';
    var URL = domain + "/Employee";

    j2HTML.Get({
            url: URL
        },
        function (error, data) {

            j2HTML.Carousel({
                Data: data[0],
                AppendTo: '#divCarousel',
                Image: {
                    Column: 'PhotoPath',
                    Width: '450',
                    Height: '450'
                },
                Caption: 'Full Name'
            });
            j2HTML.HideLoadingModal();
        });


}